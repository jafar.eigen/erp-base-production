export interface IResponses {
    statusCode: number;
    message: any;
    error: any;
}
