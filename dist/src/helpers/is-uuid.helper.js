"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isUUID = void 0;
const isUUID = (value) => {
    return value.match('^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$');
};
exports.isUUID = isUUID;
//# sourceMappingURL=is-uuid.helper.js.map