export declare class GetRollbackStatusHelper<Entity> {
    protected data: Entity;
    constructor(data: Entity);
    get(): Promise<Entity>;
    private checkAndGetRollbackStatus;
}
