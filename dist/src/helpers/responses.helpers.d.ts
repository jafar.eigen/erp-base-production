import { IResponses } from './responses-interface';
export declare class Responses {
    json(status: number, data?: any, errors?: any): IResponses;
}
