"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CopyDocumentFile = void 0;
const fs = require("fs");
const common_1 = require("@nestjs/common");
const moment = require("moment");
class CopyDocumentFile {
    constructor(id, dates) {
        this.id = id;
        this.dates = dates;
    }
    async run() {
        try {
            return await this.copyFiles(this.dates);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async copyFiles(files) {
        return await Promise.all(files.map(async (date) => {
            const srcDir = this.getSrcDir(date.document_path);
            const destDir = this.getDestDir();
            const fileName = this.getNameFile(date.document_path);
            const copyFile = `./uploads/${srcDir}/${fileName}-copy`;
            await fs.mkdirSync(`./uploads/${destDir}`, { recursive: true });
            await fs.copyFileSync(`./uploads/${srcDir}/${fileName}`, copyFile);
            await fs.renameSync(copyFile, `./uploads/${destDir}/${fileName}`);
            date.document_path = `${destDir}/${fileName}`;
            return date;
        }));
    }
    getSrcDir(file) {
        const files = file.split('/');
        const srcDir = files.splice(0, 4);
        return srcDir.join('/');
    }
    getDestDir() {
        const year = moment().format('YYYY');
        const month = moment().format('M');
        return `documents/legality/${year}/${month}/${this.id}-legal`;
    }
    getNameFile(file) {
        const name = file.split('/');
        return name[name.length - 1];
    }
}
exports.CopyDocumentFile = CopyDocumentFile;
//# sourceMappingURL=CopyDocumentFile.js.map