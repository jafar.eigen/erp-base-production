"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFilePath = void 0;
const getFilePath = (file) => {
    if (!file)
        return null;
    const path = file.path.split('/');
    path.shift();
    return path.join('/');
};
exports.getFilePath = getFilePath;
//# sourceMappingURL=get-file-path.helper.js.map