"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
class HandlerExceptionHelper {
    execute(message, status_code) {
        console.log('Helper exeption handler.');
        throw new common_1.BadRequestException(message);
    }
}
exports.default = HandlerExceptionHelper;
//# sourceMappingURL=handler-exception.helper.js.map