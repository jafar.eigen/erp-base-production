export declare class JSONParse {
    encrypt(payload: any): string;
    decrypt(payload: string): any;
}
