"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRollbackStatusHelper = void 0;
const base_entity_1 = require("../base/domain/entities/base.entity");
class GetRollbackStatusHelper {
    constructor(data) {
        this.data = data;
    }
    async get() {
        await this.checkAndGetRollbackStatus();
        return this.data;
    }
    async checkAndGetRollbackStatus() {
        switch (this.data['status']) {
            case base_entity_1.STATUS.OPEN:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.DRAFT
                });
                return;
            case base_entity_1.STATUS.WAITING:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.OPEN
                });
                return;
            case base_entity_1.STATUS.DONE:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.WAITING
                });
                return;
        }
    }
}
exports.GetRollbackStatusHelper = GetRollbackStatusHelper;
//# sourceMappingURL=get-rollback-status.helper.js.map