import { LegalityDatesEntity } from '../modules/legality/command/domain/entities/legality-dates.entity';
export declare class CopyDocumentFile {
    protected id: string;
    protected dates: LegalityDatesEntity[];
    constructor(id: string, dates: LegalityDatesEntity[]);
    run(): Promise<LegalityDatesEntity[]>;
    copyFiles(files: LegalityDatesEntity[]): Promise<LegalityDatesEntity[]>;
    protected getSrcDir(file: string): string;
    protected getDestDir(): string;
    protected getNameFile(file: string): string;
}
