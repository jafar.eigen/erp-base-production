"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetProcessStatusHelper = void 0;
const base_entity_1 = require("../base/domain/entities/base.entity");
class GetProcessStatusHelper {
    constructor(data) {
        this.data = data;
    }
    async get() {
        await this.checkAndGetProcessStatus();
        return this.data;
    }
    async checkAndGetProcessStatus() {
        switch (this.data['status']) {
            case base_entity_1.STATUS.DRAFT:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.OPEN
                });
                return;
            case base_entity_1.STATUS.OPEN:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.WAITING
                });
                return;
            case base_entity_1.STATUS.WAITING:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.DONE
                });
                return;
        }
    }
}
exports.GetProcessStatusHelper = GetProcessStatusHelper;
//# sourceMappingURL=get-process-status.helper.js.map