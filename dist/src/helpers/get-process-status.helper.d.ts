export declare class GetProcessStatusHelper<Entity> {
    protected data: Entity;
    constructor(data: Entity);
    get(): Promise<Entity>;
    private checkAndGetProcessStatus;
}
