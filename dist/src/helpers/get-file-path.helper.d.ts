/// <reference types="express-serve-static-core" />
/// <reference types="multer" />
export declare const getFilePath: (file: Express.Multer.File) => string;
