import { Approval, ApprovalPerCompany } from '../base/domain/entities/approval.entity';
import { IUserPayload } from '../base/domain/entities/user/user-payload-interface.ts';
import { KafkaPayload } from '../base/domain/entities/kafka/payload.interface';
export declare enum ResponseType {
    APPROVED = "approved",
    DECLINED = "declined"
}
export declare class ResponseApproval<TransactionData, DataService, ProducerService> {
    protected step: number;
    protected user: IUserPayload;
    protected isApproved: boolean;
    protected data: TransactionData;
    protected kafkaPayload: KafkaPayload<IUserPayload, TransactionData, TransactionData>;
    protected dataService: DataService;
    protected producerService: ProducerService;
    protected responseType: ResponseType;
    protected approval: ApprovalPerCompany;
    protected approvalStep: Approval;
    protected produceDataUpdatedFunction: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected produceDataDeletedFunction: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected produceDataActivatedFunction: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected produceDataActivatedCustomFunction: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected updateUnfinishedApprovalDataFunction: (data: TransactionData) => Promise<TransactionData>;
    protected deleteDataFunction: (data: TransactionData) => Promise<TransactionData>;
    protected updateDataFunction: (data: TransactionData) => Promise<TransactionData>;
    protected statusesData: string;
    protected isDraftEdit: boolean;
    protected isNewAddress: boolean;
    setResponseType(responseType: string): this;
    setUser(user: IUserPayload): this;
    setDataService(dataService: DataService): this;
    setKafkaService(producerService: ProducerService): this;
    setData(data: TransactionData): this;
    setStepNumber(step: number): this;
    setUnfinishedDataApprovalFunction(callback: (data: TransactionData) => Promise<TransactionData>): this;
    setUpdateDataFunction(callback: (data: TransactionData) => Promise<TransactionData>): this;
    setDeleteDataFunction(callback: (data: TransactionData) => Promise<TransactionData>): this;
    setDataDeletedProducer(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    setDataUpdatedProducer(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    setDataActivatedProducer(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    setCustomActivatedDataProducer(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    run(): Promise<TransactionData>;
    protected setKafkaPayload(): void;
    protected isSubmissionOnValidStatus(): void;
    protected getApproval(): void;
    protected getApprovalOnSpecifiedStep(): void;
    protected setUserApproval(): void;
    protected isAllMemberApproved(): void;
    protected isAllStageDone(): void;
    protected applyChange(): Promise<void>;
    protected deleteData(): Promise<TransactionData>;
    protected updateUnfinishedApproval(): Promise<TransactionData>;
    protected updateData(): Promise<TransactionData>;
    protected produceDataActivated(): Promise<void>;
    protected produceDataActivatedCustom(): Promise<void>;
    protected produceDataDeleted(): Promise<void>;
    protected produceDataUpdated(): Promise<void>;
    protected mapData(): void;
}
