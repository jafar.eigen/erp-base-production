"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseApproval = exports.ResponseType = void 0;
const _ = require("lodash");
const approval_entity_1 = require("../base/domain/entities/approval.entity");
const base_entity_1 = require("../base/domain/entities/base.entity");
const check_submission_in_valid_status_1 = require("./approval/usecases/check-submission-in-valid-status");
const find_company_approval_helper_1 = require("./approval/find-company-approval.helper");
var ResponseType;
(function (ResponseType) {
    ResponseType["APPROVED"] = "approved";
    ResponseType["DECLINED"] = "declined";
})(ResponseType = exports.ResponseType || (exports.ResponseType = {}));
class ResponseApproval {
    setResponseType(responseType) {
        Object.assign(this, { responseType });
        return this;
    }
    setUser(user) {
        Object.assign(this, { user });
        return this;
    }
    setDataService(dataService) {
        Object.assign(this, { dataService });
        return this;
    }
    setKafkaService(producerService) {
        Object.assign(this, { producerService });
        return this;
    }
    setData(data) {
        var _a;
        Object.assign(this, { data });
        Object.assign(this, {
            isDraftEdit: !!data['requested_data']
        });
        Object.assign(this, {
            isNewAddress: (_a = data['requested_data']) === null || _a === void 0 ? void 0 : _a.new_address
        });
        return this;
    }
    setStepNumber(step) {
        Object.assign(this, { step });
        return this;
    }
    setUnfinishedDataApprovalFunction(callback) {
        Object.assign(this, { updateUnfinishedApprovalDataFunction: callback });
        return this;
    }
    setUpdateDataFunction(callback) {
        Object.assign(this, { updateDataFunction: callback });
        return this;
    }
    setDeleteDataFunction(callback) {
        Object.assign(this, { deleteDataFunction: callback });
        return this;
    }
    setDataDeletedProducer(callback) {
        Object.assign(this, { produceDataDeletedFunction: callback });
        return this;
    }
    setDataUpdatedProducer(callback) {
        Object.assign(this, { produceDataUpdatedFunction: callback });
        return this;
    }
    setDataActivatedProducer(callback) {
        Object.assign(this, { produceDataActivatedFunction: callback });
        return this;
    }
    setCustomActivatedDataProducer(callback) {
        Object.assign(this, { produceDataActivatedCustomFunction: callback });
        return this;
    }
    async run() {
        this.isSubmissionOnValidStatus();
        this.setKafkaPayload();
        this.getApproval();
        this.getApprovalOnSpecifiedStep();
        this.setUserApproval();
        this.isAllMemberApproved();
        this.isAllStageDone();
        this.mapData();
        await this.applyChange();
        return this.data;
    }
    setKafkaPayload() {
        Object.assign(this, {
            kafkaPayload: {
                user: this.user,
                data: null,
                old: _.cloneDeep(this.data)
            }
        });
    }
    isSubmissionOnValidStatus() {
        new check_submission_in_valid_status_1.CheckSubmissionOnValidStatus(this.data, this.user.user_category.uuid, this.step);
    }
    getApproval() {
        const approval = this.data['approval'];
        Object.assign(this, { approval });
    }
    getApprovalOnSpecifiedStep() {
        const approvalStep = (0, find_company_approval_helper_1.getApprovalOnStep)(this.approval, this.step);
        Object.assign(this, { approvalStep });
    }
    setUserApproval() {
        const updatedApprover = this.approvalStep.approver.map((approver) => {
            if (approver.id === this.user.user_category.uuid) {
                return Object.assign(Object.assign({}, approver), { status: approver.status == null ? this.responseType : null });
            }
            return Object.assign({}, approver);
        });
        Object.assign(this.approvalStep, {
            approver: updatedApprover
        });
    }
    isAllMemberApproved() {
        let isDone = false;
        switch (this.approvalStep.type) {
            case approval_entity_1.ApprovalType.STEP:
                const highestLvl = this.approvalStep.approver.find((approver) => {
                    return (approver.level === approval_entity_1.ApproverHighestLevel &&
                        approver.status === approval_entity_1.ApproverStatus.APPROVED);
                });
                if (highestLvl)
                    isDone = true;
                break;
            case approval_entity_1.ApprovalType.HIGHEST:
                const highestUser = this.approvalStep.approver.find((approver) => {
                    return approver.status === approval_entity_1.ApproverStatus.APPROVED;
                });
                if (highestUser)
                    isDone = true;
                break;
            default:
                isDone = this.approvalStep.approver.every((approver) => {
                    if (approver.status != approval_entity_1.ApproverStatus.APPROVED)
                        return false;
                    return true;
                });
        }
        Object.assign(this.approvalStep.process, { is_done: isDone });
    }
    isAllStageDone() {
        let isApproved = false;
        const approvalStep = _.partition(this.approval.approval, {
            step: approval_entity_1.HighestStage
        });
        approvalStep[0].forEach((highestStep) => {
            if (highestStep.process.is_done === true)
                isApproved = true;
        });
        if (!isApproved) {
            isApproved = approvalStep[1].every((approvalStep) => {
                return approvalStep.process.is_done === true;
            });
        }
        Object.assign(this, { isApproved });
    }
    async applyChange() {
        let transactionData = null;
        switch (this.data['has_requested_process']) {
            case false:
                switch (this.data['request_info']) {
                    case approval_entity_1.RequestInfo.DELETE_DATA:
                        if (this.responseType === ResponseType.APPROVED) {
                            transactionData = await this.deleteData();
                            this.produceDataDeleted();
                            break;
                        }
                    default:
                        transactionData = await this.updateData();
                        switch (this.data['request_info']) {
                            case approval_entity_1.RequestInfo.CREATE_DATA:
                                if (this.isDraftEdit) {
                                    this.produceDataUpdated();
                                }
                                else {
                                    this.produceDataActivated();
                                }
                                break;
                            case approval_entity_1.RequestInfo.EDIT_ACTIVE:
                                if (this.statusesData === base_entity_1.STATUS.DRAFT) {
                                    this.produceDataActivatedCustom();
                                }
                                else {
                                    this.produceDataUpdated();
                                }
                                break;
                            default:
                                this.produceDataUpdated();
                                break;
                        }
                        break;
                }
                break;
            default:
                transactionData = await this.updateUnfinishedApproval();
                break;
        }
        Object.assign(this, transactionData);
    }
    async deleteData() {
        return this.deleteDataFunction(this.data);
    }
    async updateUnfinishedApproval() {
        return await this.updateUnfinishedApprovalDataFunction(this.data);
    }
    async updateData() {
        return await this.updateDataFunction(this.data);
    }
    async produceDataActivated() {
        return await this.produceDataActivatedFunction(this.kafkaPayload);
    }
    async produceDataActivatedCustom() {
        return await this.produceDataActivatedCustomFunction(this.kafkaPayload);
    }
    async produceDataDeleted() {
        return await this.produceDataDeletedFunction(this.kafkaPayload);
    }
    async produceDataUpdated() {
        return await this.produceDataUpdatedFunction(this.kafkaPayload);
    }
    mapData() {
        var _a;
        this.statusesData = this.data['status'];
        switch (this.isApproved) {
            case true:
                const requestedData = this.data['requested_data'];
                switch (this.data['request_info']) {
                    case approval_entity_1.RequestInfo.CREATE_DATA:
                        Object.assign(this.data, Object.assign(Object.assign({}, requestedData), { status: base_entity_1.STATUS.ACTIVE, requested_data: null, approval: this.approval, has_requested_process: false }));
                        Object.assign(this.kafkaPayload, { data: this.data });
                        break;
                    case approval_entity_1.RequestInfo.EDIT_DATA:
                        const isDraft = this.data['status'] === base_entity_1.STATUS.DRAFT;
                        const createApproval = (_a = this.data['requested_data']) === null || _a === void 0 ? void 0 : _a.approval;
                        Object.assign(this.data, Object.assign(Object.assign({}, requestedData), { requested_data: null, approval: isDraft ? createApproval : this.approval, has_requested_process: false }));
                        Object.assign(this.kafkaPayload, { data: this.data });
                        break;
                    case approval_entity_1.RequestInfo.EDIT_ACTIVE:
                        Object.assign(this.data, {
                            status: base_entity_1.STATUS.ACTIVE,
                            approval: this.approval,
                            request_info: approval_entity_1.RequestInfo.EDIT_ACTIVE,
                            has_requested_process: false
                        });
                        Object.assign(this.kafkaPayload, { data: this.data });
                        break;
                    case approval_entity_1.RequestInfo.EDIT_INACTIVE:
                        Object.assign(this.data, {
                            status: base_entity_1.STATUS.INACTIVE,
                            approval: this.approval,
                            request_info: approval_entity_1.RequestInfo.EDIT_INACTIVE,
                            has_requested_process: false
                        });
                        Object.assign(this.kafkaPayload, { data: this.data });
                        break;
                    case approval_entity_1.RequestInfo.DELETE_DATA:
                        Object.assign(this.data, {
                            status: base_entity_1.STATUS.DELETED,
                            approval: this.approval,
                            request_info: approval_entity_1.RequestInfo.DELETE_DATA,
                            has_requested_process: false
                        });
                        Object.assign(this.kafkaPayload, { data: null });
                        break;
                }
                break;
            case false:
                switch (this.responseType) {
                    case ResponseType.DECLINED:
                        this.approval.approval = this.approval.approval.map((approval) => {
                            approval.approver.map((approver) => {
                                if (approver.id === this.user.user_category.uuid) {
                                    approval.process.has_decline = !approval.process.has_decline;
                                }
                                return approver;
                            });
                            return approval;
                        });
                        Object.assign(this.data, {
                            approval: this.approval,
                            has_requested_process: this.data['has_requested_process'] === false
                        });
                        break;
                    case ResponseType.APPROVED:
                        Object.assign(this.data, {
                            approval: this.approval,
                            has_requested_process: true
                        });
                        break;
                }
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
        }
    }
}
exports.ResponseApproval = ResponseApproval;
//# sourceMappingURL=response-approval.helper.js.map