/// <reference types="multer" />
import { ApprovalPerCompany } from '../../base/domain/entities/approval.entity';
import { IUserPayload } from '../../base/domain/entities/user/user-payload-interface.ts';
export interface UpdateLocation {
    moduleName: string;
    actionsName: string;
    subModuleName?: string;
    activityName?: string;
}
export declare class GenerateApproval<TransactionData> {
    protected user: IUserPayload;
    protected data: TransactionData;
    protected editedData: Partial<TransactionData>;
    protected approvals: ApprovalPerCompany[];
    protected file: Express.Multer.File;
    protected newAddress: string;
    setData(data: TransactionData): this;
    setNewData(editedData: Partial<TransactionData>): this;
    setUser(user: IUserPayload): this;
    setApproval(approvals: ApprovalPerCompany[]): this;
    setFile(file: Express.Multer.File): this;
    setNewAddress(newAddress: string): this;
    run(): Promise<TransactionData>;
    protected isAvailableForUpdate(): void;
    protected mergeData(): void;
}
