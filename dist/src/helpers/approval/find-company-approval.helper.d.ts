import { Approval, ApprovalPerCompany } from '../../base/domain/entities/approval.entity';
import { IUserPayload } from '../../base/domain/entities/user/user-payload-interface.ts';
export declare const findApproval: <Entity>(approvals: ApprovalPerCompany[], data: Entity, user: IUserPayload) => ApprovalPerCompany;
export declare const getApprovalOnStep: (approval: ApprovalPerCompany, step: number) => Approval;
export declare const getInProgressLowerStep: (approval: ApprovalPerCompany, currentStep: number) => Approval;
