import { KafkaPayload } from '../../base/domain/entities/kafka/payload.interface';
import { IUserPayload } from '../../base/domain/entities/user/user-payload-interface.ts';
export declare enum RequestType {
    UPDATE = "update",
    CREATE = "create",
    ACTIVE_WITH_CHILD = "active_with_child",
    ACTIVE = "active",
    INACTIVE = "inactive",
    INACTIVE_WITH_CHILD = "inactive_with_child",
    DELETE = "delete",
    DELETE_WITH_CHILD = "delete_with_child",
    REQUEST = "request",
    PROCESS = "process",
    ROLLBACK = "rollback",
    CANCEL = "cancel"
}
export declare class SubmitRequest<TransactionData, DataService, ProducerService> {
    protected data: TransactionData;
    protected submitRequestFunction: (data: TransactionData) => Promise<TransactionData>;
    protected passApprovalFunction: (data: TransactionData) => Promise<TransactionData>;
    protected produceSubmitRequestTopic: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected producePassApprovalTopic: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>;
    protected requestType: RequestType;
    protected needApprovals: boolean;
    protected dataService: DataService;
    protected producerService: ProducerService;
    protected kafkaPayload: KafkaPayload<IUserPayload, TransactionData, TransactionData>;
    protected user: IUserPayload;
    protected isDraftData: boolean;
    protected isDraftEdit: boolean;
    protected isNewAddress: boolean;
    protected approvallStatus: string;
    setRequestType(requestType: string): this;
    setUser(user: IUserPayload): this;
    setDataService(dataService: DataService): this;
    setKafkaService(producerService: ProducerService): this;
    setApprovallStatus(approvallStatus: string): this;
    setData(data: TransactionData): this;
    setSubmitRequestFunction(callback: (data: TransactionData) => Promise<TransactionData>): this;
    setProducerTopicFunction(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    setPassApprovalFunction<T>(callback: (data: TransactionData) => Promise<TransactionData | T>): this;
    setProduceSubmitRequestTopic(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    setProducePassApprovalTopic(callback: (payload: KafkaPayload<IUserPayload, TransactionData, TransactionData>) => Promise<void>): this;
    protected setKafkaPayload(): void;
    run(): Promise<TransactionData>;
    protected isAvailableForUpdate(): void;
    protected isApprovalNeeded(): void;
    protected setWithoutApprovalData(): void;
    protected setDataWithApproval(): void;
    protected processWithoutApproval(): Promise<TransactionData>;
    protected processDataWithApproval(): Promise<TransactionData>;
    protected submissionTopic(): Promise<void>;
    protected passApprovalTopic(): Promise<void>;
}
