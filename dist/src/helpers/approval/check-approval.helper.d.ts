import { ApprovalPerCompany } from '../../base/domain/entities/approval.entity';
export declare const isNeedApproval: (approvalPerCompany: ApprovalPerCompany) => boolean;
