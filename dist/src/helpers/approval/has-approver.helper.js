"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasApprover = void 0;
const hasApprover = (approval) => {
    let result = false;
    if (!approval)
        return false;
    approval.approval.forEach((approval) => {
        const foundHasApprover = approval.approver.find((approver) => {
            return approver.status !== null;
        });
        if (foundHasApprover) {
            result = true;
            return;
        }
    });
    return result;
};
exports.hasApprover = hasApprover;
//# sourceMappingURL=has-approver.helper.js.map