"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitRequest = exports.RequestType = void 0;
const _ = require("lodash");
const approval_entity_1 = require("../../base/domain/entities/approval.entity");
const base_entity_1 = require("../../base/domain/entities/base.entity");
const check_approval_helper_1 = require("../../helpers/approval/check-approval.helper");
const check_update_availability_1 = require("./usecases/check-update-availability");
var RequestType;
(function (RequestType) {
    RequestType["UPDATE"] = "update";
    RequestType["CREATE"] = "create";
    RequestType["ACTIVE_WITH_CHILD"] = "active_with_child";
    RequestType["ACTIVE"] = "active";
    RequestType["INACTIVE"] = "inactive";
    RequestType["INACTIVE_WITH_CHILD"] = "inactive_with_child";
    RequestType["DELETE"] = "delete";
    RequestType["DELETE_WITH_CHILD"] = "delete_with_child";
    RequestType["REQUEST"] = "request";
    RequestType["PROCESS"] = "process";
    RequestType["ROLLBACK"] = "rollback";
    RequestType["CANCEL"] = "cancel";
})(RequestType = exports.RequestType || (exports.RequestType = {}));
class SubmitRequest {
    setRequestType(requestType) {
        Object.assign(this, { requestType });
        return this;
    }
    setUser(user) {
        Object.assign(this, { user });
        return this;
    }
    setDataService(dataService) {
        Object.assign(this, { dataService });
        return this;
    }
    setKafkaService(producerService) {
        Object.assign(this, { producerService });
        return this;
    }
    setApprovallStatus(approvallStatus) {
        Object.assign(this, { approvallStatus });
        return this;
    }
    setData(data) {
        var _a;
        Object.assign(this, { data });
        Object.assign(this, {
            isDraftData: data['status'] === base_entity_1.STATUS.DRAFT
        });
        Object.assign(this, {
            isDraftEdit: !!data['requested_data']
        });
        Object.assign(this, {
            isNewAddress: (_a = data['requested_data']) === null || _a === void 0 ? void 0 : _a.new_address
        });
        return this;
    }
    setSubmitRequestFunction(callback) {
        Object.assign(this, { submitRequestFunction: callback });
        return this;
    }
    setProducerTopicFunction(callback) {
        Object.assign(this, { produceTopicFunction: callback });
        return this;
    }
    setPassApprovalFunction(callback) {
        Object.assign(this, { passApprovalFunction: callback });
        return this;
    }
    setProduceSubmitRequestTopic(callback) {
        Object.assign(this, { produceSubmitRequestTopic: callback });
        return this;
    }
    setProducePassApprovalTopic(callback) {
        Object.assign(this, { producePassApprovalTopic: callback });
        return this;
    }
    setKafkaPayload() {
        Object.assign(this, {
            kafkaPayload: {
                user: this.user,
                data: null,
                old: _.cloneDeep(this.data)
            }
        });
    }
    async run() {
        this.isAvailableForUpdate();
        this.setKafkaPayload();
        this.isApprovalNeeded();
        switch (this.needApprovals) {
            case false:
                this.setWithoutApprovalData();
                const result = await this.processWithoutApproval();
                switch (this.requestType) {
                    case RequestType.DELETE:
                        this.passApprovalTopic();
                        break;
                    default:
                        switch (this.isDraftData) {
                            case true:
                                this.passApprovalTopic();
                                break;
                            default:
                                this.submissionTopic();
                                break;
                        }
                }
                return result;
            case true:
                this.setDataWithApproval();
                this.submissionTopic();
                return await this.processDataWithApproval();
        }
    }
    isAvailableForUpdate() {
        new check_update_availability_1.CheckDataAvailableForUpdate(this.data);
    }
    isApprovalNeeded() {
        const needApprovals = (0, check_approval_helper_1.isNeedApproval)(this.data['approval']);
        Object.assign(this, { needApprovals: needApprovals });
    }
    setWithoutApprovalData() {
        let requestInfoTransaction = approval_entity_1.RequestInfo.PROCESS;
        switch (this.requestType) {
            case RequestType.CREATE:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.ACTIVE,
                    request_info: approval_entity_1.RequestInfo.CREATE_DATA,
                    requested_data: null,
                    has_requested_process: false
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.UPDATE:
                const requestedData = this.data['requested_data'];
                const isDraft = this.data['status'] === base_entity_1.STATUS.DRAFT;
                Object.assign(this.data, Object.assign(Object.assign({}, requestedData), { request_info: isDraft
                        ? approval_entity_1.RequestInfo.CREATE_DATA
                        : approval_entity_1.RequestInfo.EDIT_DATA, requested_data: null, has_requested_process: false }));
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.CANCEL:
                requestInfoTransaction = approval_entity_1.RequestInfo.CANCEL;
            case RequestType.ROLLBACK:
                requestInfoTransaction = approval_entity_1.RequestInfo.ROLLBACK;
            case RequestType.PROCESS:
                const requestedDataProcess = this.data['requested_data'];
                Object.assign(this.data, Object.assign(Object.assign({}, requestedDataProcess), { request_info: requestInfoTransaction, requested_data: null, has_requested_process: false }));
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.ACTIVE:
            case RequestType.ACTIVE_WITH_CHILD:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.ACTIVE,
                    request_info: approval_entity_1.RequestInfo.EDIT_ACTIVE,
                    has_requested_process: false
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.INACTIVE:
            case RequestType.INACTIVE_WITH_CHILD:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.INACTIVE,
                    request_info: approval_entity_1.RequestInfo.EDIT_INACTIVE,
                    has_requested_process: false
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.DELETE:
                Object.assign(this.data, {
                    status: base_entity_1.STATUS.DELETED,
                    request_info: approval_entity_1.RequestInfo.DELETE_DATA,
                    has_requested_process: false
                });
                Object.assign(this.kafkaPayload, { data: null });
                break;
        }
    }
    setDataWithApproval() {
        let requestInfoTransaction = approval_entity_1.RequestInfo.PROCESS;
        switch (this.requestType) {
            case RequestType.CREATE:
                Object.assign(this.data, {
                    has_requested_process: true,
                    request_info: approval_entity_1.RequestInfo.CREATE_DATA,
                    requested_data: null
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.UPDATE:
                const requestedData = this.data['requested_data'];
                Object.assign(this.data, {
                    has_requested_process: true,
                    request_info: approval_entity_1.RequestInfo.EDIT_DATA,
                    requested_data: Object.assign({}, requestedData)
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.CANCEL:
                requestInfoTransaction = approval_entity_1.RequestInfo.CANCEL;
            case RequestType.ROLLBACK:
                requestInfoTransaction = approval_entity_1.RequestInfo.ROLLBACK;
            case RequestType.PROCESS:
                const requestedDataProcess = this.data['requested_data'];
                Object.assign(this.data, {
                    has_requested_process: true,
                    request_info: requestInfoTransaction,
                    requested_data: Object.assign({}, requestedDataProcess)
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.ACTIVE:
                Object.assign(this.data, {
                    request_info: approval_entity_1.RequestInfo.EDIT_ACTIVE,
                    has_requested_process: true
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.ACTIVE_WITH_CHILD:
                Object.assign(this.data, {
                    request_info: approval_entity_1.RequestInfo.EDIT_ACTIVE_WITH_CHILD,
                    has_requested_process: true
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.INACTIVE:
                Object.assign(this.data, {
                    request_info: approval_entity_1.RequestInfo.EDIT_INACTIVE,
                    has_requested_process: true
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.INACTIVE_WITH_CHILD:
                Object.assign(this.data, {
                    request_info: approval_entity_1.RequestInfo.EDIT_INACTIVE_WITH_CHILD,
                    has_requested_process: true
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
            case RequestType.DELETE:
                Object.assign(this.data, {
                    request_info: approval_entity_1.RequestInfo.DELETE_DATA,
                    has_requested_process: true
                });
                Object.assign(this.kafkaPayload, { data: this.data });
                break;
        }
    }
    async processWithoutApproval() {
        return await this.passApprovalFunction(this.data);
    }
    async processDataWithApproval() {
        return await this.submitRequestFunction(this.data);
    }
    async submissionTopic() {
        return await this.produceSubmitRequestTopic(this.kafkaPayload);
    }
    async passApprovalTopic() {
        return await this.producePassApprovalTopic(this.kafkaPayload);
    }
}
exports.SubmitRequest = SubmitRequest;
//# sourceMappingURL=submit-request.helper.js.map