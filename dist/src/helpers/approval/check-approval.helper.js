"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNeedApproval = void 0;
const isNeedApproval = (approvalPerCompany) => {
    const approval = approvalPerCompany === null || approvalPerCompany === void 0 ? void 0 : approvalPerCompany.approval;
    return (approval === null || approval === void 0 ? void 0 : approval.length) > 0;
};
exports.isNeedApproval = isNeedApproval;
//# sourceMappingURL=check-approval.helper.js.map