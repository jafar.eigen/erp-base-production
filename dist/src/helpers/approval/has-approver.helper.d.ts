import { ApprovalPerCompany } from '../../base/domain/entities/approval.entity';
export declare const hasApprover: (approval: ApprovalPerCompany) => boolean;
