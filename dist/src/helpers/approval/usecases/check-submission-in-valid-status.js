"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckSubmissionOnValidStatus = void 0;
const common_1 = require("@nestjs/common");
const approval_entity_1 = require("../../../base/domain/entities/approval.entity");
const find_company_approval_helper_1 = require("../find-company-approval.helper");
class CheckSubmissionOnValidStatus {
    constructor(approval, userCategoryId, step) {
        this.approval = approval;
        this.userCategoryId = userCategoryId;
        this.step = step;
        this.setApprover();
        this.isApproverMember();
        this.previousStepShouldBeDone();
        this.hasRequestedProcess();
        this.isValidToRevokeRecentApproval();
    }
    setApprover() {
        const currentStep = (0, find_company_approval_helper_1.getApprovalOnStep)(this.approval['approval'], this.step);
        Object.assign(this, { currentStep });
    }
    isApproverMember() {
        const loginUserIsApproverMember = this.currentStep.approver.find((approver) => approver.id === this.userCategoryId);
        if (!loginUserIsApproverMember) {
            throw new common_1.HttpException('Login user is not approver', common_1.HttpStatus.BAD_REQUEST);
        }
    }
    previousStepShouldBeDone() {
        const previousStep = (0, find_company_approval_helper_1.getInProgressLowerStep)(this.approval['approval'], this.currentStep.step);
        if (this.currentStep.step === approval_entity_1.HighestStage)
            return;
        if (previousStep) {
            throw new common_1.HttpException('Complete lower step before submit response for this step', common_1.HttpStatus.BAD_REQUEST);
        }
    }
    hasRequestedProcess() {
        var _a;
        const hasRequestedProcess = (_a = this.approval) === null || _a === void 0 ? void 0 : _a['has_requested_process'];
        if (hasRequestedProcess === false && !this.isUnDeclineProcess()) {
            throw new common_1.HttpException('Data is not on requested process', common_1.HttpStatus.BAD_REQUEST);
        }
    }
    isValidToRevokeRecentApproval() {
        this.currentStep.approver.forEach((approver) => {
            const isLoginUser = this.isLoginUser(approver);
            const currentStepIsInProgress = this.currentStepIsInProgress();
            if (isLoginUser && currentStepIsInProgress)
                return;
            if (!currentStepIsInProgress) {
                throw new common_1.HttpException(`This step already done, You're unable to revoke approval`, common_1.HttpStatus.BAD_REQUEST);
            }
            if (approver.status === approval_entity_1.ApproverStatus.DECLINED) {
                throw new common_1.HttpException(`Other user has decline this process, You're unable to revoke approval`, common_1.HttpStatus.BAD_REQUEST);
            }
        });
    }
    isUnDeclineProcess() {
        let isUnDeclineProcess = false;
        this.currentStep.approver.forEach((approver) => {
            const isLoginUser = this.isLoginUser(approver);
            if (isLoginUser && approver.status === approval_entity_1.ApproverStatus.DECLINED) {
                isUnDeclineProcess = true;
            }
        });
        return isUnDeclineProcess;
    }
    currentStepIsInProgress() {
        return this.currentStep.process.is_done === false;
    }
    isLoginUser(approver) {
        return approver.id === this.userCategoryId;
    }
}
exports.CheckSubmissionOnValidStatus = CheckSubmissionOnValidStatus;
//# sourceMappingURL=check-submission-in-valid-status.js.map