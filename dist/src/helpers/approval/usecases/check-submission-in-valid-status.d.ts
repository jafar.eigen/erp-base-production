import { Approval, Approver } from '../../../base/domain/entities/approval.entity';
export declare class CheckSubmissionOnValidStatus<Entity> {
    private readonly approval;
    private readonly userCategoryId;
    private readonly step;
    protected readonly currentStep: Approval;
    constructor(approval: Entity, userCategoryId: string, step: number);
    protected setApprover(): void;
    protected isApproverMember(): void;
    protected previousStepShouldBeDone(): void;
    protected hasRequestedProcess(): void;
    protected isValidToRevokeRecentApproval(): void;
    protected isUnDeclineProcess(): boolean;
    protected currentStepIsInProgress(): boolean;
    protected isLoginUser(approver: Approver): boolean;
}
