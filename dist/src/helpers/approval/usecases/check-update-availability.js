"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckDataAvailableForUpdate = void 0;
const common_1 = require("@nestjs/common");
class CheckDataAvailableForUpdate {
    constructor(approval) {
        if (approval['has_requested_process'] === true)
            throw new common_1.HttpException('Approval is in request progress', common_1.HttpStatus.BAD_REQUEST);
    }
}
exports.CheckDataAvailableForUpdate = CheckDataAvailableForUpdate;
//# sourceMappingURL=check-update-availability.js.map