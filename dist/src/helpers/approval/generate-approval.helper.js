"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateApproval = void 0;
const approval_entity_1 = require("../../base/domain/entities/approval.entity");
const base_entity_1 = require("../../base/domain/entities/base.entity");
const get_file_path_helper_1 = require("../get-file-path.helper");
const find_company_approval_helper_1 = require("./find-company-approval.helper");
const check_update_availability_1 = require("./usecases/check-update-availability");
class GenerateApproval {
    setData(data) {
        Object.assign(this, { data });
        return this;
    }
    setNewData(editedData) {
        Object.assign(this, { editedData });
        return this;
    }
    setUser(user) {
        Object.assign(this, { user });
        return this;
    }
    setApproval(approvals) {
        Object.assign(this, { approvals });
        return this;
    }
    setFile(file) {
        Object.assign(this, { file });
        return this;
    }
    setNewAddress(newAddress) {
        Object.assign(this, { newAddress });
        return this;
    }
    async run() {
        this.isAvailableForUpdate();
        this.mergeData();
        return this.data;
    }
    isAvailableForUpdate() {
        new check_update_availability_1.CheckDataAvailableForUpdate(this.data);
    }
    mergeData() {
        var _a, _b, _c;
        const isDraft = this.data['status'] === base_entity_1.STATUS.DRAFT;
        const createApproval = isDraft ? this.data['approval'] : null;
        const editApproval = (0, find_company_approval_helper_1.findApproval)(this.approvals, this.data, this.user);
        const pathImage = this.file
            ? (0, get_file_path_helper_1.getFilePath)(this.file)
            : (_a = this.data['logo_url']) !== null && _a !== void 0 ? _a : null;
        Object.assign(this.data, {
            approval: editApproval,
            editor_id: (_b = this.user.uuid) !== null && _b !== void 0 ? _b : this.user.id,
            editor_name: this.user.username,
            request_info: approval_entity_1.RequestInfo.EDIT_DATA,
            requested_data: Object.assign(Object.assign({}, this.editedData), { approval: createApproval, request_info: isDraft ? approval_entity_1.RequestInfo.CREATE_DATA : approval_entity_1.RequestInfo.EDIT_DATA, editor_id: (_c = this.user.uuid) !== null && _c !== void 0 ? _c : this.user.id, editor_name: this.user.username, logo_url: pathImage, new_address: this.newAddress === '1' })
        });
    }
}
exports.GenerateApproval = GenerateApproval;
//# sourceMappingURL=generate-approval.helper.js.map