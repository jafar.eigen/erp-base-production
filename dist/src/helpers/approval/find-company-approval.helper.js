"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getInProgressLowerStep = exports.getApprovalOnStep = exports.findApproval = void 0;
const findApproval = (approvals, data, user) => {
    if (!approvals)
        return null;
    return approvals.find((approval) => {
        return (matchCompanyIdOnData(data, approval) ||
            approval.company_id === user.company.uuid);
    });
};
exports.findApproval = findApproval;
const getApprovalOnStep = (approval, step) => {
    return approval.approval.find((approvalStep) => approvalStep.step === step);
};
exports.getApprovalOnStep = getApprovalOnStep;
const getInProgressLowerStep = (approval, currentStep) => {
    return approval.approval.find((approval) => {
        return approval.step > currentStep && approval.process.is_done === false;
    });
};
exports.getInProgressLowerStep = getInProgressLowerStep;
const matchCompanyIdOnData = (data, approval) => {
    if (!data)
        return false;
    return (approval.company_id === data['company_id'] ||
        approval.company_id === data['id'] ||
        approval.company_id === data['parent_id']);
};
//# sourceMappingURL=find-company-approval.helper.js.map