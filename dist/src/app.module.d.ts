import { SentryService } from '@ntegral/nestjs-sentry';
export declare class AppModule {
    private readonly client;
    constructor(client: SentryService);
}
