"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteConsumerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const src_1 = require("src");
const company_read_topics_1 = require("../../company-read.topics");
const company_read_data_service_1 = require("../../data/services/company-read-data.service");
let SiteConsumerControllerImpl = class SiteConsumerControllerImpl {
    constructor(companyDataService, responses) {
        this.companyDataService = companyDataService;
        this.responses = responses;
    }
    async isSiteExistOnCompany(message) {
        try {
            const siteCompanyId = await message.value;
            const company = await this.companyDataService.findOne(siteCompanyId);
            return this.responses.json(common_1.HttpStatus.FOUND, company ? true : false);
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
};
__decorate([
    (0, microservices_1.MessagePattern)(company_read_topics_1.CHECK_EXISTENCE_SITE_ON_COMPANY),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SiteConsumerControllerImpl.prototype, "isSiteExistOnCompany", null);
SiteConsumerControllerImpl = __decorate([
    (0, common_1.Controller)('company'),
    __metadata("design:paramtypes", [company_read_data_service_1.CompanyReadDataServiceImpl,
        src_1.Responses])
], SiteConsumerControllerImpl);
exports.SiteConsumerControllerImpl = SiteConsumerControllerImpl;
//# sourceMappingURL=site-consumer.controller.js.map