import { IResponses, Responses } from 'src';
import { CompanyReadDataServiceImpl } from '../../data/services/company-read-data.service';
import { SiteConsumerController } from '../../domain/contracts/site-consumer-service.contract';
import { KafkaMessage } from '../../domain/entities/kafka/kafka.message';
export declare class SiteConsumerControllerImpl implements SiteConsumerController {
    private companyDataService;
    private responses;
    constructor(companyDataService: CompanyReadDataServiceImpl, responses: Responses);
    isSiteExistOnCompany(message: KafkaMessage): Promise<IResponses>;
}
