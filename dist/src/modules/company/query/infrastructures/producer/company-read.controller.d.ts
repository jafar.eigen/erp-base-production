import { Responses, IResponses } from 'src';
import { CompanyReadOrchestrator } from 'src/modules/company/query/domain/usecases/company-read.orchestrator';
import { CompanyReadController } from 'src/modules/company/query/domain/contracts/company-read-controller.contract';
import { CompanyFilterDTO } from './dto/filter-company.dto';
export declare class CompanyReadControllerImpl implements CompanyReadController {
    private responses;
    private companyReadOrchestrator;
    constructor(responses: Responses, companyReadOrchestrator: CompanyReadOrchestrator);
    index(page: number, limit: number, params: CompanyFilterDTO): Promise<IResponses>;
    show(companyId: string): Promise<IResponses>;
}
