import { CompanyFilterEntity } from '../../../domain/entities/company-filter.entity';
export declare class CompanyFilterDTO implements CompanyFilterEntity {
    parent: string;
    q: string;
    updated_by: string[];
    name: string;
    phone: string;
    address: string;
    statuses: string[];
    requests_info: string[];
    approval_statuses: string[];
    last_update_from: string;
    last_update_to: string;
}
