"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyFilterDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CompanyFilterDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => { var _a; return (_a = body.parent) === null || _a === void 0 ? void 0 : _a.toString(); }),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'If true only parent, if false only child, if didnt send this, get all data.'
    }),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "parent", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.q),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'Search by: Branch name, Company name, Address, Phone, User'
    }),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "q", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Filter company by user (last edited/updated)'
    }),
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.updated_by) ? body.updated_by : [body.updated_by];
    }),
    __metadata("design:type", Array)
], CompanyFilterDTO.prototype, "updated_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'Filter company by name'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.name),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'Filter company by phone'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.phone),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'Filter company by address'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.address),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : draft, inactive, active, requested, declined'
    }),
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.statuses) ? body.statuses : [body.statuses];
    }),
    __metadata("design:type", Array)
], CompanyFilterDTO.prototype, "statuses", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Filter company by request_info'
    }),
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.requests_info)
            ? body.requests_info
            : [body.requests_info];
    }),
    __metadata("design:type", Array)
], CompanyFilterDTO.prototype, "requests_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Filter company by approval status'
    }),
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.approval_statuses)
            ? body.approval_statuses
            : [body.approval_statuses];
    }),
    __metadata("design:type", Array)
], CompanyFilterDTO.prototype, "approval_statuses", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update from',
        required: false
    }),
    (0, class_validator_1.ValidateIf)((body) => body.last_update_from),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "last_update_from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update to',
        required: false
    }),
    (0, class_validator_1.ValidateIf)((body) => body.last_update_to),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CompanyFilterDTO.prototype, "last_update_to", void 0);
exports.CompanyFilterDTO = CompanyFilterDTO;
//# sourceMappingURL=filter-company.dto.js.map