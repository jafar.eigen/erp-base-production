"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyReadControllerImpl = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const company_read_orchestrator_1 = require("src/modules/company/query/domain/usecases/company-read.orchestrator");
const companies_transformer_1 = require("./transformers/companies.transformer");
const company_transformer_1 = require("./transformers/company.transformer");
const filter_company_dto_1 = require("./dto/filter-company.dto");
const swagger_1 = require("@nestjs/swagger");
let CompanyReadControllerImpl = class CompanyReadControllerImpl {
    constructor(responses, companyReadOrchestrator) {
        this.responses = responses;
        this.companyReadOrchestrator = companyReadOrchestrator;
    }
    async index(page, limit, params) {
        try {
            const companies = await this.companyReadOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new companies_transformer_1.CompaniesTransformer(companies).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, err.message);
        }
    }
    async show(companyId) {
        try {
            const company = await this.companyReadOrchestrator.show(companyId);
            return this.responses.json(common_1.HttpStatus.OK, new company_transformer_1.CompanyTransformer(company).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, err.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_company_dto_1.CompanyFilterDTO]),
    __metadata("design:returntype", Promise)
], CompanyReadControllerImpl.prototype, "index", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CompanyReadControllerImpl.prototype, "show", null);
CompanyReadControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('company-indexes'),
    (0, common_1.Controller)('company'),
    __metadata("design:paramtypes", [src_1.Responses,
        company_read_orchestrator_1.CompanyReadOrchestrator])
], CompanyReadControllerImpl);
exports.CompanyReadControllerImpl = CompanyReadControllerImpl;
//# sourceMappingURL=company-read.controller.js.map