"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompaniesTransformer = void 0;
const moment = require("moment");
class CompaniesTransformer {
    constructor(companies) {
        Object.assign(this, companies);
    }
    apply() {
        const companies = this.items.map((company) => {
            var _a, _b, _c;
            return {
                id: company.id,
                code: company.code,
                name: company.name,
                parent_id: company === null || company === void 0 ? void 0 : company.parent_id,
                parent_name: (_a = company === null || company === void 0 ? void 0 : company.parent) === null || _a === void 0 ? void 0 : _a.name,
                parent_code: (_b = company === null || company === void 0 ? void 0 : company.parent) === null || _b === void 0 ? void 0 : _b.code,
                city: company.city,
                country: company.country,
                sub_district: company.sub_district,
                postal_code: company.postal_code,
                province: company.province,
                hamlet: company.hamlet,
                village: company.village,
                type: company.type,
                website_url: company.website_url,
                address: company.address,
                logo_url: company === null || company === void 0 ? void 0 : company.logo_url,
                status: company.status,
                request_info: company.request_info,
                latitude: company.latitude,
                longitude: company.longitude,
                user_date: `${moment(company.updated_at).format('DD/MM/YY,HH:mm')}`,
                approval: company.approval,
                creator_id: company.creator_id,
                creator_name: company.creator_name,
                editor_id: company.editor_id,
                editor_name: company.editor_name,
                deleted_by_id: company.deleted_by_id,
                deleted_by_name: company.deleted_by_name,
                has_requested_process: company.has_requested_process,
                requested_data: company.requested_data,
                contacts: (_c = company.contacts) === null || _c === void 0 ? void 0 : _c.map((contact) => contact),
                updated_at: company.updated_at
            };
        });
        return {
            items: companies,
            meta: this.meta,
            links: this.links
        };
    }
}
exports.CompaniesTransformer = CompaniesTransformer;
//# sourceMappingURL=companies.transformer.js.map