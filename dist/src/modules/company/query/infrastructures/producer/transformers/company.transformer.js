"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyTransformer = void 0;
const moment = require("moment");
class CompanyTransformer {
    constructor(company) {
        this.company = company;
    }
    transform() {
        var _a, _b, _c, _d, _e, _f, _g;
        return {
            id: this.company.id,
            code: this.company.code,
            name: this.company.name,
            parent_id: (_a = this.company) === null || _a === void 0 ? void 0 : _a.parent_id,
            parent_name: (_c = (_b = this.company) === null || _b === void 0 ? void 0 : _b.parent) === null || _c === void 0 ? void 0 : _c.name,
            parent_code: (_e = (_d = this.company) === null || _d === void 0 ? void 0 : _d.parent) === null || _e === void 0 ? void 0 : _e.code,
            city: this.company.city,
            country: this.company.country,
            sub_district: this.company.sub_district,
            postal_code: this.company.postal_code,
            province: this.company.province,
            hamlet: this.company.hamlet,
            village: this.company.village,
            type: this.company.type,
            website_url: this.company.website_url,
            address: this.company.address,
            logo_url: (_f = this.company) === null || _f === void 0 ? void 0 : _f.logo_url,
            status: this.company.status,
            request_info: this.company.request_info,
            latitude: this.company.latitude,
            longitude: this.company.longitude,
            user_date: `${moment(this.company.updated_at).format('DD/MM/YY,HH:mm')}`,
            approval: this.company.approval,
            creator_id: this.company.creator_id,
            creator_name: this.company.creator_name,
            editor_id: this.company.editor_id,
            editor_name: this.company.editor_name,
            deleted_by_id: this.company.deleted_by_id,
            deleted_by_name: this.company.deleted_by_name,
            has_requested_process: this.company.has_requested_process,
            requested_data: this.company.requested_data,
            contacts: (_g = this.company.contacts) === null || _g === void 0 ? void 0 : _g.map((contact) => contact),
            updated_at: this.company.updated_at
        };
    }
}
exports.CompanyTransformer = CompanyTransformer;
//# sourceMappingURL=company.transformer.js.map