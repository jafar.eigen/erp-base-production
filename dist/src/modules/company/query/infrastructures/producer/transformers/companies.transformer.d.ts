import { CompanyEntity } from 'src/modules/company/query/domain/entities/company.entity';
import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { ICompaniesTransformer } from '../../../domain/contracts/companies-transformer.contract';
export declare class CompaniesTransformer implements ICompaniesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: CompanyEntity[];
    constructor(companies: Pagination<CompanyEntity>);
    apply(): Pagination<Partial<CompanyEntity>>;
}
