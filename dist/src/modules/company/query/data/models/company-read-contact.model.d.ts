import { CompanyContactEntity } from 'src/modules/company/query/domain/entities/company-contact.entity';
import { CompanyRead } from './company-read.model';
export declare class ContactRead implements CompanyContactEntity {
    id: string;
    company_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    companies: CompanyRead;
}
