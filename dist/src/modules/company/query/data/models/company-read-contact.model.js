"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactRead = void 0;
const typeorm_1 = require("typeorm");
const company_read_model_1 = require("./company-read.model");
let ContactRead = class ContactRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], ContactRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_id' }),
    __metadata("design:type", String)
], ContactRead.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'phone', nullable: true, length: 20 }),
    __metadata("design:type", String)
], ContactRead.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'ext', nullable: true, length: 20 }),
    __metadata("design:type", String)
], ContactRead.prototype, "ext", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'email', length: 32 }),
    __metadata("design:type", String)
], ContactRead.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'notes', nullable: true }),
    __metadata("design:type", String)
], ContactRead.prototype, "notes", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], ContactRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], ContactRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], ContactRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => company_read_model_1.CompanyRead, (company) => company.contacts),
    (0, typeorm_1.JoinColumn)({ name: 'company_id' }),
    __metadata("design:type", company_read_model_1.CompanyRead)
], ContactRead.prototype, "companies", void 0);
ContactRead = __decorate([
    (0, typeorm_1.Entity)('company_contacts')
], ContactRead);
exports.ContactRead = ContactRead;
//# sourceMappingURL=company-read-contact.model.js.map