"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var CompanyRead_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyRead = void 0;
const typeorm_1 = require("typeorm");
const company_entity_1 = require("../../domain/entities/company.entity");
const company_read_contact_model_1 = require("./company-read-contact.model");
let CompanyRead = CompanyRead_1 = class CompanyRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], CompanyRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: company_entity_1.CompanyType }),
    __metadata("design:type", String)
], CompanyRead.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: company_entity_1.CompanyStatus }),
    __metadata("design:type", String)
], CompanyRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: company_entity_1.RequestInfo }),
    __metadata("design:type", String)
], CompanyRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'parent_id', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "parent_id", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], CompanyRead.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], CompanyRead.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], CompanyRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], CompanyRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], CompanyRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'logo_url', length: '100', nullable: true }),
    __metadata("design:type", String)
], CompanyRead.prototype, "logo_url", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => company_read_contact_model_1.ContactRead, (contact) => contact.companies),
    __metadata("design:type", Array)
], CompanyRead.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => CompanyRead_1, (company) => company.childs, {
        cascade: ['insert', 'soft-remove', 'update'],
        onUpdate: 'CASCADE'
    }),
    __metadata("design:type", Array)
], CompanyRead.prototype, "childs", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => CompanyRead_1, (company) => company.parent, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        cascade: ['update']
    }),
    (0, typeorm_1.JoinColumn)({ name: 'parent_id' }),
    __metadata("design:type", CompanyRead)
], CompanyRead.prototype, "parent", void 0);
CompanyRead = CompanyRead_1 = __decorate([
    (0, typeorm_1.Entity)('companies')
], CompanyRead);
exports.CompanyRead = CompanyRead;
//# sourceMappingURL=company-read.model.js.map