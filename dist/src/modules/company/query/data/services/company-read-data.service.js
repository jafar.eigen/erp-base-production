"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyReadDataServiceImpl = void 0;
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("@nestjs/typeorm");
const company_read_model_1 = require("../models/company-read.model");
const database_config_1 = require("src/utils/database.config");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const company_relations_entity_1 = require("src/modules/company/query/domain/entities/company-relations.entity");
let CompanyReadDataServiceImpl = class CompanyReadDataServiceImpl {
    constructor(companyReadRepo) {
        this.companyReadRepo = companyReadRepo;
    }
    async save(company) {
        await this.companyReadRepo.save(company);
    }
    async findOneOrFail(companyId) {
        return await this.companyReadRepo.findOneOrFail(companyId, {
            relations: [company_relations_entity_1.default.CompanyContacts, company_relations_entity_1.default.PARENT]
        });
    }
    async findOne(companyId) {
        return await this.companyReadRepo.findOne(companyId);
    }
    async getMany(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.companyReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.companyReadRepo, options);
    }
};
CompanyReadDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(company_read_model_1.CompanyRead, database_config_1.COMPANY_READ_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], CompanyReadDataServiceImpl);
exports.CompanyReadDataServiceImpl = CompanyReadDataServiceImpl;
//# sourceMappingURL=company-read-data.service.js.map