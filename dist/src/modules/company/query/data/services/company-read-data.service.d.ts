import { FindManyOptions, Repository } from 'typeorm';
import { CompanyRead } from '../models/company-read.model';
import { CompanyEntity } from 'src/modules/company/query/domain/entities/company.entity';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { CompanyReadDataService } from 'src/modules/company/query/domain/contracts/company-read-data-service.contract';
export declare class CompanyReadDataServiceImpl implements CompanyReadDataService {
    private companyReadRepo;
    constructor(companyReadRepo: Repository<CompanyRead>);
    save(company: CompanyEntity): Promise<void>;
    findOneOrFail(companyId: string): Promise<CompanyEntity>;
    findOne(companyId: string): Promise<CompanyEntity>;
    getMany(options: IPaginationOptions, extraQuery?: FindManyOptions<CompanyEntity>): Promise<Pagination<CompanyEntity>>;
}
