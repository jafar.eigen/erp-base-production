"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const config_2 = require("src/utils/config");
const company_read_model_1 = require("src/modules/company/query/data/models/company-read.model");
const company_read_contact_model_1 = require("src/modules/company/query/data/models/company-read-contact.model");
exports.default = (0, config_1.registerAs)('typeormCompanyQuery', () => ({
    type: 'mysql',
    host: config_2.variableConfig.COMPANY_MYSQL_QUERY_HOST,
    port: config_2.variableConfig.COMPANY_MYSQL_QUERY_PORT,
    username: config_2.variableConfig.COMPANY_MYSQL_QUERY_USERNAME,
    password: config_2.variableConfig.COMPANY_MYSQL_QUERY_PASSWORD,
    database: config_2.variableConfig.COMPANY_MYSQL_QUERY_DATABASE,
    entities: [company_read_model_1.CompanyRead, company_read_contact_model_1.ContactRead]
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map