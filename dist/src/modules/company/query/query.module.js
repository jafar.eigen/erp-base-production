"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("@nestjs/config");
const src_1 = require("src");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const database_config_1 = require("src/utils/database.config");
const company_read_model_1 = require("./data/models/company-read.model");
const company_read_orchestrator_1 = require("./domain/usecases/company-read.orchestrator");
const company_read_data_service_1 = require("./data/services/company-read-data.service");
const company_read_controller_1 = require("./infrastructures/producer/company-read.controller");
const site_consumer_controller_1 = require("./infrastructures/consumer/site-consumer.controller");
let QueryModule = class QueryModule {
};
QueryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default],
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.COMPANY_READ_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormCompanyQuery'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([company_read_model_1.CompanyRead], database_config_1.COMPANY_READ_CONNECTION)
        ],
        providers: [company_read_orchestrator_1.CompanyReadOrchestrator, company_read_data_service_1.CompanyReadDataServiceImpl, src_1.Responses],
        controllers: [company_read_controller_1.CompanyReadControllerImpl, site_consumer_controller_1.SiteConsumerControllerImpl]
    })
], QueryModule);
exports.QueryModule = QueryModule;
//# sourceMappingURL=query.module.js.map