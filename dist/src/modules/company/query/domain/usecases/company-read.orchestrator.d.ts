import { Pagination } from 'nestjs-typeorm-paginate';
import { CompanyEntity } from '../entities/company.entity';
import { CompanyReadDataServiceImpl } from 'src/modules/company/query/data/services/company-read-data.service';
import { CompanyFilterDTO } from '../../infrastructures/producer/dto/filter-company.dto';
export declare class CompanyReadOrchestrator {
    private companyDataService;
    constructor(companyDataService: CompanyReadDataServiceImpl);
    save(company: CompanyEntity): Promise<void>;
    index(page: number, limit: number, params: CompanyFilterDTO): Promise<Pagination<CompanyEntity>>;
    show(companyId: string): Promise<CompanyEntity>;
}
