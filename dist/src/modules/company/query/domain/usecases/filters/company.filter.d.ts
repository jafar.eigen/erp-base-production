import { CompanyEntity } from 'src/modules/company/query/domain/entities/company.entity';
import { SelectQueryBuilder } from 'typeorm';
import { CompanyFilterDTO } from '../../../infrastructures/producer/dto/filter-company.dto';
import { CompanyFilterEntity } from '../../entities/company-filter.entity';
export declare class CompanyFilter extends CompanyFilterEntity {
    constructor(params: CompanyFilterDTO);
    apply(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected bySearch(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byStatuses(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byName(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byAddress(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byUser(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byPhone(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byParent(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byRequestsInfo(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byLastUpdateDate(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected byApprovalStatuses(query: SelectQueryBuilder<CompanyEntity>): SelectQueryBuilder<CompanyEntity>;
    protected getQueryApprovalStatus(query: string): string;
}
