"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyFilter = void 0;
const typeorm_1 = require("typeorm");
const company_filter_entity_1 = require("../../entities/company-filter.entity");
const moment = require("moment");
class CompanyFilter extends company_filter_entity_1.CompanyFilterEntity {
    constructor(params) {
        super();
        this.q = params.q;
        this.statuses = params.statuses;
        this.address = params.address;
        this.phone = params.phone;
        this.updated_by = params.updated_by;
        this.name = params.name;
        this.parent = params.parent;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.approval_statuses = params.approval_statuses;
        this.requests_info = params.requests_info;
    }
    apply(query) {
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.name)
            query = this.byName(query);
        if (this.phone)
            query = this.byPhone(query);
        if (this.updated_by)
            query = this.byUser(query);
        if (this.parent)
            query = this.byParent(query);
        if (this.q)
            query = this.bySearch(query);
        if (this.last_update_from && this.last_update_to) {
            query = this.byLastUpdateDate(query);
        }
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.approval_statuses)
            query = this.byApprovalStatuses(query);
        return query;
    }
    bySearch(query) {
        return query.orWhere(new typeorm_1.Brackets((subQuery) => {
            subQuery
                .orWhere('companies.name LIKE :name', {
                name: `%${this.q}%`
            })
                .orWhere('companies.code LIKE :code', {
                code: `%${this.q}%`
            })
                .orWhere('companies.status LIKE :status', {
                status: `%${this.q}%`
            })
                .orWhere('companies.address LIKE :address', {
                address: `%${this.q}%`
            })
                .orWhere('contacts.phone LIKE :phone', {
                phone: `%${this.q}%`
            })
                .orWhere('companies.creator_name LIKE :creator_name', {
                creator_name: `%${this.q}%`
            })
                .orWhere('companies.editor_name LIKE :editor_name', {
                editor_name: `%${this.q}%`
            })
                .orWhere('companies.request_info LIKE :request_info', {
                request_info: `%${this.q}%`
            })
                .orWhere(`companies.updated_at LIKE :lastUpdated`, {
                lastUpdated: `%${moment(new Date(this.q)).format('YYYY-MM-DD')}%`
            })
                .orWhere(`parent.code LIKE :parentCode`, {
                parentCode: `%${this.q}%`
            })
                .orWhere(`parent.name LIKE :parentName`, {
                parentName: `%${this.q}%`
            });
        }));
    }
    byStatuses(query) {
        return query.andWhere('companies.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byName(query) {
        return query.orWhere('companies.name LIKE :name', {
            name: `%${this.name}%`
        });
    }
    byAddress(query) {
        return query.orWhere('companies.address LIKE :address', {
            address: `%${this.address}%`
        });
    }
    byUser(query) {
        return query
            .andWhere('companies.editor_name IN (:...user)', {
            user: `%${this.updated_by}%`
        })
            .orWhere('companies.creator_name IN (:...user)', {
            user: `%${this.updated_by}%`
        });
    }
    byPhone(query) {
        return query.orWhere('contacts.phone LIKE :phone', {
            phone: `%${this.phone}%`
        });
    }
    byParent(query) {
        if (this.parent === '1') {
            return query.andWhere('companies.parent_id is NULL');
        }
        if (this.parent === '0') {
            return query.andWhere('companies.parent_id is not NULL');
        }
    }
    byRequestsInfo(query) {
        return query.andWhere(`companies.request_info IN (:...requests_info)`, {
            requests_info: this.requests_info
        });
    }
    byLastUpdateDate(query) {
        return query.andWhere('companies.updated_at BETWEEN :from AND :to', {
            from: moment(new Date(this.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
            to: moment(new Date(this.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
        });
    }
    byApprovalStatuses(query) {
        const maxLength = this.approval_statuses.length;
        let queries = '';
        if (!Array.isArray(this.approval_statuses)) {
            return query.orWhere(this.getQueryApprovalStatus(this.approval_statuses));
        }
        for (let inc = 0; inc < maxLength; inc++) {
            if (inc === this.approval_statuses.length - 1) {
                queries += this.getQueryApprovalStatus(this.approval_statuses[inc]);
            }
            else {
                queries += `${this.getQueryApprovalStatus(this.approval_statuses[inc])} OR `;
            }
        }
        return query.orWhere(`${queries}`);
    }
    getQueryApprovalStatus(query) {
        switch (query) {
            case 'approved':
                return `companies.has_requested_process = 0 AND companies.approval LIKE '%"is_done": true%'`;
            case 'declined':
                return `companies.has_requested_process = 0 AND companies.approval LIKE '%"has_decline": true%'`;
            default:
                return `companies.has_requested_process = 1 AND companies.approval LIKE '%"has_decline": false%' AND companies.approval LIKE '%"is_done": false%'`;
        }
    }
}
exports.CompanyFilter = CompanyFilter;
//# sourceMappingURL=company.filter.js.map