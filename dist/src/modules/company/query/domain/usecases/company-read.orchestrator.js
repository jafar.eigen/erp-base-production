"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyReadOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const company_read_data_service_1 = require("src/modules/company/query/data/services/company-read-data.service");
const company_filter_1 = require("./filters/company.filter");
const company_relations_entity_1 = require("../entities/company-relations.entity");
let CompanyReadOrchestrator = class CompanyReadOrchestrator {
    constructor(companyDataService) {
        this.companyDataService = companyDataService;
    }
    async save(company) {
        await this.companyDataService.save(company);
    }
    async index(page, limit, params) {
        const filter = new company_filter_1.CompanyFilter(params);
        return await this.companyDataService.getMany({
            page: page,
            limit: limit,
            route: '/api/company'
        }, {
            relations: [company_relations_entity_1.default.CompanyContacts, company_relations_entity_1.default.PARENT],
            join: {
                alias: 'companies',
                leftJoin: {
                    contacts: 'companies.contacts',
                    parent: 'companies.parent'
                }
            },
            order: { created_at: 'DESC' },
            where: (companyQuery) => {
                filter.apply(companyQuery);
            }
        });
    }
    async show(companyId) {
        return await this.companyDataService.findOneOrFail(companyId);
    }
};
CompanyReadOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [company_read_data_service_1.CompanyReadDataServiceImpl])
], CompanyReadOrchestrator);
exports.CompanyReadOrchestrator = CompanyReadOrchestrator;
//# sourceMappingURL=company-read.orchestrator.js.map