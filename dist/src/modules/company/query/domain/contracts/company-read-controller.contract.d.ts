import { IResponses } from 'src';
import { CompanyFilterEntity } from '../entities/company-filter.entity';
export interface CompanyReadController {
    index(page: number, limit: number, params: CompanyFilterEntity, headers: any): Promise<IResponses>;
    show(companyId: string): Promise<IResponses>;
}
