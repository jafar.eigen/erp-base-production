import { KafkaContext } from '@nestjs/microservices';
import { IResponses } from 'src';
import { KafkaMessage } from '../entities/kafka/kafka.message';
export interface SiteConsumerController {
    isSiteExistOnCompany(message: KafkaMessage, context: KafkaContext): Promise<IResponses>;
}
