import { CompanyEntity } from '../entities/company.entity';
export interface ICompanyTransformer {
    transform(): Partial<CompanyEntity>;
}
