import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { CompanyEntity } from '../entities/company.entity';
export interface CompanyReadDataService {
    save(company: CompanyEntity): Promise<void>;
    findOneOrFail(companyId: string): Promise<CompanyEntity>;
    findOne(companyId: string): Promise<CompanyEntity>;
    getMany(options: IPaginationOptions): Promise<Pagination<CompanyEntity>>;
}
