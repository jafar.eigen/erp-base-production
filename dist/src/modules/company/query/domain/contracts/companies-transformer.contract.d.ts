import { CompanyEntity } from '../entities/company.entity';
import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
export interface ICompaniesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: CompanyEntity[];
    apply(): Pagination<Partial<CompanyEntity>>;
}
