import { Approval } from 'src';
import { CompanyContactEntity } from './company-contact.entity';
export declare enum CompanyStatus {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export declare enum RequestInfo {
    CREATE_DATA = "Create Data",
    EDIT_DATA = "Edit Data",
    EDIT_ACTIVE = "Edit Active",
    EDIT_INACTIVE = "Edit Inactive",
    EDIT_DATA_AND_ACTIVE = "Edit Data & Status Active",
    EDIT_DATA_AND_INACTIVE = "Edit Data & Status Inactive",
    DELETE_DATA = "Delete Data"
}
export declare enum CompanyType {
    PT = "Perseroan Terbatas",
    CV = "CV",
    Organization = "Organization",
    Individual = "Individual"
}
export interface CompanyEntity {
    id: string;
    name: string;
    code: string;
    type: string;
    status: string;
    request_info: string;
    address: string;
    city: string;
    province: string;
    country: string;
    postal_code: number;
    website_url: string;
    parent_id: string;
    longitude: string;
    latitude: string;
    sub_district: string;
    hamlet: string;
    village: string;
    logo_url: string;
    creator_id?: string;
    creator_name?: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: Approval;
    requested_data: any;
    has_requested_process: any;
    contacts?: CompanyContactEntity[];
    childs?: CompanyEntity[];
    parent: CompanyEntity;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
