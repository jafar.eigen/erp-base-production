"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyType = exports.RequestInfo = exports.CompanyStatus = void 0;
var CompanyStatus;
(function (CompanyStatus) {
    CompanyStatus["DRAFT"] = "draft";
    CompanyStatus["ACTIVE"] = "active";
    CompanyStatus["INACTIVE"] = "inactive";
    CompanyStatus["REQUESTED"] = "requested";
    CompanyStatus["OPEN"] = "open";
    CompanyStatus["DECLINE"] = "declined";
})(CompanyStatus = exports.CompanyStatus || (exports.CompanyStatus = {}));
var RequestInfo;
(function (RequestInfo) {
    RequestInfo["CREATE_DATA"] = "Create Data";
    RequestInfo["EDIT_DATA"] = "Edit Data";
    RequestInfo["EDIT_ACTIVE"] = "Edit Active";
    RequestInfo["EDIT_INACTIVE"] = "Edit Inactive";
    RequestInfo["EDIT_DATA_AND_ACTIVE"] = "Edit Data & Status Active";
    RequestInfo["EDIT_DATA_AND_INACTIVE"] = "Edit Data & Status Inactive";
    RequestInfo["DELETE_DATA"] = "Delete Data";
})(RequestInfo = exports.RequestInfo || (exports.RequestInfo = {}));
var CompanyType;
(function (CompanyType) {
    CompanyType["PT"] = "Perseroan Terbatas";
    CompanyType["CV"] = "CV";
    CompanyType["Organization"] = "Organization";
    CompanyType["Individual"] = "Individual";
})(CompanyType = exports.CompanyType || (exports.CompanyType = {}));
//# sourceMappingURL=company.entity.js.map