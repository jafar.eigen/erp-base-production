export declare class CompanyFilterEntity {
    q: string;
    statuses: string[];
    requests_info: string[];
    approval_statuses: string[];
    updated_by: string[];
    last_update_from: string;
    last_update_to: string;
    name: string;
    phone: string;
    address: string;
    parent: string;
}
