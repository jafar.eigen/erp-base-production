"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const microservice_config_1 = require("../../../utils/microservice.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const company_model_1 = require("./data/models/company.model");
const company_contact_model_1 = require("./data/models/company-contact.model");
const database_config_1 = require("src/utils/database.config");
const group_companies_model_1 = require("./data/models/group-companies.model");
const company_orchestrator_1 = require("./domain/usecases/company.orchestrator");
const company_data_service_1 = require("./data/services/company-data.service");
const company_group_data_service_1 = require("./data/services/company-group-data.service");
const company_producer_service_1 = require("./infrastructures/producer/company-producer.service");
const company_producer_controller_1 = require("./infrastructures/producer/company-producer.controller");
const group_sites_model_1 = require("src/modules/site/command/data/models/group-sites.model");
const site_model_1 = require("src/modules/site/command/data/models/site.model");
const site_level_model_1 = require("src/modules/site/command/data/models/site-level.model");
const site_contact_model_1 = require("src/modules/site/command/data/models/site-contact.model");
const site_company_model_1 = require("src/modules/site/command/data/models/site-company.model");
const group_site_level_model_1 = require("src/modules/site/command/data/models/group-site-level.model");
const group_site_contact_model_1 = require("src/modules/site/command/data/models/group-site-contact.model");
const group_site_company_model_1 = require("src/modules/site/command/data/models/group-site-company.model");
const microservices_1 = require("@nestjs/microservices");
let CommandModule = class CommandModule {
};
CommandModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default, microservice_config_1.default],
                isGlobal: true
            }),
            microservices_1.ClientsModule.registerAsync([
                {
                    name: microservice_config_1.KAFKA_CLIENT_NAME,
                    imports: [config_1.ConfigModule],
                    useFactory: (configService) => configService.get('kafkaClientConfig'),
                    inject: [config_1.ConfigService]
                }
            ]),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.COMPANY_CUD_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormCompanyCommand'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([
                site_model_1.Site,
                site_company_model_1.SiteCompany,
                site_contact_model_1.SiteContact,
                site_level_model_1.SiteLevel,
                group_sites_model_1.GroupSite,
                group_site_contact_model_1.GroupSiteContact,
                group_site_level_model_1.GroupSiteLevel,
                group_site_company_model_1.GroupSiteCompany,
                group_companies_model_1.GroupCompanies,
                company_model_1.Company,
                company_contact_model_1.Contact
            ], database_config_1.COMPANY_CUD_CONNECTION)
        ],
        providers: [
            src_1.Responses,
            src_1.JSONParse,
            company_orchestrator_1.CompanyOrchestrator,
            company_data_service_1.CompanyDataServiceImpl,
            company_producer_service_1.CompanyProducerServiceImpl,
            company_group_data_service_1.GroupCompanyDataServiceImpl
        ],
        controllers: [company_producer_controller_1.CompanyProducerControllerImpl]
    })
], CommandModule);
exports.CommandModule = CommandModule;
//# sourceMappingURL=command.module.js.map