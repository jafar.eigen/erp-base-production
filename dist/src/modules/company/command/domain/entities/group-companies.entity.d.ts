import { CompanyEntity } from './company.entity';
export interface GroupCompaniesEntity {
    id?: string;
    code: string;
    companies?: CompanyEntity[];
    updated_at?: Date;
    created_at?: Date;
    deleted_at?: Date;
}
