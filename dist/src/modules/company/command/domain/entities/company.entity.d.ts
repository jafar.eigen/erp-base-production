import { CompanyContactEntity } from './company-contact.entity';
import { ApprovalPerCompany, BaseEntity, STATUS, RequestInfo } from 'src';
export declare enum CompanyStatus {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export declare enum CompanyType {
    PT = "Perseroan Terbatas",
    CV = "CV",
    Organization = "Organization",
    Individual = "Individual"
}
export interface CompanyEntity extends BaseEntity {
    id: string;
    name: string;
    code: string;
    type: string;
    status: STATUS;
    request_info: RequestInfo;
    address: string;
    city: string;
    province: string;
    country: string;
    postal_code: number;
    website_url: string;
    parent_id: string;
    longitude: string;
    latitude: string;
    sub_district: string;
    hamlet: string;
    village: string;
    logo_url: string;
    creator_id?: string;
    creator_name?: string;
    group_id: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    contacts?: CompanyContactEntity[];
    childs?: CompanyEntity[];
    parent: CompanyEntity;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
