export interface CompanyResultBatch {
    success: string[];
    failed: string[];
}
