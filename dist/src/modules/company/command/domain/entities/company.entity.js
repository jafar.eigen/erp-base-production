"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyType = exports.CompanyStatus = void 0;
var CompanyStatus;
(function (CompanyStatus) {
    CompanyStatus["DRAFT"] = "draft";
    CompanyStatus["ACTIVE"] = "active";
    CompanyStatus["INACTIVE"] = "inactive";
    CompanyStatus["REQUESTED"] = "requested";
    CompanyStatus["OPEN"] = "open";
    CompanyStatus["DECLINE"] = "declined";
})(CompanyStatus = exports.CompanyStatus || (exports.CompanyStatus = {}));
var CompanyType;
(function (CompanyType) {
    CompanyType["PT"] = "Perseroan Terbatas";
    CompanyType["CV"] = "CV";
    CompanyType["Organization"] = "Organization";
    CompanyType["Individual"] = "Individual";
})(CompanyType = exports.CompanyType || (exports.CompanyType = {}));
//# sourceMappingURL=company.entity.js.map