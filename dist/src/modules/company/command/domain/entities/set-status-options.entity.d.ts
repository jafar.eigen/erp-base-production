import { CompanyStatus } from './company.entity';
export interface SetStatusOptions {
    status: CompanyStatus;
}
