import { DeleteResult } from 'typeorm';
import { GroupCompaniesEntity } from '../entities/group-companies.entity';
export interface GroupCompanyDataService {
    save(group: GroupCompaniesEntity): Promise<GroupCompaniesEntity>;
    findOneOrFail(groupId: string): Promise<GroupCompaniesEntity>;
    delete(groupIds: string[]): Promise<DeleteResult>;
}
