"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
class Name {
    constructor(companyDataService, company, companyId = null) {
        this.companyDataService = companyDataService;
        this.company = company;
        this.companyId = companyId;
    }
    async execute() {
        const company = await this.companyDataService.findByName(this.company.name);
        if (company[0] && this.companyId !== company[0].id) {
            throw new Error('Name already exists.');
        }
        if (!this.companyId && company[0]) {
            throw new Error('Name already exists.');
        }
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map