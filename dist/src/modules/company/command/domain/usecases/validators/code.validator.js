"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
class Code {
    constructor(companyDataService, company, companyId = null) {
        this.companyDataService = companyDataService;
        this.company = company;
        this.companyId = companyId;
    }
    async execute() {
        const company = await this.companyDataService.findByCode(this.company.code);
        if (company && this.companyId !== company.id) {
            throw new Error('Code already exists.');
        }
        if (!this.companyId && company) {
            throw new Error('Code already exists.');
        }
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map