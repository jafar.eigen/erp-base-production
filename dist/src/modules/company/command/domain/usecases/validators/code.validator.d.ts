import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class Code {
    private companyDataService;
    private company;
    private companyId;
    constructor(companyDataService: CompanyDataServiceImpl, company: CompanyEntity, companyId?: string);
    execute(): Promise<void | Error>;
}
