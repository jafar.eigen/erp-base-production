/// <reference types="multer" />
import { BaseOrchestrator } from 'src';
import { CompanyEntity } from '../entities/company.entity';
import { CompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-data.service';
import { CompanyProducerServiceImpl } from 'src/modules/company/command/infrastructures/producer/company-producer.service';
import { GroupCompanyDataServiceImpl } from '../../data/services/company-group-data.service';
import { CompanyResultBatch } from '../entities/company-result-batch.entity';
export declare class CompanyOrchestrator extends BaseOrchestrator<CompanyEntity> {
    private companyDataService;
    private companyProducerService;
    private groupCompanyDataService;
    constructor(companyDataService: CompanyDataServiceImpl, companyProducerService: CompanyProducerServiceImpl, groupCompanyDataService: GroupCompanyDataServiceImpl);
    create(company: CompanyEntity): Promise<CompanyEntity>;
    request(companyId: string): Promise<CompanyEntity>;
    approve(companyId: string, step: number): Promise<CompanyEntity>;
    decline(companyId: string, step: number): Promise<CompanyEntity>;
    activate(companyIds: string[], withChild: string): Promise<CompanyResultBatch>;
    deactivate(companyIds: string[], withChild: string): Promise<CompanyResultBatch>;
    update(companyId: string, updatedData: CompanyEntity, file: Express.Multer.File, newAddress: string): Promise<CompanyEntity>;
    cancel(companyId: string): Promise<CompanyEntity>;
    delete(companyIds: string[], withChild: string): Promise<CompanyResultBatch>;
    validateDuplicateAttributes(company: CompanyEntity, companyId?: string): Promise<void | Error | string>;
    rollbackStatusTrx(entityId: string[]): Promise<CompanyEntity>;
}
