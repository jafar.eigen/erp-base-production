import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class AssignApprovalToChilds {
    protected readonly dataService: CompanyDataServiceImpl;
    protected readonly company: CompanyEntity;
    constructor(dataService: CompanyDataServiceImpl, company: CompanyEntity);
    run(): Promise<void>;
    protected getChilds(): Promise<CompanyEntity[]>;
}
