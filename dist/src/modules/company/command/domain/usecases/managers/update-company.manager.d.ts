/// <reference types="multer" />
import { UpdateManager } from 'src';
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyProducerServiceImpl } from '../../../infrastructures/producer/company-producer.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class UpdateCompanyManager extends UpdateManager<CompanyEntity> {
    readonly dataService: CompanyDataServiceImpl;
    readonly kafkaService: CompanyProducerServiceImpl;
    updatedData: CompanyEntity;
    file: Express.Multer.File;
    newAddress: string;
    entityId: string;
    constructor(dataService: CompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl, companyId: string, updatedData: CompanyEntity, file: Express.Multer.File, newAddress: string);
    beforeProcess(): Promise<void>;
    prepareData(): Promise<CompanyEntity>;
    afterProcess(entity: CompanyEntity): Promise<void>;
    protected validate(): Promise<void>;
    protected hasChilds(): Promise<boolean>;
}
