"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeactivateCompanyManager = void 0;
const src_1 = require("src");
const assign_approval_to_childs_usecase_1 = require("./assign-approval-to-childs.usecase");
class DeactivateCompanyManager extends src_1.DeactiveManager {
    constructor(companyProducerService, companyDataService, companyIds, withChild) {
        super(companyIds, companyDataService, companyProducerService);
        this.companyProducerService = companyProducerService;
        this.companyDataService = companyDataService;
        this.companyIds = companyIds;
        this.withChild = withChild;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(companies) {
        companies.forEach(async (company) => {
            var _a;
            this.data = company;
            const parentId = (_a = this.data) === null || _a === void 0 ? void 0 : _a.parent_id;
            if (!this.isApprovalDone() && this.isWithChild() && !parentId) {
                await new assign_approval_to_childs_usecase_1.AssignApprovalToChilds(this.companyDataService, this.data).run();
            }
            if (this.isApprovalDone() && this.isWithChild() && !parentId) {
                await this.updateToChilds();
            }
            if (this.isApprovalDone() && !this.isWithChild() && parentId) {
                await this.removeParent();
            }
            this.result.success.push(`company with id ${company.code} is not active.`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
    isApprovalDone() {
        const hasApproval = (0, src_1.isNeedApproval)(this.data.approval);
        return (!hasApproval ||
            (this.data.status === src_1.STATUS.INACTIVE && !this.data.has_requested_process));
    }
    isWithChild() {
        return this.withChild === '1';
    }
    async updateToChilds() {
        const childs = await this.getChilds();
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                var _a;
                const dataOld = Object.assign({}, child);
                Object.assign(child, {
                    request_info: src_1.RequestInfo.EDIT_INACTIVE,
                    status: src_1.STATUS.INACTIVE,
                    editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
                    editor_name: this.user.username,
                    appoval: this.data.approval
                });
                await this.companyDataService.save(child);
                await this.companyProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: dataOld
                });
            }));
        }
    }
    async removeParent() {
        const childs = await this.getChilds();
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                const oldData = Object.assign({}, child);
                Object.assign(child, {
                    parent_id: null
                });
                await this.companyDataService.save(child);
                await this.companyProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: oldData
                });
            }));
        }
    }
    async getChilds() {
        return await this.companyDataService.findChilds(this.data.id);
    }
}
exports.DeactivateCompanyManager = DeactivateCompanyManager;
//# sourceMappingURL=deactivate-company.manager.js.map