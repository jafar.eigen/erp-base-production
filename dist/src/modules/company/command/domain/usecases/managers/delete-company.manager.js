"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteCompanyManager = void 0;
const src_1 = require("src");
class DeleteCompanyManager extends src_1.DeleteManager {
    constructor(companyIds, companyDataService, companyProducerService, withChild) {
        super(companyIds, companyDataService, companyProducerService);
        this.companyIds = companyIds;
        this.companyDataService = companyDataService;
        this.companyProducerService = companyProducerService;
        this.withChild = withChild;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(company) {
        if (this.isWithChild()) {
            if (await this.hasRelation(company)) {
                return false;
            }
            if (await this.isChildsHasRelation(company)) {
                return false;
            }
        }
        else {
            if (await this.hasRelation(company)) {
                return false;
            }
        }
        return true;
    }
    async onSuccess(companies) {
        companies.forEach((company) => {
            this.result.success.push(`branch with id ${company.code} successfully deleted`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
    isWithChild() {
        return this.withChild === '1';
    }
    async hasRelation(company) {
        const responseKafka = await this.companyDataService.isCompanyExistOnSite(company.id);
        return responseKafka;
    }
    async isChildsHasRelation(company) {
        const childs = await this.companyDataService.findChilds(company.id);
        let isProcessDone = false;
        if (!childs)
            return isProcessDone;
        for (const child of childs) {
            if (await this.hasRelation(child)) {
                isProcessDone = true;
                break;
            }
            if (!isProcessDone)
                break;
        }
        return isProcessDone;
    }
}
exports.DeleteCompanyManager = DeleteCompanyManager;
//# sourceMappingURL=delete-company.manager.js.map