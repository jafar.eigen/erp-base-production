"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclinedCompanyRequestManager = void 0;
const src_1 = require("src");
class DeclinedCompanyRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.DECLINED;
    }
}
exports.DeclinedCompanyRequestManager = DeclinedCompanyRequestManager;
//# sourceMappingURL=decline-company-request.manager.js.map