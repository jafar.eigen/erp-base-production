import { ApprovalPerCompany, Approver, IUserPayload, KafkaPayload } from 'src';
import { CompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-data.service';
import { CompanyProducerServiceImpl } from 'src/modules/company/command/infrastructures/producer/company-producer.service';
import { CompanyEntity } from '../../../entities/company.entity';
export declare class ApprovalSubmitCompanyRequest {
    private readonly entityId;
    private readonly user;
    private readonly approvals;
    private readonly dataService;
    private readonly kafkaService;
    requestType: string;
    approvallStatus: string;
    statusGroup: boolean;
    withChild?: string;
    data: CompanyEntity;
    approver: Approver;
    constructor(entityId: string, user: IUserPayload, approvals: ApprovalPerCompany[], dataService: CompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl, requestType: string, approvallStatus: string, statusGroup: boolean, withChild?: string);
    execute(): Promise<CompanyEntity>;
    protected getRecentApproval(): Promise<void>;
    protected processRequest(): Promise<void>;
    protected assignApproval(): void;
    protected submitRequest(data: CompanyEntity): Promise<CompanyEntity>;
    protected skipApproval(data: {
        id: string;
    } & CompanyEntity): Promise<CompanyEntity>;
    protected skipApprovalWithChild(data: {
        id: string;
    } & CompanyEntity): Promise<CompanyEntity>;
    protected skipApprovalWithChildDeactivate(data: {
        id: string;
    } & CompanyEntity): Promise<CompanyEntity>;
    protected skipApprovalDelete(data: {
        id: string;
    } & CompanyEntity): Promise<CompanyEntity>;
    protected skipApprovalRequest(data: {
        id: string;
    } & CompanyEntity): Promise<CompanyEntity>;
    produceApprovalSubmitted(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
    protected produceApprovalSkipped(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
}
