import { ActiveManager, BaseResultBatch } from 'src';
import { CompanyEntity } from '../../entities/company.entity';
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyProducerServiceImpl } from '../../../infrastructures/producer/company-producer.service';
export declare class ActivateCompanyManager extends ActiveManager<CompanyEntity> {
    companyProducerService: CompanyProducerServiceImpl;
    companyDataService: CompanyDataServiceImpl;
    companyIds: string[];
    withChild: string;
    result: BaseResultBatch;
    data: CompanyEntity;
    constructor(companyProducerService: CompanyProducerServiceImpl, companyDataService: CompanyDataServiceImpl, companyIds: string[], withChild: string);
    onSuccess(companies: CompanyEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
    protected updateAndProduceToChilds(): Promise<void>;
    protected isWithChild(): boolean;
}
