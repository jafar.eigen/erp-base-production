"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateCompanyManager = void 0;
const src_1 = require("src");
const assign_approval_to_childs_usecase_1 = require("./assign-approval-to-childs.usecase");
class ActivateCompanyManager extends src_1.ActiveManager {
    constructor(companyProducerService, companyDataService, companyIds, withChild) {
        super(companyIds, companyDataService, companyProducerService);
        this.companyProducerService = companyProducerService;
        this.companyDataService = companyDataService;
        this.companyIds = companyIds;
        this.withChild = withChild;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(companies) {
        companies.forEach(async (company) => {
            var _a;
            this.data = company;
            if (this.isWithChild() && !((_a = this.data) === null || _a === void 0 ? void 0 : _a.has_requested_process)) {
                await this.updateAndProduceToChilds();
            }
            if (this.isWithChild() && this.data.has_requested_process) {
                await new assign_approval_to_childs_usecase_1.AssignApprovalToChilds(this.companyDataService, this.data).run();
            }
            this.result.success.push(`company with id ${company.code} successfully activated`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
    async updateAndProduceToChilds() {
        const childs = await this.companyDataService.findChilds(this.data.id);
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                var _a;
                const oldData = Object.assign({}, child);
                Object.assign(child, {
                    request_info: src_1.RequestInfo.EDIT_ACTIVE,
                    status: src_1.STATUS.ACTIVE,
                    editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
                    editor_name: this.user.username
                });
                await this.companyDataService.save(child);
                await this.companyProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: oldData
                });
            }));
        }
    }
    isWithChild() {
        return this.withChild === '1';
    }
}
exports.ActivateCompanyManager = ActivateCompanyManager;
//# sourceMappingURL=activate-company.manager.js.map