"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveCompanyRequestManager = void 0;
const src_1 = require("src");
class ApproveCompanyRequestManager extends src_1.BaseActionManager {
    constructor(dataService, groupService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.groupService = groupService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.APPROVED;
    }
    async produceEntityActivatedWithSiteTopic(payload) {
        return this.kafkaService.baseActivated(payload);
    }
}
exports.ApproveCompanyRequestManager = ApproveCompanyRequestManager;
//# sourceMappingURL=approve-company-request.manager.js.map