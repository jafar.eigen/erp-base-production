import { SubmitManager } from 'src';
import { CompanyEntity } from '../../entities/company.entity';
import { CompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-data.service';
import { CompanyProducerServiceImpl } from 'src/modules/company/command/infrastructures/producer/company-producer.service';
import { GroupCompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-group-data.service';
export declare class SubmitCompanyRequestManager extends SubmitManager<CompanyEntity> {
    readonly id: string;
    readonly dataService: CompanyDataServiceImpl;
    readonly kafkaService: CompanyProducerServiceImpl;
    readonly groupDataService: GroupCompanyDataServiceImpl;
    result: CompanyEntity;
    constructor(id: string, dataService: CompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl, groupDataService: GroupCompanyDataServiceImpl);
    processEachEntity(): Promise<void>;
    onSuccess(companies: CompanyEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): CompanyEntity;
    protected isApprovalRequestDone(): boolean;
    protected isAbleToGenerateGroup(): boolean;
    protected generateGroup(): Promise<void>;
    protected getParent(): Promise<CompanyEntity>;
}
