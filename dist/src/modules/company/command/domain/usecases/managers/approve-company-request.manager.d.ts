import { KafkaPayload, IUserPayload, BaseActionManager } from 'src';
import { CompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-data.service';
import { GroupCompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-group-data.service';
import { CompanyProducerServiceImpl } from 'src/modules/company/command/infrastructures/producer/company-producer.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class ApproveCompanyRequestManager extends BaseActionManager<CompanyEntity> {
    readonly dataService: CompanyDataServiceImpl;
    readonly groupService: GroupCompanyDataServiceImpl;
    readonly kafkaService: CompanyProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: CompanyDataServiceImpl, groupService: GroupCompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl, id: string, step: number);
    produceEntityActivatedWithSiteTopic(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
}
