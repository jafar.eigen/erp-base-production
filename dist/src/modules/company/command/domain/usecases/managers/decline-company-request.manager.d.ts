import { BaseActionManager } from 'src';
import { CompanyEntity } from '../../entities/company.entity';
import { CompanyDataServiceImpl } from 'src/modules/company/command/data/services/company-data.service';
import { CompanyProducerServiceImpl } from 'src/modules/company/command/infrastructures/producer/company-producer.service';
export declare class DeclinedCompanyRequestManager extends BaseActionManager<CompanyEntity> {
    readonly dataService: CompanyDataServiceImpl;
    readonly kafkaService: CompanyProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: CompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl, id: string, step: number);
}
