"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApprovalSubmitCompanyRequest = void 0;
const src_1 = require("src");
class ApprovalSubmitCompanyRequest {
    constructor(entityId, user, approvals, dataService, kafkaService, requestType, approvallStatus, statusGroup, withChild) {
        this.entityId = entityId;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.requestType = requestType;
        this.approvallStatus = approvallStatus;
        this.statusGroup = statusGroup;
        this.withChild = withChild;
    }
    async execute() {
        await this.getRecentApproval();
        await this.processRequest();
        return this.data;
    }
    async getRecentApproval() {
        const data = await this.dataService.findOne(this.entityId);
        Object.assign(this, { data });
        if (this.requestType !== src_1.RequestType.REQUEST)
            this.assignApproval();
    }
    async processRequest() {
        const builder = new src_1.SubmitRequest();
        let skipApprovalFunction;
        if (this.requestType === src_1.RequestType.DELETE) {
            skipApprovalFunction = this.skipApprovalDelete;
        }
        else if (this.requestType === src_1.RequestType.REQUEST) {
            skipApprovalFunction = this.skipApprovalRequest;
        }
        else {
            skipApprovalFunction = this.skipApproval;
        }
        let requestType;
        if (this.requestType === src_1.RequestType.REQUEST) {
            requestType =
                this.data.request_info === src_1.RequestInfo.EDIT_DATA
                    ? src_1.RequestType.UPDATE
                    : src_1.RequestType.CREATE;
        }
        else {
            if (this.withChild) {
                if (this.requestType === src_1.RequestType.ACTIVE) {
                    requestType = src_1.RequestType.ACTIVE_WITH_CHILD;
                    skipApprovalFunction = this.skipApprovalWithChild;
                }
                else if (this.requestType === src_1.RequestType.INACTIVE) {
                    requestType = src_1.RequestType.INACTIVE_WITH_CHILD;
                    skipApprovalFunction = this.skipApprovalWithChildDeactivate;
                }
            }
            else {
                requestType = this.requestType;
            }
        }
        const data = await builder
            .setRequestType(requestType)
            .setApprovallStatus(this.approvallStatus)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setData(this.data)
            .setPassApprovalFunction(skipApprovalFunction)
            .setSubmitRequestFunction(this.submitRequest)
            .setProduceSubmitRequestTopic(this.produceApprovalSubmitted)
            .setProducePassApprovalTopic(this.produceApprovalSkipped)
            .run();
        Object.assign(this, { data });
    }
    assignApproval() {
        const approval = (0, src_1.findApproval)(this.approvals, this.data, this.user);
        Object.assign(this.data, { approval });
    }
    async submitRequest(data) {
        return await this.dataService.update(data, this.entityId);
    }
    async skipApproval(data) {
        Object.assign(data, {
            status: this.approvallStatus
        });
        return await this.dataService.update(data, this.entityId);
    }
    async skipApprovalWithChild(data) {
        return await this.dataService.activateWithChild(data, this.withChild);
    }
    async skipApprovalWithChildDeactivate(data) {
        return await this.dataService.activateWithChild(data, this.withChild);
    }
    async skipApprovalDelete(data) {
        await this.dataService.delete([data.id]);
        return data;
    }
    async skipApprovalRequest(data) {
        if (this.statusGroup && this.requestType === src_1.RequestType.CREATE) {
            await this.dataService.generateTableGroup();
            await this.dataService.saveGroup(data);
        }
        return await this.dataService.update(data, this.entityId);
    }
    async produceApprovalSubmitted(payload) {
        if (payload.data.status === src_1.STATUS.DRAFT)
            return;
        if (this['isNewAddress'] === true && !this['data'].has_requested_process) {
            return this.kafkaService.companyChangedNewAddress(payload);
        }
        return this.kafkaService.baseChanged(payload);
    }
    async produceApprovalSkipped(payload) {
        if (this.requestType === src_1.RequestType.DELETE) {
            return this.kafkaService.baseDeleted(payload);
        }
        else {
            if (this.data.status === src_1.STATUS.DRAFT)
                return;
            console.info('generate without site');
            return this.kafkaService.baseActivated(payload);
        }
    }
}
exports.ApprovalSubmitCompanyRequest = ApprovalSubmitCompanyRequest;
//# sourceMappingURL=approval-submit.manager.js.map