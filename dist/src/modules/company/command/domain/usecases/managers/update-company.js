"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCompany = void 0;
class UpdateCompany {
    constructor(companyDataService, company) {
        this.companyDataService = companyDataService;
        this.company = company;
    }
    setUpdatedData(company) {
        this.updatedData = company;
        return this;
    }
    validateImage(file) {
        if (!file)
            delete this.updatedData.logo_url;
        return this;
    }
    async execute() {
        const company = await this.companyDataService.update(this.updatedData, this.company.id);
        return company;
    }
}
exports.UpdateCompany = UpdateCompany;
//# sourceMappingURL=update-company.js.map