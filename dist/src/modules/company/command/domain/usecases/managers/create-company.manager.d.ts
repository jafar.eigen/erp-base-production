import { CreateManager, ApprovalPerCompany } from 'src';
import { CompanyEntity } from '../../entities/company.entity';
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { GroupCompanyDataServiceImpl } from '../../../data/services/company-group-data.service';
import { CompanyProducerServiceImpl } from '../../../infrastructures/producer/company-producer.service';
export declare class CreateCompanyManager extends CreateManager<CompanyEntity> {
    private companyDataService;
    private groupDataService;
    private companyProducerService;
    private company;
    constructor(companyDataService: CompanyDataServiceImpl, groupDataService: GroupCompanyDataServiceImpl, companyProducerService: CompanyProducerServiceImpl, company: CompanyEntity);
    prepareData(): Promise<CompanyEntity>;
    beforeProcess(): Promise<void>;
    _findApprovals(): ApprovalPerCompany;
    protected validateDuplicateAttributes(): Promise<void | Error>;
    protected validateExistenceParent(): Promise<CompanyEntity>;
}
