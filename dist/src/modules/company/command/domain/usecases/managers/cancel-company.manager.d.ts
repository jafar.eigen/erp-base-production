import { CancelManager } from 'src';
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyProducerServiceImpl } from '../../../infrastructures/producer/company-producer.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class CancelCompanyManager extends CancelManager<CompanyEntity> {
    readonly companyId: string;
    readonly dataService: CompanyDataServiceImpl;
    readonly kafkaService: CompanyProducerServiceImpl;
    constructor(companyId: string, dataService: CompanyDataServiceImpl, kafkaService: CompanyProducerServiceImpl);
}
