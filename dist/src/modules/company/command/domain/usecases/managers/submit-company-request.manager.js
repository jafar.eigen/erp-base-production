"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitCompanyRequestManager = void 0;
const src_1 = require("src");
const approval_submit_manager_1 = require("./approvals/approval-submit.manager");
class SubmitCompanyRequestManager extends src_1.SubmitManager {
    constructor(id, dataService, kafkaService, groupDataService) {
        super([id], dataService, kafkaService);
        this.id = id;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.groupDataService = groupDataService;
    }
    async processEachEntity() {
        await Promise.all(await this.entities.map(async (entity) => {
            try {
                const validateResult = await this.validateStatus(entity);
                if (validateResult) {
                    const entityResult = await new approval_submit_manager_1.ApprovalSubmitCompanyRequest(entity.id, this.user, this.approvals, this.dataService, this.kafkaService, this.requestType, this.approvallStatus, this.statusGroupApply).execute();
                    this.succeedProcess.push(entityResult);
                }
                else {
                    this.failedProcess.push(`Can't process entity with code: ${entity.code}.`);
                }
            }
            catch (err) {
                console.log('Error Process: ', err.message);
                this.failedProcess.push(err.message);
            }
        }));
    }
    async onSuccess(companies) {
        companies.map(async (company) => {
            this.result = company;
            if (this.isApprovalRequestDone()) {
                await this.dataService.findAndDeleteNulledRelation();
            }
            if (this.isAbleToGenerateGroup()) {
                await this.generateGroup();
            }
        });
    }
    async onFailed(messages) {
        if (messages.length > 0)
            throw new Error(messages[0]);
    }
    getResult() {
        return this.result;
    }
    isApprovalRequestDone() {
        return this.result.approval == null || !this.result.has_requested_process;
    }
    isAbleToGenerateGroup() {
        const approvalNoneAndHasParentId = (0, src_1.isNeedApproval)(this.result.approval) && !!this.result.parent_id;
        const haveNotGroupAndRequestedProcess = !this.result.group_id && !this.result.has_requested_process;
        const isRequestInfoCreateOrEdit = this.result.request_info === src_1.RequestInfo.CREATE_DATA ||
            this.result.request_info === src_1.RequestInfo.EDIT_DATA;
        return (approvalNoneAndHasParentId &&
            haveNotGroupAndRequestedProcess &&
            isRequestInfoCreateOrEdit);
    }
    async generateGroup() {
        if (!this.result.parent_id)
            return;
        const parent = await this.getParent();
        if (parent.group_id) {
            await this.groupDataService.findOneOrFail(parent.group_id).catch(() => {
                throw new Error(`Group company parent with id ${parent.group_id} is not found.`);
            });
            Object.assign(this.result, {
                group_id: parent.group_id
            });
        }
        if (!parent.group_id) {
            const validFormGroup = {
                code: 'GP-' + Math.floor(Math.random() * 999)
            };
            const group = await this.groupDataService.save(validFormGroup);
            Object.assign(group, {
                data: this.result
            });
            Object.assign(this.result, {
                group_id: group.id
            });
            Object.assign(parent, {
                group_id: group.id
            });
            await this.dataService.update(parent, parent.id);
            this.kafkaService.groupCompanyCreated({
                user: this.user,
                data: group,
                old: null
            });
        }
        await this.dataService.update(this.result, this.id);
    }
    async getParent() {
        return this.dataService.findOneOrFail(this.result.parent_id).catch(() => {
            throw new Error(`Company parent with id ${this.result.parent_id} is not found.`);
        });
    }
}
exports.SubmitCompanyRequestManager = SubmitCompanyRequestManager;
//# sourceMappingURL=submit-company-request.manager.js.map