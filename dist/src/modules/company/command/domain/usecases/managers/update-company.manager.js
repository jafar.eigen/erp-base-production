"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCompanyManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const code_validator_1 = require("../validators/code.validator");
const name_validator_1 = require("../validators/name.validator");
const update_company_1 = require("./update-company");
class UpdateCompanyManager extends src_1.UpdateManager {
    constructor(dataService, kafkaService, companyId, updatedData, file, newAddress) {
        super(dataService, kafkaService, false, file, newAddress);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.updatedData = updatedData;
        this.file = file;
        this.newAddress = newAddress;
        this.entityId = companyId;
    }
    async beforeProcess() {
        await this.validate();
    }
    async prepareData() {
        const company = await this.dataService.findOne(this.entityId);
        if (!this.updatedData.parent_id && !company.parent_id) {
            Object.assign(this.updatedData, { parent_name: null });
        }
        if (this.updatedData.parent_id) {
            const parent = await this.dataService.findOne(this.updatedData.parent_id);
            if (!parent)
                throw new Error(`Company parent with id ${this.updatedData.parent_id} is not found`);
            Object.assign(this.updatedData, {
                parent_id: parent.id,
                parent_name: parent.name
            });
        }
        return this.updatedData;
    }
    async afterProcess(entity) {
        await new update_company_1.UpdateCompany(this.dataService, entity)
            .setUpdatedData(entity)
            .validateImage(this.file)
            .execute();
    }
    async validate() {
        if ((await this.hasChilds()) && this.updatedData.parent_id) {
            throw new common_1.BadRequestException(common_1.HttpStatus.BAD_REQUEST, `Cannot assign parent to company parent.`);
        }
        await new name_validator_1.Name(this.dataService, this.updatedData, this.entityId).execute();
        await new code_validator_1.Code(this.dataService, this.updatedData, this.entityId).execute();
    }
    async hasChilds() {
        const childs = await this.dataService.findChilds(this.updatedData.id);
        return childs.length > 0;
    }
}
exports.UpdateCompanyManager = UpdateCompanyManager;
//# sourceMappingURL=update-company.manager.js.map