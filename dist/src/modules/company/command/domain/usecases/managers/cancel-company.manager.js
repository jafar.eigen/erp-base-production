"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelCompanyManager = void 0;
const src_1 = require("src");
class CancelCompanyManager extends src_1.CancelManager {
    constructor(companyId, dataService, kafkaService) {
        super(companyId, dataService, kafkaService);
        this.companyId = companyId;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
}
exports.CancelCompanyManager = CancelCompanyManager;
//# sourceMappingURL=cancel-company.manager.js.map