/// <reference types="multer" />
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyEntity } from '../../entities/company.entity';
export declare class UpdateCompany {
    private companyDataService;
    private company;
    constructor(companyDataService: CompanyDataServiceImpl, company: CompanyEntity);
    private updatedData;
    setUpdatedData(company: CompanyEntity): UpdateCompany;
    validateImage(file: Express.Multer.File): UpdateCompany;
    execute(): Promise<CompanyEntity>;
}
