"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateCompanyManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const code_validator_1 = require("../validators/code.validator");
const name_validator_1 = require("../validators/name.validator");
class CreateCompanyManager extends src_1.CreateManager {
    constructor(companyDataService, groupDataService, companyProducerService, company) {
        super(companyDataService, companyProducerService);
        this.companyDataService = companyDataService;
        this.groupDataService = groupDataService;
        this.companyProducerService = companyProducerService;
        this.company = company;
    }
    async prepareData() {
        return this.company;
    }
    async beforeProcess() {
        await this.validateDuplicateAttributes();
        await this.validateExistenceParent();
    }
    _findApprovals() {
        return (0, src_1.findApproval)(this.approvals, this.company, this.user);
    }
    async validateDuplicateAttributes() {
        await new name_validator_1.Name(this.companyDataService, this.company).execute();
        await new code_validator_1.Code(this.companyDataService, this.company).execute();
    }
    async validateExistenceParent() {
        if (!this.company.parent_id)
            return;
        const company = await this.companyDataService.findOneOrFail(this.company.parent_id);
        if (!company) {
            throw new common_1.HttpException(`Parent company with id ${this.company.parent_id}`, common_1.HttpStatus.BAD_REQUEST);
        }
        if (company === null || company === void 0 ? void 0 : company.parent_id) {
            throw new common_1.HttpException('Cannot assign parent to company child.', common_1.HttpStatus.BAD_REQUEST);
        }
        return company;
    }
}
exports.CreateCompanyManager = CreateCompanyManager;
//# sourceMappingURL=create-company.manager.js.map