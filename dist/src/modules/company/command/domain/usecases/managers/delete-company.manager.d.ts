import { DeleteManager, BaseResultBatch } from 'src';
import { CompanyEntity } from '../../entities/company.entity';
import { CompanyDataServiceImpl } from '../../../data/services/company-data.service';
import { CompanyProducerServiceImpl } from '../../../infrastructures/producer/company-producer.service';
export declare class DeleteCompanyManager extends DeleteManager<CompanyEntity> {
    companyIds: string[];
    companyDataService: CompanyDataServiceImpl;
    companyProducerService: CompanyProducerServiceImpl;
    withChild: string;
    result: BaseResultBatch;
    constructor(companyIds: string[], companyDataService: CompanyDataServiceImpl, companyProducerService: CompanyProducerServiceImpl, withChild: string);
    validateStatus(company: CompanyEntity): Promise<boolean>;
    onSuccess(companies: CompanyEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
    protected isWithChild(): boolean;
    protected hasRelation(company: CompanyEntity): Promise<boolean>;
    protected isChildsHasRelation(company: CompanyEntity): Promise<boolean>;
}
