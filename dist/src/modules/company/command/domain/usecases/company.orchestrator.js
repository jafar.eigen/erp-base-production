"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const company_data_service_1 = require("src/modules/company/command/data/services/company-data.service");
const company_producer_service_1 = require("src/modules/company/command/infrastructures/producer/company-producer.service");
const company_group_data_service_1 = require("../../data/services/company-group-data.service");
const delete_company_manager_1 = require("./managers/delete-company.manager");
const name_validator_1 = require("./validators/name.validator");
const code_validator_1 = require("./validators/code.validator");
const submit_company_request_manager_1 = require("./managers/submit-company-request.manager");
const approve_company_request_manager_1 = require("./managers/approve-company-request.manager");
const decline_company_request_manager_1 = require("./managers/decline-company-request.manager");
const activate_company_manager_1 = require("./managers/activate-company.manager");
const deactivate_company_manager_1 = require("./managers/deactivate-company.manager");
const create_company_manager_1 = require("./managers/create-company.manager");
const update_company_manager_1 = require("./managers/update-company.manager");
const cancel_company_manager_1 = require("./managers/cancel-company.manager");
let CompanyOrchestrator = class CompanyOrchestrator extends src_1.BaseOrchestrator {
    constructor(companyDataService, companyProducerService, groupCompanyDataService) {
        super();
        this.companyDataService = companyDataService;
        this.companyProducerService = companyProducerService;
        this.groupCompanyDataService = groupCompanyDataService;
    }
    async create(company) {
        return await new create_company_manager_1.CreateCompanyManager(this.companyDataService, this.groupCompanyDataService, this.companyProducerService, company).execute();
    }
    async request(companyId) {
        const submitCompany = new submit_company_request_manager_1.SubmitCompanyRequestManager(companyId, this.companyDataService, this.companyProducerService, this.groupCompanyDataService);
        await submitCompany.execute();
        return submitCompany.getResult();
    }
    async approve(companyId, step) {
        return await new approve_company_request_manager_1.ApproveCompanyRequestManager(this.companyDataService, this.groupCompanyDataService, this.companyProducerService, companyId, step).execute();
    }
    async decline(companyId, step) {
        return await new decline_company_request_manager_1.DeclinedCompanyRequestManager(this.companyDataService, this.companyProducerService, companyId, step).execute();
    }
    async activate(companyIds, withChild) {
        const activateCompany = new activate_company_manager_1.ActivateCompanyManager(this.companyProducerService, this.companyDataService, companyIds, withChild);
        await activateCompany.execute();
        return activateCompany.getResult();
    }
    async deactivate(companyIds, withChild) {
        const deactivateCompany = new deactivate_company_manager_1.DeactivateCompanyManager(this.companyProducerService, this.companyDataService, companyIds, withChild);
        await deactivateCompany.execute();
        return deactivateCompany.getResult();
    }
    async update(companyId, updatedData, file, newAddress) {
        return await new update_company_manager_1.UpdateCompanyManager(this.companyDataService, this.companyProducerService, companyId, updatedData, file, newAddress).execute();
    }
    async cancel(companyId) {
        return await new cancel_company_manager_1.CancelCompanyManager(companyId, this.companyDataService, this.companyProducerService).execute();
    }
    async delete(companyIds, withChild) {
        const deleteCompany = new delete_company_manager_1.DeleteCompanyManager(companyIds, this.companyDataService, this.companyProducerService, withChild);
        await deleteCompany.execute();
        return deleteCompany.getResult();
    }
    async validateDuplicateAttributes(company, companyId = null) {
        await new name_validator_1.Name(this.companyDataService, company, companyId).execute();
        await new code_validator_1.Code(this.companyDataService, company, companyId).execute();
    }
    async rollbackStatusTrx(entityId) {
        throw new Error('Method not implemented.');
    }
};
CompanyOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [company_data_service_1.CompanyDataServiceImpl,
        company_producer_service_1.CompanyProducerServiceImpl,
        company_group_data_service_1.GroupCompanyDataServiceImpl])
], CompanyOrchestrator);
exports.CompanyOrchestrator = CompanyOrchestrator;
//# sourceMappingURL=company.orchestrator.js.map