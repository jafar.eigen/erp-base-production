"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const site_model_1 = require("src/modules/site/command/data/models/site.model");
const site_company_model_1 = require("src/modules/site/command/data/models/site-company.model");
const site_contact_model_1 = require("src/modules/site/command/data/models/site-contact.model");
const site_level_model_1 = require("src/modules/site/command/data/models/site-level.model");
const group_sites_model_1 = require("src/modules/site/command/data/models/group-sites.model");
const group_site_contact_model_1 = require("src/modules/site/command/data/models/group-site-contact.model");
const group_site_level_model_1 = require("src/modules/site/command/data/models/group-site-level.model");
const group_site_company_model_1 = require("src/modules/site/command/data/models/group-site-company.model");
const group_companies_model_1 = require("src/modules/company/command/data/models/group-companies.model");
const company_model_1 = require("src/modules/company/command/data/models/company.model");
const company_contact_model_1 = require("src/modules/company/command/data/models/company-contact.model");
const config_2 = require("src/utils/config");
exports.default = (0, config_1.registerAs)('typeormCompanyCommand', () => ({
    type: 'mysql',
    host: config_2.variableConfig.COMPANY_MYSQL_COMMAND_HOST,
    port: config_2.variableConfig.COMPANY_MYSQL_COMMAND_PORT,
    username: config_2.variableConfig.COMPANY_MYSQL_COMMAND_USERNAME,
    password: config_2.variableConfig.COMPANY_MYSQL_COMMAND_PASSWORD,
    database: config_2.variableConfig.COMPANY_MYSQL_COMMAND_DATABASE,
    entities: [
        company_model_1.Company,
        company_contact_model_1.Contact,
        group_companies_model_1.GroupCompanies,
        group_sites_model_1.GroupSite,
        group_site_level_model_1.GroupSiteLevel,
        group_site_contact_model_1.GroupSiteContact,
        group_site_company_model_1.GroupSiteCompany,
        site_model_1.Site,
        site_level_model_1.SiteLevel,
        site_contact_model_1.SiteContact,
        site_company_model_1.SiteCompany
    ],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map