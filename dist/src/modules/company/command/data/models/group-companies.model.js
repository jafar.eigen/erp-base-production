"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupCompanies = void 0;
const group_sites_model_1 = require("src/modules/site/command/data/models/group-sites.model");
const typeorm_1 = require("typeorm");
const company_model_1 = require("./company.model");
let GroupCompanies = class GroupCompanies {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupCompanies.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '50' }),
    __metadata("design:type", String)
], GroupCompanies.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupCompanies.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupCompanies.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupCompanies.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => company_model_1.Company, (companies) => companies.group, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupCompanies.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_sites_model_1.GroupSite, (groupSite) => groupSite.group_company),
    __metadata("design:type", Array)
], GroupCompanies.prototype, "group_sites", void 0);
GroupCompanies = __decorate([
    (0, typeorm_1.Entity)('group_companies')
], GroupCompanies);
exports.GroupCompanies = GroupCompanies;
//# sourceMappingURL=group-companies.model.js.map