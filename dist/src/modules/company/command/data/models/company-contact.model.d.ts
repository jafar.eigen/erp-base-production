import { CompanyContactEntity } from 'src/modules/company/command/domain/entities/company-contact.entity';
import { Company } from './company.model';
export declare class Contact implements CompanyContactEntity {
    id: string;
    company_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    company: Company;
}
