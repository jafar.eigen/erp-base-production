"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Company_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Company = void 0;
const typeorm_1 = require("typeorm");
const src_1 = require("src");
const company_entity_1 = require("src/modules/company/command/domain/entities/company.entity");
const company_contact_model_1 = require("./company-contact.model");
const group_companies_model_1 = require("./group-companies.model");
let Company = Company_1 = class Company {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Company.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], Company.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10' }),
    __metadata("design:type", String)
], Company.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: company_entity_1.CompanyType }),
    __metadata("design:type", String)
], Company.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: company_entity_1.CompanyStatus }),
    __metadata("design:type", String)
], Company.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], Company.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'parent_id', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "parent_id", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], Company.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], Company.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], Company.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], Company.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], Company.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'logo_url', length: '100', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "logo_url", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], Company.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], Company.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], Company.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], Company.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Company.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Company.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Company.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => company_contact_model_1.Contact, (contact) => contact.company, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], Company.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Company_1, (company) => company.childs, {
        cascade: ['update', 'soft-remove']
    }),
    __metadata("design:type", Array)
], Company.prototype, "childs", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Company_1, (company) => company.parent, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'parent_id' }),
    __metadata("design:type", Company)
], Company.prototype, "parent", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_companies_model_1.GroupCompanies, (group) => group.companies),
    (0, typeorm_1.JoinColumn)({ name: 'group_id' }),
    __metadata("design:type", group_companies_model_1.GroupCompanies)
], Company.prototype, "group", void 0);
Company = Company_1 = __decorate([
    (0, typeorm_1.Entity)('companies')
], Company);
exports.Company = Company;
//# sourceMappingURL=company.model.js.map