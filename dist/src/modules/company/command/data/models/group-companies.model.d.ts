import { GroupSite } from 'src/modules/site/command/data/models/group-sites.model';
import { GroupCompaniesEntity } from '../../domain/entities/group-companies.entity';
import { Company } from './company.model';
export declare class GroupCompanies implements GroupCompaniesEntity {
    id: string;
    code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    companies: Company[];
    group_sites: GroupSite[];
}
