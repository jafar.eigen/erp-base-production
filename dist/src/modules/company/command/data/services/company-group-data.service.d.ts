import { DeleteResult, Repository } from 'typeorm';
import { GroupCompanyDataService } from '../../domain/contracts/company-group-data-service.contract';
import { GroupCompaniesEntity } from '../../domain/entities/group-companies.entity';
import { GroupCompanies } from '../models/group-companies.model';
export declare class GroupCompanyDataServiceImpl implements GroupCompanyDataService {
    private groupCompaniesRepo;
    constructor(groupCompaniesRepo: Repository<GroupCompanies>);
    save(group: GroupCompaniesEntity): Promise<GroupCompaniesEntity>;
    findOneOrFail(groupId: string): Promise<GroupCompaniesEntity>;
    delete(groupIds: string[]): Promise<DeleteResult>;
}
