import { Repository, SelectQueryBuilder } from 'typeorm';
import { BaseDataService } from 'src';
import { Company } from '../models/company.model';
import { CompanyEntity } from 'src/modules/company/command/domain/entities/company.entity';
import { SetStatusOptions } from 'src/modules/company/command/domain/entities/set-status-options.entity';
import { Contact } from '../models/company-contact.model';
import { Site } from 'src/modules/site/command/data/models/site.model';
export declare class CompanyDataServiceImpl extends BaseDataService<CompanyEntity> {
    private companyRepo;
    private companyContactRepo;
    private siteRepo;
    relations: string[];
    constructor(companyRepo: Repository<Company>, companyContactRepo: Repository<Contact>, siteRepo: Repository<Site>);
    update(updatedData: CompanyEntity | SetStatusOptions | any, companyId: string): Promise<CompanyEntity>;
    findByCode(code: string): Promise<CompanyEntity>;
    findByName(name: string): Promise<CompanyEntity[]>;
    findByIds(companyIds: string[]): Promise<CompanyEntity[]>;
    findOneOrFail(entityBaseId: string, extraQuery?: SelectQueryBuilder<CompanyEntity>): Promise<CompanyEntity>;
    findChilds(parentId: string): Promise<CompanyEntity[]>;
    findAndDeleteNulledRelation(): Promise<void>;
    activateCompany(company: CompanyEntity, withChild: string): Promise<CompanyEntity>;
    deactivateCompany(company: CompanyEntity, withChild: boolean): Promise<CompanyEntity>;
    isCompanyExistOnSite(companyId: string): Promise<boolean>;
}
