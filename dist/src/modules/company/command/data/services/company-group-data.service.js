"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupCompanyDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const database_config_1 = require("src/utils/database.config");
const typeorm_2 = require("typeorm");
const group_companies_model_1 = require("../models/group-companies.model");
let GroupCompanyDataServiceImpl = class GroupCompanyDataServiceImpl {
    constructor(groupCompaniesRepo) {
        this.groupCompaniesRepo = groupCompaniesRepo;
    }
    async save(group) {
        return await this.groupCompaniesRepo.save(group);
    }
    async findOneOrFail(groupId) {
        return await this.groupCompaniesRepo.findOneOrFail(groupId, {
            relations: ['companies']
        });
    }
    async delete(groupIds) {
        return await this.groupCompaniesRepo.delete(groupIds);
    }
};
GroupCompanyDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(group_companies_model_1.GroupCompanies, database_config_1.COMPANY_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], GroupCompanyDataServiceImpl);
exports.GroupCompanyDataServiceImpl = GroupCompanyDataServiceImpl;
//# sourceMappingURL=company-group-data.service.js.map