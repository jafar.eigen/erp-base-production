"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const src_1 = require("src");
const company_model_1 = require("../models/company.model");
const database_config_1 = require("src/utils/database.config");
const company_contact_model_1 = require("../models/company-contact.model");
const site_model_1 = require("src/modules/site/command/data/models/site.model");
let CompanyDataServiceImpl = class CompanyDataServiceImpl extends src_1.BaseDataService {
    constructor(companyRepo, companyContactRepo, siteRepo) {
        super(companyRepo);
        this.companyRepo = companyRepo;
        this.companyContactRepo = companyContactRepo;
        this.siteRepo = siteRepo;
        this.relations = ['contacts', 'parent'];
    }
    async update(updatedData, companyId) {
        const company = await this.companyRepo.findOneOrFail(companyId, {
            relations: ['contacts', 'parent']
        });
        Object.assign(company, updatedData);
        delete company.parent;
        return await this.companyRepo.save(company);
    }
    async findByCode(code) {
        return await this.companyRepo.findOne({ where: { code: code } });
    }
    async findByName(name) {
        return await this.companyRepo.find({ where: { name: name } });
    }
    async findByIds(companyIds) {
        return await this.companyRepo.findByIds(companyIds);
    }
    async findOneOrFail(entityBaseId, extraQuery = null) {
        const query = { id: entityBaseId };
        Object.assign(query, extraQuery);
        return await this.companyRepo.findOneOrFail({ where: query });
    }
    async findChilds(parentId) {
        return await this.companyRepo.find({ where: { parent_id: parentId } });
    }
    async findAndDeleteNulledRelation() {
        const contacts = await this.companyContactRepo.find({
            where: {
                company_id: (0, typeorm_2.IsNull)()
            }
        });
        if (contacts) {
            await this.companyContactRepo.remove(contacts);
        }
    }
    async activateCompany(company, withChild) {
        if (withChild) {
            let childCompany = await this.findChilds(company.id);
            childCompany = childCompany.map((company) => {
                return Object.assign(Object.assign({}, company), { status: src_1.STATUS.ACTIVE });
            });
            await this.companyRepo.save(childCompany);
        }
        return await this.companyRepo.save(company);
    }
    async deactivateCompany(company, withChild) {
        if (withChild) {
            let childCompany = await this.findChilds(company.id);
            childCompany = childCompany.map((company) => {
                return Object.assign(Object.assign({}, company), { status: src_1.STATUS.INACTIVE });
            });
            await this.companyRepo.save(childCompany);
        }
        return await this.companyRepo.save(company);
    }
    async isCompanyExistOnSite(companyId) {
        const result = await this.siteRepo.find({
            relations: ['contacts', 'levels', 'companies'],
            join: {
                alias: 'site',
                innerJoin: {
                    companies: 'site.companies'
                }
            },
            where: (querySite) => {
                querySite.where('companies.company_id = :companyId AND site.is_generated <> 1', { companyId });
            }
        });
        return result.length !== 0;
    }
};
CompanyDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(company_model_1.Company, database_config_1.COMPANY_CUD_CONNECTION)),
    __param(1, (0, typeorm_1.InjectRepository)(company_contact_model_1.Contact, database_config_1.COMPANY_CUD_CONNECTION)),
    __param(2, (0, typeorm_1.InjectRepository)(site_model_1.Site, database_config_1.COMPANY_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], CompanyDataServiceImpl);
exports.CompanyDataServiceImpl = CompanyDataServiceImpl;
//# sourceMappingURL=company-data.service.js.map