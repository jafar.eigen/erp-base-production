"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyProducerServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const src_1 = require("src");
const company_topics_1 = require("src/modules/company/command/company.topics");
const microservice_config_1 = require("src/utils/microservice.config");
let CompanyProducerServiceImpl = class CompanyProducerServiceImpl extends src_1.BaseProducerServiceImpl {
    constructor(client) {
        super(client);
        this.client = client;
        this.moduleName = company_topics_1.MODULE_TOPIC_COMPANY;
        this.TOPIC_CREATED = company_topics_1.HANDLE_COMPANY_CREATED;
        this.TOPIC_CHANGED = company_topics_1.HANDLE_COMPANY_CHANGED;
        this.TOPIC_ACTIVATED = company_topics_1.COMPANY_ACTIVATED;
        this.TOPIC_DELETED = company_topics_1.HANDLE_COMPANY_DELETED;
    }
    addNewTopics() {
        return [
            company_topics_1.HANDLE_COMPANY_CHANGED_NEW_ADDRESS,
            company_topics_1.HANDLE_COMPANY_CANCELED,
            company_topics_1.CHECK_EXISTENCE_COMPANY_ON_SITE,
            company_topics_1.COMPANY_GROUP_CREATED,
            company_topics_1.COMPANY_GROUP_CHANGED
        ];
    }
    async baseChanged(payload) {
        console.log('company changed triggered');
        if (payload.data) {
            Object.assign(payload.data, {
                branch_id: '-',
                branch_name: '-'
            });
        }
        if (payload.old) {
            Object.assign(payload.old, {
                branch_id: '-',
                branch_name: '-'
            });
        }
        this.client.send(this.TOPIC_CHANGED, payload).toPromise();
    }
    async companyChangedNewAddress(payload) {
        console.log('company changed new address triggered');
        if (payload.data) {
            Object.assign(payload.data, {
                branch_id: '-',
                branch_name: '-'
            });
        }
        if (payload.old) {
            Object.assign(payload.old, {
                branch_id: '-',
                branch_name: '-'
            });
        }
        this.client.send(company_topics_1.HANDLE_COMPANY_CHANGED_NEW_ADDRESS, payload).toPromise();
    }
    async companyCancel(payload) {
        console.log('company has_requested_process canceled');
        this.client.send(company_topics_1.HANDLE_COMPANY_CANCELED, payload).toPromise();
    }
    async baseActivated(payload) {
        console.log(this.TOPIC_ACTIVATED, ' triggered');
        Object.assign(payload.data, {
            branch_id: '-',
            branch_name: '-'
        });
        this.client.send(this.TOPIC_ACTIVATED, payload).toPromise();
    }
    async isCompanyExistOnSite(companyId) {
        try {
            const result = await this.client
                .send(company_topics_1.CHECK_EXISTENCE_COMPANY_ON_SITE, {
                data: { companyId },
                old: { id: '-' }
            })
                .toPromise();
            return result;
        }
        catch (err) {
            throw new Error(err.message);
        }
    }
    async groupCompanyCreated(payload) {
        console.info('groupCompanyCreated triggered');
    }
    async groupCompanyChanged(payload) {
        console.info('groupCompanyChanged triggered');
    }
};
CompanyProducerServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(microservice_config_1.KAFKA_CLIENT_NAME)),
    __metadata("design:paramtypes", [microservices_1.ClientKafka])
], CompanyProducerServiceImpl);
exports.CompanyProducerServiceImpl = CompanyProducerServiceImpl;
//# sourceMappingURL=company-producer.service.js.map