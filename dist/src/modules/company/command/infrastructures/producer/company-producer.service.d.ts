import { ClientKafka } from '@nestjs/microservices';
import { KafkaPayload, IUserPayload, IResponses, BaseProducerServiceImpl } from 'src';
import { CompanyEntity } from 'src/modules/company/command/domain/entities/company.entity';
import { GroupCompaniesEntity } from '../../domain/entities/group-companies.entity';
export declare class CompanyProducerServiceImpl extends BaseProducerServiceImpl<CompanyEntity> {
    client: ClientKafka;
    moduleName: string;
    TOPIC_CREATED: string;
    TOPIC_CHANGED: string;
    TOPIC_ACTIVATED: string;
    TOPIC_DELETED: string;
    constructor(client: ClientKafka);
    addNewTopics(): string[];
    baseChanged(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
    companyChangedNewAddress(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
    companyCancel(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
    baseActivated(payload: KafkaPayload<IUserPayload, CompanyEntity, CompanyEntity>): Promise<void>;
    isCompanyExistOnSite(companyId: string): Promise<IResponses>;
    groupCompanyCreated(payload: KafkaPayload<IUserPayload, GroupCompaniesEntity, GroupCompaniesEntity>): Promise<void>;
    groupCompanyChanged(payload: KafkaPayload<IUserPayload, GroupCompaniesEntity, GroupCompaniesEntity>): Promise<void>;
}
