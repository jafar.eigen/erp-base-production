"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyProducerControllerImpl = void 0;
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const platform_express_1 = require("@nestjs/platform-express");
const common_1 = require("@nestjs/common");
const create_company_dto_1 = require("./dto/create-company.dto");
const update_company_dto_1 = require("./dto/update-company.dto");
const store_file_config_1 = require("src/utils/store-file.config");
const company_orchestrator_1 = require("src/modules/company/command/domain/usecases/company.orchestrator");
const BaseController = (0, src_1.getBaseController)({
    createModelVm: create_company_dto_1.CreateCompanyDTO,
    updateModelVm: update_company_dto_1.UpdateCompanyDTO
});
let CompanyProducerControllerImpl = class CompanyProducerControllerImpl extends BaseController {
    constructor(companyOrchestrator) {
        super(new src_1.Responses(), companyOrchestrator, new src_1.JSONParse());
        this.companyOrchestrator = companyOrchestrator;
    }
    async create(body, headers, file) {
        try {
            body.logo_url = (0, src_1.getFilePath)(file);
            return super.create(body, headers);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
    async activate(headers, baseIds, withChild) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const result = await this.baseOrchestrator.activate(baseIds, withChild);
            return this.responses.json(common_1.HttpStatus.OK, result);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
    async deactivate(headers, baseIds, withChild) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const result = await this.baseOrchestrator.deactivate(baseIds, withChild);
            return this.responses.json(common_1.HttpStatus.OK, result);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
    async update(baseId, body, file, headers, newAddress) {
        try {
            body.logo_url = (0, src_1.getFilePath)(file);
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const result = await this.baseOrchestrator.update(baseId, body, file, newAddress);
            return this.responses.json(common_1.HttpStatus.OK, result);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
    async delete(headers, baseIds, withChild) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const result = await this.baseOrchestrator.delete(baseIds, withChild);
            return this.responses.json(common_1.HttpStatus.OK, result);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({ type: create_company_dto_1.CreateCompanyDTO }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('logo_url', store_file_config_1.StoreFileConfig)),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Headers)()),
    __param(2, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_company_dto_1.CreateCompanyDTO, String, Object]),
    __metadata("design:returntype", Promise)
], CompanyProducerControllerImpl.prototype, "create", null);
__decorate([
    (0, common_1.Put)('activate'),
    __param(0, (0, common_1.Headers)()),
    __param(1, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(2, (0, common_1.Query)('with_child')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Array, String]),
    __metadata("design:returntype", Promise)
], CompanyProducerControllerImpl.prototype, "activate", null);
__decorate([
    (0, common_1.Put)('deactivate'),
    __param(0, (0, common_1.Headers)()),
    __param(1, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(2, (0, common_1.Query)('with_child')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Array, String]),
    __metadata("design:returntype", Promise)
], CompanyProducerControllerImpl.prototype, "deactivate", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({ type: update_company_dto_1.UpdateCompanyDTO }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('logo_url', store_file_config_1.StoreFileConfig)),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFile)()),
    __param(3, (0, common_1.Headers)()),
    __param(4, (0, common_1.Query)('new_address')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_company_dto_1.UpdateCompanyDTO, Object, String, String]),
    __metadata("design:returntype", Promise)
], CompanyProducerControllerImpl.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(),
    __param(0, (0, common_1.Headers)()),
    __param(1, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(2, (0, common_1.Query)('with_child')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Array, String]),
    __metadata("design:returntype", Promise)
], CompanyProducerControllerImpl.prototype, "delete", null);
CompanyProducerControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('company'),
    (0, common_1.Controller)('company'),
    __metadata("design:paramtypes", [company_orchestrator_1.CompanyOrchestrator])
], CompanyProducerControllerImpl);
exports.CompanyProducerControllerImpl = CompanyProducerControllerImpl;
//# sourceMappingURL=company-producer.controller.js.map