/// <reference types="multer" />
import { IResponses } from 'src';
import { CreateCompanyDTO } from './dto/create-company.dto';
import { UpdateCompanyDTO } from './dto/update-company.dto';
import { CompanyOrchestrator } from 'src/modules/company/command/domain/usecases/company.orchestrator';
declare const BaseController: any;
export declare class CompanyProducerControllerImpl extends BaseController {
    private companyOrchestrator;
    constructor(companyOrchestrator: CompanyOrchestrator);
    create(body: CreateCompanyDTO, headers: string, file: Express.Multer.File): Promise<IResponses>;
    activate(headers: string, baseIds: string[], withChild: string): Promise<IResponses>;
    deactivate(headers: string, baseIds: string[], withChild: string): Promise<IResponses>;
    update(baseId: string, body: UpdateCompanyDTO, file: Express.Multer.File, headers: string, newAddress: string): Promise<IResponses>;
    delete(headers: string, baseIds: string[], withChild: string): Promise<IResponses>;
}
export {};
