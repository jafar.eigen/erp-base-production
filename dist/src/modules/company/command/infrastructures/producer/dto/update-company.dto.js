"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCompanyDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const src_1 = require("src");
const company_entity_1 = require("src/modules/company/command/domain/entities/company.entity");
const update_company_contacts_dto_1 = require("./update-company-contacts.dto");
class UpdateCompanyDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "request_info", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateCompanyDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateCompanyDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateCompanyDTO.prototype, "deleted_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(5),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enum: Object.values(company_entity_1.CompanyType),
        enumName: 'Company Type'
    }),
    (0, class_validator_1.IsIn)(Object.values(company_entity_1.CompanyType)),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Kota Bandung","value":{"long_name":"Kota Bandung","short_name":"Kota Bandung","types":["administrative_area_level_2","political"]}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Jawa Barat","value":{"long_name":"Jawa Barat","short_name":"Jawa Barat","types":["administrative_area_level_1","political"],"state_code":"Jawa Barat","country_code":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "province", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Indonesia","value":{"long_name":"Indonesia","short_name":"ID","types":["country","political"],"iso2":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "country", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.postal_code),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value !== '' ? parseInt(value) : null;
    }),
    (0, class_validator_1.IsInt)(),
    __metadata("design:type", Number)
], UpdateCompanyDTO.prototype, "postal_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.website_url),
    (0, class_validator_1.IsUrl)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "website_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.longitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "longitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.latitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "latitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.sub_district),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "sub_district", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.helmet),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "hamlet", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.village),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "village", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value === 'null' || value === '' || !value ? null : value;
    }),
    (0, class_validator_1.ValidateIf)((body) => body.parent_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "parent_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', format: 'binary', required: false }),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "logo_url", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "group_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateCompanyDTO.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.parent),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateCompanyDTO.prototype, "parent", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_company_contacts_dto_1.UpdateCompanyContactsDTO],
        required: false,
        default: [
            [
                {
                    id: 'uuid contacts company',
                    email: 'string',
                    phone: '628889',
                    ext: '62889',
                    notes: 'notes'
                }
            ]
        ]
    }),
    (0, class_transformer_1.Transform)((contacts) => JSON.parse(contacts.value)),
    (0, class_transformer_1.Type)(() => update_company_contacts_dto_1.UpdateCompanyContactsDTO),
    (0, class_validator_1.ValidateNested)({ each: true }),
    __metadata("design:type", Array)
], UpdateCompanyDTO.prototype, "contacts", void 0);
exports.UpdateCompanyDTO = UpdateCompanyDTO;
//# sourceMappingURL=update-company.dto.js.map