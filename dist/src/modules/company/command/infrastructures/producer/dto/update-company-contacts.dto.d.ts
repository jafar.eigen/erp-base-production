import { CompanyContactEntity } from 'src/modules/company/command/domain/entities/company-contact.entity';
export declare class UpdateCompanyContactsDTO implements CompanyContactEntity {
    id: string;
    company_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
