"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateValidatorManager = void 0;
const available_data_validator_1 = require("../../validators/available-data.validator");
const child_validator_1 = require("../../validators/child.validator");
const code_validator_1 = require("../../validators/code.validator");
const name_validator_1 = require("../../validators/name.validator");
const check_category_validator_manager_1 = require("./check-category-validator.manager");
class UpdateValidatorManager {
    constructor(locationDataService, locationUpdate, id = null) {
        this.locationDataService = locationDataService;
        this.locationUpdate = locationUpdate;
        this.id = id;
    }
    async execute() {
        await new available_data_validator_1.AvailableData(this.locationDataService, this.id).execute();
        await new child_validator_1.Child(this.locationDataService, this.locationUpdate, this.id).execute();
        await new check_category_validator_manager_1.CategoryValidatorManager(this.locationDataService, this.locationUpdate, this.id).execute();
        await new name_validator_1.Name(this.locationDataService, this.locationUpdate, this.id).execute();
        await new code_validator_1.Code(this.locationDataService, this.locationUpdate, this.id).execute();
    }
}
exports.UpdateValidatorManager = UpdateValidatorManager;
//# sourceMappingURL=update-validator.manager.js.map