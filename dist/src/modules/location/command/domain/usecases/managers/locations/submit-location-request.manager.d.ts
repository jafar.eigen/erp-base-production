import { KafkaPayload, Approver, IUserPayload } from 'src';
import { LocationEntity } from '../../../entities/location.entity';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
export declare class SubmitLocationRequestManager {
    private readonly id;
    private readonly dataService;
    private readonly kafkaService;
    location: LocationEntity;
    approver: Approver;
    user: IUserPayload;
    constructor(id: string, dataService: LocationDataServiceImpl, kafkaService: LocationProducerServiceImpl);
    getParameters(): Promise<void>;
    execute(): Promise<LocationEntity>;
    protected getRecentApproval(): Promise<void>;
    protected processRequest(): Promise<void>;
    protected submitRequest(location: LocationEntity): Promise<LocationEntity>;
    protected skiApproval(location: LocationEntity): Promise<LocationEntity>;
    protected produceApprovalSubmittedTopic(payload: KafkaPayload<IUserPayload, LocationEntity, LocationEntity>): Promise<void>;
    protected produceApprovalPassedTopic(payload: KafkaPayload<IUserPayload, LocationEntity, LocationEntity>): Promise<void>;
}
