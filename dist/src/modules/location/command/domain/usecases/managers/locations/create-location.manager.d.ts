import { ApprovalPerCompany, IUserPayload } from 'src';
import { LocationDataServiceImpl } from '../../../../data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
export declare class CreateLocationManager {
    private locationDataService;
    private locationProducerService;
    private location;
    user: IUserPayload;
    approvals: ApprovalPerCompany[];
    protected locationRes: LocationEntity[];
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl, location: LocationEntity);
    getParameters(): Promise<void>;
    execute(): Promise<LocationEntity[]>;
    protected createValidator(): Promise<void>;
    protected createMappingValidator(): Promise<void>;
    protected mappingData(): Promise<void>;
    protected validatorCategory(): Promise<void>;
    protected createLocationWarehouseConsignment(location: LocationEntity): Promise<void>;
    protected createLocationProductionOperational(location: LocationEntity): Promise<void>;
    protected checkLocationOwnership(location: LocationEntity): Promise<void>;
    protected pushRes(data: LocationEntity): void;
    protected getResult(): LocationEntity[];
    protected produceTopic(locations: LocationEntity[]): Promise<void>;
}
