import { BaseActionManager } from 'src';
import { LocationEntity } from '../../../entities/location.entity';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
export declare class DeclinedLocationRequestManager extends BaseActionManager<LocationEntity> {
    readonly dataService: LocationDataServiceImpl;
    readonly kafkaService: LocationProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: LocationDataServiceImpl, kafkaService: LocationProducerServiceImpl, id: string, step: number);
}
