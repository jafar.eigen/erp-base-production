"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryValidatorManager = void 0;
const category_validator_1 = require("../../validators/category.validator");
const type_validator_1 = require("../../validators/type.validator");
class CategoryValidatorManager {
    constructor(locationDataService, locationUpdate, id = null) {
        this.locationDataService = locationDataService;
        this.locationUpdate = locationUpdate;
        this.id = id;
    }
    async execute() {
        await new category_validator_1.Category(this.locationDataService, this.locationUpdate, this.id).execute();
        await new type_validator_1.Type(this.locationDataService, this.locationUpdate, this.id).execute();
    }
}
exports.CategoryValidatorManager = CategoryValidatorManager;
//# sourceMappingURL=check-category-validator.manager.js.map