"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLocationStockScrap = void 0;
const location_entity_1 = require("../../../entities/location.entity");
const create_location_scrap_manager_1 = require("./create-location-scrap.manager");
const create_location_stock_manager_1 = require("./create-location-stock.manager");
class CreateLocationStockScrap {
    constructor(locationDataService, location) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationRes = [];
    }
    async execute() {
        this.validateCategory();
        await this.generateData();
        return this.getResult();
    }
    validateCategory() {
        if (this.location.category === location_entity_1.LocationCategories.PRODUCTION) {
            Object.assign(this.location, {
                is_production: true,
                is_operational: false
            });
        }
        else {
            Object.assign(this.location, {
                is_production: false,
                is_operational: true
            });
        }
    }
    async generateData() {
        const locationStockRes = await new create_location_stock_manager_1.CreateLocationStock(this.locationDataService, JSON.parse(JSON.stringify(this.location))).execute();
        this.pushRes(locationStockRes);
        const locationScrapRes = await new create_location_scrap_manager_1.CreateLocationScrap(this.locationDataService, JSON.parse(JSON.stringify(this.location))).execute();
        this.pushRes(locationScrapRes);
    }
    pushRes(data) {
        this.locationRes.push(JSON.parse(JSON.stringify(data)));
    }
    getResult() {
        return this.locationRes;
    }
}
exports.CreateLocationStockScrap = CreateLocationStockScrap;
//# sourceMappingURL=create-location-stock-scrap.manager.js.map