import { BaseResultBatch, DeactiveManager } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class DeactivateLocationManager extends DeactiveManager<LocationEntity> {
    locationIds: string[];
    locationDataService: LocationDataServiceImpl;
    locationProducerService: LocationProducerServiceImpl;
    withChild: string;
    result: BaseResultBatch;
    constructor(locationIds: string[], locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl, withChild: string);
    validateStatus(location: LocationEntity): Promise<boolean>;
    onSuccess(locations: LocationEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    protected isApprovalDone(location: LocationEntity): boolean;
    protected isWithChild(): boolean;
    protected updateToChilds(location: LocationEntity): Promise<void>;
    protected removeParent(location: LocationEntity): Promise<void>;
    protected getChilds(location: LocationEntity): Promise<LocationEntity[]>;
    getResult(): BaseResultBatch;
}
