"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateLocationChild = void 0;
const location_entity_1 = require("../../../../entities/location.entity");
class updateLocationChild {
    constructor(location, locationDataService, locationProducerService, user) {
        this.location = location;
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
        this.user = user;
    }
    async execute() {
        await this.getLocationChild();
        await this.mappingData();
    }
    async getLocationChild() {
        if (this.location.category === location_entity_1.LocationCategories.PRODUCTION) {
            const locationChilds = await this.locationDataService.findChildsProduction(this.location.id);
            Object.assign(this, { childs: locationChilds });
        }
        else {
            const locationChilds = await this.locationDataService.findChildsOperational(this.location.id);
            Object.assign(this, { childs: locationChilds });
        }
    }
    async mappingData() {
        var _a;
        await Promise.all((_a = this.childs) === null || _a === void 0 ? void 0 : _a.map(async (child) => {
            if (child.type === location_entity_1.LocationType.STOCK) {
                this.locationStock = Object.assign(Object.assign({}, this.location), { id: child.id, name: `${this.location.name} (STOCK)`, code: `${this.location.code} (ST)`, parent_id: this.location.id, parent_name: this.location.name, parent_code: this.location.code, type: child.type, transaction_type: child.transaction_type, is_production: child.is_production, is_operational: child.is_operational });
                await this.generateAndProduce(this.locationStock, child);
            }
            else {
                this.locationScrap = Object.assign(Object.assign({}, this.location), { id: child.id, name: `${this.location.name} (SCRAP)`, code: `${this.location.code} (SC)`, parent_id: this.location.id, parent_name: this.location.name, parent_code: this.location.code, type: child.type, transaction_type: child.transaction_type, is_production: child.is_production, is_operational: child.is_operational });
                await this.generateAndProduce(this.locationScrap, child);
            }
        }));
    }
    async generateAndProduce(location, oldData) {
        const locationRes = await this.locationDataService.updatePartial(location);
        await this.produceTopic(locationRes, oldData);
    }
    async produceTopic(location, oldData) {
        await this.locationProducerService.baseChanged({
            user: this.user,
            activity: location.request_info,
            mac_address: 'XX-XX-XX-XX-XX-XX',
            data: location,
            old: oldData
        });
    }
}
exports.updateLocationChild = updateLocationChild;
//# sourceMappingURL=update-location-child-approval.manager.js.map