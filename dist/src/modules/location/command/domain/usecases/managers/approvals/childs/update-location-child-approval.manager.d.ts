import { IUserPayload } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { LocationEntity } from '../../../../entities/location.entity';
export declare class updateLocationChild {
    private location;
    private locationDataService;
    private locationProducerService;
    private readonly user;
    constructor(location: LocationEntity, locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl, user: IUserPayload);
    private childs;
    private locationStock;
    private locationScrap;
    execute(): Promise<void>;
    protected getLocationChild(): Promise<void>;
    protected mappingData(): Promise<void>;
    protected generateAndProduce(location: Partial<LocationEntity>, oldData: LocationEntity): Promise<void>;
    protected produceTopic(location: LocationEntity, oldData: LocationEntity): Promise<void>;
}
