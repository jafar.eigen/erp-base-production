import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class CreateMappingValidatorManager {
    private locationDataService;
    private location;
    private locationId;
    constructor(locationDataService: LocationDataServiceImpl, location: LocationEntity, locationId?: string);
    execute(): Promise<void | Error>;
}
