import { LocationDataServiceImpl } from '../../../../data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class AssignApprovalToChilds {
    protected readonly dataService: LocationDataServiceImpl;
    protected readonly location: LocationEntity;
    constructor(dataService: LocationDataServiceImpl, location: LocationEntity);
    run(): Promise<void>;
    protected getChilds(): Promise<LocationEntity[]>;
}
