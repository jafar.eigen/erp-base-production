import { UpdateManager } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class UpdateLocationManager extends UpdateManager<LocationEntity> {
    locationDataService: LocationDataServiceImpl;
    id: string;
    updatedData: LocationEntity;
    entityId: string;
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl, id: string, updatedData: LocationEntity);
    beforeProcess(): Promise<void>;
    afterProcess(entity: LocationEntity): Promise<void>;
    protected updateValidator(): Promise<void>;
    protected updateMappingValidator(): Promise<void>;
    prepareData(): Promise<LocationEntity>;
    protected checkLocationOwnership(location: LocationEntity): Promise<void>;
}
