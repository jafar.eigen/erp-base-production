"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLocationScrap = void 0;
const location_entity_1 = require("../../../entities/location.entity");
class CreateLocationScrap {
    constructor(locationDataService, updatedData) {
        this.locationDataService = locationDataService;
        this.updatedData = updatedData;
    }
    async execute() {
        Object.assign(this.updatedData, {
            name: `${this.updatedData.name} (SCRAP)`,
            code: `${this.updatedData.code} (SC)`,
            type: location_entity_1.LocationType.SCRAP,
            parent_id: this.updatedData.id,
            transaction_type: `[${location_entity_1.TransactionType.INTERNAL_TRANSFER}]`
        });
        delete this.updatedData.id;
        return await this.locationDataService.save(this.updatedData);
    }
}
exports.CreateLocationScrap = CreateLocationScrap;
//# sourceMappingURL=create-location-scrap.manager.js.map