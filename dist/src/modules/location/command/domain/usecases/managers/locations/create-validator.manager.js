"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateValidatorManager = void 0;
const code_validator_1 = require("../../validators/code.validator");
const name_validator_1 = require("../../validators/name.validator");
class CreateValidatorManager {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        await new name_validator_1.Name(this.locationDataService, this.location, this.locationId).execute();
        await new code_validator_1.Code(this.locationDataService, this.location, this.locationId).execute();
    }
}
exports.CreateValidatorManager = CreateValidatorManager;
//# sourceMappingURL=create-validator.manager.js.map