"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteLocationManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../entities/location.entity");
class DeleteLocationManager extends src_1.DeleteManager {
    constructor(locationIds, locationDataService, locationProducerService) {
        super(locationIds, locationDataService, locationProducerService);
        this.locationIds = locationIds;
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(location) {
        if (location.is_operational || location.is_production) {
            return false;
        }
        if (location.is_generated) {
            return false;
        }
        const res = async () => await this.isActiveParent(location);
        return !res;
    }
    async onSuccess(locations) {
        locations.forEach((location) => {
            this.result.success.push(`Location with id ${location.code} successfully deleted`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    async isActiveParent(location) {
        if (location.category === location_entity_1.LocationCategories.OPERATIONAL ||
            location.category === location_entity_1.LocationCategories.PRODUCTION) {
            return false;
        }
        const childLocation = await this.locationDataService.findChilds(location.id);
        return childLocation.length > 0;
    }
    getResult() {
        return this.result;
    }
}
exports.DeleteLocationManager = DeleteLocationManager;
//# sourceMappingURL=delete-location.manager.js.map