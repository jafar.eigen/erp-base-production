import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from '../../../../../../location/command/domain/entities/location.entity';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { CancelManager } from 'src';
export declare class CancelLocationRequestManager extends CancelManager<LocationEntity> {
    readonly LocationId: string;
    readonly dataService: LocationDataServiceImpl;
    readonly kafkaService: LocationProducerServiceImpl;
    constructor(LocationId: string, dataService: LocationDataServiceImpl, kafkaService: LocationProducerServiceImpl);
}
