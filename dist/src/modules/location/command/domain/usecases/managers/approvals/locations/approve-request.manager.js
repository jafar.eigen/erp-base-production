"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveRequestManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../../entities/location.entity");
const update_location_child_approval_manager_1 = require("../childs/update-location-child-approval.manager");
class ApproveRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.APPROVED;
    }
    async afterProcess() {
        if (this.isApprovalDone() && this.isEditData()) {
            if (this.entity.category === location_entity_1.LocationCategories.PRODUCTION ||
                this.entity.category === location_entity_1.LocationCategories.OPERATIONAL) {
                await new update_location_child_approval_manager_1.updateLocationChild(this.entity, this.dataService, this.kafkaService, this.user).execute();
            }
        }
        return this.entity;
    }
    isApprovalDone() {
        return !this.entity.has_requested_process || !this.entity.approval;
    }
    isEditData() {
        return this.entity.request_info === src_1.RequestInfo.EDIT_DATA;
    }
}
exports.ApproveRequestManager = ApproveRequestManager;
//# sourceMappingURL=approve-request.manager.js.map