"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitLocationRequestManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../entities/location.entity");
const update_location_child_approval_manager_1 = require("../approvals/childs/update-location-child-approval.manager");
class SubmitLocationRequestManager {
    constructor(id, dataService, kafkaService) {
        this.id = id;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.getParameters();
    }
    async getParameters() {
        const session = src_1.UserSession.getInstance();
        this.user = session.getUser();
    }
    async execute() {
        await this.getRecentApproval();
        await this.processRequest();
        return this.location;
    }
    async getRecentApproval() {
        const location = await this.dataService.findOne(this.id);
        Object.assign(this, { location });
    }
    async processRequest() {
        const builder = new src_1.SubmitRequest();
        const requestType = this.location.request_info === src_1.RequestInfo.EDIT_DATA
            ? src_1.RequestType.UPDATE
            : src_1.RequestType.CREATE;
        const location = await builder
            .setRequestType(requestType)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setData(this.location)
            .setSubmitRequestFunction(this.submitRequest)
            .setPassApprovalFunction(this.skiApproval)
            .setProduceSubmitRequestTopic(this.produceApprovalSubmittedTopic)
            .setProducePassApprovalTopic(this.produceApprovalPassedTopic)
            .run();
        Object.assign(this, { location });
    }
    async submitRequest(location) {
        return await this.dataService.update(location);
    }
    async skiApproval(location) {
        const res = await this.dataService.update(location);
        if (res.category === location_entity_1.LocationCategories.PRODUCTION ||
            res.category === location_entity_1.LocationCategories.OPERATIONAL) {
            await new update_location_child_approval_manager_1.updateLocationChild(res, this.dataService, this.kafkaService, this.user).execute();
        }
        return res;
    }
    async produceApprovalSubmittedTopic(payload) {
        return this.kafkaService.baseChanged(payload);
    }
    async produceApprovalPassedTopic(payload) {
        return this.kafkaService.baseActivated(payload);
    }
}
exports.SubmitLocationRequestManager = SubmitLocationRequestManager;
//# sourceMappingURL=submit-location-request.manager.js.map