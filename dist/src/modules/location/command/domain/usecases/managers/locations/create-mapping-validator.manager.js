"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMappingValidatorManager = void 0;
const column_name_validator_1 = require("../../validators/column-name.validator");
class CreateMappingValidatorManager {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        if (this.location.maps) {
            await new column_name_validator_1.ColumnName(this.locationDataService, this.location, this.locationId).execute();
        }
    }
}
exports.CreateMappingValidatorManager = CreateMappingValidatorManager;
//# sourceMappingURL=create-mapping-validator.manager.js.map