import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class CategoryValidatorManager {
    private locationDataService;
    private locationUpdate;
    private id;
    constructor(locationDataService: LocationDataServiceImpl, locationUpdate: LocationEntity, id?: string);
    execute(): Promise<void>;
}
