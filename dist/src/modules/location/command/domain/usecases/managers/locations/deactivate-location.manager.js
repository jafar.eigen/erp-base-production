"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeactivateLocationManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../entities/location.entity");
const assign_approval_to_childs_usecase_1 = require("./assign-approval-to-childs.usecase");
class DeactivateLocationManager extends src_1.DeactiveManager {
    constructor(locationIds, locationDataService, locationProducerService, withChild) {
        super(locationIds, locationDataService, locationProducerService);
        this.locationIds = locationIds;
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
        this.withChild = withChild;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(location) {
        if (location.category === location_entity_1.LocationCategories.OPERATIONAL ||
            location.category === location_entity_1.LocationCategories.PRODUCTION) {
            this.withChild = '1';
        }
        if (location.status !== src_1.STATUS.ACTIVE) {
            return false;
        }
        if (location.is_generated) {
            return false;
        }
        if (location.is_operational || location.is_production) {
            return false;
        }
        return true;
    }
    async onSuccess(locations) {
        locations.forEach(async (location) => {
            const parentId = location === null || location === void 0 ? void 0 : location.parent_id;
            if (!this.isApprovalDone(location) && this.isWithChild() && !parentId) {
                await new assign_approval_to_childs_usecase_1.AssignApprovalToChilds(this.locationDataService, location).run();
            }
            if (this.isApprovalDone(location) && this.isWithChild() && !parentId) {
                await this.updateToChilds(location);
            }
            if (this.isApprovalDone(location) && !this.isWithChild() && parentId) {
                await this.removeParent(location);
            }
            this.result.success.push(`Location with id ${location.code} is not active.`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    isApprovalDone(location) {
        const hasApproval = (0, src_1.isNeedApproval)(location.approval);
        return (!hasApproval ||
            (location.status === src_1.STATUS.INACTIVE && !location.has_requested_process));
    }
    isWithChild() {
        return this.withChild === '1';
    }
    async updateToChilds(location) {
        const childs = await this.getChilds(location);
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                var _a;
                const dataOld = Object.assign({}, child);
                Object.assign(child, {
                    request_info: src_1.RequestInfo.EDIT_INACTIVE_WITH_CHILD,
                    status: src_1.STATUS.INACTIVE,
                    editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
                    editor_name: this.user.username,
                    approval: location.approval
                });
                await this.locationDataService.save(child);
                await this.locationProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: dataOld
                });
            }));
        }
    }
    async removeParent(location) {
        const childs = await this.getChilds(location);
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                const oldData = Object.assign({}, child);
                Object.assign(child, {
                    parent_id: null
                });
                await this.locationDataService.save(child);
                await this.locationProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: oldData
                });
            }));
        }
    }
    async getChilds(location) {
        return await this.locationDataService.findChilds(location.id);
    }
    getResult() {
        return this.result;
    }
}
exports.DeactivateLocationManager = DeactivateLocationManager;
//# sourceMappingURL=deactivate-location.manager.js.map