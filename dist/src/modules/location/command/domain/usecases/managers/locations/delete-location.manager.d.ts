import { DeleteManager, BaseResultBatch } from 'src';
import { LocationEntity } from '../../../entities/location.entity';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
export declare class DeleteLocationManager extends DeleteManager<LocationEntity> {
    private locationIds;
    private locationDataService;
    private locationProducerService;
    result: BaseResultBatch;
    constructor(locationIds: string[], locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl);
    validateStatus(location: LocationEntity): Promise<boolean>;
    onSuccess(locations: LocationEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    protected isActiveParent(location: LocationEntity): Promise<boolean>;
    getResult(): BaseResultBatch;
}
