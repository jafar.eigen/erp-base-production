"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateMappingValidatorManager = void 0;
const column_name_validator_1 = require("../../validators/column-name.validator");
class UpdateMappingValidatorManager {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        if (this.location.maps) {
            await new column_name_validator_1.ColumnName(this.locationDataService, this.location, this.locationId).execute();
        }
    }
}
exports.UpdateMappingValidatorManager = UpdateMappingValidatorManager;
//# sourceMappingURL=update-mapping-validator.manager.js.map