import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class CreateLocationScrap {
    private locationDataService;
    private updatedData;
    constructor(locationDataService: LocationDataServiceImpl, updatedData: LocationEntity);
    execute(): Promise<LocationEntity>;
}
