"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLocationManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../entities/location.entity");
const create_validator_manager_1 = require("./create-validator.manager");
const create_location_stock_scrap_manager_1 = require("./create-location-stock-scrap.manager");
const create_mapping_validator_manager_1 = require("./create-mapping-validator.manager");
class CreateLocationManager {
    constructor(locationDataService, locationProducerService, location) {
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
        this.location = location;
        this.locationRes = [];
        this.getParameters();
    }
    async getParameters() {
        const session = src_1.UserSession.getInstance();
        this.user = session.getUser();
        this.approvals = session.getApprovals();
    }
    async execute() {
        await this.createValidator();
        await this.createMappingValidator();
        await this.mappingData();
        await this.validatorCategory();
        return this.getResult();
    }
    async createValidator() {
        await new create_validator_manager_1.CreateValidatorManager(this.locationDataService, this.location).execute();
    }
    async createMappingValidator() {
        await new create_mapping_validator_manager_1.CreateMappingValidatorManager(this.locationDataService, this.location).execute();
    }
    async mappingData() {
        var _a, _b;
        const approval = (0, src_1.findApproval)(this.approvals, null, this.user);
        Object.assign(this.location, {
            creator_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
            creator_name: this.user.username,
            editor_id: (_b = this.user.uuid) !== null && _b !== void 0 ? _b : this.user.id,
            editor_name: this.user.username,
            has_requested_process: false,
            is_generated: false,
            is_head_office: false,
            approval: approval
        });
    }
    async validatorCategory() {
        switch (this.location.category === location_entity_1.LocationCategories.WAREHOUSE ||
            this.location.category === location_entity_1.LocationCategories.CONSIGNMENT) {
            case true:
                await this.createLocationWarehouseConsignment(this.location);
                break;
            default:
                await this.createLocationProductionOperational(this.location);
                break;
        }
    }
    async createLocationWarehouseConsignment(location) {
        await this.checkLocationOwnership(location);
        await Promise.all(location.maps.map(async (locationMap) => {
            Object.assign(locationMap, {
                location_id: location.id,
                location_type: location.type,
                location_code: location.code,
                location_name: location.name
            });
        }));
        const locationRes = await this.locationDataService.save(location);
        this.pushRes(locationRes);
        await this.produceTopic(this.locationRes);
    }
    async createLocationProductionOperational(location) {
        delete location.type;
        Object.assign(location, { is_transaction: true });
        const locationRes = await this.locationDataService.save(location);
        this.pushRes(locationRes);
        const locationStockScrapRes = await new create_location_stock_scrap_manager_1.CreateLocationStockScrap(this.locationDataService, locationRes).execute();
        locationStockScrapRes.map((locationRes) => this.pushRes(locationRes));
        await this.produceTopic(this.locationRes);
    }
    async checkLocationOwnership(location) {
        if (this.location.owner_id) {
            await Promise.all(location.maps.map(async (locationMap) => {
                await Promise.all(locationMap.rows.map(async (raw) => {
                    Object.assign(raw, {
                        contact_id: location.owner_id,
                        contact_code: location.owner_code,
                        contact_name: location.owner_name,
                        location_id: location.id
                    });
                }));
            }));
        }
    }
    pushRes(data) {
        this.locationRes.push(JSON.parse(JSON.stringify(data)));
    }
    getResult() {
        return this.locationRes;
    }
    async produceTopic(locations) {
        await Promise.all(locations.map(async (location) => {
            await this.locationProducerService.baseCreated({
                user: this.user,
                activity: src_1.RequestInfo.CREATE_DATA,
                mac_address: 'XX-XX-XX-XX-XX-XX',
                data: location,
                old: null
            });
        }));
    }
}
exports.CreateLocationManager = CreateLocationManager;
//# sourceMappingURL=create-location.manager.js.map