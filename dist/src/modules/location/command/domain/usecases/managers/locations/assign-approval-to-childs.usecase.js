"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignApprovalToChilds = void 0;
class AssignApprovalToChilds {
    constructor(dataService, location) {
        this.dataService = dataService;
        this.location = location;
    }
    async run() {
        const childs = await this.getChilds();
        const { has_requested_process, approval, request_info } = this.location;
        await Promise.all(childs === null || childs === void 0 ? void 0 : childs.map(async (child) => {
            Object.assign(child, {
                has_requested_process,
                request_info,
                approval
            });
            await this.dataService.save(child);
        }));
    }
    async getChilds() {
        return await this.dataService.findChilds(this.location.id);
    }
}
exports.AssignApprovalToChilds = AssignApprovalToChilds;
//# sourceMappingURL=assign-approval-to-childs.usecase.js.map