"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLocationStock = void 0;
const location_entity_1 = require("../../../entities/location.entity");
class CreateLocationStock {
    constructor(locationDataService, updatedData) {
        this.locationDataService = locationDataService;
        this.updatedData = updatedData;
    }
    async execute() {
        Object.assign(this.updatedData, {
            name: `${this.updatedData.name} (STOCK)`,
            code: `${this.updatedData.code} (ST)`,
            type: location_entity_1.LocationType.STOCK,
            parent_id: this.updatedData.id,
            transaction_type: `[${location_entity_1.TransactionType.INTERNAL_TRANSFER}]`
        });
        delete this.updatedData.id;
        return await this.locationDataService.save(this.updatedData);
    }
}
exports.CreateLocationStock = CreateLocationStock;
//# sourceMappingURL=create-location-stock.manager.js.map