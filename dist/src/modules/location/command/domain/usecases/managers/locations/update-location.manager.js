"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLocationManager = void 0;
const src_1 = require("src");
const update_mapping_validator_manager_1 = require("./update-mapping-validator.manager");
const update_validator_manager_1 = require("./update-validator.manager");
class UpdateLocationManager extends src_1.UpdateManager {
    constructor(locationDataService, locationProducerService, id, updatedData) {
        super(locationDataService, locationProducerService);
        this.locationDataService = locationDataService;
        this.id = id;
        this.updatedData = updatedData;
        this.entityId = id;
    }
    async beforeProcess() {
        await this.updateValidator();
        await this.updateMappingValidator();
    }
    async afterProcess(entity) {
        return;
    }
    async updateValidator() {
        await new update_validator_manager_1.UpdateValidatorManager(this.locationDataService, this.updatedData, this.id).execute();
    }
    async updateMappingValidator() {
        await new update_mapping_validator_manager_1.UpdateMappingValidatorManager(this.locationDataService, this.updatedData, this.id).execute();
    }
    async prepareData() {
        await this.checkLocationOwnership(this.updatedData);
        return this.updatedData;
    }
    async checkLocationOwnership(location) {
        var _a, _b;
        if (((_a = this.updatedData) === null || _a === void 0 ? void 0 : _a.owner_id) && ((_b = this.updatedData) === null || _b === void 0 ? void 0 : _b.maps)) {
            await Promise.all(location.maps.map(async (locationMap) => {
                await Promise.all(locationMap.rows.map(async (raw) => {
                    Object.assign(raw, {
                        contact_id: location.owner_id,
                        contact_code: location.owner_code,
                        contact_name: location.owner_name,
                        location_id: location.id
                    });
                }));
            }));
        }
    }
}
exports.UpdateLocationManager = UpdateLocationManager;
//# sourceMappingURL=update-location.manager.js.map