"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateLocationManager = void 0;
const src_1 = require("src");
const location_entity_1 = require("../../../entities/location.entity");
const assign_approval_to_childs_usecase_1 = require("./assign-approval-to-childs.usecase");
class ActivateLocationManager extends src_1.ActiveManager {
    constructor(ids, locationDataService, locationProducerService, withChild) {
        super(ids, locationDataService, locationProducerService);
        this.ids = ids;
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
        this.withChild = withChild;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(location) {
        if (location.category === location_entity_1.LocationCategories.OPERATIONAL ||
            location.category === location_entity_1.LocationCategories.PRODUCTION) {
            this.withChild = '1';
        }
        if (location.status !== src_1.STATUS.INACTIVE) {
            return false;
        }
        if (location.is_generated) {
            return false;
        }
        if (location.is_operational || location.is_production) {
            return false;
        }
        return true;
    }
    async onSuccess(locations) {
        locations.forEach(async (location) => {
            if (this.isWithChild() && !(location === null || location === void 0 ? void 0 : location.has_requested_process)) {
                await this.updateAndProduceToChilds(location);
            }
            if (this.isWithChild() && location.has_requested_process) {
                await new assign_approval_to_childs_usecase_1.AssignApprovalToChilds(this.locationDataService, location).run();
            }
            this.result.success.push(`Location with id ${location.code} successfully activated`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    isWithChild() {
        return this.withChild === '1';
    }
    async updateAndProduceToChilds(location) {
        const childs = await this.locationDataService.findChilds(location.id);
        if (Array.isArray(childs) && childs.length > 0) {
            await Promise.all(childs.map(async (child) => {
                var _a;
                const oldData = Object.assign({}, child);
                Object.assign(child, {
                    request_info: src_1.RequestInfo.EDIT_ACTIVE_WITH_CHILD,
                    status: src_1.STATUS.ACTIVE,
                    editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
                    editor_name: this.user.username
                });
                await this.locationDataService.save(child);
                await this.locationProducerService.baseChanged({
                    user: this.user,
                    data: child,
                    old: oldData
                });
            }));
        }
    }
    getResult() {
        return this.result;
    }
}
exports.ActivateLocationManager = ActivateLocationManager;
//# sourceMappingURL=activate-location.manager.js.map