import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from '../../../entities/location.entity';
export declare class CreateLocationStockScrap {
    private locationDataService;
    private location;
    constructor(locationDataService: LocationDataServiceImpl, location: LocationEntity);
    protected locationRes: LocationEntity[];
    execute(): Promise<LocationEntity[]>;
    protected validateCategory(): void;
    protected generateData(): Promise<void>;
    protected pushRes(data: LocationEntity): void;
    protected getResult(): LocationEntity[];
}
