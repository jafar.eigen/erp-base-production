import { ActiveManager, BaseResultBatch } from 'src';
import { LocationEntity } from '../../../entities/location.entity';
import { LocationProducerServiceImpl } from 'src/modules/location/command/infrastructures/producers/location-producer.service';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
export declare class ActivateLocationManager extends ActiveManager<LocationEntity> {
    private ids;
    private locationDataService;
    private locationProducerService;
    private withChild;
    result: BaseResultBatch;
    constructor(ids: string[], locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl, withChild: string);
    validateStatus(location: LocationEntity): Promise<boolean>;
    onSuccess(locations: LocationEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    protected isWithChild(): boolean;
    protected updateAndProduceToChilds(location: LocationEntity): Promise<void>;
    getResult(): BaseResultBatch;
}
