"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelLocationRequestManager = void 0;
const src_1 = require("src");
class CancelLocationRequestManager extends src_1.CancelManager {
    constructor(LocationId, dataService, kafkaService) {
        super(LocationId, dataService, kafkaService);
        this.LocationId = LocationId;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
}
exports.CancelLocationRequestManager = CancelLocationRequestManager;
//# sourceMappingURL=cancel-location-request.manager.js.map