"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColumnName = void 0;
const is_company_equal_validator_1 = require("./is-company-equal.validator");
class ColumnName {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        await Promise.all(this.location.maps.map(async (map) => {
            const locationsMapRes = await this.locationDataService.findByColumnName(map.column_name);
            await Promise.all(locationsMapRes.map(async (locationMap) => {
                const locationRes = await this.locationDataService.findById(locationMap.location_id);
                if (locationRes) {
                    if (locationRes &&
                        !this.locationId &&
                        (0, is_company_equal_validator_1.IsCompanyEqual)(locationRes, this.location)) {
                        throw new Error('Column Name already exists.');
                    }
                    if (this.locationId !== (locationRes === null || locationRes === void 0 ? void 0 : locationRes.id) &&
                        (0, is_company_equal_validator_1.IsCompanyEqual)(this.location, locationRes)) {
                        throw new Error('Column Name already exists.');
                    }
                }
            }));
        }));
    }
}
exports.ColumnName = ColumnName;
//# sourceMappingURL=column-name.validator.js.map