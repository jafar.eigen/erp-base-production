import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
export declare class AvailableData {
    private locationDataService;
    private locationId;
    constructor(locationDataService: LocationDataServiceImpl, locationId?: string);
    execute(): Promise<void | Error>;
}
