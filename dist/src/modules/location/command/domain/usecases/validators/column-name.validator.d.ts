import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
import { LocationEntity } from '../../entities/location.entity';
export declare class ColumnName {
    private locationDataService;
    private location;
    private locationId;
    constructor(locationDataService: LocationDataServiceImpl, location: LocationEntity, locationId?: string);
    execute(): Promise<void | Error>;
}
