"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = void 0;
class Type {
    constructor(locationDataService, updatedData, locationId) {
        this.locationDataService = locationDataService;
        this.updatedData = updatedData;
        this.locationId = locationId;
    }
    async execute() {
        this.location = await this.locationDataService.findOne(this.locationId);
        if (this.location.type !== this.updatedData.type) {
            throw new Error('location type cannot be changed!');
        }
    }
}
exports.Type = Type;
//# sourceMappingURL=type.validator.js.map