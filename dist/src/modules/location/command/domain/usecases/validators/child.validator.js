"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Child = void 0;
class Child {
    constructor(locationDataService, updatedData, id = null) {
        this.locationDataService = locationDataService;
        this.updatedData = updatedData;
        this.id = id;
    }
    async execute() {
        const locationRes = await this.locationDataService.findById(this.id);
        if ((locationRes === null || locationRes === void 0 ? void 0 : locationRes.parent_id) &&
            (locationRes === null || locationRes === void 0 ? void 0 : locationRes.is_production) !== null &&
            (locationRes === null || locationRes === void 0 ? void 0 : locationRes.is_operational) !== null) {
            const { company_id, site_id, code, name, parent_id, owner_id, is_transaction, transaction_type } = this.updatedData;
            if (locationRes.company_id !== company_id ||
                locationRes.site_id !== site_id ||
                locationRes.code !== code ||
                locationRes.name !== name ||
                locationRes.parent_id !== parent_id ||
                locationRes.owner_id !== owner_id ||
                locationRes.is_transaction !== is_transaction ||
                locationRes.company_id !== company_id ||
                locationRes.transaction_type !== transaction_type) {
                throw new Error('Location Data can only be changed ext, level and map.');
            }
        }
        return;
    }
}
exports.Child = Child;
//# sourceMappingURL=child.validator.js.map