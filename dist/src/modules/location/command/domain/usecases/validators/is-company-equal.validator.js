"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsCompanyEqual = void 0;
const IsCompanyEqual = (locationRes, locationToCheck) => {
    const companyIdRes = locationRes === null || locationRes === void 0 ? void 0 : locationRes.company_id;
    const companyIdToCheck = locationToCheck === null || locationToCheck === void 0 ? void 0 : locationToCheck.company_id;
    return companyIdToCheck === companyIdRes;
};
exports.IsCompanyEqual = IsCompanyEqual;
//# sourceMappingURL=is-company-equal.validator.js.map