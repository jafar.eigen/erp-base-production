import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
import { LocationEntity } from '../../entities/location.entity';
export declare class Code {
    private locationDataService;
    private location;
    private locationId;
    constructor(locationDataService: LocationDataServiceImpl, location: LocationEntity, locationId?: string);
    protected transactionData: LocationEntity[];
    execute(): Promise<void | Error>;
    protected getData(): Promise<void>;
    protected validate(): Promise<void>;
}
