"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
const is_company_equal_validator_1 = require("./is-company-equal.validator");
class Code {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        await this.getData();
        await this.validate();
    }
    async getData() {
        this.transactionData = [
            await this.locationDataService.findByCode(this.location.code)
        ];
    }
    async validate() {
        var _a;
        if (this.transactionData) {
            await Promise.all((_a = this.transactionData) === null || _a === void 0 ? void 0 : _a.map(async (data) => {
                if (data && !this.locationId && (0, is_company_equal_validator_1.IsCompanyEqual)(data, this.location)) {
                    throw new Error('Code already exists.');
                }
                if (this.locationId !== (data === null || data === void 0 ? void 0 : data.id) &&
                    (0, is_company_equal_validator_1.IsCompanyEqual)(this.location, data)) {
                    throw new Error('Code already exists.');
                }
            }));
        }
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map