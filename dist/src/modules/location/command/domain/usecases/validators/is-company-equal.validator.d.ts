import { LocationEntity } from '../../entities/location.entity';
export declare const IsCompanyEqual: (locationRes: LocationEntity, locationToCheck: LocationEntity) => boolean;
