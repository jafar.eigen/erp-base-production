import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
import { LocationEntity } from '../../entities/location.entity';
export declare class Child {
    private locationDataService;
    private updatedData;
    private id;
    constructor(locationDataService: LocationDataServiceImpl, updatedData: LocationEntity, id?: string);
    execute(): Promise<void | Error>;
}
