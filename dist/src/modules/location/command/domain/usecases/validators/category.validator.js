"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Category = void 0;
class Category {
    constructor(locationDataService, updatedData, id) {
        this.locationDataService = locationDataService;
        this.updatedData = updatedData;
        this.id = id;
    }
    async execute() {
        this.location = await this.locationDataService.findOne(this.id);
        if (this.location.category !== this.updatedData.category) {
            throw new Error('Location category cannot be changed!');
        }
    }
}
exports.Category = Category;
//# sourceMappingURL=category.validator.js.map