"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvailableData = void 0;
class AvailableData {
    constructor(locationDataService, locationId = null) {
        this.locationDataService = locationDataService;
        this.locationId = locationId;
    }
    async execute() {
        const locationRes = await this.locationDataService.findById(this.locationId);
        if (!locationRes) {
            throw new Error(`Location ${this.locationId} not found.`);
        }
    }
}
exports.AvailableData = AvailableData;
//# sourceMappingURL=available-data.validator.js.map