import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
import { LocationEntity } from '../../entities/location.entity';
export declare class Category {
    private locationDataService;
    private updatedData;
    private id;
    constructor(locationDataService: LocationDataServiceImpl, updatedData: LocationEntity, id: string);
    private location;
    execute(): Promise<void | Error>;
}
