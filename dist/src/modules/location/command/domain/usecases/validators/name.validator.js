"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
const is_company_equal_validator_1 = require("./is-company-equal.validator");
class Name {
    constructor(locationDataService, location, locationId = null) {
        this.locationDataService = locationDataService;
        this.location = location;
        this.locationId = locationId;
    }
    async execute() {
        const locationsRes = await this.locationDataService.findByName(this.location.name);
        if (locationsRes) {
            await Promise.all(locationsRes === null || locationsRes === void 0 ? void 0 : locationsRes.map(async (locationRes) => {
                if (locationRes &&
                    !this.locationId &&
                    (0, is_company_equal_validator_1.IsCompanyEqual)(locationRes, this.location)) {
                    throw new Error('Name already exists.');
                }
                if (this.locationId !== (locationRes === null || locationRes === void 0 ? void 0 : locationRes.id) &&
                    (0, is_company_equal_validator_1.IsCompanyEqual)(this.location, locationRes)) {
                    throw new Error('Name already exists.');
                }
            }));
        }
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map