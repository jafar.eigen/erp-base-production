import { LocationDataServiceImpl } from '../../../data/services/location-data.service';
import { LocationEntity } from '../../entities/location.entity';
export declare class Type {
    private locationDataService;
    private updatedData;
    private locationId;
    constructor(locationDataService: LocationDataServiceImpl, updatedData: LocationEntity, locationId: string);
    private location;
    execute(): Promise<void | Error>;
}
