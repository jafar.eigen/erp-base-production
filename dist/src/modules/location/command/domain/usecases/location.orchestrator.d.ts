import { LocationDataServiceImpl } from '../../data/services/location-data.service';
import { LocationProducerServiceImpl } from '../../infrastructures/producers/location-producer.service';
import { LocationResultBatch } from '../entities/location-result-batch.entity';
import { LocationEntity } from '../entities/location.entity';
export declare class LocationOrchestrator {
    private locationDataService;
    private locationProducerService;
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl);
    create(data: LocationEntity): Promise<LocationEntity[]>;
    activate(ids: string[], withChild: string): Promise<LocationResultBatch>;
    deactivate(ids: string[], withChild: string): Promise<LocationResultBatch>;
    update(id: string, updatedData: LocationEntity): Promise<LocationEntity>;
    approve(id: string, step: number): Promise<LocationEntity>;
    delete(ids: string[]): Promise<LocationResultBatch>;
    decline(locationId: string, step: number): Promise<LocationEntity>;
    request(id: string): Promise<LocationEntity>;
    rollbackStatusTrx(entityId: string[]): Promise<LocationEntity>;
    cancel(locationId: string): Promise<LocationEntity>;
}
