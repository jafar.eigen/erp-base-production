"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const location_data_service_1 = require("../../data/services/location-data.service");
const location_producer_service_1 = require("../../infrastructures/producers/location-producer.service");
const activate_location_manager_1 = require("./managers/locations/activate-location.manager");
const create_location_manager_1 = require("./managers/locations/create-location.manager");
const delete_location_manager_1 = require("./managers/locations/delete-location.manager");
const submit_location_request_manager_1 = require("./managers/locations/submit-location-request.manager");
const update_location_manager_1 = require("./managers/locations/update-location.manager");
const deactivate_location_manager_1 = require("./managers/locations/deactivate-location.manager");
const approve_request_manager_1 = require("./managers/approvals/locations/approve-request.manager");
const decline_location_request_manager_1 = require("./managers/locations/decline-location-request.manager");
const cancel_location_request_manager_1 = require("./managers/locations/cancel-location-request.manager");
let LocationOrchestrator = class LocationOrchestrator {
    constructor(locationDataService, locationProducerService) {
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
    }
    async create(data) {
        return await new create_location_manager_1.CreateLocationManager(this.locationDataService, this.locationProducerService, data).execute();
    }
    async activate(ids, withChild) {
        const activateLocation = new activate_location_manager_1.ActivateLocationManager(ids, this.locationDataService, this.locationProducerService, withChild);
        await activateLocation.execute();
        return activateLocation.getResult();
    }
    async deactivate(ids, withChild) {
        const deactivateLocation = new deactivate_location_manager_1.DeactivateLocationManager(ids, this.locationDataService, this.locationProducerService, withChild);
        await deactivateLocation.execute();
        return deactivateLocation.getResult();
    }
    async update(id, updatedData) {
        return await new update_location_manager_1.UpdateLocationManager(this.locationDataService, this.locationProducerService, id, updatedData).execute();
    }
    async approve(id, step) {
        return await new approve_request_manager_1.ApproveRequestManager(this.locationDataService, this.locationProducerService, id, step).execute();
    }
    async delete(ids) {
        const deleteManager = new delete_location_manager_1.DeleteLocationManager(ids, this.locationDataService, this.locationProducerService);
        await deleteManager.execute();
        return deleteManager.getResult();
    }
    async decline(locationId, step) {
        return await new decline_location_request_manager_1.DeclinedLocationRequestManager(this.locationDataService, this.locationProducerService, locationId, step).execute();
    }
    async request(id) {
        return await new submit_location_request_manager_1.SubmitLocationRequestManager(id, this.locationDataService, this.locationProducerService).execute();
    }
    async rollbackStatusTrx(entityId) {
        throw new Error('This feature is not available!');
    }
    async cancel(locationId) {
        return await new cancel_location_request_manager_1.CancelLocationRequestManager(locationId, this.locationDataService, this.locationProducerService).execute();
    }
};
LocationOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_data_service_1.LocationDataServiceImpl,
        location_producer_service_1.LocationProducerServiceImpl])
], LocationOrchestrator);
exports.LocationOrchestrator = LocationOrchestrator;
//# sourceMappingURL=location.orchestrator.js.map