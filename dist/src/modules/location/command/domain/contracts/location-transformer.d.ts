import { LocationEntity } from '../entities/location.entity';
export interface ILocationConsumerTransformer {
    transform(): Partial<LocationEntity>;
}
