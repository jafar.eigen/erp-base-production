"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionalType = exports.LocationType = exports.LocationCategories = void 0;
var LocationCategories;
(function (LocationCategories) {
    LocationCategories["WAREHOUSE"] = "Warehouse";
    LocationCategories["CONSIGNMENT"] = "Consignment";
    LocationCategories["PRODUCTION"] = "Production";
    LocationCategories["OPERATIONAL"] = "Operational";
})(LocationCategories = exports.LocationCategories || (exports.LocationCategories = {}));
var LocationType;
(function (LocationType) {
    LocationType["STOCK"] = "Stock";
    LocationType["SCRAP"] = "Scrap";
    LocationType["QUARANTINE"] = "Quarantine";
})(LocationType = exports.LocationType || (exports.LocationType = {}));
var TransactionalType;
(function (TransactionalType) {
    TransactionalType["EXTERNAL_DELIVERY"] = "External Delivery";
    TransactionalType["EXTERNAL_RECEIPTS"] = "External Receipts";
    TransactionalType["INTERNAL_TRANSFER"] = "Internal Transfer";
})(TransactionalType = exports.TransactionalType || (exports.TransactionalType = {}));
//# sourceMappingURL=group-location.entity.js.map