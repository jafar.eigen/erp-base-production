import { BaseEntity } from 'src';
import { LocationMapEntity } from './location-map.entity';
export declare enum LocationCategories {
    WAREHOUSE = "Warehouse",
    CONSIGNMENT = "Consignment",
    PRODUCTION = "Production",
    OPERATIONAL = "Operational"
}
export declare enum LocationType {
    STOCK = "Stock",
    SCRAP = "Scrap",
    QUARANTINE = "Quarantine"
}
export declare enum TransactionType {
    EXTERNAL_RECEIPTS = "External Receipts",
    EXTERNAL_DELIVER = "External Deliver",
    INTERNAL_TRANSFER = "Internal Transfer"
}
export interface LocationEntity extends BaseEntity {
    company_id: string;
    company_name: string;
    company_code: string;
    site_id: string;
    site_name: string;
    site_code: string;
    level_id: string;
    level_number: number;
    level_name: string;
    name: string;
    parent_id: string;
    parent_name: string;
    parent_code: string;
    owner_id: string;
    owner_name: string;
    owner_code: string;
    ext: string;
    category: LocationCategories;
    type: LocationType;
    is_transaction: boolean;
    transaction_type: string;
    group_id: string;
    creator_name: string;
    editor_name?: string;
    deleted_by_name?: string;
    is_generated?: boolean;
    is_head_office?: boolean;
    is_production?: boolean;
    is_operational?: boolean;
    childs?: LocationEntity[];
    parent: LocationEntity;
    maps: LocationMapEntity[];
}
