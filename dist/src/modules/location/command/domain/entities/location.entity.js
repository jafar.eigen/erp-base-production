"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionType = exports.LocationType = exports.LocationCategories = void 0;
var LocationCategories;
(function (LocationCategories) {
    LocationCategories["WAREHOUSE"] = "Warehouse";
    LocationCategories["CONSIGNMENT"] = "Consignment";
    LocationCategories["PRODUCTION"] = "Production";
    LocationCategories["OPERATIONAL"] = "Operational";
})(LocationCategories = exports.LocationCategories || (exports.LocationCategories = {}));
var LocationType;
(function (LocationType) {
    LocationType["STOCK"] = "Stock";
    LocationType["SCRAP"] = "Scrap";
    LocationType["QUARANTINE"] = "Quarantine";
})(LocationType = exports.LocationType || (exports.LocationType = {}));
var TransactionType;
(function (TransactionType) {
    TransactionType["EXTERNAL_RECEIPTS"] = "External Receipts";
    TransactionType["EXTERNAL_DELIVER"] = "External Deliver";
    TransactionType["INTERNAL_TRANSFER"] = "Internal Transfer";
})(TransactionType = exports.TransactionType || (exports.TransactionType = {}));
//# sourceMappingURL=location.entity.js.map