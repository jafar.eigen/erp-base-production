import { LocationType } from './location.entity';
export interface LocationMapRowEntity {
    id: string;
    location_id: string;
    type: LocationType;
    map_id: string;
    contact_id: string;
    contact_code: string;
    contact_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
