export interface LocationResultBatch {
    success: string[];
    failed: string[];
}
