import { LocationMapRowEntity } from './location-map-raw.entity';
import { LocationType } from './location.entity';
export interface LocationMapEntity {
    id: string;
    column_name: string;
    location_id: string;
    location_code: string;
    location_name: string;
    location_type: LocationType;
    rows: LocationMapRowEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
