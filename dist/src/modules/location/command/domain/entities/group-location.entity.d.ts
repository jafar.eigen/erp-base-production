import { Approval } from 'src';
import { LocationEntity } from './location.entity';
export declare enum LocationCategories {
    WAREHOUSE = "Warehouse",
    CONSIGNMENT = "Consignment",
    PRODUCTION = "Production",
    OPERATIONAL = "Operational"
}
export declare enum LocationType {
    STOCK = "Stock",
    SCRAP = "Scrap",
    QUARANTINE = "Quarantine"
}
export declare enum TransactionalType {
    EXTERNAL_DELIVERY = "External Delivery",
    EXTERNAL_RECEIPTS = "External Receipts",
    INTERNAL_TRANSFER = "Internal Transfer"
}
export interface GroupLocationEntity {
    id: string;
    gid: string;
    group_map_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    site_id: string;
    site_name: string;
    site_code: string;
    code: string;
    name: string;
    parent_id: string;
    owner_id: string;
    owner_name: string;
    owner_code: string;
    ext: any;
    category: LocationCategories;
    type: LocationType;
    is_transaction: boolean;
    transaction_type: TransactionalType;
    group_id: string;
    status: string;
    request_info: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: Approval;
    requested_data?: any;
    has_requested_process?: boolean;
    location?: LocationEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
