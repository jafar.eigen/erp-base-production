"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Location_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Location = void 0;
const src_1 = require("src");
const typeorm_1 = require("typeorm");
const location_entity_1 = require("../../domain/entities/location.entity");
const group_location_model_1 = require("./group-location.model");
const location_map_model_1 = require("./location-map.model");
let Location = Location_1 = class Location {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Location.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'company_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'branch_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "branch_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "branch_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'site_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "site_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_code', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "site_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'level_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "level_id", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'level_number', nullable: true }),
    __metadata("design:type", Number)
], Location.prototype, "level_number", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'level_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "level_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '15', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'parent_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "parent_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'parent_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "parent_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'parent_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "parent_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'owner_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "owner_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'owner_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "owner_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'owner_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "owner_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'ext', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "ext", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: location_entity_1.LocationCategories }),
    __metadata("design:type", String)
], Location.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: location_entity_1.LocationType, nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_transaction', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "is_transaction", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'transaction_type', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "transaction_type", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.STATUS }),
    __metadata("design:type", String)
], Location.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], Location.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], Location.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_generated', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "is_generated", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_head_office', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "is_head_office", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_production', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "is_production", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_operational', nullable: true }),
    __metadata("design:type", Boolean)
], Location.prototype, "is_operational", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], Location.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], Location.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Location.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Location.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Location.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Location_1, (location) => location.parent, {
        cascade: ['update', 'soft-remove']
    }),
    __metadata("design:type", Array)
], Location.prototype, "childs", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Location_1, (location) => location.childs, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'parent_id' }),
    __metadata("design:type", Location)
], Location.prototype, "parent", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => location_map_model_1.LocationMap, (locationMap) => locationMap.location, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], Location.prototype, "maps", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_location_model_1.GroupLocation, (group) => group.locations),
    (0, typeorm_1.JoinColumn)({ name: 'group_id' }),
    __metadata("design:type", group_location_model_1.GroupLocation)
], Location.prototype, "group", void 0);
Location = Location_1 = __decorate([
    (0, typeorm_1.Entity)('locations')
], Location);
exports.Location = Location;
//# sourceMappingURL=location.model.js.map