import { LocationMapRowEntity } from '../../domain/entities/location-map-raw.entity';
import { LocationType } from '../../domain/entities/location.entity';
import { LocationMap } from './location-map.model';
import { Location } from './location.model';
export declare class LocationMapRow implements LocationMapRowEntity {
    id: string;
    type: LocationType;
    map_id: string;
    contact_id: string;
    contact_code: string;
    contact_name: string;
    location_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    map: LocationMap;
    location: Location;
}
