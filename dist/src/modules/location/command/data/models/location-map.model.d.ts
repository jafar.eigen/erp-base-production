import { LocationMapEntity } from '../../domain/entities/location-map.entity';
import { LocationType } from '../../domain/entities/location.entity';
import { LocationMapRow } from './location-map-raw.model';
import { Location } from './location.model';
export declare class LocationMap implements LocationMapEntity {
    id: string;
    column_name: string;
    location_id: string;
    location_type: LocationType;
    location_code: string;
    location_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    location: Location;
    rows: LocationMapRow[];
}
