"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationMap = void 0;
const typeorm_1 = require("typeorm");
const location_entity_1 = require("../../domain/entities/location.entity");
const location_map_raw_model_1 = require("./location-map-raw.model");
const location_model_1 = require("./location.model");
let LocationMap = class LocationMap {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], LocationMap.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'column_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], LocationMap.prototype, "column_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'location_id', nullable: true }),
    __metadata("design:type", String)
], LocationMap.prototype, "location_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: location_entity_1.LocationType }),
    __metadata("design:type", String)
], LocationMap.prototype, "location_type", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'location_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], LocationMap.prototype, "location_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'location_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], LocationMap.prototype, "location_name", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMap.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMap.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMap.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => location_model_1.Location, (location) => location.maps, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'location_id' }),
    __metadata("design:type", location_model_1.Location)
], LocationMap.prototype, "location", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => location_map_raw_model_1.LocationMapRow, (locationMapRaw) => locationMapRaw.map, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], LocationMap.prototype, "rows", void 0);
LocationMap = __decorate([
    (0, typeorm_1.Entity)('location_maps')
], LocationMap);
exports.LocationMap = LocationMap;
//# sourceMappingURL=location-map.model.js.map