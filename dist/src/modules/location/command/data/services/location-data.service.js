"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const src_1 = require("src");
const database_config_1 = require("src/utils/database.config");
const location_map_model_1 = require("../models/location-map.model");
const location_model_1 = require("../models/location.model");
let LocationDataServiceImpl = class LocationDataServiceImpl extends src_1.BaseDataService {
    constructor(locationRepo, locationMapRepo) {
        super(locationRepo);
        this.locationRepo = locationRepo;
        this.locationMapRepo = locationMapRepo;
        this.relations = ['maps', 'maps.rows'];
    }
    async findByColumnName(column_name) {
        return await this.locationMapRepo.find({
            where: { column_name }
        });
    }
    async findLikeName(name, id) {
        return this.locationRepo.find({
            where: [{ name: (0, typeorm_2.ILike)(`%${name}%`), company_id: (0, typeorm_2.Equal)(id) }]
        });
    }
    async findByIds(ids) {
        return await this.locationRepo.findByIds(ids);
    }
    async findChildsProduction(parentId) {
        return await this.locationRepo.find({
            where: {
                parent_id: parentId,
                is_production: true
            }
        });
    }
    async findChildsOperational(parentId) {
        return await this.locationRepo.find({
            where: {
                parent_id: parentId,
                is_operational: true
            }
        });
    }
    async findBySitesId(siteId) {
        return await this.locationRepo.find({ where: { site_id: siteId } });
    }
    async activateLocation(location, withChild) {
        if (withChild) {
            let childLocation = await this.findChilds(location.id);
            childLocation = childLocation.map((location) => {
                return Object.assign(Object.assign({}, location), { status: src_1.STATUS.ACTIVE });
            });
            await this.locationRepo.save(childLocation);
        }
        return await this.locationRepo.save(location);
    }
    async deactivateLocation(location, withChild) {
        if (withChild) {
            let childLocation = await this.findChilds(location.id);
            childLocation = childLocation.map((location) => {
                return Object.assign(Object.assign({}, location), { status: src_1.STATUS.INACTIVE });
            });
            await this.locationRepo.save(childLocation);
        }
        return await this.locationRepo.save(location);
    }
    async updatePartial(updateData) {
        return await this.locationRepo.save(updateData);
    }
    async deleteBySiteId(id) {
        return await this.locationRepo.delete({ site_id: id });
    }
};
LocationDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(location_model_1.Location, database_config_1.LOCATION_CUD_CONNECTION)),
    __param(1, (0, typeorm_1.InjectRepository)(location_map_model_1.LocationMap, database_config_1.LOCATION_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], LocationDataServiceImpl);
exports.LocationDataServiceImpl = LocationDataServiceImpl;
//# sourceMappingURL=location-data.service.js.map