import { Repository, DeleteResult } from 'typeorm';
import { BaseDataService } from 'src';
import { LocationMapEntity } from '../../domain/entities/location-map.entity';
import { LocationEntity } from '../../domain/entities/location.entity';
import { LocationMap } from '../models/location-map.model';
import { Location } from '../models/location.model';
export declare class LocationDataServiceImpl extends BaseDataService<LocationEntity> {
    private locationRepo;
    private locationMapRepo;
    relations: string[];
    constructor(locationRepo: Repository<Location>, locationMapRepo: Repository<LocationMap>);
    findByColumnName(column_name: string): Promise<LocationMapEntity[]>;
    findLikeName(name: string, id: string): Promise<LocationEntity[]>;
    findByIds(ids: string[]): Promise<LocationEntity[]>;
    findChildsProduction(parentId: string): Promise<LocationEntity[]>;
    findChildsOperational(parentId: string): Promise<LocationEntity[]>;
    findBySitesId(siteId: string): Promise<LocationEntity[]>;
    activateLocation(location: LocationEntity, withChild: string): Promise<LocationEntity>;
    deactivateLocation(location: LocationEntity, withChild: boolean): Promise<LocationEntity>;
    updatePartial(updateData: Partial<LocationEntity>): Promise<LocationEntity>;
    deleteBySiteId(id: string): Promise<DeleteResult>;
}
