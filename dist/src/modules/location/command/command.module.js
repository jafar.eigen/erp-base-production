"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const microservice_config_1 = require("../../../utils/microservice.config");
const database_config_1 = require("src/utils/database.config");
const location_model_1 = require("./data/models/location.model");
const group_location_model_1 = require("./data/models/group-location.model");
const location_map_model_1 = require("./data/models/location-map.model");
const location_producer_controller_1 = require("./infrastructures/producers/location-producer.controller");
const location_orchestrator_1 = require("./domain/usecases/location.orchestrator");
const location_data_service_1 = require("./data/services/location-data.service");
const location_producer_service_1 = require("./infrastructures/producers/location-producer.service");
const location_map_raw_model_1 = require("./data/models/location-map-raw.model");
const check_existence_site_on_location_controller_1 = require("./infrastructures/consumers/site/infrastructure/check-existence-site-on-location.controller");
const site_activated_controller_1 = require("./infrastructures/consumers/site/infrastructure/site-activated.controller");
const site_changed_controller_1 = require("./infrastructures/consumers/site/infrastructure/site-changed.controller");
const check_existence_site_on_location_orchestrator_1 = require("./infrastructures/consumers/site/domain/check-existence-site-on-location.orchestrator");
const site_activated_manager_1 = require("./infrastructures/consumers/site/domain/site-activated.manager");
const site_changed_manager_1 = require("./infrastructures/consumers/site/domain/site-changed.manager");
const microservices_1 = require("@nestjs/microservices");
const site_deleted_controller_1 = require("./infrastructures/consumers/site/infrastructure/site-deleted.controller");
const site_deleted_manager_1 = require("./infrastructures/consumers/site/domain/site-deleted.manager");
let CommandModule = class CommandModule {
};
CommandModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default, microservice_config_1.default],
                isGlobal: true
            }),
            microservices_1.ClientsModule.registerAsync([
                {
                    name: microservice_config_1.KAFKA_CLIENT_NAME,
                    imports: [config_1.ConfigModule],
                    useFactory: (configService) => configService.get('kafkaClientConfig'),
                    inject: [config_1.ConfigService]
                }
            ]),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.LOCATION_CUD_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormLocationCommand'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([location_model_1.Location, group_location_model_1.GroupLocation, location_map_model_1.LocationMap, location_map_raw_model_1.LocationMapRow], database_config_1.LOCATION_CUD_CONNECTION)
        ],
        providers: [
            src_1.Responses,
            src_1.JSONParse,
            location_orchestrator_1.LocationOrchestrator,
            location_data_service_1.LocationDataServiceImpl,
            location_producer_service_1.LocationProducerServiceImpl,
            check_existence_site_on_location_orchestrator_1.CheckExistenceSiteOnLocationOrchestrator,
            site_activated_manager_1.SiteActivatedManager,
            site_changed_manager_1.SiteChangedManager,
            site_deleted_manager_1.SiteDeletedManager
        ],
        controllers: [
            location_producer_controller_1.LocationControllerImpl,
            check_existence_site_on_location_controller_1.CheckExistenceSiteOnLocationController,
            site_activated_controller_1.SiteActivatedController,
            site_changed_controller_1.SiteChangedController,
            site_deleted_controller_1.SiteDeletedController
        ]
    })
], CommandModule);
exports.CommandModule = CommandModule;
//# sourceMappingURL=command.module.js.map