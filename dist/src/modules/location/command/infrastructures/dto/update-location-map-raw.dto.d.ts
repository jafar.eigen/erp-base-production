import { LocationMapRowEntity } from '../../domain/entities/location-map-raw.entity';
import { LocationType } from '../../domain/entities/location.entity';
export declare class UpdateLocationMapRowDTO implements LocationMapRowEntity {
    id: string;
    type: LocationType;
    map_id: string;
    contact_id: string;
    contact_code: string;
    contact_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    location_id: string;
}
