import { LocationMapEntity } from '../../domain/entities/location-map.entity';
import { LocationType } from '../../domain/entities/location.entity';
import { UpdateLocationMapRowDTO } from './update-location-map-raw.dto';
export declare class UpdateLocationMapDTO implements LocationMapEntity {
    id: string;
    column_name: string;
    location_id: string;
    location_type: LocationType;
    location_code: string;
    location_name: string;
    rows: UpdateLocationMapRowDTO[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
