"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLocationMapRowDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const location_entity_1 = require("../../domain/entities/location.entity");
class UpdateLocationMapRowDTO {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enum: Object.values(location_entity_1.LocationType),
        enumName: 'Location Types',
        required: false,
        nullable: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(location_entity_1.LocationType)),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "map_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsUUID)(),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "contact_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "contact_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "contact_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapRowDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapRowDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapRowDTO.prototype, "deleted_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapRowDTO.prototype, "location_id", void 0);
exports.UpdateLocationMapRowDTO = UpdateLocationMapRowDTO;
//# sourceMappingURL=update-location-map-raw.dto.js.map