"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLocationMapDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const location_entity_1 = require("../../domain/entities/location.entity");
const update_location_map_raw_dto_1 = require("./update-location-map-raw.dto");
class UpdateLocationMapDTO {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "column_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "location_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_type),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "location_type", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_code),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "location_code", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLocationMapDTO.prototype, "location_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_location_map_raw_dto_1.UpdateLocationMapRowDTO],
        default: update_location_map_raw_dto_1.UpdateLocationMapRowDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_location_map_raw_dto_1.UpdateLocationMapRowDTO),
    __metadata("design:type", Array)
], UpdateLocationMapDTO.prototype, "rows", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLocationMapDTO.prototype, "deleted_at", void 0);
exports.UpdateLocationMapDTO = UpdateLocationMapDTO;
//# sourceMappingURL=update-location-map.dto.js.map