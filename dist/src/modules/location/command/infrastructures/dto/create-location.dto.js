"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLocationDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const src_1 = require("src");
const location_entity_1 = require("../../domain/entities/location.entity");
const location_map_dto_1 = require("./location-map.dto");
class CreateLocationDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "company_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "company_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "company_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "site_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "site_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "site_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "level_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsInt)(),
    (0, class_transformer_1.Transform)(({ value }) => parseInt(value)),
    __metadata("design:type", Number)
], CreateLocationDTO.prototype, "level_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "level_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: true, nullable: false }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value !== '' ? value : null;
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "parent_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: false }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    (0, class_validator_1.ValidateIf)((body) => body.parent_id === null),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "parent_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: false }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.parent_id === null),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "parent_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value !== '' ? value : null;
    }),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "owner_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "owner_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "owner_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        example: '001',
        required: false,
        nullable: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "ext", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enum: Object.values(location_entity_1.LocationCategories),
        enumName: 'Location Categories',
        required: true,
        nullable: false
    }),
    (0, class_validator_1.IsIn)(Object.values(location_entity_1.LocationCategories)),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enum: Object.values(location_entity_1.LocationType),
        enumName: 'Location Types',
        required: false,
        nullable: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(location_entity_1.LocationType)),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'boolean', required: true, nullable: false }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "is_transaction", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        example: "['External Receipts','External Deliver','Internal Transfer']",
        required: false,
        nullable: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.ValidateIf)((body) => body.is_transaction === false),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "transaction_type", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "group_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "request_info", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLocationDTO.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateLocationDTO.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateLocationDTO.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.is_generated),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "is_generated", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.is_head_office),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "is_head_office", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.is_production),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "is_production", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.is_operational),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLocationDTO.prototype, "is_operational", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.parent),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateLocationDTO.prototype, "parent", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [location_map_dto_1.LocationMapDTO],
        default: location_map_dto_1.LocationMapDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => location_map_dto_1.LocationMapDTO),
    (0, class_validator_1.ValidateIf)((body) => body.category == location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Array)
], CreateLocationDTO.prototype, "maps", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLocationDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLocationDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLocationDTO.prototype, "deleted_at", void 0);
exports.CreateLocationDTO = CreateLocationDTO;
//# sourceMappingURL=create-location.dto.js.map