"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationMapDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const location_entity_1 = require("../../domain/entities/location.entity");
const location_map_raw_dto_1 = require("./location-map-raw.dto");
class LocationMapDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    (0, class_validator_1.ValidateIf)((body) => body.category === location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "column_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "location_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_type),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "location_type", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_code),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "location_code", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.location_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], LocationMapDTO.prototype, "location_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [location_map_raw_dto_1.LocationMapRowDTO],
        default: location_map_raw_dto_1.LocationMapRowDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => location_map_raw_dto_1.LocationMapRowDTO),
    (0, class_validator_1.ValidateIf)((body) => body.category == location_entity_1.LocationCategories.PRODUCTION),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Array)
], LocationMapDTO.prototype, "rows", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], LocationMapDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], LocationMapDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], LocationMapDTO.prototype, "deleted_at", void 0);
exports.LocationMapDTO = LocationMapDTO;
//# sourceMappingURL=location-map.dto.js.map