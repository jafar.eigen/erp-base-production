import { IResponses, Responses } from 'src';
import { LocationOrchestrator } from '../../domain/usecases/location.orchestrator';
declare const BaseController: any;
export declare class LocationControllerImpl extends BaseController {
    private responses;
    private locationOrchestrator;
    constructor(responses: Responses, locationOrchestrator: LocationOrchestrator);
    activate(ids: string[], withChild: string, headers: string): Promise<IResponses>;
    deactivate(ids: string[], withChild: string, headers: string): Promise<IResponses>;
}
export {};
