"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const location_orchestrator_1 = require("../../domain/usecases/location.orchestrator");
const create_location_dto_1 = require("../dto/create-location.dto");
const update_location_dto_1 = require("../dto/update-location.dto");
const BaseController = (0, src_1.getBaseController)({
    createModelVm: create_location_dto_1.CreateLocationDTO,
    updateModelVm: update_location_dto_1.UpdateLocationDTO
});
let LocationControllerImpl = class LocationControllerImpl extends BaseController {
    constructor(responses, locationOrchestrator) {
        super(new src_1.Responses(), locationOrchestrator, new src_1.JSONParse());
        this.responses = responses;
        this.locationOrchestrator = locationOrchestrator;
    }
    async activate(ids, withChild, headers) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const location = await this.locationOrchestrator.activate(ids, withChild);
            return this.responses.json(common_1.HttpStatus.OK, location);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
    async deactivate(ids, withChild, headers) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const location = await this.locationOrchestrator.deactivate(ids, withChild);
            return this.responses.json(common_1.HttpStatus.OK, location);
        }
        catch (err) {
            throw new common_1.BadRequestException(err.message);
        }
    }
};
__decorate([
    (0, common_1.Put)('activate'),
    __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(1, (0, common_1.Query)('with_child')),
    __param(2, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String, String]),
    __metadata("design:returntype", Promise)
], LocationControllerImpl.prototype, "activate", null);
__decorate([
    (0, common_1.Put)('deactivate'),
    __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(1, (0, common_1.Query)('with_child')),
    __param(2, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String, String]),
    __metadata("design:returntype", Promise)
], LocationControllerImpl.prototype, "deactivate", null);
LocationControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('locations'),
    (0, common_1.Controller)('locations'),
    __metadata("design:paramtypes", [src_1.Responses,
        location_orchestrator_1.LocationOrchestrator])
], LocationControllerImpl);
exports.LocationControllerImpl = LocationControllerImpl;
//# sourceMappingURL=location-producer.controller.js.map