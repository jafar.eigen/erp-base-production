import { ClientKafka } from '@nestjs/microservices';
import { BaseProducerServiceImpl } from 'src';
import { LocationEntity } from '../../domain/entities/location.entity';
export declare class LocationProducerServiceImpl extends BaseProducerServiceImpl<LocationEntity> {
    client: ClientKafka;
    moduleName: string;
    TOPIC_CREATED: string;
    TOPIC_CHANGED: string;
    TOPIC_ACTIVATED: string;
    TOPIC_DELETED: string;
    constructor(client: ClientKafka);
    addNewTopics(): string[];
}
