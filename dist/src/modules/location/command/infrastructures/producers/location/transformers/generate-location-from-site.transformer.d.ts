import { ILocationConsumerTransformer } from 'src/modules/location/command/domain/contracts/location-transformer';
import { LocationEntity } from 'src/modules/location/command/domain/entities/location.entity';
export declare class GenerateLocationFromSiteTransformer implements ILocationConsumerTransformer {
    private readonly dataSite;
    constructor(dataSite: any);
    transform(): Partial<LocationEntity>;
}
