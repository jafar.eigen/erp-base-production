"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateLocationFromSiteTransformer = void 0;
const location_entity_1 = require("src/modules/location/command/domain/entities/location.entity");
class GenerateLocationFromSiteTransformer {
    constructor(dataSite) {
        this.dataSite = dataSite;
    }
    transform() {
        const { id, name, code, status, request_info, creator_id, creator_name, editor_id, editor_name, deleted_by_id, deleted_by_name, approval, requested_data, has_requested_process, companies, levels = [] } = this.dataSite.data;
        return {
            company_id: companies[0].company_id,
            company_name: companies[0].company_name,
            company_code: companies[0].company_code,
            site_id: id,
            site_name: name,
            site_code: code,
            level_id: levels.length ? levels[0].id : null,
            level_number: levels.length ? levels[0].number : null,
            level_name: levels.length ? levels[0].name : null,
            code: code,
            name: name,
            parent_id: null,
            owner_id: null,
            owner_name: null,
            owner_code: null,
            ext: null,
            category: location_entity_1.LocationCategories.WAREHOUSE,
            type: location_entity_1.LocationType.STOCK,
            is_transaction: true,
            transaction_type: "['External Receipts','External Deliver','Internal Transaction']",
            group_id: null,
            status,
            request_info,
            creator_id,
            creator_name,
            editor_id,
            editor_name,
            deleted_by_id,
            deleted_by_name,
            approval,
            requested_data,
            has_requested_process,
            is_generated: true,
            is_head_office: true,
            maps: []
        };
    }
}
exports.GenerateLocationFromSiteTransformer = GenerateLocationFromSiteTransformer;
//# sourceMappingURL=generate-location-from-site.transformer.js.map