"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckExistenceSiteOnLocationOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const location_data_service_1 = require("src/modules/location/command/data/services/location-data.service");
const handle_check_site_on_location_manager_1 = require("./managers/handle-check-site-on-location.manager");
let CheckExistenceSiteOnLocationOrchestrator = class CheckExistenceSiteOnLocationOrchestrator extends src_1.BaseConsumerManager {
    constructor(locationDataService) {
        super(locationDataService);
        this.locationDataService = locationDataService;
    }
    async execute(message) {
        new handle_check_site_on_location_manager_1.HandleCheckSiteOnLocationManager(message.value, this.locationDataService).run();
    }
};
CheckExistenceSiteOnLocationOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_data_service_1.LocationDataServiceImpl])
], CheckExistenceSiteOnLocationOrchestrator);
exports.CheckExistenceSiteOnLocationOrchestrator = CheckExistenceSiteOnLocationOrchestrator;
//# sourceMappingURL=check-existence-site-on-location.orchestrator.js.map