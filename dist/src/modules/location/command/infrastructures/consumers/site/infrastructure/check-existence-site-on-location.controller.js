"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckExistenceSiteOnLocationController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const src_1 = require("src");
const location_topics_1 = require("../../../../location.topics");
const check_existence_site_on_location_orchestrator_1 = require("../domain/check-existence-site-on-location.orchestrator");
const location_consumer_topic_1 = require("../../location-consumer.topic");
const location_producer_service_1 = require("../../../producers/location-producer.service");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const BaseConsumerController = (0, src_1.getBaseConsumerController)(location_topics_1.MODULE_TOPIC_LOCATION, location_consumer_topic_1.CHECK_EXISTENCE_SITE_ON_LOCATION);
let CheckExistenceSiteOnLocationController = class CheckExistenceSiteOnLocationController extends BaseConsumerController {
    constructor(logger, clientSentry, checkSiteOrchestrator, locationProducerService) {
        super(logger, clientSentry, checkSiteOrchestrator, locationProducerService);
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.checkSiteOrchestrator = checkSiteOrchestrator;
        this.locationProducerService = locationProducerService;
    }
};
CheckExistenceSiteOnLocationController = __decorate([
    (0, common_1.Controller)('location'),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        check_existence_site_on_location_orchestrator_1.CheckExistenceSiteOnLocationOrchestrator,
        location_producer_service_1.LocationProducerServiceImpl])
], CheckExistenceSiteOnLocationController);
exports.CheckExistenceSiteOnLocationController = CheckExistenceSiteOnLocationController;
//# sourceMappingURL=check-existence-site-on-location.controller.js.map