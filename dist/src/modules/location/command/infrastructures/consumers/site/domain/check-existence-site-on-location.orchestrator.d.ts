import { BaseConsumerManager, KafkaMessage } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
export declare class CheckExistenceSiteOnLocationOrchestrator extends BaseConsumerManager {
    private locationDataService;
    constructor(locationDataService: LocationDataServiceImpl);
    execute(message: KafkaMessage): Promise<void>;
}
