import { Logger } from 'winston';
import { SiteActivatedManager } from '../domain/site-activated.manager';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class SiteActivatedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private siteActivatedManager;
    private locationProducerService;
    constructor(logger: Logger, clientSentry: SentryService, siteActivatedManager: SiteActivatedManager, locationProducerService: LocationProducerServiceImpl);
}
export {};
