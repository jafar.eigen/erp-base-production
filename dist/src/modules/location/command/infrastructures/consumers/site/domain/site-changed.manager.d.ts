import { BaseConsumerManager, IUserPayload } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
import { LocationEntity } from 'src/modules/location/command/domain/entities/location.entity';
export declare class SiteChangedManager extends BaseConsumerManager {
    private locationDataService;
    private locationProducerService;
    site: any;
    user: IUserPayload;
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(locations: Partial<any[]>): Promise<void>;
    protected produceTopic(locations: LocationEntity[]): Promise<void>;
}
