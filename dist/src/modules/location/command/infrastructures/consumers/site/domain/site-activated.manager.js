"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteActivatedManager = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const location_data_service_1 = require("src/modules/location/command/data/services/location-data.service");
const location_producer_service_1 = require("../../../producers/location-producer.service");
const generate_location_from_site_transformer_1 = require("../../../producers/location/transformers/generate-location-from-site.transformer");
let SiteActivatedManager = class SiteActivatedManager extends src_1.BaseConsumerManager {
    constructor(locationDataService, locationProducerService) {
        super(locationDataService);
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
    }
    async transformData(value) {
        this.user = value.user;
        const locationDataRes = new generate_location_from_site_transformer_1.GenerateLocationFromSiteTransformer(value).transform();
        Object.assign(value, { locationDataRes });
        return value;
    }
    async processData(dataTransform) {
        await this.locationDataService.save(dataTransform.locationDataRes);
        await this.produceTopic(dataTransform.locationDataRes);
    }
    async produceTopic(dataLocation) {
        await this.locationProducerService.baseCreated({
            user: this.user,
            activity: src_1.RequestInfo.CREATE_DATA,
            mac_address: 'XX-XX-XX-XX-XX-XX',
            data: dataLocation,
            old: null
        });
    }
};
SiteActivatedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_data_service_1.LocationDataServiceImpl,
        location_producer_service_1.LocationProducerServiceImpl])
], SiteActivatedManager);
exports.SiteActivatedManager = SiteActivatedManager;
//# sourceMappingURL=site-activated.manager.js.map