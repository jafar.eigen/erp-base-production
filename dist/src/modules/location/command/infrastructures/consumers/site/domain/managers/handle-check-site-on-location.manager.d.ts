import { IResponses } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
export declare class HandleCheckSiteOnLocationManager {
    private id;
    private locationDataService;
    constructor(id: any, locationDataService: LocationDataServiceImpl);
    run(): Promise<IResponses>;
    protected checkData(): Promise<IResponses>;
}
