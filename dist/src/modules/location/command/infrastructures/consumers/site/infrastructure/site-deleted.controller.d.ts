import { Logger } from 'winston';
import { SiteDeletedManager } from '../domain/site-deleted.manager';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class SiteDeletedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private siteDeletedManager;
    private locationProducerService;
    constructor(logger: Logger, clientSentry: SentryService, siteDeletedManager: SiteDeletedManager, locationProducerService: LocationProducerServiceImpl);
}
export {};
