import { BaseConsumerManager, IUserPayload } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationEntity } from 'src/modules/location/command/domain/entities/location.entity';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
export declare class SiteActivatedManager extends BaseConsumerManager {
    private locationDataService;
    private locationProducerService;
    user: IUserPayload;
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(dataTransform: Partial<any>): Promise<void>;
    protected produceTopic(dataLocation: LocationEntity): Promise<void>;
}
