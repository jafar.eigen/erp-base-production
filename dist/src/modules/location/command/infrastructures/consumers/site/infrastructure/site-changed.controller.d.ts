import { Logger } from 'winston';
import { SiteChangedManager } from '../domain/site-changed.manager';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class SiteChangedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private siteChangedManager;
    private locationProducerService;
    constructor(logger: Logger, clientSentry: SentryService, siteChangedManager: SiteChangedManager, locationProducerService: LocationProducerServiceImpl);
}
export {};
