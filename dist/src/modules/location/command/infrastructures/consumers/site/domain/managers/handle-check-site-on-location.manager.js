"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HandleCheckSiteOnLocationManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
class HandleCheckSiteOnLocationManager {
    constructor(id, locationDataService) {
        this.id = id;
        this.locationDataService = locationDataService;
    }
    async run() {
        return await this.checkData();
    }
    async checkData() {
        const siteId = this.id;
        const locations = await this.locationDataService.findBySitesId(siteId);
        if (locations.length > 0) {
            return new src_1.Responses().json(common_1.HttpStatus.BAD_REQUEST, true);
        }
        return new src_1.Responses().json(common_1.HttpStatus.OK, false);
    }
}
exports.HandleCheckSiteOnLocationManager = HandleCheckSiteOnLocationManager;
//# sourceMappingURL=handle-check-site-on-location.manager.js.map