"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteDeletedManager = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const location_data_service_1 = require("src/modules/location/command/data/services/location-data.service");
const location_producer_service_1 = require("../../../producers/location-producer.service");
let SiteDeletedManager = class SiteDeletedManager extends src_1.BaseConsumerManager {
    constructor(locationDataService, locationProducerService) {
        super(locationDataService);
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
    }
    async transformData(value) {
        var _a;
        const id = (_a = value.old.id) !== null && _a !== void 0 ? _a : value.data.id;
        return {
            id
        };
    }
    async processData(dataTransform) {
        await this.locationDataService.deleteBySiteId(dataTransform.id);
    }
};
SiteDeletedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_data_service_1.LocationDataServiceImpl,
        location_producer_service_1.LocationProducerServiceImpl])
], SiteDeletedManager);
exports.SiteDeletedManager = SiteDeletedManager;
//# sourceMappingURL=site-deleted.manager.js.map