"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteDeletedController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const src_1 = require("src");
const location_topics_1 = require("../../../../location.topics");
const site_deleted_manager_1 = require("../domain/site-deleted.manager");
const location_consumer_topic_1 = require("../../location-consumer.topic");
const location_producer_service_1 = require("../../../producers/location-producer.service");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const BaseConsumerController = (0, src_1.getBaseConsumerController)(location_topics_1.MODULE_TOPIC_LOCATION, location_consumer_topic_1.HANDLE_SITE_DELETED);
let SiteDeletedController = class SiteDeletedController extends BaseConsumerController {
    constructor(logger, clientSentry, siteDeletedManager, locationProducerService) {
        super(logger, clientSentry, siteDeletedManager, locationProducerService);
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.siteDeletedManager = siteDeletedManager;
        this.locationProducerService = locationProducerService;
    }
};
SiteDeletedController = __decorate([
    (0, common_1.Controller)('location'),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        site_deleted_manager_1.SiteDeletedManager,
        location_producer_service_1.LocationProducerServiceImpl])
], SiteDeletedController);
exports.SiteDeletedController = SiteDeletedController;
//# sourceMappingURL=site-deleted.controller.js.map