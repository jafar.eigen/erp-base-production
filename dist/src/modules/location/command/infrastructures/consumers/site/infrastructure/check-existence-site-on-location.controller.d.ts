import { Logger } from 'winston';
import { CheckExistenceSiteOnLocationOrchestrator } from '../domain/check-existence-site-on-location.orchestrator';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CheckExistenceSiteOnLocationController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private checkSiteOrchestrator;
    private locationProducerService;
    constructor(logger: Logger, clientSentry: SentryService, checkSiteOrchestrator: CheckExistenceSiteOnLocationOrchestrator, locationProducerService: LocationProducerServiceImpl);
}
export {};
