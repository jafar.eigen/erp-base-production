"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteChangedManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const location_data_service_1 = require("src/modules/location/command/data/services/location-data.service");
const location_producer_service_1 = require("../../../producers/location-producer.service");
let SiteChangedManager = class SiteChangedManager extends src_1.BaseConsumerManager {
    constructor(locationDataService, locationProducerService) {
        super(locationDataService);
        this.locationDataService = locationDataService;
        this.locationProducerService = locationProducerService;
    }
    async transformData(value) {
        var _a;
        this.site = (_a = value.data) !== null && _a !== void 0 ? _a : value.old;
        this.user = value.user;
        const { levels = [] } = this.site;
        const locations = await this.locationDataService.findBySitesId(this.site.id);
        return await Promise.all(locations.map(async (location) => {
            return Object.assign(location, {
                site_id: this.site.id,
                site_name: this.site.name,
                site_code: this.site.code,
                branch_id: this.site.branch_id,
                branch_name: this.site.branch_name,
                branch_code: this.site.branch_code,
                level_id: levels.length ? levels[0].id : null,
                level_number: levels.length ? levels[0].number : null,
                level_name: levels.length ? levels[0].name : null,
                company_id: this.site.companies.length
                    ? this.site.companies[0].company_id
                    : null,
                company_name: levels.length
                    ? this.site.companies[0].company_name
                    : null,
                company_code: levels.length
                    ? this.site.companies[0].company_code
                    : null,
                status: this.site.status,
                request_info: this.site.request_info
            });
        }));
    }
    async processData(locations) {
        await this.locationDataService.saveMany(locations);
        await this.produceTopic(locations);
    }
    async produceTopic(locations) {
        await Promise.all(locations.map(async (location) => {
            await this.locationProducerService.baseCreated({
                user: this.user,
                activity: location.request_info,
                mac_address: 'XX-XX-XX-XX-XX-XX',
                data: location,
                old: null
            });
        }));
    }
};
SiteChangedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_data_service_1.LocationDataServiceImpl,
        location_producer_service_1.LocationProducerServiceImpl])
], SiteChangedManager);
exports.SiteChangedManager = SiteChangedManager;
//# sourceMappingURL=site-changed.manager.js.map