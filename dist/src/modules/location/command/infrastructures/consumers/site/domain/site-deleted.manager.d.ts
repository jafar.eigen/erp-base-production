import { BaseConsumerManager } from 'src';
import { LocationDataServiceImpl } from 'src/modules/location/command/data/services/location-data.service';
import { LocationProducerServiceImpl } from '../../../producers/location-producer.service';
export declare class SiteDeletedManager extends BaseConsumerManager {
    private locationDataService;
    private locationProducerService;
    constructor(locationDataService: LocationDataServiceImpl, locationProducerService: LocationProducerServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(dataTransform: Partial<any>): Promise<void>;
}
