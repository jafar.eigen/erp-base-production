export declare const MODULE_TOPIC_LOCATION = "LOCATION";
export declare const HANDLE_LOCATION_CREATED = "HANDLE_LOCATION_CREATED";
export declare const HANDLE_LOCATION_CHANGED = "HANDLE_LOCATION_CHANGED";
export declare const LOCATION_ACTIVATED = "LOCATION_ACTIVATED";
export declare const HANDLE_LOCATION_DELETED = "HANDLE_LOCATION_DELETED";
