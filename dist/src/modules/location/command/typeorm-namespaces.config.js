"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const location_model_1 = require("./data/models/location.model");
const group_location_model_1 = require("./data/models/group-location.model");
const location_map_raw_model_1 = require("./data/models/location-map-raw.model");
const location_map_model_1 = require("./data/models/location-map.model");
const config_2 = require("src/utils/config");
exports.default = (0, config_1.registerAs)('typeormLocationCommand', () => ({
    type: 'mysql',
    host: config_2.variableConfig.LOCATION_MYSQL_COMMAND_HOST,
    port: config_2.variableConfig.LOCATION_MYSQL_COMMAND_PORT,
    username: config_2.variableConfig.LOCATION_MYSQL_COMMAND_USERNAME,
    password: config_2.variableConfig.LOCATION_MYSQL_COMMAND_PASSWORD,
    database: config_2.variableConfig.LOCATION_MYSQL_COMMAND_DATABASE,
    entities: [location_model_1.Location, group_location_model_1.GroupLocation, location_map_model_1.LocationMap, location_map_raw_model_1.LocationMapRow],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map