"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const config_2 = require("src/utils/config");
const group_location_read_model_1 = require("./data/models/group-location-read.model");
const location_map_raw_read_model_1 = require("./data/models/location-map-raw-read.model");
const location_map_read_model_1 = require("./data/models/location-map-read.model");
const location_read_model_1 = require("./data/models/location-read.model");
exports.default = (0, config_1.registerAs)('typeormLocationQuery', () => ({
    type: 'mysql',
    host: config_2.variableConfig.LOCATION_MYSQL_QUERY_HOST,
    port: config_2.variableConfig.LOCATION_MYSQL_QUERY_PORT,
    username: config_2.variableConfig.LOCATION_MYSQL_QUERY_USERNAME,
    password: config_2.variableConfig.LOCATION_MYSQL_QUERY_PASSWORD,
    database: config_2.variableConfig.LOCATION_MYSQL_QUERY_DATABASE,
    entities: [
        location_read_model_1.LocationRead,
        group_location_read_model_1.GroupLocationRead,
        location_map_read_model_1.LocationMapRead,
        location_map_raw_read_model_1.LocationMapRowRead
    ],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map