"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationFilter = void 0;
const moment = require("moment");
const typeorm_1 = require("typeorm");
const location_filter_entity_1 = require("../../entities/location-filter.entity");
class LocationFilter extends location_filter_entity_1.LocationFilterEntity {
    constructor(params) {
        super();
        this.q = params.q;
        this.statuses = params.statuses;
        this.requests_info = params.requests_info;
        this.categories = params.categories;
        this.with_childs = params.with_childs;
        this.types = params.types;
        this.companies = params.companies;
        this.branches = params.branches;
        this.sites = params.sites;
        this.site_id = params.site_id;
        this.names = params.names;
        this.codes = params.codes;
        this.group_ids = params.group_ids;
        this.owner_names = params.owner_names;
        this.updated_by = params.updated_by;
        this.is_generated = params.is_generated;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.approval_statuses = params.approval_statuses;
    }
    apply(query) {
        if (this.q)
            query = this.bySearch(query);
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.categories)
            query = this.byCategories(query);
        if (this.with_childs)
            query = this.byWithChilds(query);
        if (this.types)
            query = this.byTypes(query);
        if (this.companies)
            query = this.byCompanies(query);
        if (this.branches)
            query = this.byBranches(query);
        if (this.sites)
            query = this.bySites(query);
        if (this.site_id)
            query = this.bySiteIds(query);
        if (this.names)
            query = this.byNames(query);
        if (this.codes)
            query = this.byCodes(query);
        if (this.group_ids)
            query = this.byGroupIds(query);
        if (this.owner_names)
            query = this.byOwnerNames(query);
        if (this.updated_by)
            query = this.byLastUpdateUser(query);
        if (this.is_generated)
            query = this.byIsGenerated(query);
        if (this.last_update_from && this.last_update_to)
            query = this.byLastUpdateDate(query);
        if (this.approval_statuses)
            query = this.byApprovalStatuses(query);
        return query;
    }
    bySearch(query) {
        return query.orWhere(new typeorm_1.Brackets((qb) => {
            qb.orWhere('locations.name LIKE :name', {
                name: `%${this.q}%`
            })
                .orWhere('locations.code LIKE :code', {
                code: `%${this.q}%`
            })
                .orWhere('locations.company_name LIKE :company_name', {
                company_name: `%${this.q}%`
            })
                .orWhere('locations.branch_name LIKE :branch_name', {
                branch_name: `%${this.q}%`
            })
                .orWhere('locations.site_name LIKE :site_name', {
                site_name: `%${this.q}%`
            })
                .orWhere('locations.owner_name LIKE :owner_name', {
                owner_name: `%${this.q}%`
            })
                .orWhere('locations.creator_name LIKE :creator_name', {
                creator_name: `%${this.q}%`
            })
                .orWhere('locations.editor_name LIKE :editor_name', {
                editor_name: `%${this.q}%`
            });
        }));
    }
    byStatuses(query) {
        console.info(this.statuses);
        return query.andWhere('locations.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byRequestsInfo(query) {
        return query.andWhere('locations.request_info IN (:...requestsInfo)', {
            requestsInfo: this.requests_info
        });
    }
    byCategories(query) {
        return query.andWhere('locations.category IN (:...category)', {
            category: this.categories
        });
    }
    byWithChilds(query) {
        if (this.with_childs === 'false') {
            return query.andWhere(new typeorm_1.Brackets((qb) => {
                qb.andWhere(`locations.parent_id IS NULL`);
            }));
        }
    }
    byTypes(query) {
        return query.andWhere('locations.type IN (:...type)', {
            type: this.types
        });
    }
    byCompanies(query) {
        return query.andWhere('locations.company_name IN (:...companyName)', {
            companyName: this.companies
        });
    }
    byBranches(query) {
        return query.andWhere('locations.branch_name IN (:...branchName)', {
            branchName: this.branches
        });
    }
    bySites(query) {
        return query.andWhere('locations.site_name IN (:...siteName)', {
            siteName: this.sites
        });
    }
    bySiteIds(query) {
        return query.andWhere('locations.site_id IN (:...siteId)', {
            siteId: this.site_id
        });
    }
    byNames(query) {
        return query.andWhere('locations.name IN (:...nameName)', {
            nameName: this.names
        });
    }
    byCodes(query) {
        return query.andWhere('locations.code IN (:...codeName)', {
            codeName: this.codes
        });
    }
    byGroupIds(query) {
        return query.andWhere('locations.group_id IN (:...groupId)', {
            groupId: this.group_ids
        });
    }
    byOwnerNames(query) {
        return query.andWhere('locations.owner_name IN (:...ownerName)', {
            ownerName: this.owner_names
        });
    }
    byLastUpdateUser(query) {
        return query.andWhere('locations.editor_name IN (:...byName)', {
            byName: this.updated_by
        });
    }
    byIsGenerated(query) {
        if (this.is_generated === '1') {
            return query.andWhere('locations.is_generated is true');
        }
        if (this.is_generated === '0') {
            return query.andWhere('locations.is_generated is false');
        }
    }
    byLastUpdateDate(query) {
        return query.andWhere('locations.updated_at BETWEEN :from AND :to', {
            from: moment(new Date(this.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
            to: moment(new Date(this.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
        });
    }
    byApprovalStatuses(query) {
        const maxLength = this.approval_statuses.length;
        let queries = '';
        if (!Array.isArray(this.approval_statuses)) {
            return query.andWhere(this.getQueryApprovalStatus(this.approval_statuses));
        }
        for (let inc = 0; inc < maxLength; inc++) {
            if (inc === this.approval_statuses.length - 1) {
                queries += this.getQueryApprovalStatus(this.approval_statuses[inc]);
            }
            else {
                queries += `${this.getQueryApprovalStatus(this.approval_statuses[inc])} OR `;
            }
        }
        return query.andWhere(`${queries}`);
    }
    getQueryApprovalStatus(query) {
        switch (query) {
            case 'approved':
                return `locations.has_requested_process = 0 AND locations.approval LIKE '%"is_done": true%'`;
            case 'declined':
                return `locations.has_requested_process = 0 AND locations.approval LIKE '%"has_decline": true%'`;
            default:
                return `locations.has_requested_process = 1 AND locations.approval LIKE '%"has_decline": false%' AND locations.approval LIKE '%"is_done": false%'`;
        }
    }
}
exports.LocationFilter = LocationFilter;
//# sourceMappingURL=location.filter.js.map