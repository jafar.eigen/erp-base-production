import { SelectQueryBuilder } from 'typeorm';
import { FilterLocationDTO } from '../../../infrastructures/dto/filter-location.dto';
import { LocationFilterEntity } from '../../entities/location-filter.entity';
import { LocationReadEntity } from '../../entities/location-read.entity';
export declare class LocationFilter extends LocationFilterEntity {
    constructor(params: FilterLocationDTO);
    apply(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected bySearch(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byStatuses(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byRequestsInfo(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byCategories(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byWithChilds(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byTypes(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byCompanies(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byBranches(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected bySites(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected bySiteIds(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byNames(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byCodes(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byGroupIds(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byOwnerNames(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byLastUpdateUser(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byIsGenerated(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byLastUpdateDate(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected byApprovalStatuses(query: SelectQueryBuilder<LocationReadEntity>): SelectQueryBuilder<LocationReadEntity>;
    protected getQueryApprovalStatus(query: string): string;
}
