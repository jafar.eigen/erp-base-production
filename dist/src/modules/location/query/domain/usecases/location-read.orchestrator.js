"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationReadOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const location_read_data_service_1 = require("../../data/services/location-read-data.service");
const location_filter_1 = require("./filters/location.filter");
let LocationReadOrchestrator = class LocationReadOrchestrator {
    constructor(locationReadDataService) {
        this.locationReadDataService = locationReadDataService;
        this.relations = ['maps', 'maps.rows', 'parent', 'childs'];
    }
    async show(locationId) {
        return await this.locationReadDataService.findOneOrFail(locationId);
    }
    async index(page, limit, params) {
        const filter = new location_filter_1.LocationFilter(params);
        return await this.locationReadDataService.index({
            page: page,
            limit: limit,
            route: 'api/locations'
        }, {
            relations: this.relations,
            join: {
                alias: 'locations',
                leftJoin: {
                    maps: 'locations.maps',
                    parent: 'locations.parent',
                    childs: 'locations.childs'
                }
            },
            where: (queryLocation) => {
                filter.apply(queryLocation);
            },
            order: { created_at: 'DESC' }
        });
    }
    async getLocationsByCompany(page, limit, companyId) {
        return await this.locationReadDataService.findLocationByCompany({
            page: page,
            limit: limit,
            route: '/api/locations'
        }, companyId);
    }
};
LocationReadOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [location_read_data_service_1.LocationReadDataServiceImpl])
], LocationReadOrchestrator);
exports.LocationReadOrchestrator = LocationReadOrchestrator;
//# sourceMappingURL=location-read.orchestrator.js.map