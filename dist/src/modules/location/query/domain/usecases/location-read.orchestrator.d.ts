import { Pagination } from 'nestjs-typeorm-paginate';
import { LocationReadDataServiceImpl } from '../../data/services/location-read-data.service';
import { FilterLocationDTO } from '../../infrastructures/dto/filter-location.dto';
import { LocationReadEntity } from '../entities/location-read.entity';
export declare class LocationReadOrchestrator {
    private locationReadDataService;
    constructor(locationReadDataService: LocationReadDataServiceImpl);
    relations: string[];
    show(locationId: string): Promise<LocationReadEntity>;
    index(page: number, limit: number, params: FilterLocationDTO): Promise<Pagination<LocationReadEntity>>;
    getLocationsByCompany(page: number, limit: number, companyId: string): Promise<Pagination<LocationReadEntity>>;
}
