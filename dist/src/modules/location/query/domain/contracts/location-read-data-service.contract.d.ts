import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { LocationReadEntity } from '../entities/location-read.entity';
export interface LocationReadDataService {
    findOneOrFail(locationId: string): Promise<LocationReadEntity>;
    findLocationByCompany(options: IPaginationOptions, companyId: string): Promise<Pagination<LocationReadEntity>>;
}
