import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { LocationReadEntity } from '../entities/location-read.entity';
export interface ILocationsTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: LocationReadEntity[];
    apply(): Pagination<Partial<LocationReadEntity>>;
}
