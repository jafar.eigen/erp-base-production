import { LocationReadEntity } from '../entities/location-read.entity';
export interface ILocationTransformer {
    transform(): Partial<LocationReadEntity>;
}
