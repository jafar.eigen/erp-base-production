import { IResponses } from 'src';
import { FilterLocationDTO } from '../../infrastructures/dto/filter-location.dto';
export interface LocationReadController {
    index(page: number, limit: number, params: FilterLocationDTO): Promise<IResponses>;
    show(branchId: string): Promise<IResponses>;
    showByCompany(page: number, limit: number, companyId: string): Promise<IResponses>;
}
