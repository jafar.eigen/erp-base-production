"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionalReadType = exports.LocationReadType = exports.LocationReadCategories = void 0;
var LocationReadCategories;
(function (LocationReadCategories) {
    LocationReadCategories["WAREHOUSE"] = "Warehouse";
    LocationReadCategories["CONSIGNMENT"] = "Consignment";
    LocationReadCategories["PRODUCTION"] = "Production";
    LocationReadCategories["OPERATIONAL"] = "Operational";
})(LocationReadCategories = exports.LocationReadCategories || (exports.LocationReadCategories = {}));
var LocationReadType;
(function (LocationReadType) {
    LocationReadType["STOCK"] = "Stock";
    LocationReadType["SCRAP"] = "Scrap";
    LocationReadType["QUARANTINE"] = "Quarantine";
})(LocationReadType = exports.LocationReadType || (exports.LocationReadType = {}));
var TransactionalReadType;
(function (TransactionalReadType) {
    TransactionalReadType["EXTERNAL_DELIVERY"] = "External Delivery";
    TransactionalReadType["EXTERNAL_RECEIPTS"] = "External Receipts";
    TransactionalReadType["INTERNAL_TRANSFER"] = "Internal Transfer";
})(TransactionalReadType = exports.TransactionalReadType || (exports.TransactionalReadType = {}));
//# sourceMappingURL=group-location-read.entity.js.map