import { Approval } from 'src';
import { LocationReadEntity } from './location-read.entity';
export declare enum LocationReadCategories {
    WAREHOUSE = "Warehouse",
    CONSIGNMENT = "Consignment",
    PRODUCTION = "Production",
    OPERATIONAL = "Operational"
}
export declare enum LocationReadType {
    STOCK = "Stock",
    SCRAP = "Scrap",
    QUARANTINE = "Quarantine"
}
export declare enum TransactionalReadType {
    EXTERNAL_DELIVERY = "External Delivery",
    EXTERNAL_RECEIPTS = "External Receipts",
    INTERNAL_TRANSFER = "Internal Transfer"
}
export interface GroupLocationReadEntity {
    id: string;
    gid: string;
    group_map_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    site_id: string;
    site_name: string;
    site_code: string;
    code: string;
    name: string;
    parent_id: string;
    owner_id: string;
    owner_name: string;
    owner_code: string;
    ext: any;
    category: LocationReadCategories;
    type: LocationReadType;
    is_transaction: boolean;
    transaction_type: string;
    group_id: string;
    status: string;
    request_info: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: Approval;
    requested_data?: any;
    has_requested_process?: boolean;
    location?: LocationReadEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
