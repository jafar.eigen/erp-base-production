import { LocationMapRawReadEntity } from './location-map-raw-read.entity';
import { LocationReadType } from './location-read.entity';
export interface LocationMapReadEntity {
    id: string;
    column_name: string;
    location_id: string;
    location_code: string;
    location_name: string;
    location_type: LocationReadType;
    rows: LocationMapRawReadEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
