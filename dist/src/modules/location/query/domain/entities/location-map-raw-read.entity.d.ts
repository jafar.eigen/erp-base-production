import { LocationReadType } from './location-read.entity';
export interface LocationMapRawReadEntity {
    id: string;
    location_id: string;
    type: LocationReadType;
    map_id: string;
    contact_id: string;
    contact_code: string;
    contact_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
