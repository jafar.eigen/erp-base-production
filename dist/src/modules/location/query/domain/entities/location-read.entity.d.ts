import { ApprovalPerCompany } from 'src';
import { LocationMapReadEntity } from './location-map-read.entity';
export declare enum LocationReadCategories {
    WAREHOUSE = "Warehouse",
    CONSIGNMENT = "Consignment",
    PRODUCTION = "Production",
    OPERATIONAL = "Operational"
}
export declare enum LocationReadType {
    STOCK = "Stock",
    SCRAP = "Scrap",
    QUARANTINE = "Quarantine"
}
export interface LocationReadEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    site_id: string;
    site_name: string;
    site_code: string;
    level_id: string;
    level_number: number;
    level_name: string;
    code: string;
    name: string;
    parent_id: string;
    parent_name: string;
    parent_code: string;
    owner_id: string;
    owner_name: string;
    owner_code: string;
    ext: string;
    category: LocationReadCategories;
    type: LocationReadType;
    is_transaction: boolean;
    transaction_type: string;
    group_id: string;
    status: string;
    request_info: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    is_generated?: boolean;
    is_head_office?: boolean;
    is_production?: boolean;
    is_operational?: boolean;
    childs?: LocationReadEntity[];
    parent: LocationReadEntity;
    maps: LocationMapReadEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
