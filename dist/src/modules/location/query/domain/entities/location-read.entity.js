"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationReadType = exports.LocationReadCategories = void 0;
var LocationReadCategories;
(function (LocationReadCategories) {
    LocationReadCategories["WAREHOUSE"] = "Warehouse";
    LocationReadCategories["CONSIGNMENT"] = "Consignment";
    LocationReadCategories["PRODUCTION"] = "Production";
    LocationReadCategories["OPERATIONAL"] = "Operational";
})(LocationReadCategories = exports.LocationReadCategories || (exports.LocationReadCategories = {}));
var LocationReadType;
(function (LocationReadType) {
    LocationReadType["STOCK"] = "Stock";
    LocationReadType["SCRAP"] = "Scrap";
    LocationReadType["QUARANTINE"] = "Quarantine";
})(LocationReadType = exports.LocationReadType || (exports.LocationReadType = {}));
//# sourceMappingURL=location-read.entity.js.map