import { RequestInfo } from 'src';
export declare class LocationFilterEntity {
    q: string;
    statuses: string[];
    requests_info: RequestInfo[];
    categories: string[];
    with_childs: string;
    types: string[];
    companies: string[];
    sites: string[];
    site_id: string[];
    names: string[];
    codes: string[];
    branches: string[];
    group_ids: string[];
    owner_names: string[];
    updated_by: string[];
    is_generated: string;
    last_update_from: string;
    last_update_to: string;
    approval_statuses: string[];
}
