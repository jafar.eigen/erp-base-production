"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const database_config_1 = require("src/utils/database.config");
const location_read_model_1 = require("./data/models/location-read.model");
const group_location_read_model_1 = require("./data/models/group-location-read.model");
const location_map_read_model_1 = require("./data/models/location-map-read.model");
const location_read_orchestrator_1 = require("./domain/usecases/location-read.orchestrator");
const location_read_data_service_1 = require("./data/services/location-read-data.service");
const location_read_controller_1 = require("./infrastructures/location-read.controller");
const location_map_raw_read_model_1 = require("./data/models/location-map-raw-read.model");
let QueryModule = class QueryModule {
};
QueryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default],
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.LOCATION_READ_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormLocationQuery'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([location_read_model_1.LocationRead, group_location_read_model_1.GroupLocationRead, location_map_read_model_1.LocationMapRead, location_map_raw_read_model_1.LocationMapRowRead], database_config_1.LOCATION_READ_CONNECTION)
        ],
        providers: [src_1.Responses, location_read_orchestrator_1.LocationReadOrchestrator, location_read_data_service_1.LocationReadDataServiceImpl],
        controllers: [location_read_controller_1.LocationReadControllerImpl]
    })
], QueryModule);
exports.QueryModule = QueryModule;
//# sourceMappingURL=query.module.js.map