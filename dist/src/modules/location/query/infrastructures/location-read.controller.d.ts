import { IResponses } from 'src';
import { Responses } from 'src';
import { LocationReadController } from '../domain/contracts/location-read-controller.contract';
import { LocationReadOrchestrator } from '../domain/usecases/location-read.orchestrator';
import { FilterLocationDTO } from './dto/filter-location.dto';
export declare class LocationReadControllerImpl implements LocationReadController {
    private responses;
    private locationReadOrchestrator;
    constructor(responses: Responses, locationReadOrchestrator: LocationReadOrchestrator);
    index(page: number, limit: number, params: FilterLocationDTO): Promise<IResponses>;
    showByCompany(page: number, limit: number, companyId: string): Promise<IResponses>;
    show(locationId: string): Promise<IResponses>;
}
