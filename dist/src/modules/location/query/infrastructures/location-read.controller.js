"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationReadControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const location_read_orchestrator_1 = require("../domain/usecases/location-read.orchestrator");
const filter_location_dto_1 = require("./dto/filter-location.dto");
const location_transformer_1 = require("./transformers/location.transformer");
const locations_transformer_1 = require("./transformers/locations.transformer");
let LocationReadControllerImpl = class LocationReadControllerImpl {
    constructor(responses, locationReadOrchestrator) {
        this.responses = responses;
        this.locationReadOrchestrator = locationReadOrchestrator;
    }
    async index(page, limit, params) {
        try {
            const locations = await this.locationReadOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new locations_transformer_1.LocationsTransformer(locations).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async showByCompany(page, limit, companyId) {
        try {
            const locations = await this.locationReadOrchestrator.getLocationsByCompany(page, limit, companyId);
            return this.responses.json(common_1.HttpStatus.OK, new locations_transformer_1.LocationsTransformer(locations).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async show(locationId) {
        try {
            const location = await this.locationReadOrchestrator.show(locationId);
            return this.responses.json(common_1.HttpStatus.OK, new location_transformer_1.LocationTransformer(location).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_location_dto_1.FilterLocationDTO]),
    __metadata("design:returntype", Promise)
], LocationReadControllerImpl.prototype, "index", null);
__decorate([
    (0, common_1.Get)('/companies'),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)('company_id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], LocationReadControllerImpl.prototype, "showByCompany", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], LocationReadControllerImpl.prototype, "show", null);
LocationReadControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('location indexes'),
    (0, common_1.Controller)('locations'),
    __metadata("design:paramtypes", [src_1.Responses,
        location_read_orchestrator_1.LocationReadOrchestrator])
], LocationReadControllerImpl);
exports.LocationReadControllerImpl = LocationReadControllerImpl;
//# sourceMappingURL=location-read.controller.js.map