"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterLocationDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class FilterLocationDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.q),
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Search by: Location Name, Location Code, Company name, Branch name, Site name, Location name, Owner name, Creator Name, Editor Name'
    }),
    __metadata("design:type", String)
], FilterLocationDTO.prototype, "q", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.statuses),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Draft, Inactive, Active, Requested, Declined'
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "statuses", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requests_info),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Create Data, Edit Data, Edit Active, Edit Inactive, Edit Data & Status Active, Edit Data & Status Inactive, Delete Data'
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "requests_info", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.categories),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Warehouse, Consignment, Production, Operational'
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "categories", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.with_childs),
    (0, class_transformer_1.Transform)((body) => body.value.toString()),
    (0, swagger_1.ApiProperty)({
        type: 'boolean',
        required: false,
        description: 'Filter Get Data by With Childs (false | true)'
    }),
    __metadata("design:type", String)
], FilterLocationDTO.prototype, "with_childs", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.types),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Stock, Scrap, Quarantine'
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "types", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.companies) ? body.companies : [body.companies];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by company names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.branches) ? body.branches : [body.branches];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by branch names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "branches", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.sites) ? body.sites : [body.sites];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by site names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "sites", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.site_id) ? body.site_id : [body.site_id];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by site ids',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "site_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.names) ? body.names : [body.names];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by location names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "names", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.codes) ? body.codes : [body.codes];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by location codes',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "codes", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.group_ids) ? body.group_ids : [body.group_ids];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by group ids',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "group_ids", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.owner_names)
            ? body.owner_names
            : [body.owner_names];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by location Owner Names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "owner_names", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.updated_by) ? body.updated_by : [body.updated_by];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        description: 'Find based on updated by',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "updated_by", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => { var _a; return (_a = body.is_generated) === null || _a === void 0 ? void 0 : _a.toString(); }),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        description: 'Filter by is_generated Data (0:false,1:true)'
    }),
    __metadata("design:type", String)
], FilterLocationDTO.prototype, "is_generated", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update from',
        required: false
    }),
    (0, class_validator_1.ValidateIf)((body) => body.last_update_from),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterLocationDTO.prototype, "last_update_from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update to',
        required: false
    }),
    (0, class_validator_1.ValidateIf)((body) => body.last_update_to),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterLocationDTO.prototype, "last_update_to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : approved, declined, requesting)'
    }),
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.approval_statuses)
            ? body.approval_statuses
            : [body.approval_statuses];
    }),
    __metadata("design:type", Array)
], FilterLocationDTO.prototype, "approval_statuses", void 0);
exports.FilterLocationDTO = FilterLocationDTO;
//# sourceMappingURL=filter-location.dto.js.map