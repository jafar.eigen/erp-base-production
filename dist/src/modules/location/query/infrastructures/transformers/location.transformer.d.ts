import { ILocationTransformer } from '../../domain/contracts/location-transformer.contract';
import { LocationReadEntity } from '../../domain/entities/location-read.entity';
export declare class LocationTransformer implements ILocationTransformer {
    private readonly location;
    constructor(location: LocationReadEntity);
    transform(): Partial<LocationReadEntity>;
}
