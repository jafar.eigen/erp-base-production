import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { ILocationsTransformer } from '../../domain/contracts/locations-transformer.contract';
import { LocationReadEntity } from '../../domain/entities/location-read.entity';
export declare class LocationsTransformer implements ILocationsTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: LocationReadEntity[];
    constructor(locations: Pagination<LocationReadEntity>);
    apply(): Pagination<Partial<LocationReadEntity>>;
}
