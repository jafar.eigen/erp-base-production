"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationTransformer = void 0;
class LocationTransformer {
    constructor(location) {
        this.location = location;
    }
    transform() {
        const { id, company_id, company_name, company_code, site_id, site_name, site_code, level_id, level_number, level_name, code, name, parent_id, owner_id, owner_name, owner_code, ext: number, category, type, is_transaction, transaction_type, group_id, status, request_info, creator_id, creator_name, editor_id, editor_name, deleted_by_id, deleted_by_name, approval, requested_data, has_requested_process, is_generated, is_head_office, is_production, is_operational, maps, parent, childs, created_at, updated_at, deleted_at } = this.location;
        return {
            id,
            company_id,
            company_name,
            company_code,
            site_id,
            site_name,
            site_code,
            level_id,
            level_number,
            level_name,
            code,
            name,
            parent_id,
            parent_name: parent ? parent === null || parent === void 0 ? void 0 : parent.name : null,
            parent_code: parent ? parent === null || parent === void 0 ? void 0 : parent.code : null,
            owner_id,
            owner_name,
            owner_code,
            ext: number,
            category,
            type,
            is_transaction,
            transaction_type,
            group_id,
            status,
            request_info,
            creator_id,
            creator_name,
            editor_id,
            editor_name,
            deleted_by_id,
            deleted_by_name,
            approval,
            requested_data,
            has_requested_process,
            is_generated,
            is_head_office,
            is_production,
            is_operational,
            maps: maps.map((map) => map),
            created_at,
            updated_at,
            deleted_at,
            parent,
            childs
        };
    }
}
exports.LocationTransformer = LocationTransformer;
//# sourceMappingURL=location.transformer.js.map