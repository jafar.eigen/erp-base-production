"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsTransformer = void 0;
const moment = require("moment");
class LocationsTransformer {
    constructor(locations) {
        Object.assign(this, locations);
    }
    apply() {
        const locations = this.items.map((location) => {
            const { id, company_id, company_name, company_code, site_id, site_name, site_code, level_id, level_number, level_name, code, name, parent_id, owner_id, owner_name, owner_code, ext: number, category, type, is_transaction, transaction_type, group_id, status, request_info, creator_id, creator_name, editor_id, editor_name, deleted_by_id, deleted_by_name, approval, requested_data, has_requested_process, is_generated, is_head_office, is_production, is_operational, maps, parent, childs, created_at, updated_at, deleted_at } = location;
            return {
                id,
                company_id,
                company_name,
                company_code,
                site_id,
                site_name,
                site_code,
                level_id,
                level_number,
                level_name,
                code,
                name,
                parent_id,
                parent_name: parent ? parent === null || parent === void 0 ? void 0 : parent.name : null,
                parent_code: parent ? parent === null || parent === void 0 ? void 0 : parent.name : null,
                owner_id,
                owner_name,
                owner_code,
                ext: number,
                category,
                type,
                is_transaction,
                transaction_type,
                group_id,
                status,
                request_info,
                creator_id,
                creator_name,
                editor_id,
                editor_name,
                deleted_by_id,
                deleted_by_name,
                approval,
                requested_data,
                has_requested_process,
                is_generated,
                is_head_office,
                is_production,
                is_operational,
                maps: maps.map((map) => map),
                user_date: `${location.editor_name},${moment(location.updated_at).format('DD/MM/YY,HH:mm')}`,
                created_at,
                updated_at,
                deleted_at,
                parent,
                childs
            };
        });
        return {
            items: locations,
            meta: this.meta,
            links: this.links
        };
    }
}
exports.LocationsTransformer = LocationsTransformer;
//# sourceMappingURL=locations.transformer.js.map