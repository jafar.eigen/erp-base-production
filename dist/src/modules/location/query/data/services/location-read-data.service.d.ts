import { FindManyOptions, Repository } from 'typeorm';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { LocationRead } from '../models/location-read.model';
import { LocationReadEntity } from '../../domain/entities/location-read.entity';
import { LocationReadDataService } from '../../domain/contracts/location-read-data-service.contract';
import { LocationMapRead } from '../models/location-map-read.model';
export declare class LocationReadDataServiceImpl implements LocationReadDataService {
    private locationReadRepo;
    private locationMapReadRepo;
    constructor(locationReadRepo: Repository<LocationRead>, locationMapReadRepo: Repository<LocationMapRead>);
    relations: string[];
    findOneOrFail(locationId: string): Promise<LocationReadEntity>;
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<LocationReadEntity>): Promise<Pagination<LocationReadEntity>>;
    findLocationByCompany(options: IPaginationOptions, companyId: string): Promise<Pagination<LocationReadEntity>>;
}
