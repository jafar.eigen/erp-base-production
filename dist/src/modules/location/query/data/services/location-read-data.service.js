"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationReadDataServiceImpl = void 0;
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("@nestjs/typeorm");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const location_read_model_1 = require("../models/location-read.model");
const database_config_1 = require("src/utils/database.config");
const location_map_read_model_1 = require("../models/location-map-read.model");
let LocationReadDataServiceImpl = class LocationReadDataServiceImpl {
    constructor(locationReadRepo, locationMapReadRepo) {
        this.locationReadRepo = locationReadRepo;
        this.locationMapReadRepo = locationMapReadRepo;
        this.relations = ['maps', 'maps.rows', 'parent', 'childs'];
    }
    async findOneOrFail(locationId) {
        const maps = await this.locationMapReadRepo.find({
            where: { location_id: locationId },
            order: { created_at: 'ASC' },
            relations: ['rows']
        });
        const location = await this.locationReadRepo.findOneOrFail(locationId, {
            relations: this.relations
        });
        Object.assign(location, { maps: maps });
        return location;
    }
    async index(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.locationReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.locationReadRepo, options, {
            relations: this.relations
        });
    }
    async findLocationByCompany(options, companyId) {
        return (0, nestjs_typeorm_paginate_1.paginate)(this.locationReadRepo, options, {
            relations: this.relations,
            join: {
                alias: 'locations',
                leftJoin: {
                    maps: 'locations.maps',
                    parent: 'locations.parent'
                }
            },
            where: (queryLocation) => {
                queryLocation.where('locations.company_id = :companyId', { companyId });
            }
        });
    }
};
LocationReadDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(location_read_model_1.LocationRead, database_config_1.LOCATION_READ_CONNECTION)),
    __param(1, (0, typeorm_2.InjectRepository)(location_map_read_model_1.LocationMapRead, database_config_1.LOCATION_READ_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository])
], LocationReadDataServiceImpl);
exports.LocationReadDataServiceImpl = LocationReadDataServiceImpl;
//# sourceMappingURL=location-read-data.service.js.map