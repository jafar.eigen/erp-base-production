import { LocationMapRawReadEntity } from '../../domain/entities/location-map-raw-read.entity';
import { LocationRead } from './location-read.model';
import { LocationMapRead } from './location-map-read.model';
import { LocationReadType } from '../../domain/entities/location-read.entity';
export declare class LocationMapRowRead implements LocationMapRawReadEntity {
    id: string;
    type: LocationReadType;
    map_id: string;
    contact_id: string;
    contact_code: string;
    contact_name: string;
    location_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    map: LocationMapRead;
    location: LocationRead;
}
