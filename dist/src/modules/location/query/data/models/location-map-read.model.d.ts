import { LocationMapReadEntity } from '../../domain/entities/location-map-read.entity';
import { LocationReadType } from '../../domain/entities/location-read.entity';
import { LocationMapRowRead } from './location-map-raw-read.model';
import { LocationRead } from './location-read.model';
export declare class LocationMapRead implements LocationMapReadEntity {
    id: string;
    column_name: string;
    location_id: string;
    location_type: LocationReadType;
    location_code: string;
    location_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    location: LocationRead;
    rows: LocationMapRowRead[];
}
