"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationMapRowRead = void 0;
const typeorm_1 = require("typeorm");
const location_read_model_1 = require("./location-read.model");
const location_map_read_model_1 = require("./location-map-read.model");
const location_read_entity_1 = require("../../domain/entities/location-read.entity");
let LocationMapRowRead = class LocationMapRowRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: location_read_entity_1.LocationReadType, nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'map_id', nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "map_id", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'contact_id', nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "contact_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'contact_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "contact_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'contact_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "contact_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'location_id', nullable: true }),
    __metadata("design:type", String)
], LocationMapRowRead.prototype, "location_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMapRowRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMapRowRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LocationMapRowRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => location_map_read_model_1.LocationMapRead, (locationMapRead) => locationMapRead.rows, {
        onDelete: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'map_id' }),
    __metadata("design:type", location_map_read_model_1.LocationMapRead)
], LocationMapRowRead.prototype, "map", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => location_read_model_1.LocationRead, (locationRead) => locationRead.maps, {
        onDelete: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'location_id' }),
    __metadata("design:type", location_read_model_1.LocationRead)
], LocationMapRowRead.prototype, "location", void 0);
LocationMapRowRead = __decorate([
    (0, typeorm_1.Entity)('location_map_raws')
], LocationMapRowRead);
exports.LocationMapRowRead = LocationMapRowRead;
//# sourceMappingURL=location-map-raw-read.model.js.map