"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupLocationRead = void 0;
const src_1 = require("src");
const typeorm_1 = require("typeorm");
const group_location_read_entity_1 = require("../../domain/entities/group-location-read.entity");
const location_read_model_1 = require("./location-read.model");
let GroupLocationRead = class GroupLocationRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'gid' }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "gid", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_company_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "group_map_id", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'company_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'site_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "site_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_code', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "site_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'parent_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "parent_id", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'owner_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "owner_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'owner_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "owner_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'owner_code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "owner_code", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'ext', nullable: true }),
    __metadata("design:type", Object)
], GroupLocationRead.prototype, "ext", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: group_location_read_entity_1.LocationReadCategories }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: group_location_read_entity_1.LocationReadType }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_transaction', nullable: true }),
    __metadata("design:type", Boolean)
], GroupLocationRead.prototype, "is_transaction", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: group_location_read_entity_1.TransactionalReadType }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "transaction_type", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.STATUS }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], GroupLocationRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], GroupLocationRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], GroupLocationRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], GroupLocationRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupLocationRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupLocationRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupLocationRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => location_read_model_1.LocationRead, (location) => location.group),
    __metadata("design:type", Array)
], GroupLocationRead.prototype, "locations", void 0);
GroupLocationRead = __decorate([
    (0, typeorm_1.Entity)('group_locations')
], GroupLocationRead);
exports.GroupLocationRead = GroupLocationRead;
//# sourceMappingURL=group-location-read.model.js.map