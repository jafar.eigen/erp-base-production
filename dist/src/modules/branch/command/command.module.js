"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const database_config_1 = require("src/utils/database.config");
const microservice_config_1 = require("../../../utils/microservice.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const branch_model_1 = require("./data/models/branch.model");
const branch_orchestrator_1 = require("./domain/usecases/branch.orchestrator");
const branch_data_service_1 = require("./data/services/branch-data.service");
const branch_producer_controller_1 = require("./infrastructure/producers/branch-producer.controller");
const group_branch_model_1 = require("./data/models/group-branch.model");
const branch_company_model_1 = require("./data/models/branch-company.model");
const branch_producer_service_1 = require("./infrastructure/producers/branch-producer.service");
const branch_site_model_1 = require("./data/models/branch-site.model");
const branch_site_data_service_1 = require("./data/services/branch-site-data.service");
const branch_company_data_service_1 = require("./data/services/branch-company-data.service");
const microservices_1 = require("@nestjs/microservices");
const branch_consumer_controller_1 = require("./infrastructure/consumers/infrastructure/branch-consumer.controller");
let CommandModule = class CommandModule {
};
CommandModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default, microservice_config_1.default],
                isGlobal: true
            }),
            microservices_1.ClientsModule.registerAsync([
                {
                    name: microservice_config_1.KAFKA_CLIENT_NAME,
                    imports: [config_1.ConfigModule],
                    useFactory: (configService) => configService.get('kafkaClientConfig'),
                    inject: [config_1.ConfigService]
                }
            ]),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.BRANCH_CUD_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormBranchCommand'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([branch_model_1.Branch, branch_company_model_1.BranchCompany, branch_site_model_1.BranchSite, group_branch_model_1.GroupBranch], database_config_1.BRANCH_CUD_CONNECTION)
        ],
        providers: [
            src_1.Responses,
            src_1.JSONParse,
            branch_orchestrator_1.BranchOrchestrator,
            branch_data_service_1.BranchDataServiceImpl,
            branch_company_data_service_1.BranchCompanyDataServiceImpl,
            branch_site_data_service_1.BranchSiteDataServiceImpl,
            branch_producer_service_1.BranchProducerServiceImpl
        ],
        controllers: [branch_producer_controller_1.BranchProducerControllerImpl, branch_consumer_controller_1.BranchConsumerControllerImpl]
    })
], CommandModule);
exports.CommandModule = CommandModule;
//# sourceMappingURL=command.module.js.map