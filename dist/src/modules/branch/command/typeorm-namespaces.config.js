"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const branch_model_1 = require("src/modules/branch/command/data/models/branch.model");
const branch_company_model_1 = require("src/modules/branch/command/data/models/branch-company.model");
const branch_site_model_1 = require("src/modules/branch/command/data/models/branch-site.model");
const group_branch_model_1 = require("src/modules/branch/command/data/models/group-branch.model");
const config_2 = require("src/utils/config");
exports.default = (0, config_1.registerAs)('typeormBranchCommand', () => ({
    type: 'mysql',
    host: config_2.variableConfig.BRANCH_MYSQL_COMMAND_HOST,
    port: config_2.variableConfig.BRANCH_MYSQL_COMMAND_PORT,
    username: config_2.variableConfig.BRANCH_MYSQL_COMMAND_USERNAME,
    password: config_2.variableConfig.BRANCH_MYSQL_COMMAND_PASSWORD,
    database: config_2.variableConfig.BRANCH_MYSQL_COMMAND_DATABASE,
    entities: [branch_model_1.Branch, branch_company_model_1.BranchCompany, branch_site_model_1.BranchSite, group_branch_model_1.GroupBranch],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map