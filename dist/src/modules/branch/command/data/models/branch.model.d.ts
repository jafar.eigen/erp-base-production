import { ApprovalPerCompany, RequestInfo, BaseExtendModel } from 'src';
import { BranchEntity } from '../../domain/entities/branch.entity';
import { BranchCompany } from './branch-company.model';
import { BranchSite } from './branch-site.model';
import { GroupBranch } from './group-branch.model';
export declare class Branch extends BaseExtendModel implements BranchEntity {
    code: string;
    name: string;
    amount_sites: number;
    group_name: string;
    group_id: string;
    request_info: RequestInfo;
    creator_id: string;
    creator_name: string;
    editor_id: string;
    editor_name: string;
    deleted_by_id: string;
    deleted_by_name: string;
    has_requested_process: boolean;
    approval: ApprovalPerCompany;
    requested_data: Partial<BranchEntity>;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    companies: BranchCompany[];
    sites: BranchSite[];
    group: GroupBranch;
}
