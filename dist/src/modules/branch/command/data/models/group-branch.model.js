"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupBranch = void 0;
const typeorm_1 = require("typeorm");
const branch_model_1 = require("./branch.model");
let GroupBranch = class GroupBranch {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupBranch.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'gid' }),
    __metadata("design:type", String)
], GroupBranch.prototype, "gid", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupBranch.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranch.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('integer', { name: 'sites', nullable: true }),
    __metadata("design:type", Number)
], GroupBranch.prototype, "amount_sites", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranch.prototype, "group_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranch.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranch.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranch.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranch.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => branch_model_1.Branch, (branch) => branch.group),
    __metadata("design:type", Array)
], GroupBranch.prototype, "branches", void 0);
GroupBranch = __decorate([
    (0, typeorm_1.Entity)('group_branches')
], GroupBranch);
exports.GroupBranch = GroupBranch;
//# sourceMappingURL=group-branch.model.js.map