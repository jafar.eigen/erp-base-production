"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchCompany = void 0;
const typeorm_1 = require("typeorm");
const branch_model_1 = require("./branch.model");
let BranchCompany = class BranchCompany {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], BranchCompany.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], BranchCompany.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', length: '50' }),
    __metadata("design:type", String)
], BranchCompany.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '10' }),
    __metadata("design:type", String)
], BranchCompany.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_status' }),
    __metadata("design:type", String)
], BranchCompany.prototype, "company_status", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompany.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompany.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompany.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => branch_model_1.Branch, (branch) => branch.companies),
    (0, typeorm_1.JoinTable)({ name: 'branches_companies' }),
    __metadata("design:type", Array)
], BranchCompany.prototype, "branches", void 0);
BranchCompany = __decorate([
    (0, typeorm_1.Entity)('branch_companies')
], BranchCompany);
exports.BranchCompany = BranchCompany;
//# sourceMappingURL=branch-company.model.js.map