import { GroupBranchEntity } from '../../domain/entities/group-branch.entity';
import { Branch } from './branch.model';
export declare class GroupBranch implements GroupBranchEntity {
    id: string;
    gid: string;
    code: string;
    name: string;
    amount_sites: number;
    group_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    branches?: Branch[];
}
