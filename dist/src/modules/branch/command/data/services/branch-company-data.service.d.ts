import { Repository } from 'typeorm';
import { BranchCompanyDataService } from '../../domain/contracts/branch-company-data-service.contract';
import { BranchCompanyEntity } from '../../domain/entities/branch-company.entity';
import { BranchCompany } from '../models/branch-company.model';
export declare class BranchCompanyDataServiceImpl implements BranchCompanyDataService {
    private branchCompanyRepo;
    protected relations: string[];
    constructor(branchCompanyRepo: Repository<BranchCompany>);
    save(data: BranchCompanyEntity): Promise<BranchCompanyEntity>;
    findOne(id: string): Promise<BranchCompanyEntity>;
}
