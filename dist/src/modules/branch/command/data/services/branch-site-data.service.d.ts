import { BaseDataService } from 'src';
import { DeleteResult, Repository } from 'typeorm';
import { BranchSiteEntity } from '../../domain/entities/branch-site.entity';
import { BranchSite } from '../models/branch-site.model';
export declare class BranchSiteDataServiceImpl extends BaseDataService<BranchSiteEntity> {
    private branchSiteRepo;
    constructor(branchSiteRepo: Repository<BranchSite>);
    deleteBySiteId(ids: string): Promise<DeleteResult>;
    findBySiteId(id: string): Promise<BranchSiteEntity>;
    findByBranchId(id: string): Promise<BranchSiteEntity[]>;
}
