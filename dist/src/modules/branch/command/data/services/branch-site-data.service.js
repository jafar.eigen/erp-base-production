"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchSiteDataServiceImpl = void 0;
const src_1 = require("src");
const typeorm_1 = require("@nestjs/typeorm");
const database_config_1 = require("src/utils/database.config");
const typeorm_2 = require("typeorm");
const branch_site_model_1 = require("../models/branch-site.model");
let BranchSiteDataServiceImpl = class BranchSiteDataServiceImpl extends src_1.BaseDataService {
    constructor(branchSiteRepo) {
        super(branchSiteRepo);
        this.branchSiteRepo = branchSiteRepo;
    }
    async deleteBySiteId(ids) {
        return await this.branchSiteRepo.delete({ site_id: ids });
    }
    async findBySiteId(id) {
        return await this.branchSiteRepo.findOne({
            where: { site_id: id }
        });
    }
    async findByBranchId(id) {
        return await this.branchSiteRepo.find({
            where: { branch_id: id },
            relations: ['branches']
        });
    }
};
BranchSiteDataServiceImpl = __decorate([
    __param(0, (0, typeorm_1.InjectRepository)(branch_site_model_1.BranchSite, database_config_1.BRANCH_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], BranchSiteDataServiceImpl);
exports.BranchSiteDataServiceImpl = BranchSiteDataServiceImpl;
//# sourceMappingURL=branch-site-data.service.js.map