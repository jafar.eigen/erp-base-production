import { Repository } from 'typeorm';
import { BaseDataService } from 'src';
import { BranchEntity } from '../../domain/entities/branch.entity';
import { Branch } from '../models/branch.model';
import { BranchSite } from '../models/branch-site.model';
import { BranchSiteEntity } from '../../domain/entities/branch-site.entity';
export declare class BranchDataServiceImpl extends BaseDataService<BranchEntity> {
    private branchRepo;
    private branchSiteRepo;
    relations: string[];
    constructor(branchRepo: Repository<Branch>, branchSiteRepo: Repository<BranchSite>);
    updateAmountSite(branch_id: string, amount_sites: number): Promise<void>;
    findBranchSiteByBranchId(branch_id: string): Promise<BranchSiteEntity[]>;
}
