import { BranchCompanyEntity } from '../entities/branch-company.entity';
export interface BranchCompanyDataService {
    save(data: BranchCompanyEntity): Promise<BranchCompanyEntity>;
    findOne(id: string): Promise<BranchCompanyEntity>;
}
