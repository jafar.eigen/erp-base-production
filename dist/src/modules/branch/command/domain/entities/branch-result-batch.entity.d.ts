export interface BranchResultBatch {
    success: string[];
    failed: string[];
}
