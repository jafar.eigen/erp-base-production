import { RequestInfo, BaseEntity } from 'src';
import { BranchCompanyEntity } from './branch-company.entity';
import { BranchSiteEntity } from './branch-site.entity';
export interface BranchEntity extends BaseEntity {
    id: string;
    name: string;
    amount_sites: number;
    group_name: string;
    group_id: string;
    request_info: RequestInfo;
    creator_id: string;
    creator_name: string;
    editor_name?: string;
    deleted_by_name?: string;
    companies?: BranchCompanyEntity[];
    sites?: BranchSiteEntity[];
}
