import { BranchEntity } from './branch.entity';
export interface GroupBranchEntity {
    id: string;
    gid: string;
    code: string;
    name: string;
    group_name: string;
    amount_sites: number;
    branches?: BranchEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
