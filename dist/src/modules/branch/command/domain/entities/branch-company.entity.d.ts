import { BranchEntity } from './branch.entity';
export interface BranchCompanyEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    branches: BranchEntity[];
}
