"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const branch_company_data_service_1 = require("../../data/services/branch-company-data.service");
const branch_data_service_1 = require("../../data/services/branch-data.service");
const branch_producer_service_1 = require("../../infrastructure/producers/branch-producer.service");
const activate_branch_manager_1 = require("./managers/company/activate-branch.manager");
const approve_branch_request_manager_1 = require("./managers/company/approve-branch-request.manager");
const cancel_branch_request_manager_1 = require("./managers/company/cancel-branch-request.manager");
const deactivate_branch_manager_1 = require("./managers/company/deactivate-branch.manager");
const decline_branch_request_manager_1 = require("./managers/company/decline-branch-request.manager");
const delete_branch_manager_1 = require("./managers/company/delete-branch.manager");
const submit_branch_request_manager_1 = require("./managers/company/submit-branch-request.manager");
const update_branch_manager_1 = require("./managers/company/update-branch.manager");
const create_branch_manager_1 = require("./managers/create-branch.manager");
let BranchOrchestrator = class BranchOrchestrator extends src_1.BaseOrchestrator {
    constructor(branchDataService, branchCompanyDataService, branchProducerService) {
        super();
        this.branchDataService = branchDataService;
        this.branchCompanyDataService = branchCompanyDataService;
        this.branchProducerService = branchProducerService;
    }
    async create(branch) {
        return await new create_branch_manager_1.CreateBranchManager(this.branchDataService, this.branchCompanyDataService, this.branchProducerService, branch).execute();
    }
    async request(branchId) {
        const submitRequest = await new submit_branch_request_manager_1.SubmitBranchRequestManager(branchId, this.branchDataService, this.branchProducerService);
        await submitRequest.execute();
        return submitRequest.getResult();
    }
    async update(branchId, updatedData) {
        const result = await new update_branch_manager_1.UpdateBranchManager(this.branchDataService, this.branchCompanyDataService, this.branchProducerService, branchId, updatedData).execute();
        return result;
    }
    async approve(branchId, step) {
        return await new approve_branch_request_manager_1.ApproveBranchRequestManager(this.branchDataService, this.branchProducerService, branchId, step).execute();
    }
    async decline(branchId, step) {
        return await new decline_branch_request_manager_1.DeclinedBranchRequestManager(this.branchDataService, this.branchProducerService, branchId, step).execute();
    }
    async activate(branchIds) {
        const activateManager = new activate_branch_manager_1.ActivateBranchManager(branchIds, this.branchDataService, this.branchProducerService);
        await activateManager.execute();
        return activateManager.getResult();
    }
    async deactivate(branchIds) {
        const deactivateManager = new deactivate_branch_manager_1.DeactivateBranchManager(branchIds, this.branchDataService, this.branchProducerService);
        await deactivateManager.execute();
        return deactivateManager.getResult();
    }
    async cancel(branchId) {
        return await new cancel_branch_request_manager_1.CancelBranchRequestManager(branchId, this.branchDataService, this.branchProducerService).execute();
    }
    async delete(branchIds) {
        const deleteManager = new delete_branch_manager_1.DeleteBranchManager(branchIds, this.branchDataService, this.branchProducerService);
        await deleteManager.execute();
        return deleteManager.getResult();
    }
    async rollbackStatusTrx(entityId) {
        throw new Error('Method not implemented.');
    }
};
BranchOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [branch_data_service_1.BranchDataServiceImpl,
        branch_company_data_service_1.BranchCompanyDataServiceImpl,
        branch_producer_service_1.BranchProducerServiceImpl])
], BranchOrchestrator);
exports.BranchOrchestrator = BranchOrchestrator;
//# sourceMappingURL=branch.orchestrator.js.map