import { BranchCompanyDataServiceImpl } from '../../../data/services/branch-company-data.service';
import { BranchDataServiceImpl } from '../../../data/services/branch-data.service';
import { BranchEntity } from '../../entities/branch.entity';
export declare class ValidatorManager {
    private branchDataService;
    private branchCompanyDataService;
    private branch;
    private id;
    constructor(branchDataService: BranchDataServiceImpl, branchCompanyDataService: BranchCompanyDataServiceImpl, branch: BranchEntity, id?: string);
    execute(): Promise<void | Error>;
}
