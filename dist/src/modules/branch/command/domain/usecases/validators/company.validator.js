"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Company = void 0;
class Company {
    constructor(branchCompanyDataService, data) {
        this.branchCompanyDataService = branchCompanyDataService;
        this.data = data;
    }
    async execute() {
        await this.setData();
        await this.getData();
    }
    setData() {
        this.companyData = this.data.companies;
    }
    async getData() {
        await Promise.all(this.companyData.map(async (company) => {
            const transactionData = await this.branchCompanyDataService.findOne(company.company_id);
            await this.validate(company, transactionData);
        }));
    }
    async validate(company, transactionData) {
        if (transactionData === undefined) {
            await this.generateData(company);
        }
    }
    async generateData(company) {
        Object.assign(company, { id: company === null || company === void 0 ? void 0 : company.company_id });
        await this.branchCompanyDataService.save(company);
    }
}
exports.Company = Company;
//# sourceMappingURL=company.validator.js.map