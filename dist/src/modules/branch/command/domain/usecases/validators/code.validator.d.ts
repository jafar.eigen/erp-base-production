import { BranchDataServiceImpl } from '../../../data/services/branch-data.service';
import { BranchEntity } from '../../entities/branch.entity';
export declare class Code {
    private branchDataService;
    private branch;
    private branchId;
    constructor(branchDataService: BranchDataServiceImpl, branch: BranchEntity, branchId?: string);
    execute(): Promise<void | Error>;
}
