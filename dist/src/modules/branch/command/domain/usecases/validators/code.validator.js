"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
const is_company_id_equal_1 = require("./is-company-id-equal");
class Code {
    constructor(branchDataService, branch, branchId = null) {
        this.branchDataService = branchDataService;
        this.branch = branch;
        this.branchId = branchId;
    }
    async execute() {
        const branch = [
            await this.branchDataService.findByCode(this.branch.code)
        ];
        if (branch) {
            await Promise.all(branch === null || branch === void 0 ? void 0 : branch.map(async (branchRes) => {
                var _a, _b;
                if (branchRes &&
                    !this.branchId &&
                    (0, is_company_id_equal_1.IsCompanyEqual)(branchRes, this.branch)) {
                    throw new Error(`Company ${this.branch.companies[0].company_name} already exists on branch code ${(_a = this.branch) === null || _a === void 0 ? void 0 : _a.name}.`);
                }
                if (this.branchId !== (branchRes === null || branchRes === void 0 ? void 0 : branchRes.id) &&
                    (0, is_company_id_equal_1.IsCompanyEqual)(this.branch, branchRes)) {
                    throw new Error(`Company ${this.branch.companies[0].company_name} already exists on branch code ${(_b = this.branch) === null || _b === void 0 ? void 0 : _b.name}.`);
                }
            }));
        }
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map