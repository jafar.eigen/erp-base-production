"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsCompanyEqual = void 0;
const IsCompanyEqual = (branchToCheck, branch) => {
    var _a, _b;
    const companyIdToCheck = (_a = branchToCheck === null || branchToCheck === void 0 ? void 0 : branchToCheck.companies[0]) === null || _a === void 0 ? void 0 : _a.company_id;
    const companyId = (_b = branch === null || branch === void 0 ? void 0 : branch.companies[0]) === null || _b === void 0 ? void 0 : _b.company_id;
    return companyIdToCheck === companyId;
};
exports.IsCompanyEqual = IsCompanyEqual;
//# sourceMappingURL=is-company-id-equal.js.map