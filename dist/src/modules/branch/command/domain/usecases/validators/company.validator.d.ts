import { BranchCompanyDataServiceImpl } from '../../../data/services/branch-company-data.service';
import { BranchCompanyEntity } from '../../entities/branch-company.entity';
import { BranchEntity } from '../../entities/branch.entity';
export declare class Company {
    private branchCompanyDataService;
    private data;
    constructor(branchCompanyDataService: BranchCompanyDataServiceImpl, data: BranchEntity);
    protected transactionData: BranchCompanyEntity;
    protected companyData: BranchCompanyEntity[];
    execute(): Promise<void>;
    protected setData(): void;
    protected getData(): Promise<void>;
    protected validate(company: BranchCompanyEntity, transactionData: BranchCompanyEntity): Promise<void>;
    protected generateData(company: BranchCompanyEntity): Promise<void>;
}
