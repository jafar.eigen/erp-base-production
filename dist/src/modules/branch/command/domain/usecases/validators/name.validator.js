"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
const is_company_id_equal_1 = require("./is-company-id-equal");
class Name {
    constructor(branchDataService, branch, branchId = null) {
        this.branchDataService = branchDataService;
        this.branch = branch;
        this.branchId = branchId;
    }
    async execute() {
        const branches = await this.branchDataService.findByName(this.branch.name);
        if (branches) {
            await Promise.all(branches === null || branches === void 0 ? void 0 : branches.map(async (branchRes) => {
                var _a, _b;
                if (branchRes &&
                    !this.branchId &&
                    (0, is_company_id_equal_1.IsCompanyEqual)(branchRes, this.branch)) {
                    throw new Error(`Company ${this.branch.companies[0].company_name} already exists on branch name ${(_a = this.branch) === null || _a === void 0 ? void 0 : _a.name}.`);
                }
                if (this.branchId !== (branchRes === null || branchRes === void 0 ? void 0 : branchRes.id) &&
                    (0, is_company_id_equal_1.IsCompanyEqual)(this.branch, branchRes)) {
                    throw new Error(`Company ${this.branch.companies[0].company_name} already exists on branch name ${(_b = this.branch) === null || _b === void 0 ? void 0 : _b.name}.`);
                }
            }));
        }
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map