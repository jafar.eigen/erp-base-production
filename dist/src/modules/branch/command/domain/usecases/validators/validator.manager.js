"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidatorManager = void 0;
const code_validator_1 = require("./code.validator");
const company_validator_1 = require("./company.validator");
const name_validator_1 = require("./name.validator");
class ValidatorManager {
    constructor(branchDataService, branchCompanyDataService, branch, id = null) {
        this.branchDataService = branchDataService;
        this.branchCompanyDataService = branchCompanyDataService;
        this.branch = branch;
        this.id = id;
    }
    async execute() {
        await new name_validator_1.Name(this.branchDataService, this.branch, this.id).execute();
        await new code_validator_1.Code(this.branchDataService, this.branch, this.id).execute();
        await new company_validator_1.Company(this.branchCompanyDataService, this.branch).execute();
    }
}
exports.ValidatorManager = ValidatorManager;
//# sourceMappingURL=validator.manager.js.map