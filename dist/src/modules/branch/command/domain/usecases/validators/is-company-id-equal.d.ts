import { BranchEntity } from '../../entities/branch.entity';
export declare const IsCompanyEqual: (branchToCheck: BranchEntity, branch: BranchEntity) => boolean;
