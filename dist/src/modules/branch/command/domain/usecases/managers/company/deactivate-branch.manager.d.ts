import { BaseResultBatch, DeactiveManager } from 'src';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../../entities/branch.entity';
export declare class DeactivateBranchManager extends DeactiveManager<BranchEntity> {
    result: BaseResultBatch;
    constructor(branchIds: string[], branchDataService: BranchDataServiceImpl, branchProducerService: BranchProducerServiceImpl);
    onSuccess(branchs: BranchEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
}
