import { CreateManager } from 'src';
import { BranchDataServiceImpl } from '../../../data/services/branch-data.service';
import { BranchProducerServiceImpl } from '../../../infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../entities/branch.entity';
import { BranchCompanyDataServiceImpl } from '../../../data/services/branch-company-data.service';
export declare class CreateBranchManager extends CreateManager<BranchEntity> {
    private branchDataService;
    private branchCompanyDataService;
    private branchProducerService;
    private branch;
    constructor(branchDataService: BranchDataServiceImpl, branchCompanyDataService: BranchCompanyDataServiceImpl, branchProducerService: BranchProducerServiceImpl, branch: BranchEntity);
    prepareData(): Promise<BranchEntity>;
    beforeProcess(): Promise<void>;
    protected validate(): Promise<void>;
}
