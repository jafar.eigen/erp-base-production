import { BaseActionManager } from 'src';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../../entities/branch.entity';
export declare class ApproveBranchRequestManager extends BaseActionManager<BranchEntity> {
    readonly dataService: BranchDataServiceImpl;
    readonly kafkaService: BranchProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: BranchDataServiceImpl, kafkaService: BranchProducerServiceImpl, id: string, step: number);
}
