"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclinedBranchRequestManager = void 0;
const src_1 = require("src");
class DeclinedBranchRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.DECLINED;
    }
}
exports.DeclinedBranchRequestManager = DeclinedBranchRequestManager;
//# sourceMappingURL=decline-branch-request.manager.js.map