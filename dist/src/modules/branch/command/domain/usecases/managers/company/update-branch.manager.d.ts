import { UpdateManager } from 'src';
import { BranchCompanyDataServiceImpl } from 'src/modules/branch/command/data/services/branch-company-data.service';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../../entities/branch.entity';
export declare class UpdateBranchManager extends UpdateManager<BranchEntity> {
    private branchDataService;
    private branchCompanyDataService;
    private branchProducerService;
    private id;
    private data;
    entityId: string;
    constructor(branchDataService: BranchDataServiceImpl, branchCompanyDataService: BranchCompanyDataServiceImpl, branchProducerService: BranchProducerServiceImpl, id: string, data: BranchEntity);
    beforeProcess(): Promise<void>;
    prepareData(): Promise<BranchEntity>;
    protected recalculateAmountSites(): Promise<void>;
    protected validate(): Promise<void>;
    afterProcess(entity: BranchEntity): Promise<void>;
}
