import { CancelManager } from 'src';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchEntity } from '../../../entities/branch.entity';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
export declare class CancelBranchRequestManager extends CancelManager<BranchEntity> {
    readonly branchId: string;
    readonly dataService: BranchDataServiceImpl;
    readonly kafkaService: BranchProducerServiceImpl;
    constructor(branchId: string, dataService: BranchDataServiceImpl, kafkaService: BranchProducerServiceImpl);
}
