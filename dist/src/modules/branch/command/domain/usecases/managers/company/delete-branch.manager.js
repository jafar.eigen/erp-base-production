"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteBranchManager = void 0;
const src_1 = require("src");
class DeleteBranchManager extends src_1.DeleteManager {
    constructor(branchIds, branchDataService, branchProducerService) {
        super(branchIds, branchDataService, branchProducerService);
        this.branchIds = branchIds;
        this.branchDataService = branchDataService;
        this.branchProducerService = branchProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(branchs) {
        branchs.forEach((branch) => {
            this.result.success.push(`branch with id ${branch.code} successfully deleted`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.DeleteBranchManager = DeleteBranchManager;
//# sourceMappingURL=delete-branch.manager.js.map