"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveBranchRequestManager = void 0;
const src_1 = require("src");
class ApproveBranchRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.APPROVED;
    }
}
exports.ApproveBranchRequestManager = ApproveBranchRequestManager;
//# sourceMappingURL=approve-branch-request.manager.js.map