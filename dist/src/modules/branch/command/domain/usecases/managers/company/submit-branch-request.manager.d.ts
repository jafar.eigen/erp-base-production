import { SubmitManager } from 'src';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../../entities/branch.entity';
export declare class SubmitBranchRequestManager extends SubmitManager<BranchEntity> {
    readonly dataService: BranchDataServiceImpl;
    readonly kafkaService: BranchProducerServiceImpl;
    result: BranchEntity;
    constructor(branchId: string, dataService: BranchDataServiceImpl, kafkaService: BranchProducerServiceImpl);
    onSuccess(branchs: BranchEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BranchEntity;
}
