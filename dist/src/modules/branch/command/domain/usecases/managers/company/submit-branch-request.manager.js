"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitBranchRequestManager = void 0;
const src_1 = require("src");
class SubmitBranchRequestManager extends src_1.SubmitManager {
    constructor(branchId, dataService, kafkaService) {
        super([branchId], dataService, kafkaService);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
    async onSuccess(branchs) {
        this.result = branchs[0];
    }
    async onFailed(messages) {
        if (messages.length > 0)
            throw new Error(messages[0]);
    }
    getResult() {
        return this.result;
    }
}
exports.SubmitBranchRequestManager = SubmitBranchRequestManager;
//# sourceMappingURL=submit-branch-request.manager.js.map