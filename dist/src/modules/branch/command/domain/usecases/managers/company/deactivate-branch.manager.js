"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeactivateBranchManager = void 0;
const src_1 = require("src");
class DeactivateBranchManager extends src_1.DeactiveManager {
    constructor(branchIds, branchDataService, branchProducerService) {
        super(branchIds, branchDataService, branchProducerService);
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(branchs) {
        branchs.forEach((branch) => {
            this.result.success.push(`branch with id ${branch.code} is not active.`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.DeactivateBranchManager = DeactivateBranchManager;
//# sourceMappingURL=deactivate-branch.manager.js.map