import { BaseResultBatch, DeleteManager } from 'src';
import { BranchDataServiceImpl } from 'src/modules/branch/command/data/services/branch-data.service';
import { BranchProducerServiceImpl } from 'src/modules/branch/command/infrastructure/producers/branch-producer.service';
import { BranchEntity } from '../../../entities/branch.entity';
export declare class DeleteBranchManager extends DeleteManager<BranchEntity> {
    private branchIds;
    private branchDataService;
    private branchProducerService;
    result: BaseResultBatch;
    constructor(branchIds: string[], branchDataService: BranchDataServiceImpl, branchProducerService: BranchProducerServiceImpl);
    onSuccess(branchs: BranchEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
}
