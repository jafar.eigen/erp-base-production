"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateBranchManager = void 0;
const src_1 = require("src");
const validator_manager_1 = require("../validators/validator.manager");
class CreateBranchManager extends src_1.CreateManager {
    constructor(branchDataService, branchCompanyDataService, branchProducerService, branch) {
        super(branchDataService, branchProducerService);
        this.branchDataService = branchDataService;
        this.branchCompanyDataService = branchCompanyDataService;
        this.branchProducerService = branchProducerService;
        this.branch = branch;
    }
    async prepareData() {
        await Promise.all(this.branch.companies.map(async (company) => {
            Object.assign(company, { id: company.company_id });
        }));
        return this.branch;
    }
    async beforeProcess() {
        await this.validate();
    }
    async validate() {
        await new validator_manager_1.ValidatorManager(this.branchDataService, this.branchCompanyDataService, this.branch).execute();
    }
}
exports.CreateBranchManager = CreateBranchManager;
//# sourceMappingURL=create-branch.manager.js.map