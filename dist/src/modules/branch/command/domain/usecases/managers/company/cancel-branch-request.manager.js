"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelBranchRequestManager = void 0;
const src_1 = require("src");
class CancelBranchRequestManager extends src_1.CancelManager {
    constructor(branchId, dataService, kafkaService) {
        super(branchId, dataService, kafkaService);
        this.branchId = branchId;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
}
exports.CancelBranchRequestManager = CancelBranchRequestManager;
//# sourceMappingURL=cancel-branch-request.manager.js.map