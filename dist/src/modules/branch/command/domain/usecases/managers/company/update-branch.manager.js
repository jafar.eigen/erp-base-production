"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateBranchManager = void 0;
const src_1 = require("src");
const validator_manager_1 = require("../../validators/validator.manager");
class UpdateBranchManager extends src_1.UpdateManager {
    constructor(branchDataService, branchCompanyDataService, branchProducerService, id, data) {
        super(branchDataService, branchProducerService);
        this.branchDataService = branchDataService;
        this.branchCompanyDataService = branchCompanyDataService;
        this.branchProducerService = branchProducerService;
        this.id = id;
        this.data = data;
        this.entityId = id;
    }
    async beforeProcess() {
        await this.validate();
    }
    async prepareData() {
        await Promise.all(this.data.companies.map(async (company) => {
            Object.assign(company, { id: company.company_id });
        }));
        await this.recalculateAmountSites();
        return this.data;
    }
    async recalculateAmountSites() {
        const branchSite = await this.branchDataService.findBranchSiteByBranchId(this.entityId);
        await this.branchDataService.updateAmountSite(this.entityId, branchSite.length);
    }
    async validate() {
        await new validator_manager_1.ValidatorManager(this.branchDataService, this.branchCompanyDataService, this.data, this.id).execute();
    }
    async afterProcess(entity) {
        return;
    }
}
exports.UpdateBranchManager = UpdateBranchManager;
//# sourceMappingURL=update-branch.manager.js.map