"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateBranchManager = void 0;
const src_1 = require("src");
class ActivateBranchManager extends src_1.ActiveManager {
    constructor(branchIds, branchDataService, branchProducerService) {
        super(branchIds, branchDataService, branchProducerService);
        this.branchIds = branchIds;
        this.branchDataService = branchDataService;
        this.branchProducerService = branchProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(branchs) {
        branchs.forEach((branch) => {
            this.result.success.push(`branch with id ${branch.code} successfully activated`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.ActivateBranchManager = ActivateBranchManager;
//# sourceMappingURL=activate-branch.manager.js.map