import { BaseOrchestrator } from 'src';
import { BranchCompanyDataServiceImpl } from '../../data/services/branch-company-data.service';
import { BranchDataServiceImpl } from '../../data/services/branch-data.service';
import { BranchProducerServiceImpl } from '../../infrastructure/producers/branch-producer.service';
import { BranchResultBatch } from '../entities/branch-result-batch.entity';
import { BranchEntity } from '../entities/branch.entity';
export declare class BranchOrchestrator extends BaseOrchestrator<BranchEntity> {
    private branchDataService;
    private branchCompanyDataService;
    private branchProducerService;
    constructor(branchDataService: BranchDataServiceImpl, branchCompanyDataService: BranchCompanyDataServiceImpl, branchProducerService: BranchProducerServiceImpl);
    create(branch: BranchEntity): Promise<BranchEntity>;
    request(branchId: string): Promise<BranchEntity>;
    update(branchId: string, updatedData: BranchEntity): Promise<BranchEntity>;
    approve(branchId: string, step: number): Promise<BranchEntity>;
    decline(branchId: string, step: number): Promise<BranchEntity>;
    activate(branchIds: string[]): Promise<BranchResultBatch>;
    deactivate(branchIds: string[]): Promise<BranchResultBatch>;
    cancel(branchId: string): Promise<BranchEntity>;
    delete(branchIds: string[]): Promise<BranchResultBatch>;
    rollbackStatusTrx(entityId: string[]): Promise<BranchEntity>;
}
