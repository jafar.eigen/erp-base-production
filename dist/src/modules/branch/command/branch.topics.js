"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HANDLE_BRANCH_DELETED = exports.BRANCH_ACTIVATED = exports.HANDLE_BRANCH_CHANGED = exports.HANDLE_BRANCH_CREATED = exports.MODULE_TOPIC_BRANCH = void 0;
exports.MODULE_TOPIC_BRANCH = 'BRANCH';
exports.HANDLE_BRANCH_CREATED = 'HANDLE_BRANCH_CREATED';
exports.HANDLE_BRANCH_CHANGED = 'HANDLE_BRANCH_CHANGED';
exports.BRANCH_ACTIVATED = 'BRANCH_ACTIVATED';
exports.HANDLE_BRANCH_DELETED = 'HANDLE_BRANCH_DELETED';
//# sourceMappingURL=branch.topics.js.map