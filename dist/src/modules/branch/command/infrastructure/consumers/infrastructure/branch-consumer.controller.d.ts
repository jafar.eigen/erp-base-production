import { Logger } from 'winston';
import { BaseProcessingConsumerController, ConsumerOption } from 'src';
import { BranchProducerServiceImpl } from '../../producers/branch-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
import { Repository } from 'typeorm';
import { BranchSite } from '../../../data/models/branch-site.model';
export declare class BranchConsumerControllerImpl extends BaseProcessingConsumerController {
    readonly logger: Logger;
    readonly clientSentry: SentryService;
    branchProducerService: BranchProducerServiceImpl;
    private branchSiteRepo;
    moduleName: string;
    constructor(logger: Logger, clientSentry: SentryService, branchProducerService: BranchProducerServiceImpl, branchSiteRepo: Repository<BranchSite>);
    getConsumerOptionsDefault(): ConsumerOption[];
    getConsumerOptions(): Promise<ConsumerOption[]>;
}
