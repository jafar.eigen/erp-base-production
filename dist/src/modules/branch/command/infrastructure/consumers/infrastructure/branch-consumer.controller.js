"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchConsumerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const src_1 = require("src");
const branch_topics_1 = require("src/modules/branch/command/branch.topics");
const branch_consumer_topic_1 = require("../branch-consumer.topic");
const branch_producer_service_1 = require("../../producers/branch-producer.service");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const branch_site_model_1 = require("../../../data/models/branch-site.model");
const database_config_1 = require("src/utils/database.config");
const branch_site_activated_option_1 = require("../options/site/branch-site-activated.option");
const branch_site_changed_option_1 = require("../options/site/branch-site-changed.option");
const branch_site_deleted_option_1 = require("../options/site/branch-site-deleted.option");
let BranchConsumerControllerImpl = class BranchConsumerControllerImpl extends src_1.BaseProcessingConsumerController {
    constructor(logger, clientSentry, branchProducerService, branchSiteRepo) {
        super(logger, clientSentry, branchProducerService);
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.branchProducerService = branchProducerService;
        this.branchSiteRepo = branchSiteRepo;
        this.moduleName = branch_topics_1.MODULE_TOPIC_BRANCH;
    }
    getConsumerOptionsDefault() {
        return [
            {
                topicName: branch_consumer_topic_1.HANDLE_SITE_ACTIVATED,
                repositories: [this.branchSiteRepo]
            },
            {
                topicName: branch_consumer_topic_1.HANDLE_SITE_ACTIVATED,
                repositories: [this.branchSiteRepo]
            }
        ];
    }
    async getConsumerOptions() {
        return [
            await new branch_site_activated_option_1.BranchSiteAcitvatedOption(branch_consumer_topic_1.HANDLE_SITE_ACTIVATED, [
                this.branchSiteRepo
            ]).getOption(),
            await new branch_site_changed_option_1.BranchSiteChangedOption(branch_consumer_topic_1.HANDLE_SITE_CHANGED, [this.branchSiteRepo], this.branchProducerService).getOption(),
            await new branch_site_deleted_option_1.BranchSiteDeletedOption(branch_consumer_topic_1.HANDLE_SITE_DELETED, [
                this.branchSiteRepo
            ]).getOption()
        ];
    }
};
BranchConsumerControllerImpl = __decorate([
    (0, common_1.Controller)('branch'),
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __param(3, (0, typeorm_1.InjectRepository)(branch_site_model_1.BranchSite, database_config_1.BRANCH_CUD_CONNECTION)),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        branch_producer_service_1.BranchProducerServiceImpl,
        typeorm_2.Repository])
], BranchConsumerControllerImpl);
exports.BranchConsumerControllerImpl = BranchConsumerControllerImpl;
//# sourceMappingURL=branch-consumer.controller.js.map