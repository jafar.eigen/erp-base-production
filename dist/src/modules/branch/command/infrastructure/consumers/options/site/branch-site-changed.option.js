"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchSiteChangedOption = void 0;
const src_1 = require("src");
class BranchSiteChangedOption extends src_1.BaseConsumerOption {
    constructor(topicName, repositories, producerService) {
        super(topicName, repositories);
        this.repositories = repositories;
        this.producerService = producerService;
    }
    async setConsumerOption() {
        return {
            topicName: this.topicName,
            repositories: this.repositories,
            transformData: this.transformData,
            processData: this.processData
        };
    }
    async transformData(consumerOption, message) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        const repository = consumerOption.repositories[0];
        const transactionData = await repository.findOne({
            where: { site_id: (_a = message.data) === null || _a === void 0 ? void 0 : _a.id }
        });
        return {
            id: transactionData === null || transactionData === void 0 ? void 0 : transactionData.id,
            branch_id: (_b = message.data) === null || _b === void 0 ? void 0 : _b.branch_id,
            site_id: (_c = message.data) === null || _c === void 0 ? void 0 : _c.id,
            site_name: (_d = message.data) === null || _d === void 0 ? void 0 : _d.name,
            site_code: (_e = message.data) === null || _e === void 0 ? void 0 : _e.code,
            company_id: (_g = (_f = message.data) === null || _f === void 0 ? void 0 : _f.companies[0]) === null || _g === void 0 ? void 0 : _g.company_id,
            company_name: (_j = (_h = message.data) === null || _h === void 0 ? void 0 : _h.companies[0]) === null || _j === void 0 ? void 0 : _j.company_name,
            company_code: (_l = (_k = message.data) === null || _k === void 0 ? void 0 : _k.companies[0]) === null || _l === void 0 ? void 0 : _l.company_code
        };
    }
    async processData(consumerOption, message) {
        const repository = consumerOption.repositories[0];
        if (message.branch_id !== '-') {
            if (message.branch_id === '' || null) {
                return await repository.delete({ site_id: message.branch_id });
            }
            else {
                return await repository.save(message);
            }
        }
    }
}
exports.BranchSiteChangedOption = BranchSiteChangedOption;
//# sourceMappingURL=branch-site-changed.option.js.map