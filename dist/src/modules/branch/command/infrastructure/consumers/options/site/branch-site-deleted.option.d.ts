import { BaseConsumerOption, ConsumerOption } from 'src';
import { BranchSiteEntity } from 'src/modules/branch/command/domain/entities/branch-site.entity';
import { Repository } from 'typeorm';
export declare class BranchSiteDeletedOption extends BaseConsumerOption {
    constructor(topicName: string, repositories: Repository<any>[]);
    setConsumerOption(): Promise<ConsumerOption>;
    transformData(value: any): Promise<Partial<BranchSiteEntity>>;
    processData(consumerOption: ConsumerOption, message: Partial<BranchSiteEntity>): Promise<any>;
}
