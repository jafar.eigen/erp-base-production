import { BaseConsumerOption, ConsumerOption } from 'src';
import { BranchSiteEntity } from 'src/modules/branch/command/domain/entities/branch-site.entity';
import { Repository } from 'typeorm';
export declare class BranchSiteAcitvatedOption extends BaseConsumerOption {
    constructor(topicName: string, repositories: Repository<any>[]);
    setConsumerOption(): Promise<ConsumerOption>;
    transformData(consumerOption: ConsumerOption, message: any): Promise<Partial<BranchSiteEntity>>;
    processData(consumerOption: ConsumerOption, value: Partial<BranchSiteEntity>): Promise<any>;
}
