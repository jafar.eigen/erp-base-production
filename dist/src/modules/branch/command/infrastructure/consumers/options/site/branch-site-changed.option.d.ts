import { BaseConsumerOption, ConsumerOption } from 'src';
import { Repository } from 'typeorm';
import { BranchSiteEntity } from '../../../../domain/entities/branch-site.entity';
import { BranchProducerServiceImpl } from '../../../producers/branch-producer.service';
export declare class BranchSiteChangedOption extends BaseConsumerOption {
    repositories: Repository<any>[];
    producerService: BranchProducerServiceImpl;
    constructor(topicName: string, repositories: Repository<any>[], producerService: BranchProducerServiceImpl);
    setConsumerOption(): Promise<ConsumerOption>;
    transformData(consumerOption: ConsumerOption, message: any): Promise<Partial<BranchSiteEntity>>;
    processData(consumerOption: ConsumerOption, message: Partial<BranchSiteEntity>): Promise<any>;
}
