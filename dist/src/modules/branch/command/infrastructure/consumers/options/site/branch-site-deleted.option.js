"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchSiteDeletedOption = void 0;
const src_1 = require("src");
class BranchSiteDeletedOption extends src_1.BaseConsumerOption {
    constructor(topicName, repositories) {
        super(topicName, repositories);
    }
    async setConsumerOption() {
        return {
            topicName: this.topicName,
            repositories: this.repositories,
            transformData: this.transformData,
            processData: this.processData
        };
    }
    async transformData(value) {
        var _a;
        return {
            id: (_a = value.old.id) !== null && _a !== void 0 ? _a : value.data.id
        };
    }
    async processData(consumerOption, message) {
        const repository = consumerOption.repositories[0];
        return await repository.delete({ site_id: message.id });
    }
}
exports.BranchSiteDeletedOption = BranchSiteDeletedOption;
//# sourceMappingURL=branch-site-deleted.option.js.map