"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchSiteAcitvatedOption = void 0;
const src_1 = require("src");
class BranchSiteAcitvatedOption extends src_1.BaseConsumerOption {
    constructor(topicName, repositories) {
        super(topicName, repositories);
    }
    async setConsumerOption() {
        return {
            topicName: this.topicName,
            repositories: this.repositories,
            transformData: this.transformData,
            processData: this.processData
        };
    }
    async transformData(consumerOption, message) {
        var _a, _b, _c, _d, _e, _f, _g;
        return {
            branch_id: (_a = message.data) === null || _a === void 0 ? void 0 : _a.branch_id,
            site_id: (_b = message.data) === null || _b === void 0 ? void 0 : _b.id,
            site_name: (_c = message.data) === null || _c === void 0 ? void 0 : _c.name,
            site_code: (_d = message.data) === null || _d === void 0 ? void 0 : _d.code,
            company_id: (_e = message.data) === null || _e === void 0 ? void 0 : _e.companies[0].company_id,
            company_name: (_f = message.data) === null || _f === void 0 ? void 0 : _f.companies[0].company_name,
            company_code: (_g = message.data) === null || _g === void 0 ? void 0 : _g.companies[0].company_code
        };
    }
    async processData(consumerOption, value) {
        if (value.branch_id !== '-') {
            await consumerOption.repositories[0].save(value);
        }
        return value;
    }
}
exports.BranchSiteAcitvatedOption = BranchSiteAcitvatedOption;
//# sourceMappingURL=branch-site-activated.option.js.map