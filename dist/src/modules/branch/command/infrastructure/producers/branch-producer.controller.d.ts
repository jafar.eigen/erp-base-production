import { BranchOrchestrator } from '../../domain/usecases/branch.orchestrator';
declare const BaseController: any;
export declare class BranchProducerControllerImpl extends BaseController {
    private branchOrchestrator;
    constructor(branchOrchestrator: BranchOrchestrator);
}
export {};
