import { ClientKafka } from '@nestjs/microservices';
import { BaseProducerServiceImpl } from 'src';
import { BranchEntity } from '../../domain/entities/branch.entity';
export declare class BranchProducerServiceImpl extends BaseProducerServiceImpl<BranchEntity> {
    client: ClientKafka;
    moduleName: string;
    TOPIC_CREATED: string;
    TOPIC_CHANGED: string;
    TOPIC_ACTIVATED: string;
    TOPIC_DELETED: string;
    constructor(client: ClientKafka);
    addNewTopics(): string[];
}
