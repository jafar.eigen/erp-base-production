"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchProducerServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const src_1 = require("src");
const branch_topics_1 = require("../../branch.topics");
const microservice_config_1 = require("src/utils/microservice.config");
let BranchProducerServiceImpl = class BranchProducerServiceImpl extends src_1.BaseProducerServiceImpl {
    constructor(client) {
        super(client);
        this.client = client;
        this.moduleName = branch_topics_1.MODULE_TOPIC_BRANCH;
        this.TOPIC_CREATED = branch_topics_1.HANDLE_BRANCH_CREATED;
        this.TOPIC_CHANGED = branch_topics_1.HANDLE_BRANCH_CHANGED;
        this.TOPIC_ACTIVATED = branch_topics_1.BRANCH_ACTIVATED;
        this.TOPIC_DELETED = branch_topics_1.HANDLE_BRANCH_DELETED;
    }
    addNewTopics() {
        return [];
    }
};
BranchProducerServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(microservice_config_1.KAFKA_CLIENT_NAME)),
    __metadata("design:paramtypes", [microservices_1.ClientKafka])
], BranchProducerServiceImpl);
exports.BranchProducerServiceImpl = BranchProducerServiceImpl;
//# sourceMappingURL=branch-producer.service.js.map