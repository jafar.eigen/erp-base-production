"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchProducerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const branch_orchestrator_1 = require("../../domain/usecases/branch.orchestrator");
const create_branch_dto_1 = require("../dto/create-branch.dto");
const update_branch_dto_1 = require("../dto/update-branch.dto");
const BaseController = (0, src_1.getBaseController)({
    createModelVm: create_branch_dto_1.CreateBranchDTO,
    updateModelVm: update_branch_dto_1.UpdateBranchDTO
});
let BranchProducerControllerImpl = class BranchProducerControllerImpl extends BaseController {
    constructor(branchOrchestrator) {
        super(new src_1.Responses(), branchOrchestrator, new src_1.JSONParse());
        this.branchOrchestrator = branchOrchestrator;
    }
};
BranchProducerControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('branches'),
    (0, common_1.Controller)('branches'),
    __metadata("design:paramtypes", [branch_orchestrator_1.BranchOrchestrator])
], BranchProducerControllerImpl);
exports.BranchProducerControllerImpl = BranchProducerControllerImpl;
//# sourceMappingURL=branch-producer.controller.js.map