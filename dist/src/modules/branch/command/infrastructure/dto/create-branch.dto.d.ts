import { STATUS, ApprovalPerCompany, RequestInfo } from 'src';
import { BranchEntity } from '../../domain/entities/branch.entity';
import { BranchCompanyDTO } from './branch-company.dto';
export declare class CreateBranchDTO implements BranchEntity {
    id: string;
    group_id: string;
    code: string;
    name: string;
    amount_sites: number;
    group_name: string;
    status: STATUS;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    request_info: RequestInfo;
    companies: BranchCompanyDTO[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
