"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchCompanyDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const src_1 = require("src");
class BranchCompanyDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], BranchCompanyDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.company_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], BranchCompanyDTO.prototype, "company_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], BranchCompanyDTO.prototype, "company_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(10),
    (0, class_transformer_1.Transform)((body) => body.value.toUpperCase()),
    __metadata("design:type", String)
], BranchCompanyDTO.prototype, "company_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enumName: 'Branch Company Status',
        enum: Object.values(src_1.STATUS)
    }),
    (0, class_validator_1.IsIn)(Object.values(src_1.STATUS)),
    __metadata("design:type", String)
], BranchCompanyDTO.prototype, "company_status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], BranchCompanyDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], BranchCompanyDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], BranchCompanyDTO.prototype, "deleted_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.branches),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Array)
], BranchCompanyDTO.prototype, "branches", void 0);
exports.BranchCompanyDTO = BranchCompanyDTO;
//# sourceMappingURL=branch-company.dto.js.map