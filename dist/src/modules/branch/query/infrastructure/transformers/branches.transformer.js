"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchesTransformer = void 0;
const moment = require("moment");
class BranchesTransformer {
    constructor(branches) {
        Object.assign(this, branches);
    }
    apply() {
        const branches = this.items.map((branch) => {
            return {
                id: branch.id,
                name: branch.name,
                code: branch.code,
                amount_sites: branch.sites.length,
                group_name: branch.group_name,
                group_id: branch.group_id,
                user_date: `${branch.editor_name},${moment(branch.updated_at).format('DD/MM/YY,HH:mm')}`,
                created_at: branch.created_at,
                updated_at: branch.updated_at,
                deleted_at: branch.deleted_at,
                status: branch.status,
                request_info: branch.request_info,
                approval: branch.approval,
                creator_id: branch.creator_id,
                creator_name: branch.creator_name,
                editor_id: branch.editor_id,
                editor_name: branch.editor_name,
                deleted_by_id: branch.deleted_by_id,
                deleted_by_name: branch.deleted_by_name,
                has_requested_process: branch.has_requested_process,
                requested_data: branch.requested_data,
                companies: branch.companies.map((company) => company),
                sites: branch.sites.map((sites) => sites)
            };
        });
        return {
            items: branches,
            meta: this.meta,
            links: this.links
        };
    }
}
exports.BranchesTransformer = BranchesTransformer;
//# sourceMappingURL=branches.transformer.js.map