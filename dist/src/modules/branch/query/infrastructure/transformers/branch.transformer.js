"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchTransformer = void 0;
const moment = require("moment");
class BranchTransformer {
    constructor(branch) {
        this.branch = branch;
    }
    transform() {
        return {
            id: this.branch.id,
            name: this.branch.name,
            code: this.branch.code,
            amount_sites: this.branch.sites.length,
            group_name: this.branch.group_name,
            group_id: this.branch.group_id,
            user_date: `${this.branch.editor_name},${moment(this.branch.updated_at).format('DD/MM/YY,HH:mm')}`,
            created_at: this.branch.created_at,
            updated_at: this.branch.updated_at,
            deleted_at: this.branch.deleted_at,
            status: this.branch.status,
            request_info: this.branch.request_info,
            approval: this.branch.approval,
            creator_id: this.branch.creator_id,
            creator_name: this.branch.creator_name,
            editor_id: this.branch.editor_id,
            editor_name: this.branch.editor_name,
            deleted_by_id: this.branch.deleted_by_id,
            deleted_by_name: this.branch.deleted_by_name,
            has_requested_process: this.branch.has_requested_process,
            requested_data: this.branch.requested_data,
            companies: this.branch.companies.map((company) => company),
            sites: this.branch.sites.map((site) => site)
        };
    }
}
exports.BranchTransformer = BranchTransformer;
//# sourceMappingURL=branch.transformer.js.map