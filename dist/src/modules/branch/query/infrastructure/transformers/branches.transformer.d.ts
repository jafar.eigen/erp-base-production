import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { IBranchesTransformer } from '../../domain/contracts/branches-transformer.contract';
import { BranchReadEntity } from '../../domain/entities/branch-read.entity';
export declare class BranchesTransformer implements IBranchesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: BranchReadEntity[];
    constructor(branches: Pagination<BranchReadEntity>);
    apply(): Pagination<Partial<BranchReadEntity>>;
}
