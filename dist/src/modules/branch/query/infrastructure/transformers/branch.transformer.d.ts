import { BranchCompanyRead } from '../../data/models/branch-read-company.model';
import { BranchReadSite } from '../../data/models/branch-read-site.model';
import { IBranchTransformer } from '../../domain/contracts/branch-transformer.contract';
import { BranchReadEntity } from '../../domain/entities/branch-read.entity';
export declare class BranchTransformer implements IBranchTransformer {
    private readonly branch;
    constructor(branch: BranchReadEntity);
    transform(): {
        id: string;
        name: string;
        code: string;
        amount_sites: number;
        group_name: string;
        group_id: string;
        user_date: string;
        created_at: Date;
        updated_at: Date;
        deleted_at: Date;
        status: string;
        request_info: string;
        approval: import("../../../../..").ApprovalPerCompany;
        creator_id: string;
        creator_name: string;
        editor_id: string;
        editor_name: string;
        deleted_by_id: string;
        deleted_by_name: string;
        has_requested_process: boolean;
        requested_data: any;
        companies: BranchCompanyRead[];
        sites: BranchReadSite[];
    };
}
