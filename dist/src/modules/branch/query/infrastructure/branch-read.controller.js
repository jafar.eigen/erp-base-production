"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchReadControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const branch_read_orchestrator_1 = require("../domain/usecases/branch-read.orchestrator");
const filter_branch_company_dto_1 = require("./dto/filter-branch-company.dto");
const filter_branch_dto_1 = require("./dto/filter-branch.dto");
const branch_transformer_1 = require("./transformers/branch.transformer");
const branches_transformer_1 = require("./transformers/branches.transformer");
let BranchReadControllerImpl = class BranchReadControllerImpl extends src_1.BaseReadController {
    constructor(responses, branchReadOrchestrator) {
        super();
        this.responses = responses;
        this.branchReadOrchestrator = branchReadOrchestrator;
    }
    async index(page, limit, params) {
        try {
            const branches = await this.branchReadOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new branches_transformer_1.BranchesTransformer(branches).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async showByCompany(page, limit, params) {
        try {
            const branches = await this.branchReadOrchestrator.getBranchesByCompany(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new branches_transformer_1.BranchesTransformer(branches).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async show(branchId) {
        try {
            const branch = await this.branchReadOrchestrator.show(branchId);
            return this.responses.json(common_1.HttpStatus.OK, new branch_transformer_1.BranchTransformer(branch).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_branch_dto_1.FilterBranchDTO]),
    __metadata("design:returntype", Promise)
], BranchReadControllerImpl.prototype, "index", null);
__decorate([
    (0, common_1.Get)('companies'),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_branch_company_dto_1.FilterBranchCompanyDTO]),
    __metadata("design:returntype", Promise)
], BranchReadControllerImpl.prototype, "showByCompany", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BranchReadControllerImpl.prototype, "show", null);
BranchReadControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('branch-indexes'),
    (0, common_1.Controller)('branches'),
    __metadata("design:paramtypes", [src_1.Responses,
        branch_read_orchestrator_1.BranchReadOrchestrator])
], BranchReadControllerImpl);
exports.BranchReadControllerImpl = BranchReadControllerImpl;
//# sourceMappingURL=branch-read.controller.js.map