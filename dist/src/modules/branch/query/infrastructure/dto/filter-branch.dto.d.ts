import { BranchFilterEntity } from '../../domain/entities/branch-filter.entity';
export declare class FilterBranchDTO implements BranchFilterEntity {
    q: string;
    statuses: string[];
    requests_info: string[];
    names: string[];
    codes: string[];
    companies: string[];
    company_id: string[];
    group_names: string[];
    updated_by: string[];
    last_update_from: string;
    last_update_to: string;
    approval_statuses: string[];
}
