import { BranchCompanyFilterEntity } from '../../domain/entities/branch-company-filter.entity';
export declare class FilterBranchCompanyDTO implements BranchCompanyFilterEntity {
    names: string[];
    codes: string[];
    companies: string[];
}
