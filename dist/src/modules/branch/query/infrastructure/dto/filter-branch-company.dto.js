"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterBranchCompanyDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class FilterBranchCompanyDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.names) ? body.names : [body.names];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by branch names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterBranchCompanyDTO.prototype, "names", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.codes) ? body.codes : [body.codes];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by branch codes',
        required: false
    }),
    __metadata("design:type", Array)
], FilterBranchCompanyDTO.prototype, "codes", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.companies),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by company ids',
        required: true
    }),
    __metadata("design:type", Array)
], FilterBranchCompanyDTO.prototype, "companies", void 0);
exports.FilterBranchCompanyDTO = FilterBranchCompanyDTO;
//# sourceMappingURL=filter-branch-company.dto.js.map