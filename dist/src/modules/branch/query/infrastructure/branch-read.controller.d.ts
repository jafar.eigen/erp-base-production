import { BaseReadController, IResponses, Responses } from 'src';
import { BranchReadOrchestrator } from '../domain/usecases/branch-read.orchestrator';
import { FilterBranchCompanyDTO } from './dto/filter-branch-company.dto';
import { FilterBranchDTO } from './dto/filter-branch.dto';
export declare class BranchReadControllerImpl extends BaseReadController<FilterBranchDTO> {
    private responses;
    private branchReadOrchestrator;
    constructor(responses: Responses, branchReadOrchestrator: BranchReadOrchestrator);
    index(page: number, limit: number, params: FilterBranchDTO): Promise<IResponses>;
    showByCompany(page: number, limit: number, params: FilterBranchCompanyDTO): Promise<IResponses>;
    show(branchId: string): Promise<IResponses>;
}
