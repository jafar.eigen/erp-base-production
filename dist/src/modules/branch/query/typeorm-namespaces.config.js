"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const config_2 = require("src/utils/config");
const branch_read_model_1 = require("src/modules/branch/query/data/models/branch-read.model");
const branch_read_company_model_1 = require("src/modules/branch/query/data/models/branch-read-company.model");
const branch_read_site_model_1 = require("src/modules/branch/query/data/models/branch-read-site.model");
const group_read_branch_model_1 = require("src/modules/branch/query/data/models/group-read-branch.model");
exports.default = (0, config_1.registerAs)('typeormBranchQuery', () => ({
    type: 'mysql',
    host: config_2.variableConfig.BRANCH_MYSQL_QUERY_HOST,
    port: config_2.variableConfig.BRANCH_MYSQL_QUERY_PORT,
    username: config_2.variableConfig.BRANCH_MYSQL_QUERY_USERNAME,
    password: config_2.variableConfig.BRANCH_MYSQL_QUERY_PASSWORD,
    database: config_2.variableConfig.BRANCH_MYSQL_QUERY_DATABASE,
    entities: [branch_read_model_1.BranchRead, branch_read_company_model_1.BranchCompanyRead, branch_read_site_model_1.BranchReadSite, group_read_branch_model_1.GroupBranchRead],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map