"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchCompanyFilter = void 0;
const branch_company_filter_entity_1 = require("../../entities/branch-company-filter.entity");
class BranchCompanyFilter extends branch_company_filter_entity_1.BranchCompanyFilterEntity {
    constructor(params) {
        super();
        this.names = params.names;
        this.codes = params.codes;
        this.companies = params.companies;
    }
    apply(query) {
        if (this.names)
            query = this.byNames(query);
        if (this.codes)
            query = this.byCodes(query);
        if (this.companies)
            query = this.byCompanies(query);
        return query;
    }
    byNames(query) {
        return query.andWhere('name IN (:...nameName)', {
            nameName: this.names
        });
    }
    byCodes(query) {
        return query.andWhere('code IN (:...codeName)', {
            codeName: this.codes
        });
    }
    byCompanies(query) {
        return query.andWhere('companies.company_id  IN (:...companies)', {
            companies: this.companies
        });
    }
}
exports.BranchCompanyFilter = BranchCompanyFilter;
//# sourceMappingURL=branch-company.filter.js.map