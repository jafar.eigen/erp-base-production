"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchFilter = void 0;
const branch_filter_entity_1 = require("../../entities/branch-filter.entity");
const moment = require("moment");
class BranchFilter extends branch_filter_entity_1.BranchFilterEntity {
    constructor(params) {
        super();
        this.q = params.q;
        this.statuses = params.statuses;
        this.requests_info = params.requests_info;
        this.names = params.names;
        this.codes = params.codes;
        this.companies = params.companies;
        this.company_id = params.company_id;
        this.group_names = params.group_names;
        this.updated_by = params.updated_by;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.approval_statuses = params.approval_statuses;
    }
    apply(query) {
        if (this.q)
            query = this.bySearch(query);
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.names)
            query = this.byNames(query);
        if (this.codes)
            query = this.byCodes(query);
        if (this.companies)
            query = this.byCompanies(query);
        if (this.company_id)
            query = this.byCompanyIds(query);
        if (this.group_names)
            query = this.byGroupNames(query);
        if (this.updated_by)
            query = this.byLastUpdateUser(query);
        if (this.last_update_from && this.last_update_to) {
            query = this.byLastUpdateDate(query);
        }
        if (this.approval_statuses)
            query = this.byApprovalStatuses(query);
        return query;
    }
    bySearch(query) {
        return query
            .orWhere('branches.name LIKE :branch_name', {
            branch_name: `%${this.q}%`
        })
            .orWhere('branches.code LIKE :branch_code', {
            branch_code: `%${this.q}%`
        })
            .orWhere('branches.group_name LIKE :group_name', {
            group_name: `%${this.q}%`
        })
            .orWhere('branches.creator_name LIKE :creator_name', {
            creator_name: `%${this.q}%`
        })
            .orWhere('branches.editor_name LIKE :editor_name', {
            editor_name: `%${this.q}%`
        });
    }
    byStatuses(query) {
        console.info(this.statuses);
        return query.andWhere('branches.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byRequestsInfo(query) {
        return query.andWhere('branches.request_info IN (:...requestsInfo)', {
            requestsInfo: this.requests_info
        });
    }
    byNames(query) {
        return query.andWhere('name IN (:...nameName)', {
            nameName: this.names
        });
    }
    byCodes(query) {
        return query.andWhere('code IN (:...codeName)', {
            codeName: this.codes
        });
    }
    byCompanies(query) {
        return query.andWhere('companies.company_id  IN (:...companies)', {
            companies: this.companies
        });
    }
    byCompanyIds(query) {
        return query.andWhere('companies.company_id  IN (:...company_ids)', {
            company_ids: this.company_id
        });
    }
    byGroupNames(query) {
        return query.andWhere('group_name IN (:...groupName)', {
            groupName: this.group_names
        });
    }
    byLastUpdateUser(query) {
        return query.andWhere('branches.editor_name IN (:...byName)', {
            byName: this.updated_by
        });
    }
    byLastUpdateDate(query) {
        return query.andWhere('branches.updated_at BETWEEN :from AND :to', {
            from: moment(new Date(this.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
            to: moment(new Date(this.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
        });
    }
    byApprovalStatuses(query) {
        const maxLength = this.approval_statuses.length;
        let queries = '';
        if (!Array.isArray(this.approval_statuses)) {
            return query.andWhere(this.getQueryApprovalStatus(this.approval_statuses));
        }
        for (let inc = 0; inc < maxLength; inc++) {
            if (inc === this.approval_statuses.length - 1) {
                queries += this.getQueryApprovalStatus(this.approval_statuses[inc]);
            }
            else {
                queries += `${this.getQueryApprovalStatus(this.approval_statuses[inc])} OR `;
            }
        }
        return query.andWhere(`${queries}`);
    }
    getQueryApprovalStatus(query) {
        switch (query) {
            case 'approved':
                return `branches.has_requested_process = 0 AND branches.approval LIKE '%"is_done": true%'`;
            case 'declined':
                return `branches.has_requested_process = 0 AND branches.approval LIKE '%"has_decline": true%'`;
            default:
                return `branches.has_requested_process = 1 AND branches.approval LIKE '%"has_decline": false%' AND branches.approval LIKE '%"is_done": false%'`;
        }
    }
}
exports.BranchFilter = BranchFilter;
//# sourceMappingURL=branch.filter.js.map