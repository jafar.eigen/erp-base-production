import { SelectQueryBuilder } from 'typeorm';
import { BranchReadEntity } from '../../entities/branch-read.entity';
import { BranchCompanyFilterEntity } from '../../entities/branch-company-filter.entity';
import { FilterBranchCompanyDTO } from '../../../infrastructure/dto/filter-branch-company.dto';
export declare class BranchCompanyFilter extends BranchCompanyFilterEntity {
    constructor(params: FilterBranchCompanyDTO);
    apply(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byNames(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byCodes(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byCompanies(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
}
