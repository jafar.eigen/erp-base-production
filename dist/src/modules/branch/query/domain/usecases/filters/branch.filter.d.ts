import { SelectQueryBuilder } from 'typeorm';
import { FilterBranchDTO } from '../../../infrastructure/dto/filter-branch.dto';
import { BranchFilterEntity } from '../../entities/branch-filter.entity';
import { BranchReadEntity } from '../../entities/branch-read.entity';
export declare class BranchFilter extends BranchFilterEntity {
    constructor(params: FilterBranchDTO);
    apply(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected bySearch(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byStatuses(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byRequestsInfo(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byNames(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byCodes(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byCompanies(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byCompanyIds(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byGroupNames(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byLastUpdateUser(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byLastUpdateDate(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected byApprovalStatuses(query: SelectQueryBuilder<BranchReadEntity>): SelectQueryBuilder<BranchReadEntity>;
    protected getQueryApprovalStatus(query: string): string;
}
