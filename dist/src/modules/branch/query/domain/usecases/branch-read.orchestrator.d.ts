import { Pagination } from 'nestjs-typeorm-paginate';
import { BranchReadDataServiceImpl } from '../../data/services/branch-read-data.service';
import { FilterBranchCompanyDTO } from '../../infrastructure/dto/filter-branch-company.dto';
import { FilterBranchDTO } from '../../infrastructure/dto/filter-branch.dto';
import { BranchReadEntity } from '../entities/branch-read.entity';
import { FilterOption } from 'src';
export declare class BranchReadOrchestrator {
    private branchReadDataService;
    constructor(branchReadDataService: BranchReadDataServiceImpl);
    show(branchId: string): Promise<BranchReadEntity>;
    index(page: number, limit: number, params: FilterBranchDTO): Promise<Pagination<BranchReadEntity>>;
    protected byApprovalStatuses(approval_statuses: any): [FilterOption[]];
    protected getQueryApprovalStatus(query: string): FilterOption[];
    getBranchesByCompany(page: number, limit: number, params: FilterBranchCompanyDTO): Promise<Pagination<BranchReadEntity>>;
}
