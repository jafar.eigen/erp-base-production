"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchReadOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const branch_read_data_service_1 = require("../../data/services/branch-read-data.service");
const branch_company_filter_1 = require("./filters/branch-company.filter");
const src_1 = require("src");
const moment_1 = require("moment");
let BranchReadOrchestrator = class BranchReadOrchestrator {
    constructor(branchReadDataService) {
        this.branchReadDataService = branchReadDataService;
    }
    async show(branchId) {
        return await this.branchReadDataService.findOneOrFail(branchId);
    }
    async index(page, limit, params) {
        const queryOption = {};
        if (params.q) {
            Object.assign(queryOption, {
                query: {
                    fields: [
                        'branches.name',
                        'branches.code',
                        'branches.group_name',
                        'branches.creator_name',
                        'branches.editor_name'
                    ],
                    value: params.q
                }
            });
        }
        const filterOption = [];
        if (params.statuses)
            filterOption.push([{ field: 'companies.status', value: params.statuses }]);
        if (params.requests_info)
            filterOption.push([
                { field: 'branches.request_info', value: params.requests_info }
            ]);
        if (params.names)
            filterOption.push([{ field: 'branches.name', value: params.names }]);
        if (params.codes)
            filterOption.push([{ field: 'branches.code', value: params.codes }]);
        if (params.companies)
            filterOption.push([
                { field: 'companies.company_id', value: params.companies }
            ]);
        if (params.company_id)
            filterOption.push([
                { field: 'companies.company_id', value: params.company_id }
            ]);
        if (params.group_names)
            filterOption.push([{ field: 'group_name', value: params.group_names }]);
        if (params.updated_by)
            filterOption.push([
                { field: 'branches.editor_name', value: params.updated_by }
            ]);
        if (params.last_update_from && params.last_update_to)
            filterOption.push([
                {
                    field: 'branches.updated_at',
                    operation: 'BETWEEN',
                    from: (0, moment_1.default)(new Date(params.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
                    to: (0, moment_1.default)(new Date(params.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
                }
            ]);
        if (filterOption.length > 0) {
            Object.assign(queryOption, {
                filters: filterOption
            });
        }
        const filter = new src_1.BaseFilter(queryOption);
        const result = await this.branchReadDataService.index({
            page: page,
            limit: limit,
            route: 'api/branches'
        }, {
            relations: ['companies', 'sites'],
            join: {
                alias: 'branches',
                leftJoin: {
                    companies: 'branches.companies',
                    sites: 'branches.sites'
                }
            },
            where: (queryBranch) => {
                filter.generate(queryBranch);
            },
            order: { created_at: 'DESC' }
        });
        return result;
    }
    byApprovalStatuses(approval_statuses) {
        let queries;
        if (!Array.isArray(approval_statuses)) {
            queries.push(this.getQueryApprovalStatus(approval_statuses));
        }
        for (const approvalStatus of approval_statuses) {
            queries.push(this.getQueryApprovalStatus(approvalStatus));
        }
        return queries;
    }
    getQueryApprovalStatus(query) {
        switch (query) {
            case 'approved':
                return [
                    { field: 'branches.has_requested_process', operation: '=', value: 0 },
                    {
                        field: 'branches.approval',
                        operation: 'LIKE',
                        value: '%"is_done": true%'
                    }
                ];
            case 'declined':
                return [
                    { field: 'branches.has_requested_process', operation: '=', value: 0 },
                    {
                        field: 'branches.approval',
                        operation: 'LIKE',
                        value: '%"has_decline": true%'
                    }
                ];
            default:
                return [
                    { field: 'branches.has_requested_process', operation: '=', value: 1 },
                    {
                        field: 'branches.approval',
                        operation: 'LIKE',
                        value: '%"has_decline": false%'
                    },
                    {
                        field: 'branches.approval',
                        operation: 'LIKE',
                        value: '%"is_done": false%'
                    }
                ];
        }
    }
    async getBranchesByCompany(page, limit, params) {
        const filter = new branch_company_filter_1.BranchCompanyFilter(params);
        return await this.branchReadDataService.findBranchByCompany({
            page: page,
            limit: limit,
            route: '/api/branches'
        }, {
            relations: ['companies', 'sites'],
            join: {
                alias: 'branches',
                leftJoin: {
                    companies: 'branches.companies',
                    sites: 'branches.sites'
                }
            },
            where: (queryBranch) => {
                filter.apply(queryBranch);
            },
            order: { created_at: 'DESC' }
        });
    }
};
BranchReadOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [branch_read_data_service_1.BranchReadDataServiceImpl])
], BranchReadOrchestrator);
exports.BranchReadOrchestrator = BranchReadOrchestrator;
//# sourceMappingURL=branch-read.orchestrator.js.map