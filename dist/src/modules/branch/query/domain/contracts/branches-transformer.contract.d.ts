import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { BranchReadEntity } from '../entities/branch-read.entity';
export interface IBranchesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: BranchReadEntity[];
    apply(): Pagination<Partial<BranchReadEntity>>;
}
