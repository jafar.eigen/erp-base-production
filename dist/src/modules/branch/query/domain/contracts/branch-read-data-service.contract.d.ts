import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { FindManyOptions } from 'typeorm';
import { BranchReadEntity } from '../entities/branch-read.entity';
export interface BranchReadDataService {
    findOneOrFail(branchId: string): Promise<BranchReadEntity>;
    index(options: IPaginationOptions, extraQuery: FindManyOptions<BranchReadEntity>): Promise<Pagination<BranchReadEntity>>;
    findBranchByCompany(options: IPaginationOptions, extraQuery: FindManyOptions<BranchReadEntity>): Promise<Pagination<BranchReadEntity>>;
}
