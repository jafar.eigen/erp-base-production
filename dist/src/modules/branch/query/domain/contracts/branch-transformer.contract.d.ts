import { BranchReadEntity } from '../entities/branch-read.entity';
export interface IBranchTransformer {
    transform(): Partial<BranchReadEntity>;
}
