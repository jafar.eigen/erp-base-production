export interface BranchReadSiteEntity {
    id: string;
    branch_id: string;
    site_id: string;
    site_name: string;
    site_code: string;
    company_id: string;
    company_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
