import { BranchReadEntity } from './branch-read.entity';
export interface GroupBranchReadEntity {
    id: string;
    gid: string;
    code: string;
    name: string;
    group_name: string;
    amount_sites: number;
    branches?: BranchReadEntity[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
