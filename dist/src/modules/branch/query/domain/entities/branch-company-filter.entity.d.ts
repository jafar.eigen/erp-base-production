export declare class BranchCompanyFilterEntity {
    names: string[];
    codes: string[];
    companies: string[];
}
