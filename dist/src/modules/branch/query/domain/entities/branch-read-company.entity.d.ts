import { BranchReadEntity } from './branch-read.entity';
export interface BranchReadCompanyEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    branches: BranchReadEntity[];
}
