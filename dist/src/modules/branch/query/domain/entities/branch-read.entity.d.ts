import { ApprovalPerCompany } from 'src';
import { BranchReadCompanyEntity } from './branch-read-company.entity';
import { BranchReadSiteEntity } from './branch-read-site.entity';
export interface BranchReadEntity {
    id: string;
    code: string;
    name: string;
    amount_sites: number;
    group_name: string;
    group_id: string;
    status: string;
    request_info: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    companies?: Partial<BranchReadCompanyEntity[]>;
    sites?: Partial<BranchReadSiteEntity[]>;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
