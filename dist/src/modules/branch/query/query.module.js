"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const database_config_1 = require("src/utils/database.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const branch_read_model_1 = require("./data/models/branch-read.model");
const group_read_branch_model_1 = require("./data/models/group-read-branch.model");
const branch_read_company_model_1 = require("./data/models/branch-read-company.model");
const branch_read_controller_1 = require("./infrastructure/branch-read.controller");
const branch_read_orchestrator_1 = require("./domain/usecases/branch-read.orchestrator");
const branch_read_data_service_1 = require("./data/services/branch-read-data.service");
const branch_read_site_model_1 = require("./data/models/branch-read-site.model");
let QueryModule = class QueryModule {
};
QueryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default],
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.BRANCH_READ_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormBranchQuery'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([branch_read_model_1.BranchRead, branch_read_company_model_1.BranchCompanyRead, branch_read_site_model_1.BranchReadSite, group_read_branch_model_1.GroupBranchRead], database_config_1.BRANCH_READ_CONNECTION)
        ],
        providers: [src_1.Responses, branch_read_orchestrator_1.BranchReadOrchestrator, branch_read_data_service_1.BranchReadDataServiceImpl],
        controllers: [branch_read_controller_1.BranchReadControllerImpl]
    })
], QueryModule);
exports.QueryModule = QueryModule;
//# sourceMappingURL=query.module.js.map