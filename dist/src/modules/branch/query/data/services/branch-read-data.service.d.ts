import { FindManyOptions, Repository } from 'typeorm';
import { BranchRead } from '../models/branch-read.model';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { BranchReadDataService } from '../../domain/contracts/branch-read-data-service.contract';
import { BranchReadEntity } from '../../domain/entities/branch-read.entity';
export declare class BranchReadDataServiceImpl implements BranchReadDataService {
    private branchReadRepo;
    constructor(branchReadRepo: Repository<BranchRead>);
    findOneOrFail(branchId: string): Promise<BranchReadEntity>;
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<BranchReadEntity>): Promise<Pagination<BranchReadEntity>>;
    findBranchByCompany(options: IPaginationOptions, extraQuery?: FindManyOptions<BranchReadEntity>): Promise<Pagination<BranchReadEntity>>;
}
