"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchReadDataServiceImpl = void 0;
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("@nestjs/typeorm");
const database_config_1 = require("src/utils/database.config");
const branch_read_model_1 = require("../models/branch-read.model");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
let BranchReadDataServiceImpl = class BranchReadDataServiceImpl {
    constructor(branchReadRepo) {
        this.branchReadRepo = branchReadRepo;
    }
    async findOneOrFail(branchId) {
        return await this.branchReadRepo.findOneOrFail(branchId, {
            relations: ['companies', 'sites']
        });
    }
    async index(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.branchReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.branchReadRepo, options, {
            relations: ['companies', 'sites']
        });
    }
    async findBranchByCompany(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.branchReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.branchReadRepo, options, {
            relations: ['companies', 'sites']
        });
    }
};
BranchReadDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(branch_read_model_1.BranchRead, database_config_1.BRANCH_READ_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], BranchReadDataServiceImpl);
exports.BranchReadDataServiceImpl = BranchReadDataServiceImpl;
//# sourceMappingURL=branch-read-data.service.js.map