"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchCompanyRead = void 0;
const typeorm_1 = require("typeorm");
const branch_read_model_1 = require("./branch-read.model");
let BranchCompanyRead = class BranchCompanyRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], BranchCompanyRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], BranchCompanyRead.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', length: '50' }),
    __metadata("design:type", String)
], BranchCompanyRead.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '10' }),
    __metadata("design:type", String)
], BranchCompanyRead.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_status' }),
    __metadata("design:type", String)
], BranchCompanyRead.prototype, "company_status", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompanyRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompanyRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchCompanyRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => branch_read_model_1.BranchRead, (branch) => branch.companies),
    (0, typeorm_1.JoinTable)({ name: 'branches_companies' }),
    __metadata("design:type", Array)
], BranchCompanyRead.prototype, "branches", void 0);
BranchCompanyRead = __decorate([
    (0, typeorm_1.Entity)('branch_companies')
], BranchCompanyRead);
exports.BranchCompanyRead = BranchCompanyRead;
//# sourceMappingURL=branch-read-company.model.js.map