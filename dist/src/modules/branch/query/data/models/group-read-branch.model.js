"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupBranchRead = void 0;
const typeorm_1 = require("typeorm");
const branch_read_model_1 = require("./branch-read.model");
let GroupBranchRead = class GroupBranchRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'gid' }),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "gid", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('integer', { name: 'sites', nullable: true }),
    __metadata("design:type", Number)
], GroupBranchRead.prototype, "amount_sites", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "group_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupBranchRead.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranchRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranchRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupBranchRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => branch_read_model_1.BranchRead, (branch) => branch.group),
    __metadata("design:type", Array)
], GroupBranchRead.prototype, "branches", void 0);
GroupBranchRead = __decorate([
    (0, typeorm_1.Entity)('group_branches')
], GroupBranchRead);
exports.GroupBranchRead = GroupBranchRead;
//# sourceMappingURL=group-read-branch.model.js.map