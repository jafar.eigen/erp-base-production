"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchReadSite = void 0;
const typeorm_1 = require("typeorm");
const branch_read_model_1 = require("./branch-read.model");
let BranchReadSite = class BranchReadSite {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], BranchReadSite.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_name', length: '50' }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "site_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_code', length: '50' }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "site_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', length: '50' }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', length: '10' }),
    __metadata("design:type", String)
], BranchReadSite.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchReadSite.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchReadSite.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchReadSite.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => branch_read_model_1.BranchRead, (branch) => branch.sites, {
        onDelete: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'branch_id' }),
    __metadata("design:type", branch_read_model_1.BranchRead)
], BranchReadSite.prototype, "branch", void 0);
BranchReadSite = __decorate([
    (0, typeorm_1.Entity)('branch_sites')
], BranchReadSite);
exports.BranchReadSite = BranchReadSite;
//# sourceMappingURL=branch-read-site.model.js.map