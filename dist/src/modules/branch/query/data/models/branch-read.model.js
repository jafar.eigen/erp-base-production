"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchRead = void 0;
const src_1 = require("src");
const typeorm_1 = require("typeorm");
const branch_read_company_model_1 = require("./branch-read-company.model");
const branch_read_site_model_1 = require("./branch-read-site.model");
const group_read_branch_model_1 = require("./group-read-branch.model");
let BranchRead = class BranchRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], BranchRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('integer', { name: 'sites', nullable: true }),
    __metadata("design:type", Number)
], BranchRead.prototype, "amount_sites", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "group_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.STATUS }),
    __metadata("design:type", String)
], BranchRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], BranchRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], BranchRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], BranchRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], BranchRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], BranchRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], BranchRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => branch_read_company_model_1.BranchCompanyRead, (group) => group.branches),
    __metadata("design:type", Array)
], BranchRead.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => branch_read_site_model_1.BranchReadSite, (branchSite) => branchSite.branch, {
        cascade: ['insert', 'soft-remove']
    }),
    __metadata("design:type", Array)
], BranchRead.prototype, "sites", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_read_branch_model_1.GroupBranchRead, (group) => group.branches),
    (0, typeorm_1.JoinColumn)({ name: 'group_id' }),
    __metadata("design:type", group_read_branch_model_1.GroupBranchRead)
], BranchRead.prototype, "group", void 0);
BranchRead = __decorate([
    (0, typeorm_1.Entity)('branches')
], BranchRead);
exports.BranchRead = BranchRead;
//# sourceMappingURL=branch-read.model.js.map