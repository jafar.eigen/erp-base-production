import { BranchReadCompanyEntity } from '../../domain/entities/branch-read-company.entity';
import { BranchRead } from './branch-read.model';
export declare class BranchCompanyRead implements BranchReadCompanyEntity {
    site_id: string;
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    branches: BranchRead[];
}
