import { GroupBranchReadEntity } from '../../domain/entities/group-read-branch.entity';
import { BranchRead } from './branch-read.model';
export declare class GroupBranchRead implements GroupBranchReadEntity {
    id: string;
    gid: string;
    code: string;
    name: string;
    amount_sites: number;
    group_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    branches?: BranchRead[];
}
