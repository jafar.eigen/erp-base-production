"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterSiteDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class FilterSiteDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.q),
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Search by: Branch name, Site name, Address, Phone, User'
    }),
    __metadata("design:type", String)
], FilterSiteDTO.prototype, "q", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.statuses),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Draft, Inactive, Active, Requested, Declined'
    }),
    __metadata("design:type", Array)
], FilterSiteDTO.prototype, "statuses", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requests_info),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Create Data, Edit Data, Edit Active, Edit Inactive, Edit Data & Status Active, Edit Data & Status Inactive, Delete Data'
    }),
    __metadata("design:type", Array)
], FilterSiteDTO.prototype, "requests_info", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.updated_by) ? body.updated_by : [body.updated_by];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        description: 'Find based on updated by',
        required: false
    }),
    __metadata("design:type", Array)
], FilterSiteDTO.prototype, "updated_by", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.companies) ? body.companies : [body.companies];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by company names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterSiteDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_code),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by group code',
        required: false
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterSiteDTO.prototype, "group_code", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.last_update_from),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update from',
        required: false
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterSiteDTO.prototype, "last_update_from", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.last_update_to),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update to',
        required: false
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterSiteDTO.prototype, "last_update_to", void 0);
exports.FilterSiteDTO = FilterSiteDTO;
//# sourceMappingURL=filter-site.dto.js.map