import { IResponses } from 'src';
import { Responses } from 'src';
import { SiteReadController } from '../domain/contracts/site-read-controller.contract';
import { GroupSiteOrchestrator } from '../domain/usecases/group-site.orchestrator';
import { SiteReadOrchestrator } from '../domain/usecases/site-read.orchestrator';
import { FilterGroupSiteDTO } from './dto/filter-group-site.dto';
import { FilterSiteDTO } from './dto/filter-site.dto';
export declare class SiteReadControllerImpl implements SiteReadController {
    private responses;
    private siteReadOrchestrator;
    private groupSiteOrchestrator;
    constructor(responses: Responses, siteReadOrchestrator: SiteReadOrchestrator, groupSiteOrchestrator: GroupSiteOrchestrator);
    index(page: number, limit: number, params: FilterSiteDTO): Promise<IResponses>;
    groups(page: number, limit: number, params: FilterGroupSiteDTO): Promise<IResponses>;
    showGroup(id: string): Promise<IResponses>;
    showByCompany(page: number, limit: number, companyId: string): Promise<IResponses>;
    show(siteId: string): Promise<IResponses>;
}
