"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteReadControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const group_site_orchestrator_1 = require("../domain/usecases/group-site.orchestrator");
const site_read_orchestrator_1 = require("../domain/usecases/site-read.orchestrator");
const filter_group_site_dto_1 = require("./dto/filter-group-site.dto");
const filter_site_dto_1 = require("./dto/filter-site.dto");
const group_site_transformer_1 = require("./transformer/group-site.transformer");
const group_sites_transformer_1 = require("./transformer/group-sites.transformer");
const site_transformer_1 = require("./transformer/site.transformer");
const sites_transformer_1 = require("./transformer/sites.transformer");
let SiteReadControllerImpl = class SiteReadControllerImpl {
    constructor(responses, siteReadOrchestrator, groupSiteOrchestrator) {
        this.responses = responses;
        this.siteReadOrchestrator = siteReadOrchestrator;
        this.groupSiteOrchestrator = groupSiteOrchestrator;
    }
    async index(page, limit, params) {
        try {
            const sites = await this.siteReadOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new sites_transformer_1.SitesTransformer(sites).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async groups(page, limit, params) {
        try {
            const groups = await this.groupSiteOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new group_sites_transformer_1.GroupSitesTransformer(groups).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async showGroup(id) {
        try {
            const group = await this.groupSiteOrchestrator.show(id);
            return this.responses.json(common_1.HttpStatus.OK, new group_site_transformer_1.GroupSiteTransformer(group).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async showByCompany(page, limit, companyId) {
        try {
            const sites = await this.siteReadOrchestrator.getSitesByCompany(page, limit, companyId);
            return this.responses.json(common_1.HttpStatus.OK, new sites_transformer_1.SitesTransformer(sites).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async show(siteId) {
        try {
            const site = await this.siteReadOrchestrator.show(siteId);
            return this.responses.json(common_1.HttpStatus.OK, new site_transformer_1.SiteTransformer(site).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_site_dto_1.FilterSiteDTO]),
    __metadata("design:returntype", Promise)
], SiteReadControllerImpl.prototype, "index", null);
__decorate([
    (0, common_1.Get)('/group'),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_group_site_dto_1.FilterGroupSiteDTO]),
    __metadata("design:returntype", Promise)
], SiteReadControllerImpl.prototype, "groups", null);
__decorate([
    (0, common_1.Get)('/group/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SiteReadControllerImpl.prototype, "showGroup", null);
__decorate([
    (0, common_1.Get)('/companies'),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)('company_id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], SiteReadControllerImpl.prototype, "showByCompany", null);
__decorate([
    (0, common_1.Get)('/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SiteReadControllerImpl.prototype, "show", null);
SiteReadControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('site-indexes'),
    (0, common_1.Controller)('site'),
    __metadata("design:paramtypes", [src_1.Responses,
        site_read_orchestrator_1.SiteReadOrchestrator,
        group_site_orchestrator_1.GroupSiteOrchestrator])
], SiteReadControllerImpl);
exports.SiteReadControllerImpl = SiteReadControllerImpl;
//# sourceMappingURL=site-read.controller.js.map