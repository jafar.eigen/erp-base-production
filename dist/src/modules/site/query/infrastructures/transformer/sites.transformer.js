"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SitesTransformer = void 0;
const moment = require("moment");
class SitesTransformer {
    constructor(sites) {
        Object.assign(this, sites);
    }
    apply() {
        const sites = this.items.map((site) => {
            return {
                id: site.id,
                name: site.name,
                code: site.code,
                status: site.status,
                postal_code: site.postal_code,
                latitude: site.latitude,
                longitude: site.longitude,
                address: site.address,
                city: site.city,
                country: site.country,
                sub_district: site.sub_district,
                province: site.province,
                hamlet: site.hamlet,
                village: site.village,
                website_url: site.website_url,
                request_info: site.request_info,
                user_date: `${moment(site.updated_at).format('DD/MM/YY,HH:mm')}`,
                approval: site.approval,
                creator_id: site.creator_id,
                creator_name: site.creator_name,
                editor_id: site.editor_id,
                editor_name: site.editor_name,
                deleted_by_id: site.deleted_by_id,
                deleted_by_name: site.deleted_by_name,
                has_requested_process: site.has_requested_process,
                requested_data: site.requested_data,
                branch_id: site.branch_id,
                branch_name: site.branch_name,
                branch_code: site.branch_code,
                is_generated: site.is_generated,
                is_head_office: site.is_head_office,
                levels: site.levels.map((level) => level),
                contacts: site.contacts.map((contact) => contact),
                companies: site.companies.map((company) => company)
            };
        });
        return {
            items: sites,
            meta: this.meta,
            links: this.links
        };
    }
}
exports.SitesTransformer = SitesTransformer;
//# sourceMappingURL=sites.transformer.js.map