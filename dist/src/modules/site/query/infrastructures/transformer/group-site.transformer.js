"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteTransformer = void 0;
const moment = require("moment");
class GroupSiteTransformer {
    constructor(group) {
        this.group = group;
    }
    transform() {
        const companyMembers = [];
        this.group.sites.forEach((site) => {
            site.companies.forEach((company) => {
                const isDuplicate = companyMembers.find((value) => value === company.company_name);
                if (!isDuplicate) {
                    companyMembers.push(company.company_name);
                }
            });
        });
        return {
            id: this.group.id,
            gid: this.group.gid,
            name: this.group.name,
            code: this.group.code,
            status: this.group.status,
            postal_code: this.group.postal_code,
            latitude: this.group.latitude,
            longitude: this.group.longitude,
            address: this.group.address,
            city: this.group.city,
            province: this.group.province,
            country: this.group.country,
            website_url: this.group.website_url,
            sub_district: this.group.sub_district,
            village: this.group.village,
            hamlet: this.group.hamlet,
            creator_id: this.group.creator_id,
            creator_name: this.group.editor_id,
            editor_id: this.group.editor_id,
            editor_name: this.group.editor_name,
            deleted_by_id: this.group.deleted_by_id,
            deleted_by_name: this.group.deleted_by_name,
            approval: this.group.approval,
            company_members: companyMembers,
            sites: this.group.sites,
            companies: this.group.companies,
            contacts: this.group.contacts,
            levels: this.group.levels,
            requested_data: this.group.requested_data,
            has_requested_process: this.group.has_requested_process,
            request_info: this.group.request_info,
            branch_id: this.group.branch_id,
            branch_name: this.group.branch_name,
            branch_code: this.group.branch_code,
            user_date: `${moment(this.group.updated_at).format('DD/MM/YY,HH:mm')}`,
            created_at: this.group.created_at,
            updated_at: this.group.updated_at,
            deleted_at: this.group.deleted_at
        };
    }
}
exports.GroupSiteTransformer = GroupSiteTransformer;
//# sourceMappingURL=group-site.transformer.js.map