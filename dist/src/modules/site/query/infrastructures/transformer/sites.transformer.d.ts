import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { ISitesTransformer } from '../../domain/contracts/sites-transformer.contract';
import { SiteEntity } from '../../domain/entities/site-read.entity';
export declare class SitesTransformer implements ISitesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: SiteEntity[];
    constructor(sites: Pagination<SiteEntity>);
    apply(): Pagination<Partial<SiteEntity>>;
}
