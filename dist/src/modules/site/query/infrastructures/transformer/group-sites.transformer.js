"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSitesTransformer = void 0;
const moment = require("moment");
class GroupSitesTransformer {
    constructor(sites) {
        Object.assign(this, sites);
    }
    apply() {
        const groups = this.items.map((group) => {
            const companyMembers = [];
            group.sites.forEach((site) => {
                site.companies.forEach((company) => {
                    const isDuplicate = companyMembers.find((value) => value === company.company_name);
                    if (!isDuplicate) {
                        companyMembers.push(company.company_name);
                    }
                });
            });
            return {
                id: group.id,
                gid: group.gid,
                name: group.name,
                code: group.code,
                status: group.status,
                postal_code: group.postal_code,
                latitude: group.latitude,
                longitude: group.longitude,
                address: group.address,
                city: group.city,
                province: group.province,
                country: group.country,
                website_url: group.website_url,
                sub_district: group.sub_district,
                village: group.village,
                hamlet: group.hamlet,
                creator_id: group.creator_id,
                creator_name: group.editor_id,
                editor_id: group.editor_id,
                editor_name: group.editor_name,
                deleted_by_id: group.deleted_by_id,
                deleted_by_name: group.deleted_by_name,
                approval: group.approval,
                sites: group.sites,
                company_members: companyMembers,
                companies: group.companies,
                contacts: group.contacts,
                levels: group.levels,
                requested_data: group.requested_data,
                has_requested_process: group.has_requested_process,
                request_info: group.request_info,
                branch_id: group.branch_id,
                branch_name: group.branch_name,
                branch_code: group.branch_code,
                user_date: `${moment(group.updated_at).format('DD/MM/YY,HH:mm')}`,
                created_at: group.created_at,
                updated_at: group.updated_at,
                deleted_at: group.deleted_at
            };
        });
        return {
            items: groups,
            meta: this.meta,
            links: this.links
        };
    }
}
exports.GroupSitesTransformer = GroupSitesTransformer;
//# sourceMappingURL=group-sites.transformer.js.map