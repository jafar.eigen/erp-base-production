import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { IGroupSitesTransformer } from '../../domain/contracts/group-sites-transformer.contract';
import { GroupSiteReadEntity } from '../../domain/entities/group-site-read.entity';
export declare class GroupSitesTransformer implements IGroupSitesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: GroupSiteReadEntity[];
    constructor(sites: Pagination<GroupSiteReadEntity>);
    apply(): Pagination<Partial<GroupSiteReadEntity>>;
}
