"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteTransformer = void 0;
const moment = require("moment");
class SiteTransformer {
    constructor(site) {
        this.site = site;
    }
    transform() {
        return {
            id: this.site.id,
            name: this.site.name,
            code: this.site.code,
            status: this.site.status,
            postal_code: this.site.postal_code,
            latitude: this.site.latitude,
            longitude: this.site.longitude,
            address: this.site.address,
            city: this.site.city,
            country: this.site.country,
            sub_district: this.site.sub_district,
            province: this.site.province,
            hamlet: this.site.hamlet,
            village: this.site.village,
            website_url: this.site.website_url,
            request_info: this.site.request_info,
            user_date: `${moment(this.site.updated_at).format('DD/MM/YY,HH:mm')}`,
            approval: this.site.approval,
            creator_id: this.site.creator_id,
            creator_name: this.site.creator_name,
            editor_id: this.site.editor_id,
            editor_name: this.site.editor_name,
            deleted_by_id: this.site.deleted_by_id,
            deleted_by_name: this.site.deleted_by_name,
            has_requested_process: this.site.has_requested_process,
            requested_data: this.site.requested_data,
            branch_id: this.site.branch_id,
            branch_name: this.site.branch_name,
            branch_code: this.site.branch_code,
            is_generated: this.site.is_generated,
            is_head_office: this.site.is_head_office,
            levels: this.site.levels.map((level) => level),
            contacts: this.site.contacts.map((contact) => contact),
            companies: this.site.companies.map((company) => company)
        };
    }
}
exports.SiteTransformer = SiteTransformer;
//# sourceMappingURL=site.transformer.js.map