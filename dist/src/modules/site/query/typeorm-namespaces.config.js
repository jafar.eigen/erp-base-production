"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const config_2 = require("src/utils/config");
const group_site_read_company_model_1 = require("./data/models/group-site-read-company.model");
const group_site_read_contact_model_1 = require("./data/models/group-site-read-contact.model");
const group_site_read_level_model_1 = require("./data/models/group-site-read-level.model");
const group_sites_read_model_1 = require("./data/models/group-sites-read.model");
const site_read_company_model_1 = require("./data/models/site-read-company.model");
const site_read_contact_model_1 = require("./data/models/site-read-contact.model");
const site_read_level_model_1 = require("./data/models/site-read-level.model");
const site_read_model_1 = require("./data/models/site-read.model");
exports.default = (0, config_1.registerAs)('typeormSiteQuery', () => ({
    type: 'mysql',
    host: config_2.variableConfig.SITE_MYSQL_QUERY_HOST,
    port: config_2.variableConfig.SITE_MYSQL_QUERY_PORT,
    username: config_2.variableConfig.SITE_MYSQL_QUERY_USERNAME,
    password: config_2.variableConfig.SITE_MYSQL_QUERY_PASSWORD,
    database: config_2.variableConfig.SITE_MYSQL_QUERY_DATABASE,
    entities: [
        site_read_model_1.SiteRead,
        site_read_company_model_1.SiteReadCompany,
        site_read_contact_model_1.SiteReadContact,
        site_read_level_model_1.SiteReadLevel,
        group_sites_read_model_1.GroupSiteRead,
        group_site_read_company_model_1.GroupSiteReadCompany,
        group_site_read_contact_model_1.GroupSiteReadContact,
        group_site_read_level_model_1.GroupSiteReadLevel
    ]
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map