"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const database_config_1 = require("src/utils/database.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const group_site_read_company_model_1 = require("./data/models/group-site-read-company.model");
const group_site_read_contact_model_1 = require("./data/models/group-site-read-contact.model");
const group_site_read_level_model_1 = require("./data/models/group-site-read-level.model");
const group_sites_read_model_1 = require("./data/models/group-sites-read.model");
const site_read_company_model_1 = require("./data/models/site-read-company.model");
const site_read_contact_model_1 = require("./data/models/site-read-contact.model");
const site_read_level_model_1 = require("./data/models/site-read-level.model");
const site_read_model_1 = require("./data/models/site-read.model");
const group_site_data_service_1 = require("./data/services/group-site-data.service");
const site_read_data_service_1 = require("./data/services/site-read-data.service");
const group_site_orchestrator_1 = require("./domain/usecases/group-site.orchestrator");
const site_read_orchestrator_1 = require("./domain/usecases/site-read.orchestrator");
const site_read_controller_1 = require("./infrastructures/site-read.controller");
let QueryModule = class QueryModule {
};
QueryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default],
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.SITE_READ_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormSiteQuery'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([
                site_read_model_1.SiteRead,
                site_read_company_model_1.SiteReadCompany,
                site_read_contact_model_1.SiteReadContact,
                site_read_level_model_1.SiteReadLevel,
                group_sites_read_model_1.GroupSiteRead,
                group_site_read_company_model_1.GroupSiteReadCompany,
                group_site_read_contact_model_1.GroupSiteReadContact,
                group_site_read_level_model_1.GroupSiteReadLevel
            ], database_config_1.SITE_READ_CONNECTION)
        ],
        providers: [
            site_read_orchestrator_1.SiteReadOrchestrator,
            group_site_orchestrator_1.GroupSiteOrchestrator,
            group_site_data_service_1.GroupSiteDataServiceImpl,
            site_read_data_service_1.SiteReadDataServiceImpl,
            src_1.Responses
        ],
        controllers: [site_read_controller_1.SiteReadControllerImpl]
    })
], QueryModule);
exports.QueryModule = QueryModule;
//# sourceMappingURL=query.module.js.map