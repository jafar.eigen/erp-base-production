import { FindManyOptions, Repository } from 'typeorm';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { GroupSiteDataService } from '../../domain/contracts/group-site-data-service.contract';
import { GroupSiteRead } from '../models/group-sites-read.model';
import { GroupSiteReadEntity } from '../../domain/entities/group-site-read.entity';
import { SiteRead } from '../models/site-read.model';
import { SiteEntity } from '../../domain/entities/site-read.entity';
export declare class GroupSiteDataServiceImpl implements GroupSiteDataService {
    private groupSite;
    private siteRepo;
    constructor(groupSite: Repository<GroupSiteRead>, siteRepo: Repository<SiteRead>);
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<GroupSiteReadEntity>): Promise<Pagination<GroupSiteReadEntity>>;
    findOneOrFail(id: string): Promise<GroupSiteReadEntity>;
    findSitesByGroupId(groupId: string): Promise<SiteEntity[]>;
}
