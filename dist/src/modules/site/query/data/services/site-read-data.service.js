"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteReadDataServiceImpl = void 0;
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("@nestjs/typeorm");
const database_config_1 = require("src/utils/database.config");
const site_read_model_1 = require("../models/site-read.model");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
let SiteReadDataServiceImpl = class SiteReadDataServiceImpl {
    constructor(siteReadRepo) {
        this.siteReadRepo = siteReadRepo;
    }
    async findOneOrFail(siteId) {
        return await this.siteReadRepo.findOneOrFail(siteId, {
            relations: ['companies', 'contacts', 'levels']
        });
    }
    async index(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.siteReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.siteReadRepo, options, {
            relations: ['companies', 'contacts', 'levels']
        });
    }
    async findSiteByCompany(options, companyId) {
        return (0, nestjs_typeorm_paginate_1.paginate)(this.siteReadRepo, options, {
            relations: ['companies', 'contacts', 'levels'],
            join: {
                alias: 'sites',
                leftJoin: {
                    contacts: 'sites.contacts',
                    levels: 'sites.levels',
                    companies: 'sites.companies'
                }
            },
            where: (querySite) => {
                querySite.where('companies.company_id = :companyId', { companyId });
            }
        });
    }
};
SiteReadDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(site_read_model_1.SiteRead, database_config_1.SITE_READ_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], SiteReadDataServiceImpl);
exports.SiteReadDataServiceImpl = SiteReadDataServiceImpl;
//# sourceMappingURL=site-read-data.service.js.map