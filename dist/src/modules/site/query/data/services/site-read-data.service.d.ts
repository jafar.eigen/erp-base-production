import { FindManyOptions, Repository } from 'typeorm';
import { SiteReadDataService } from '../../domain/contracts/site-read-data-service.contract';
import { SiteRead } from '../models/site-read.model';
import { SiteEntity } from '../../domain/entities/site-read.entity';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
export declare class SiteReadDataServiceImpl implements SiteReadDataService {
    private siteReadRepo;
    constructor(siteReadRepo: Repository<SiteRead>);
    findOneOrFail(siteId: string): Promise<SiteEntity>;
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<SiteEntity>): Promise<Pagination<SiteEntity>>;
    findSiteByCompany(options: IPaginationOptions, companyId: string): Promise<Pagination<SiteEntity>>;
}
