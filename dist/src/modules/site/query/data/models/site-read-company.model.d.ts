import { SiteCompanyEntity } from '../../domain/entities/site-read-company.entity';
import { SiteRead } from './site-read.model';
export declare class SiteReadCompany implements SiteCompanyEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    site: SiteRead;
}
