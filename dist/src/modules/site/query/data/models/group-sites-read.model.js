"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteRead = void 0;
const typeorm_1 = require("typeorm");
const site_read_entity_1 = require("../../domain/entities/site-read.entity");
const group_site_read_contact_model_1 = require("./group-site-read-contact.model");
const group_site_read_level_model_1 = require("./group-site-read-level.model");
const group_site_read_company_model_1 = require("./group-site-read-company.model");
const site_read_model_1 = require("./site-read.model");
let GroupSiteRead = class GroupSiteRead {
};
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_company_id', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "group_company_id", void 0);
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: site_read_entity_1.SiteStatus }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: site_read_entity_1.RequestInfo }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], GroupSiteRead.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], GroupSiteRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], GroupSiteRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], GroupSiteRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'branch_id', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "branch_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_code', length: '20', nullable: true }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "branch_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'gid' }),
    __metadata("design:type", String)
], GroupSiteRead.prototype, "gid", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_read_model_1.SiteRead, (site) => site.group),
    __metadata("design:type", Array)
], GroupSiteRead.prototype, "sites", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_read_level_model_1.GroupSiteReadLevel, (level) => level.group_site),
    __metadata("design:type", Array)
], GroupSiteRead.prototype, "levels", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_read_contact_model_1.GroupSiteReadContact, (contact) => contact.group_site, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupSiteRead.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_read_company_model_1.GroupSiteReadCompany, (company) => company.group_site, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupSiteRead.prototype, "companies", void 0);
GroupSiteRead = __decorate([
    (0, typeorm_1.Entity)('group_sites')
], GroupSiteRead);
exports.GroupSiteRead = GroupSiteRead;
//# sourceMappingURL=group-sites-read.model.js.map