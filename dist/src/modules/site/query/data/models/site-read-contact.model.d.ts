import { SiteContactEntity } from '../../domain/entities/site-read-contact.entity';
import { SiteRead } from './site-read.model';
export declare class SiteReadContact implements SiteContactEntity {
    id: string;
    site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    site: SiteRead;
}
