"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteRead = void 0;
const typeorm_1 = require("typeorm");
const site_read_entity_1 = require("../../domain/entities/site-read.entity");
const group_sites_read_model_1 = require("./group-sites-read.model");
const site_read_company_model_1 = require("./site-read-company.model");
const site_read_contact_model_1 = require("./site-read-contact.model");
const site_read_level_model_1 = require("./site-read-level.model");
let SiteRead = class SiteRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], SiteRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: site_read_entity_1.SiteStatus }),
    __metadata("design:type", String)
], SiteRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: site_read_entity_1.RequestInfo }),
    __metadata("design:type", String)
], SiteRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], SiteRead.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], SiteRead.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], SiteRead.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], SiteRead.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], SiteRead.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], SiteRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], SiteRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], SiteRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'branch_id', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "branch_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_code', length: '20', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "branch_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], SiteRead.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_generated', nullable: false, default: false }),
    __metadata("design:type", Boolean)
], SiteRead.prototype, "is_generated", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_head_office', nullable: true, default: false }),
    __metadata("design:type", Boolean)
], SiteRead.prototype, "is_head_office", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_read_level_model_1.SiteReadLevel, (siteLevel) => siteLevel.site, {
        cascade: ['insert', 'soft-remove']
    }),
    __metadata("design:type", Array)
], SiteRead.prototype, "levels", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_read_contact_model_1.SiteReadContact, (siteContact) => siteContact.site, {
        cascade: ['insert', 'soft-remove']
    }),
    __metadata("design:type", Array)
], SiteRead.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_read_company_model_1.SiteReadCompany, (siteCompany) => siteCompany.site, {
        cascade: ['insert', 'soft-remove']
    }),
    __metadata("design:type", Array)
], SiteRead.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_sites_read_model_1.GroupSiteRead, (group) => group.sites),
    (0, typeorm_1.JoinColumn)({ name: 'group_id' }),
    __metadata("design:type", group_sites_read_model_1.GroupSiteRead)
], SiteRead.prototype, "group", void 0);
SiteRead = __decorate([
    (0, typeorm_1.Entity)('sites')
], SiteRead);
exports.SiteRead = SiteRead;
//# sourceMappingURL=site-read.model.js.map