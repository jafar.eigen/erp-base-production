"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteReadLevel = void 0;
const typeorm_1 = require("typeorm");
const group_sites_read_model_1 = require("./group-sites-read.model");
let GroupSiteReadLevel = class GroupSiteReadLevel {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupSiteReadLevel.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'number' }),
    __metadata("design:type", Number)
], GroupSiteReadLevel.prototype, "number", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], GroupSiteReadLevel.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_site_id', length: '36', nullable: true }),
    __metadata("design:type", String)
], GroupSiteReadLevel.prototype, "group_site_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteReadLevel.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteReadLevel.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSiteReadLevel.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_sites_read_model_1.GroupSiteRead, (site) => site.levels, {
        onDelete: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'group_site_id' }),
    __metadata("design:type", group_sites_read_model_1.GroupSiteRead)
], GroupSiteReadLevel.prototype, "group_site", void 0);
GroupSiteReadLevel = __decorate([
    (0, typeorm_1.Entity)('group_site_levels')
], GroupSiteReadLevel);
exports.GroupSiteReadLevel = GroupSiteReadLevel;
//# sourceMappingURL=group-site-read-level.model.js.map