import { GroupSiteReadCompanyEntity } from '../../domain/entities/group-site-read-company.entity';
import { GroupSiteRead } from './group-sites-read.model';
export declare class GroupSiteReadCompany implements GroupSiteReadCompanyEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    group_site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSiteRead;
}
