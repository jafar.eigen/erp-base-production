import { GroupSiteReadContactEntity } from '../../domain/entities/group-site-read-contact.entity';
import { GroupSiteRead } from './group-sites-read.model';
export declare class GroupSiteReadContact implements Partial<GroupSiteReadContactEntity> {
    id: string;
    group_site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSiteRead;
}
