import { SiteLevelEntity } from '../../domain/entities/site-read-level.entity';
import { SiteRead } from './site-read.model';
export declare class SiteReadLevel implements SiteLevelEntity {
    id: string;
    number: number;
    name: string;
    site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    site: SiteRead;
}
