"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteReadLevel = void 0;
const typeorm_1 = require("typeorm");
const site_read_model_1 = require("./site-read.model");
let SiteReadLevel = class SiteReadLevel {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], SiteReadLevel.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'number' }),
    __metadata("design:type", Number)
], SiteReadLevel.prototype, "number", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], SiteReadLevel.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_id', length: '36' }),
    __metadata("design:type", String)
], SiteReadLevel.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteReadLevel.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteReadLevel.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], SiteReadLevel.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => site_read_model_1.SiteRead, (site) => site.levels, {
        onDelete: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'site_id' }),
    __metadata("design:type", site_read_model_1.SiteRead)
], SiteReadLevel.prototype, "site", void 0);
SiteReadLevel = __decorate([
    (0, typeorm_1.Entity)('site_levels')
], SiteReadLevel);
exports.SiteReadLevel = SiteReadLevel;
//# sourceMappingURL=site-read-level.model.js.map