import { GroupSiteReadLevelEntity } from '../../domain/entities/group-site-read-level.entity';
import { GroupSiteRead } from './group-sites-read.model';
export declare class GroupSiteReadLevel implements Partial<GroupSiteReadLevelEntity> {
    id: string;
    number: number;
    name: string;
    group_site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSiteRead;
}
