"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteReadOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const site_read_data_service_1 = require("../../data/services/site-read-data.service");
const site_filter_1 = require("./filters/site.filter");
let SiteReadOrchestrator = class SiteReadOrchestrator {
    constructor(siteReadDataService) {
        this.siteReadDataService = siteReadDataService;
    }
    async show(siteId) {
        return await this.siteReadDataService.findOneOrFail(siteId);
    }
    async index(page, limit, params) {
        const filter = new site_filter_1.SiteFilter(params);
        return await this.siteReadDataService.index({
            page: page,
            limit: limit,
            route: 'api/site'
        }, {
            relations: ['contacts', 'levels', 'companies'],
            join: {
                alias: 'sites',
                leftJoin: {
                    contacts: 'sites.contacts',
                    levels: 'sites.levels',
                    companies: 'sites.companies',
                    group: 'sites.group'
                }
            },
            where: (querySite) => {
                filter.apply(querySite);
            },
            order: { created_at: 'DESC' }
        });
    }
    async getSitesByCompany(page, limit, companyId) {
        return await this.siteReadDataService.findSiteByCompany({
            page: page,
            limit: limit,
            route: '/api/site'
        }, companyId);
    }
};
SiteReadOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_read_data_service_1.SiteReadDataServiceImpl])
], SiteReadOrchestrator);
exports.SiteReadOrchestrator = SiteReadOrchestrator;
//# sourceMappingURL=site-read.orchestrator.js.map