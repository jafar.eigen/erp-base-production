"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const group_site_data_service_1 = require("../../data/services/group-site-data.service");
const group_site_filter_1 = require("./filters/group-site.filter");
let GroupSiteOrchestrator = class GroupSiteOrchestrator {
    constructor(groupSiteReadDataService) {
        this.groupSiteReadDataService = groupSiteReadDataService;
    }
    async index(page, limit, params) {
        const filter = new group_site_filter_1.GroupSiteFilter(params);
        const groups = await this.groupSiteReadDataService.index({
            page: page,
            limit: limit,
            route: 'api/site/group'
        }, {
            relations: ['contacts', 'companies', 'levels'],
            join: {
                alias: 'group',
                leftJoinAndSelect: {
                    sites: 'group.sites',
                    levels: 'group.levels',
                    companies: 'group.companies'
                }
            },
            where: (groupSiteQuery) => {
                filter.apply(groupSiteQuery);
            }
        });
        const items = await Promise.all(groups.items.map(async (groupItem) => {
            groupItem.sites = await this.groupSiteReadDataService.findSitesByGroupId(groupItem.id);
            return groupItem;
        }));
        Object.assign(groups.items, {
            items
        });
        return groups;
    }
    async show(id) {
        const group = await this.groupSiteReadDataService.findOneOrFail(id);
        const sites = await this.groupSiteReadDataService.findSitesByGroupId(id);
        Object.assign(group, {
            sites: sites
        });
        return group;
    }
};
GroupSiteOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [group_site_data_service_1.GroupSiteDataServiceImpl])
], GroupSiteOrchestrator);
exports.GroupSiteOrchestrator = GroupSiteOrchestrator;
//# sourceMappingURL=group-site.orchestrator.js.map