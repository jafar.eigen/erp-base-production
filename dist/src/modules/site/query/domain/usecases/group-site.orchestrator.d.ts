import { Pagination } from 'nestjs-typeorm-paginate';
import { GroupSiteDataServiceImpl } from '../../data/services/group-site-data.service';
import { FilterGroupSiteDTO } from '../../infrastructures/dto/filter-group-site.dto';
import { GroupSiteReadEntity } from '../entities/group-site-read.entity';
export declare class GroupSiteOrchestrator {
    private groupSiteReadDataService;
    constructor(groupSiteReadDataService: GroupSiteDataServiceImpl);
    index(page: number, limit: number, params: FilterGroupSiteDTO): Promise<Pagination<GroupSiteReadEntity>>;
    show(id: string): Promise<GroupSiteReadEntity>;
}
