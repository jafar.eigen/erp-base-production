"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteFilter = void 0;
class GroupSiteFilter {
    constructor(params) {
        this.statuses = params.statuses;
        this.requests_info = params.requests_info;
        this.updated_by = params.updated_by;
        this.companies = params.companies;
        this.group_code = params.group_code;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.last_updated_by = params.last_updated_by;
    }
    apply(query) {
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.updated_by)
            query = this.byUpdated(query);
        if (this.companies)
            query = this.byCompanies(query);
        if (this.group_code)
            query = this.byGroupCode(query);
        if (this.last_update_from && this.last_update_to)
            query = this.byLastUpdateDate(query);
        if (this.last_updated_by)
            query = this.byLastUpdateUser(query);
        return query;
    }
    byStatuses(query) {
        return query.where('group.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byRequestsInfo(query) {
        return query.andWhere('group.request_info IN (:...requestsInfo)', {
            requestsInfo: this.requests_info
        });
    }
    byUpdated(query) {
        return query.andWhere('group.updated_by = :updatedBy', {
            updatedBy: this.updated_by
        });
    }
    byCompanies(query) {
        return query.andWhere('companies.name IN (:...companyName)', {
            companyName: this.companies
        });
    }
    byGroupCode(query) {
        return query.andWhere('group.code = :code', { code: this.group_code });
    }
    byLastUpdateDate(query) {
        return query.andWhere('group.updated_at BETWEEN :from AND :to', {
            from: this.last_update_from,
            to: this.last_update_to
        });
    }
    byLastUpdateUser(query) {
        return query.andWhere('group.editor_name IN (:...byName)', {
            byName: this.last_updated_by
        });
    }
}
exports.GroupSiteFilter = GroupSiteFilter;
//# sourceMappingURL=group-site.filter.js.map