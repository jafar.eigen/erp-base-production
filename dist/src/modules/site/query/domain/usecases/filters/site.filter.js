"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteFilter = void 0;
const moment = require("moment");
class SiteFilter {
    constructor(params) {
        this.q = params.q;
        this.statuses = params.statuses;
        this.requests_info = params.requests_info;
        this.companies = params.companies;
        this.group_code = params.group_code;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.last_updated_by = params.updated_by;
    }
    apply(query) {
        if (this.q)
            query = this.bySearch(query);
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.companies)
            query = this.byCompanies(query);
        if (this.group_code)
            query = this.byGroupCode(query);
        if (this.last_update_from && this.last_update_to)
            query = this.byLastUpdateDate(query);
        if (this.last_updated_by)
            query = this.byLastUpdateUser(query);
        return query;
    }
    bySearch(query) {
        return query
            .orWhere('sites.name LIKE :site_name', {
            site_name: `%${this.q}%`
        })
            .orWhere('sites.code LIKE :code', {
            code: `%${this.q}%`
        })
            .orWhere('sites.branch_name LIKE :branch_name', {
            branch_name: `%${this.q}%`
        })
            .orWhere('sites.address LIKE :address', {
            address: `%${this.q}%`
        })
            .orWhere('contacts.phone LIKE :phone', {
            phone: `%${this.q}%`
        })
            .orWhere('sites.creator_name LIKE :creator_name', {
            creator_name: `%${this.q}%`
        })
            .orWhere('sites.editor_name LIKE :editor_name', {
            editor_name: `%${this.q}%`
        })
            .orWhere('sites.updated_at LIKE :lastUpdate', {
            lastUpdate: `%${moment(new Date(this.q)).format('YYYY-MM-DD')}%`
        });
    }
    byStatuses(query) {
        return query.andWhere('sites.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byRequestsInfo(query) {
        return query.andWhere('sites.request_info IN (:...requestsInfo)', {
            requestsInfo: this.requests_info
        });
    }
    byCompanies(query) {
        return query.andWhere('companies.company_name IN (:...companyName)', {
            companyName: this.companies
        });
    }
    byGroupCode(query) {
        return query.orWhere('group.code = :code', { code: this.group_code });
    }
    byLastUpdateDate(query) {
        return query.andWhere('sites.updated_at BETWEEN :from AND :to', {
            from: moment(new Date(this.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
            to: moment(new Date(this.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
        });
    }
    byLastUpdateUser(query) {
        return query.andWhere('sites.editor_name IN (:...byName)', {
            byName: this.last_updated_by
        });
    }
}
exports.SiteFilter = SiteFilter;
//# sourceMappingURL=site.filter.js.map