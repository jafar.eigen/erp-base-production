import { SelectQueryBuilder } from 'typeorm';
import { FilterSiteDTO } from '../../../infrastructures/dto/filter-site.dto';
import { SiteEntity } from '../../entities/site-read.entity';
export declare class SiteFilter {
    private q;
    private statuses;
    private requests_info;
    private companies;
    private group_code;
    private last_update_from;
    private last_update_to;
    private last_updated_by;
    constructor(params: FilterSiteDTO);
    apply(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected bySearch(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byStatuses(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byRequestsInfo(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byCompanies(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byGroupCode(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byLastUpdateDate(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
    protected byLastUpdateUser(query: SelectQueryBuilder<SiteEntity>): SelectQueryBuilder<SiteEntity>;
}
