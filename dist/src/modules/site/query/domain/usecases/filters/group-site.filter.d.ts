import { SelectQueryBuilder } from 'typeorm';
import { FilterGroupSiteDTO } from '../../../infrastructures/dto/filter-group-site.dto';
import { GroupSiteReadEntity } from '../../entities/group-site-read.entity';
export declare class GroupSiteFilter {
    private statuses;
    private requests_info;
    private updated_by;
    private companies;
    private group_code;
    private last_update_from;
    private last_update_to;
    private last_updated_by;
    constructor(params: FilterGroupSiteDTO);
    apply(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byStatuses(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byRequestsInfo(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byUpdated(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byCompanies(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byGroupCode(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byLastUpdateDate(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
    byLastUpdateUser(query: SelectQueryBuilder<GroupSiteReadEntity>): SelectQueryBuilder<GroupSiteReadEntity>;
}
