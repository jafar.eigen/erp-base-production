import { Pagination } from 'nestjs-typeorm-paginate';
import { SiteReadDataServiceImpl } from '../../data/services/site-read-data.service';
import { FilterSiteDTO } from '../../infrastructures/dto/filter-site.dto';
import { SiteEntity } from '../entities/site-read.entity';
export declare class SiteReadOrchestrator {
    private siteReadDataService;
    constructor(siteReadDataService: SiteReadDataServiceImpl);
    show(siteId: string): Promise<SiteEntity>;
    index(page: number, limit: number, params: FilterSiteDTO): Promise<Pagination<SiteEntity>>;
    getSitesByCompany(page: number, limit: number, companyId: string): Promise<Pagination<SiteEntity>>;
}
