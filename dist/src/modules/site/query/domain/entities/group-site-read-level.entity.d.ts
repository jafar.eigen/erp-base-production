export interface GroupSiteReadLevelEntity {
    id: string;
    group_site_id: string;
    number: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
