"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestInfo = exports.SiteStatus = void 0;
var SiteStatus;
(function (SiteStatus) {
    SiteStatus["DRAFT"] = "draft";
    SiteStatus["ACTIVE"] = "active";
    SiteStatus["INACTIVE"] = "inactive";
    SiteStatus["REQUESTED"] = "requested";
    SiteStatus["OPEN"] = "open";
    SiteStatus["DECLINE"] = "declined";
})(SiteStatus = exports.SiteStatus || (exports.SiteStatus = {}));
var RequestInfo;
(function (RequestInfo) {
    RequestInfo["CREATE_DATA"] = "Create Data";
    RequestInfo["EDIT_DATA"] = "Edit Data";
    RequestInfo["EDIT_ACTIVE"] = "Edit Active";
    RequestInfo["EDIT_INACTIVE"] = "Edit Inactive";
    RequestInfo["EDIT_DATA_AND_ACTIVE"] = "Edit Data & Status Active";
    RequestInfo["EDIT_DATA_AND_INACTIVE"] = "Edit Data & Status Inactive";
    RequestInfo["DELETE_DATA"] = "Delete Data";
    RequestInfo["ACTIVATE_DATE"] = "Activate Data";
})(RequestInfo = exports.RequestInfo || (exports.RequestInfo = {}));
//# sourceMappingURL=site-read.entity.js.map