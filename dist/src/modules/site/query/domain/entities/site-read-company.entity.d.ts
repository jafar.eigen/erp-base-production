export declare enum SiteCompanyStatuses {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export interface SiteCompanyEntity {
    id: string;
    site_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
