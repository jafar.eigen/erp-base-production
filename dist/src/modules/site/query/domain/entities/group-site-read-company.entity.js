"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteCompanyStatuses = void 0;
var GroupSiteCompanyStatuses;
(function (GroupSiteCompanyStatuses) {
    GroupSiteCompanyStatuses["DRAFT"] = "draft";
    GroupSiteCompanyStatuses["ACTIVE"] = "active";
    GroupSiteCompanyStatuses["INACTIVE"] = "inactive";
    GroupSiteCompanyStatuses["REQUESTED"] = "requested";
    GroupSiteCompanyStatuses["OPEN"] = "open";
    GroupSiteCompanyStatuses["DECLINE"] = "declined";
})(GroupSiteCompanyStatuses = exports.GroupSiteCompanyStatuses || (exports.GroupSiteCompanyStatuses = {}));
//# sourceMappingURL=group-site-read-company.entity.js.map