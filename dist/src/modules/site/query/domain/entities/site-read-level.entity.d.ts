export interface SiteLevelEntity {
    id: string;
    site_id: string;
    number: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
