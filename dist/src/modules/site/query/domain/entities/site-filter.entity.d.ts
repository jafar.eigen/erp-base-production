export interface SiteFilterEntity {
    q: string;
    statuses: string[];
    requests_info: RequestInfo[];
    updated_by: string[];
    companies: string[];
    group_code: string;
    last_update_from: string;
    last_update_to: string;
}
