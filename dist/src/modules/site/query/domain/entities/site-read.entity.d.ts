import { Approval } from 'src';
import { SiteCompanyEntity } from './site-read-company.entity';
import { SiteContactEntity } from './site-read-contact.entity';
import { SiteLevelEntity } from './site-read-level.entity';
export declare enum SiteStatus {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export declare enum RequestInfo {
    CREATE_DATA = "Create Data",
    EDIT_DATA = "Edit Data",
    EDIT_ACTIVE = "Edit Active",
    EDIT_INACTIVE = "Edit Inactive",
    EDIT_DATA_AND_ACTIVE = "Edit Data & Status Active",
    EDIT_DATA_AND_INACTIVE = "Edit Data & Status Inactive",
    DELETE_DATA = "Delete Data",
    ACTIVATE_DATE = "Activate Data"
}
export interface SiteEntity {
    id: string;
    name: string;
    code: string;
    status: string;
    postal_code: number;
    latitude: string;
    longitude: string;
    address: string;
    city: string;
    province: string;
    country: string;
    website_url: string;
    sub_district: string;
    village: string;
    hamlet: string;
    request_info: string;
    branch_id: string;
    branch_name: string;
    branch_code: string;
    group_id?: string;
    creator_id?: string;
    creator_name?: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: Approval;
    requested_data?: any;
    has_requested_process?: boolean;
    is_generated?: boolean;
    is_head_office?: boolean;
    companies?: Partial<SiteCompanyEntity[]>;
    levels?: Partial<SiteLevelEntity[]>;
    contacts?: Partial<SiteContactEntity[]>;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
