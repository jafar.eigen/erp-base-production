import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { SiteEntity } from '../entities/site-read.entity';
export interface SiteReadDataService {
    findOneOrFail(siteId: string): Promise<SiteEntity>;
    findSiteByCompany(options: IPaginationOptions, companyId: string): Promise<Pagination<SiteEntity>>;
}
