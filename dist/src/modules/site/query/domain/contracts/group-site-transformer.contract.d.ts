import { GroupSiteReadEntity } from '../entities/group-site-read.entity';
export interface IGroupSiteTransformer {
    transform(): Partial<GroupSiteReadEntity>;
}
