import { SiteEntity } from '../entities/site-read.entity';
export interface ISiteTransformer {
    transform(): Partial<SiteEntity>;
}
