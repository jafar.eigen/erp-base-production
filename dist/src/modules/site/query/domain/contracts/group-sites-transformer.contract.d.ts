import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { GroupSiteReadEntity } from '../entities/group-site-read.entity';
export interface IGroupSitesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: GroupSiteReadEntity[];
    apply(): Pagination<Partial<GroupSiteReadEntity>>;
}
