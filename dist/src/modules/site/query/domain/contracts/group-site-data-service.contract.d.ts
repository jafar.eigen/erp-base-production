import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { FindManyOptions } from 'typeorm';
import { GroupSiteRead } from '../../data/models/group-sites-read.model';
import { GroupSiteReadEntity } from '../entities/group-site-read.entity';
export interface GroupSiteDataService {
    index(options: IPaginationOptions, extraQuery: FindManyOptions<GroupSiteRead>): Promise<Pagination<GroupSiteReadEntity>>;
    findOneOrFail(id: string): Promise<GroupSiteReadEntity>;
}
