import { IResponses } from 'src';
import { FilterGroupSiteDTO } from '../../infrastructures/dto/filter-group-site.dto';
import { FilterSiteDTO } from '../../infrastructures/dto/filter-site.dto';
export interface SiteReadController {
    index(page: number, limit: number, params: FilterSiteDTO): Promise<IResponses>;
    show(siteId: string): Promise<IResponses>;
    showByCompany(page: number, limit: number, companyId: string): Promise<IResponses>;
    groups(page: number, limit: number, params: FilterGroupSiteDTO): Promise<IResponses>;
    showGroup(id: string): Promise<IResponses>;
}
