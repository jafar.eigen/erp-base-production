import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { SiteEntity } from '../entities/site-read.entity';
export interface ISitesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: SiteEntity[];
    apply(): Pagination<Partial<SiteEntity>>;
}
