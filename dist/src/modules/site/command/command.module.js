"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const src_1 = require("src");
const src_2 = require("src");
const microservice_config_1 = require("../../../utils/microservice.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const company_contact_model_1 = require("src/modules/company/command/data/models/company-contact.model");
const company_model_1 = require("src/modules/company/command/data/models/company.model");
const group_companies_model_1 = require("src/modules/company/command/data/models/group-companies.model");
const database_config_1 = require("src/utils/database.config");
const group_site_company_model_1 = require("./data/models/group-site-company.model");
const group_site_contact_model_1 = require("./data/models/group-site-contact.model");
const group_site_level_model_1 = require("./data/models/group-site-level.model");
const group_sites_model_1 = require("./data/models/group-sites.model");
const site_company_model_1 = require("./data/models/site-company.model");
const site_contact_model_1 = require("./data/models/site-contact.model");
const site_level_model_1 = require("./data/models/site-level.model");
const site_model_1 = require("./data/models/site.model");
const group_site_data_service_1 = require("./data/services/group-site-data.service");
const site_data_service_1 = require("./data/services/site-data.service");
const group_site_orchestrator_1 = require("./domain/usecases/group-site.orchestrator");
const site_orchestrator_1 = require("./domain/usecases/site.orchestrator");
const site_group_producer_controller_1 = require("./infrastructure/producer/site-group-producer.controller");
const site_producer_controller_1 = require("./infrastructure/producer/site-producer.controller");
const site_producer_service_1 = require("./infrastructure/producer/site-producer.service");
const check_company_on_site_controller_1 = require("./infrastructure/consumer/company/infrastructure/check-company-on-site.controller");
const company_activated_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-activated.controller");
const company_changed_new_address_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-changed-new-address.controller");
const company_changed_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-changed.controller");
const company_deleted_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-deleted.controller");
const check_company_on_site_orchestrator_1 = require("./infrastructure/consumer/company/domain/check-company-on-site.orchestrator");
const company_activated_manager_1 = require("./infrastructure/consumer/company/domain/company-activated.manager");
const company_changed_manager_1 = require("./infrastructure/consumer/company/domain/company-changed.manager");
const company_deleted_orchestrator_1 = require("./infrastructure/consumer/company/domain/company-deleted.orchestrator");
const microservices_1 = require("@nestjs/microservices");
const location_model_1 = require("src/modules/location/command/data/models/location.model");
const location_map_model_1 = require("src/modules/location/command/data/models/location-map.model");
const group_location_model_1 = require("src/modules/location/command/data/models/group-location.model");
const location_map_raw_model_1 = require("src/modules/location/command/data/models/location-map-raw.model");
const branch_changed_controller_1 = require("./infrastructure/consumer/branch/infrastructure/branch-changed.controller");
const branch_changed_manager_1 = require("./infrastructure/consumer/branch/domain/branch-changed.manager");
let CommandModule = class CommandModule {
};
CommandModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default, microservice_config_1.default],
                isGlobal: true
            }),
            microservices_1.ClientsModule.registerAsync([
                {
                    name: microservice_config_1.KAFKA_CLIENT_NAME,
                    imports: [config_1.ConfigModule],
                    useFactory: (configService) => configService.get('kafkaClientConfig'),
                    inject: [config_1.ConfigService]
                }
            ]),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.SITE_CUD_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormSiteCommand'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([
                site_model_1.Site,
                site_company_model_1.SiteCompany,
                site_contact_model_1.SiteContact,
                site_level_model_1.SiteLevel,
                group_sites_model_1.GroupSite,
                group_site_contact_model_1.GroupSiteContact,
                group_site_level_model_1.GroupSiteLevel,
                group_site_company_model_1.GroupSiteCompany,
                group_companies_model_1.GroupCompanies,
                company_model_1.Company,
                company_contact_model_1.Contact,
                location_model_1.Location,
                location_map_model_1.LocationMap,
                group_location_model_1.GroupLocation,
                location_map_raw_model_1.LocationMapRow
            ], database_config_1.SITE_CUD_CONNECTION)
        ],
        providers: [
            src_2.Responses,
            src_1.JSONParse,
            site_orchestrator_1.SiteOrchestrator,
            group_site_orchestrator_1.GroupSiteOrchestrator,
            site_data_service_1.SiteDataServiceImpl,
            site_producer_service_1.SiteProducerServiceImpl,
            group_site_data_service_1.GroupSiteDataServiceImpl,
            check_company_on_site_orchestrator_1.CheckCompanyOnSiteOrchestrator,
            company_activated_manager_1.CompanyActivatedManager,
            company_changed_manager_1.CompanyChangedManager,
            company_deleted_orchestrator_1.CompanyDeletedOrchestrator,
            branch_changed_manager_1.BranchChangedManager
        ],
        controllers: [
            site_producer_controller_1.SiteProducerControllerImpl,
            site_group_producer_controller_1.SiteGroupProducerControllerImpl,
            check_company_on_site_controller_1.CheckCompanyOnSiteController,
            company_activated_controller_1.CompanyActivatedController,
            company_changed_new_address_controller_1.CompanyChangeNewAddressController,
            company_changed_controller_1.CompanyChangedController,
            company_deleted_controller_1.CompanyDeletedController,
            branch_changed_controller_1.BranchChangedController
        ]
    })
], CommandModule);
exports.CommandModule = CommandModule;
//# sourceMappingURL=command.module.js.map