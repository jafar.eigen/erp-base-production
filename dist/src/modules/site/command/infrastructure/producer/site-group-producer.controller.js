"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteGroupProducerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const src_2 = require("src");
const src_3 = require("src");
const src_4 = require("src");
const group_site_orchestrator_1 = require("../../domain/usecases/group-site.orchestrator");
const update_group_site_dto_1 = require("../dto/update-group-site.dto");
const create_group_site_dto_1 = require("../dto/create-group-site.dto");
let SiteGroupProducerControllerImpl = class SiteGroupProducerControllerImpl {
    constructor(responses, groupOrchestrator, jsonParser) {
        this.responses = responses;
        this.groupOrchestrator = groupOrchestrator;
        this.jsonParser = jsonParser;
    }
    async create(body, headers) {
        var _a, _b;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const approval = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : src_1.MockApprovalNone;
            const site = await this.groupOrchestrator.create(body, approval, user);
            return this.responses.json(common_1.HttpStatus.OK, site);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async delete(groupIds, headers) {
        var _a, _b;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const approval = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : src_1.MockApprovalNone;
            const result = await this.groupOrchestrator.delete(groupIds, approval, user);
            return this.responses.json(common_1.HttpStatus.OK, result);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async activate(groupIds, headers) {
        var _a, _b;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const approval = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : src_1.MockApprovalNone;
            const group = await this.groupOrchestrator.activate(groupIds, user, approval);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async deactivate(groupIds, headers) {
        var _a, _b;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const approval = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : src_1.MockApprovalNone;
            const group = await this.groupOrchestrator.deactivate(groupIds, user, approval);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async request(groupId, headers) {
        var _a;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const group = await this.groupOrchestrator.request(groupId, user);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async approve(groupId, headers, step) {
        var _a;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const group = await this.groupOrchestrator.approve(groupId, user, step);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async decline(groupId, headers, step) {
        var _a;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const group = await this.groupOrchestrator.decline(groupId, step, user);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
    async update(groupId, updatedData, headers) {
        var _a, _b;
        try {
            const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : src_4.MockUser;
            const approval = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : src_1.MockApprovalNone;
            const group = await this.groupOrchestrator.update(groupId, updatedData, approval, user);
            return this.responses.json(common_1.HttpStatus.OK, group);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_group_site_dto_1.CreateGroupSiteDTO, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "create", null);
__decorate([
    (0, common_1.Delete)(),
    __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(1, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "delete", null);
__decorate([
    (0, common_1.Put)('activate'),
    __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(1, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "activate", null);
__decorate([
    (0, common_1.Put)('deactivate'),
    __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
    __param(1, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "deactivate", null);
__decorate([
    (0, common_1.Put)(':id/request'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "request", null);
__decorate([
    (0, common_1.Put)(':id/approve'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Headers)()),
    __param(2, (0, common_1.Query)('step', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Number]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "approve", null);
__decorate([
    (0, common_1.Put)(':id/decline'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Headers)()),
    __param(2, (0, common_1.Query)('step', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Number]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "decline", null);
__decorate([
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_group_site_dto_1.UpdateGroupSiteDTO, String]),
    __metadata("design:returntype", Promise)
], SiteGroupProducerControllerImpl.prototype, "update", null);
SiteGroupProducerControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('site/group'),
    (0, common_1.Controller)('site/group'),
    __metadata("design:paramtypes", [src_3.Responses,
        group_site_orchestrator_1.GroupSiteOrchestrator,
        src_2.JSONParse])
], SiteGroupProducerControllerImpl);
exports.SiteGroupProducerControllerImpl = SiteGroupProducerControllerImpl;
//# sourceMappingURL=site-group-producer.controller.js.map