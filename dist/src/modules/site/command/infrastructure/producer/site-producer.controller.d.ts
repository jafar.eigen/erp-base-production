import { IResponses } from 'src';
import { SiteOrchestrator } from '../../domain/usecases/site.orchestrator';
declare const BaseController: any;
export declare class SiteProducerControllerImpl extends BaseController {
    private siteOrchestrator;
    constructor(siteOrchestrator: SiteOrchestrator);
    switchGroupSite(siteId: string, destinationGroupId: string, headers: string): Promise<IResponses>;
}
export {};
