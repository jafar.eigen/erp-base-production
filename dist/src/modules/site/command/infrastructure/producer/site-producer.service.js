"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteProducerServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const src_1 = require("src");
const site_topics_1 = require("../../site.topics");
const microservice_config_1 = require("src/utils/microservice.config");
let SiteProducerServiceImpl = class SiteProducerServiceImpl extends src_1.BaseProducerServiceImpl {
    constructor(client) {
        super(client);
        this.client = client;
        this.moduleName = site_topics_1.MODULE_TOPIC_SITE;
        this.TOPIC_CREATED = site_topics_1.HANDLE_SITE_CREATED;
        this.TOPIC_CHANGED = site_topics_1.HANDLE_SITE_CHANGED;
        this.TOPIC_ACTIVATED = site_topics_1.HANDLE_SITE_ACTIVATED;
        this.TOPIC_DELETED = site_topics_1.HANDLE_SITE_DELETED;
    }
    addNewTopics() {
        return [
            site_topics_1.CHECK_EXISTENCE_SITE_ON_COMPANY,
            site_topics_1.CHECK_EXISTENCE_SITE_ON_LOCATION,
            site_topics_1.HANDLE_SITE_CREATED_AND_ACTIVE,
            site_topics_1.HANDLE_GROUP_SITE_DELETED,
            site_topics_1.HANDLE_GROUP_SITE_CHANGED,
            site_topics_1.HANDLE_GROUP_SITE_CREATED
        ];
    }
    async isSiteExistOnCompany(siteId) {
        return await this.client
            .send(site_topics_1.CHECK_EXISTENCE_SITE_ON_COMPANY, siteId)
            .toPromise();
    }
    async isSiteExistOnLocation(siteId) {
        return await this.client
            .send(site_topics_1.CHECK_EXISTENCE_SITE_ON_LOCATION, siteId)
            .toPromise();
    }
    async siteCreatedAndActive(payload) {
        console.info('site created and activated triggered');
        this.client.send(site_topics_1.HANDLE_SITE_CREATED_AND_ACTIVE, payload).toPromise();
    }
    async groupSiteChanged(payload) {
        console.info('group site updated triggered');
        this.client.send(site_topics_1.HANDLE_GROUP_SITE_CHANGED, payload).toPromise();
    }
    async groupSiteCreated(payload) {
        console.info('group site created triggered');
        this.client.send(site_topics_1.HANDLE_GROUP_SITE_CREATED, payload).toPromise();
    }
    async groupSiteDeleted(payload) {
        console.info('group site deleted triggered');
        this.client.send(site_topics_1.HANDLE_GROUP_SITE_DELETED, payload).toPromise();
    }
};
SiteProducerServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(microservice_config_1.KAFKA_CLIENT_NAME)),
    __metadata("design:paramtypes", [microservices_1.ClientKafka])
], SiteProducerServiceImpl);
exports.SiteProducerServiceImpl = SiteProducerServiceImpl;
//# sourceMappingURL=site-producer.service.js.map