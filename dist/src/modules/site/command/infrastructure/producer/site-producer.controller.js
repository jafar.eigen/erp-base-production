"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteProducerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const site_orchestrator_1 = require("../../domain/usecases/site.orchestrator");
const create_site_dto_1 = require("../dto/create-site.dto");
const update_site_dto_1 = require("../dto/update-site.dto");
const BaseController = (0, src_1.getBaseController)({
    createModelVm: create_site_dto_1.CreateSiteDTO,
    updateModelVm: update_site_dto_1.UpdateSiteDTO
});
let SiteProducerControllerImpl = class SiteProducerControllerImpl extends BaseController {
    constructor(siteOrchestrator) {
        super(new src_1.Responses(), siteOrchestrator, new src_1.JSONParse());
        this.siteOrchestrator = siteOrchestrator;
    }
    async switchGroupSite(siteId, destinationGroupId, headers) {
        try {
            const session = src_1.UserSession.getInstance();
            session.set(headers);
            const site = await this.siteOrchestrator.switchGroup(siteId, destinationGroupId);
            return this.responses.json(common_1.HttpStatus.OK, site);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
};
__decorate([
    (0, common_1.Put)('/:id/switch-group'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)('destination_group_id')),
    __param(2, (0, common_1.Headers)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String]),
    __metadata("design:returntype", Promise)
], SiteProducerControllerImpl.prototype, "switchGroupSite", null);
SiteProducerControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('site'),
    (0, common_1.Controller)('site'),
    __metadata("design:paramtypes", [site_orchestrator_1.SiteOrchestrator])
], SiteProducerControllerImpl);
exports.SiteProducerControllerImpl = SiteProducerControllerImpl;
//# sourceMappingURL=site-producer.controller.js.map