import { JSONParse } from 'src';
import { IResponses } from 'src';
import { Responses } from 'src';
import { GroupSiteOrchestrator } from '../../domain/usecases/group-site.orchestrator';
import { UpdateGroupSiteDTO } from '../dto/update-group-site.dto';
import { CreateGroupSiteDTO } from '../dto/create-group-site.dto';
import { GroupSiteProducerController } from '../../domain/contracts/site-group-producer.controller.contract';
export declare class SiteGroupProducerControllerImpl implements GroupSiteProducerController {
    private responses;
    private groupOrchestrator;
    private jsonParser;
    constructor(responses: Responses, groupOrchestrator: GroupSiteOrchestrator, jsonParser: JSONParse);
    create(body: CreateGroupSiteDTO, headers: string): Promise<IResponses>;
    delete(groupIds: string[], headers: string): Promise<IResponses>;
    activate(groupIds: string[], headers: string): Promise<IResponses>;
    deactivate(groupIds: string[], headers: string): Promise<IResponses>;
    request(groupId: string, headers: string): Promise<IResponses>;
    approve(groupId: string, headers: string, step: number): Promise<IResponses>;
    decline(groupId: string, headers: string, step: number): Promise<IResponses>;
    update(groupId: string, updatedData: UpdateGroupSiteDTO, headers: string): Promise<IResponses>;
}
