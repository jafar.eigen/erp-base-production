import { ClientKafka } from '@nestjs/microservices';
import { BaseProducerServiceImpl } from 'src';
import { KafkaPayload } from 'src';
import { IResponses } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteEntity } from '../../domain/entities/group-site.entity';
import { SiteEntity } from '../../domain/entities/site.entity';
export declare class SiteProducerServiceImpl extends BaseProducerServiceImpl<SiteEntity> {
    client: ClientKafka;
    moduleName: string;
    TOPIC_CREATED: string;
    TOPIC_CHANGED: string;
    TOPIC_ACTIVATED: string;
    TOPIC_DELETED: string;
    constructor(client: ClientKafka);
    addNewTopics(): string[];
    isSiteExistOnCompany(siteId: string): Promise<IResponses>;
    isSiteExistOnLocation(siteId: string): Promise<IResponses>;
    siteCreatedAndActive(payload: KafkaPayload<IUserPayload, Partial<SiteEntity>, SiteEntity>): Promise<void>;
    groupSiteChanged(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    groupSiteCreated(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    groupSiteDeleted(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
}
