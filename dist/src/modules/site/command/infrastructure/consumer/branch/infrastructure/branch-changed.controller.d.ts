import { Logger } from 'winston';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
import { BranchChangedManager } from '../domain/branch-changed.manager';
declare const BaseConsumerController: any;
export declare class BranchChangedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private branchChangedManager;
    private siteProducerService;
    constructor(logger: Logger, clientSentry: SentryService, branchChangedManager: BranchChangedManager, siteProducerService: SiteProducerServiceImpl);
}
export {};
