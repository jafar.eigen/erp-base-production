import { BaseConsumerManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
export declare class BranchChangedManager extends BaseConsumerManager {
    siteDataService: SiteDataServiceImpl;
    constructor(siteDataService: SiteDataServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(sites: Partial<any[]>): Promise<void>;
}
