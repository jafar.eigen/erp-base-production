"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchChangedManager = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const site_data_service_1 = require("src/modules/site/command/data/services/site-data.service");
let BranchChangedManager = class BranchChangedManager extends src_1.BaseConsumerManager {
    constructor(siteDataService) {
        super(siteDataService);
        this.siteDataService = siteDataService;
    }
    async transformData(value) {
        const { id: branch_id, code: branch_code, name: branch_name } = value.data;
        const sites = await this.siteDataService.findSiteByBranchId(branch_id);
        sites.map((site) => {
            return Object.assign(site, {
                branch_code,
                branch_name
            });
        });
        return sites;
    }
    async processData(sites) {
        await this.siteDataService.saveMany(sites);
    }
};
BranchChangedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl])
], BranchChangedManager);
exports.BranchChangedManager = BranchChangedManager;
//# sourceMappingURL=branch-changed.manager.js.map