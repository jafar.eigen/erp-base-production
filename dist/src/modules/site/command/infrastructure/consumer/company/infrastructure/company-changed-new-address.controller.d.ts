import { Logger } from 'winston';
import { CompanyChangedManager } from '../domain/company-changed.manager';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CompanyChangeNewAddressController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private companyChangedManager;
    private siteProducerService;
    constructor(logger: Logger, clientSentry: SentryService, companyChangedManager: CompanyChangedManager, siteProducerService: SiteProducerServiceImpl);
    processMessage({ topic, nextTopicRetry, messageValue }: {
        topic: any;
        nextTopicRetry: any;
        messageValue: any;
    }): Promise<any>;
}
export {};
