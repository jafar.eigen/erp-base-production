import { SiteCompanyEntity } from 'src/modules/site/command/domain/entities/site-company.entity';
import { SiteEntity } from 'src/modules/site/command/domain/entities/site.entity';
export declare class UpdateSiteAddressManager {
    private sites;
    private companyMessage;
    private isNewAddress;
    constructor(sites: SiteEntity[], companyMessage: any, isNewAddress: boolean);
    protected companyStatus: string;
    protected companyReqInfo: string;
    protected companyAddress: string;
    execute(): Promise<SiteEntity[]>;
    protected applySitesChange(sites: SiteEntity[]): Promise<SiteEntity[]>;
    protected applySiteCompaniesChange(siteCompanies: SiteCompanyEntity[]): Promise<SiteCompanyEntity[]>;
    private getValidAddress;
}
