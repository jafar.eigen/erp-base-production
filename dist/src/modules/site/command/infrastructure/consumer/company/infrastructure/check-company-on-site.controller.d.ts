import { Logger } from 'winston';
import { CheckCompanyOnSiteOrchestrator } from '../domain/check-company-on-site.orchestrator';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CheckCompanyOnSiteController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private checkCompanyOrchestrator;
    private siteProducerService;
    constructor(logger: Logger, clientSentry: SentryService, checkCompanyOrchestrator: CheckCompanyOnSiteOrchestrator, siteProducerService: SiteProducerServiceImpl);
}
export {};
