"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyChangedController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const src_1 = require("src");
const company_changed_manager_1 = require("../domain/company-changed.manager");
const site_topics_1 = require("src/modules/site/command/site.topics");
const topics_1 = require("../../topics");
const site_producer_service_1 = require("src/modules/site/command/infrastructure/producer/site-producer.service");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const BaseConsumerController = (0, src_1.getBaseConsumerController)(site_topics_1.MODULE_TOPIC_SITE, topics_1.HANDLE_COMPANY_CHANGED);
let CompanyChangedController = class CompanyChangedController extends BaseConsumerController {
    constructor(logger, clientSentry, companyChangedManager, siteProducerService) {
        super(logger, clientSentry, companyChangedManager, siteProducerService);
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.companyChangedManager = companyChangedManager;
        this.siteProducerService = siteProducerService;
    }
    async processMessage({ topic, nextTopicRetry, messageValue }) {
        messageValue.isNewAddress = false;
        return await this.retryPattern.execute(site_topics_1.MODULE_TOPIC_SITE, topic, nextTopicRetry, messageValue);
    }
};
CompanyChangedController = __decorate([
    (0, common_1.Controller)('site'),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        company_changed_manager_1.CompanyChangedManager,
        site_producer_service_1.SiteProducerServiceImpl])
], CompanyChangedController);
exports.CompanyChangedController = CompanyChangedController;
//# sourceMappingURL=company-changed.controller.js.map