import { KafkaPayload } from 'src';
import { IUserPayload } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../../../domain/entities/site.entity';
export declare class HandleCompanyDeleted {
    protected readonly message: KafkaPayload<IUserPayload, any, any>;
    protected readonly siteDataService: SiteDataServiceImpl;
    protected readonly kafkaService: SiteProducerServiceImpl;
    constructor(message: KafkaPayload<IUserPayload, any, any>, siteDataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl);
    protected companyId: string;
    run(): Promise<void>;
    protected deleteCompanyOnSite(site: SiteEntity): Promise<void>;
    protected deleteSiteFromCompany(site: SiteEntity): Promise<void>;
    protected getSitesFromCompany(): Promise<SiteEntity[]>;
    protected delete(site: SiteEntity): Promise<void>;
    protected produceTopicDeleted(site: SiteEntity): Promise<void>;
    protected produceTopicChanged(updatedSite: SiteEntity, oldSite: SiteEntity): Promise<void>;
}
