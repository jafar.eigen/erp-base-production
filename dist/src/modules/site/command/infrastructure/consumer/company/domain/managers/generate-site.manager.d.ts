import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../../producer/site-producer.service';
export declare class GenerateSiteManager {
    private data;
    private siteDataService;
    private siteProducerDataService;
    private user;
    transactionData: any;
    kafkaPayload: any;
    constructor(data: any, siteDataService: SiteDataServiceImpl, siteProducerDataService: SiteProducerServiceImpl, user: any);
    run(): Promise<void>;
    private mapData;
    protected saveData(): Promise<void>;
    protected setKafkaPayload(): Promise<void>;
    protected produceData(): Promise<void>;
}
