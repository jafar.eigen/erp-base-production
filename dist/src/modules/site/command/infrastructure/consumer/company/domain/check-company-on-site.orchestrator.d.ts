import { BaseConsumerManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
export declare class CheckCompanyOnSiteOrchestrator extends BaseConsumerManager {
    private siteDataService;
    constructor(siteDataService: SiteDataServiceImpl);
    execute(message: any): Promise<any>;
}
