import { BaseConsumerManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../producer/site-producer.service';
export declare class CompanyDeletedOrchestrator extends BaseConsumerManager {
    private siteDataService;
    private siteProducerService;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    execute(message: any): Promise<void>;
}
