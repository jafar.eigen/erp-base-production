import { BaseConsumerManager, IUserPayload } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../producer/site-producer.service';
export declare class CompanyActivatedManager extends BaseConsumerManager {
    private siteDataService;
    private siteProducerService;
    user: IUserPayload;
    transactionData: any;
    kafkaPayload: any;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(dataTransform: any): Promise<void>;
    protected setKafkaPayload(): Promise<void>;
    protected produceData(): Promise<void>;
}
