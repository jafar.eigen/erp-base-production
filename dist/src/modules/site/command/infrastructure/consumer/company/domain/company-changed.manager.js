"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyChangedManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const site_data_service_1 = require("src/modules/site/command/data/services/site-data.service");
const site_producer_service_1 = require("../../../producer/site-producer.service");
const equal_address_site_company_consumer_1 = require("src/modules/site/command/domain/usecases/managers/consumers/equal-address-site-company-consumer");
const update_site_address_manager_1 = require("./managers/update-site-address.manager");
const generate_site_manager_1 = require("./managers/generate-site.manager");
let CompanyChangedManager = class CompanyChangedManager extends src_1.BaseConsumerManager {
    constructor(siteDataService, siteProducerService) {
        super(siteDataService);
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
    }
    async transformData(value) {
        var _a;
        this.company = (_a = value.data) !== null && _a !== void 0 ? _a : value.old;
        this.isNewAddress = value.isNewAddress;
        this.user = value.user;
        const sites = await this.siteDataService.findByCompanyId(this.company.id);
        Object.assign(this, { sites });
        return this;
    }
    async processData(dataTransform) {
        if (!this.sites)
            return;
        switch (this.isNewAddress) {
            case true:
                if (this.isNewAddressExistOnSite()) {
                    await this.removeOldHeadOffice();
                    await this.updateAndSaveSite();
                }
                else {
                    await this.generateSite();
                }
                break;
            default:
                const indexSite = this.sites.findIndex((site) => {
                    return site.is_generated === true;
                });
                this.sites[indexSite].status = this.company.status;
                await this.updateAndSaveSite();
        }
    }
    async updateAndSaveSite() {
        const formattedSites = await new update_site_address_manager_1.UpdateSiteAddressManager(this.sites, this.company, this.isNewAddress).execute();
        await this.siteDataService.saveMany(formattedSites);
        await this.produceChangedData(formattedSites);
    }
    async generateSite() {
        await this.removeOldHeadOffice();
        await new generate_site_manager_1.GenerateSiteManager(this.company, this.siteDataService, this.siteProducerService, this.user).run();
    }
    isNewAddressExistOnSite() {
        return this.sites.find((site) => {
            return (0, equal_address_site_company_consumer_1.isEqualAddress)(site, this.company);
        });
    }
    async removeOldHeadOffice() {
        if (!this.isNewAddress)
            return;
        const oldHeadoffice = this.findOldHeadOffice();
        if (!oldHeadoffice)
            return;
        oldHeadoffice.is_head_office = false;
        await this.siteDataService.save(oldHeadoffice);
    }
    findOldHeadOffice() {
        return this.sites.find((site) => {
            return site.is_head_office;
        });
    }
    async produceChangedData(sites) {
        Promise.all(sites.map(async (site) => {
            await this.siteProducerService.baseChanged({
                user: this.user,
                data: site,
                old: site
            });
        }));
    }
};
CompanyChangedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl,
        site_producer_service_1.SiteProducerServiceImpl])
], CompanyChangedManager);
exports.CompanyChangedManager = CompanyChangedManager;
//# sourceMappingURL=company-changed.manager.js.map