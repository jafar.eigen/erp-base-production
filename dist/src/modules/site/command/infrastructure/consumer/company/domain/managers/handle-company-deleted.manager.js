"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HandleCompanyDeleted = void 0;
const _ = require("lodash");
class HandleCompanyDeleted {
    constructor(message, siteDataService, kafkaService) {
        this.message = message;
        this.siteDataService = siteDataService;
        this.kafkaService = kafkaService;
    }
    async run() {
        var _a;
        this.companyId = (_a = this.message.old.id) !== null && _a !== void 0 ? _a : this.message.data.id;
        const sites = await this.getSitesFromCompany();
        await Promise.all(sites === null || sites === void 0 ? void 0 : sites.map(async (site) => {
            if (site.is_generated) {
                await this.deleteSiteFromCompany(site);
            }
            else {
                await this.deleteCompanyOnSite(site);
            }
        }));
    }
    async deleteCompanyOnSite(site) {
        const oldSite = _.cloneDeep(site);
        const found = site.companies.find((siteComp) => {
            return siteComp.company_id === this.companyId;
        });
        if (!found)
            return;
        site.companies = site.companies.filter((siteComp) => {
            return siteComp.company_id !== this.companyId;
        });
        await this.siteDataService.save(site);
        await this.siteDataService.findAndDeleteNulledRelation();
        await this.produceTopicChanged(site, oldSite);
    }
    async deleteSiteFromCompany(site) {
        await this.siteDataService.delete([site.id]);
        await this.produceTopicDeleted(site);
    }
    async getSitesFromCompany() {
        return await this.siteDataService.findByCompanyId(this.companyId);
    }
    async delete(site) {
        await this.siteDataService.delete([site.id]);
    }
    async produceTopicDeleted(site) {
        await this.kafkaService.baseDeleted({
            user: this.message.user,
            data: null,
            old: site
        });
    }
    async produceTopicChanged(updatedSite, oldSite) {
        await this.kafkaService.baseChanged({
            user: this.message.user,
            data: updatedSite,
            old: oldSite
        });
    }
}
exports.HandleCompanyDeleted = HandleCompanyDeleted;
//# sourceMappingURL=handle-company-deleted.manager.js.map