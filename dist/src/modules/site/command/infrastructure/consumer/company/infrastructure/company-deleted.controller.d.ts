import { Logger } from 'winston';
import { CompanyDeletedOrchestrator } from '../domain/company-deleted.orchestrator';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CompanyDeletedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private companyDeletedOrchestrator;
    private siteProducerService;
    constructor(logger: Logger, clientSentry: SentryService, companyDeletedOrchestrator: CompanyDeletedOrchestrator, siteProducerService: SiteProducerServiceImpl);
}
export {};
