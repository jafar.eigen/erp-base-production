"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckCompanyOnSiteOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const site_data_service_1 = require("src/modules/site/command/data/services/site-data.service");
let CheckCompanyOnSiteOrchestrator = class CheckCompanyOnSiteOrchestrator extends src_1.BaseConsumerManager {
    constructor(siteDataService) {
        super(siteDataService);
        this.siteDataService = siteDataService;
    }
    async execute(message) {
        try {
            const companyId = message.data.companyId;
            const sites = await this.siteDataService.findByCompanyId(companyId);
            if (Array.isArray(sites) && sites.length > 0) {
                const foundGeneratedSite = sites.find((site) => {
                    return site.is_generated === false;
                });
                if (foundGeneratedSite) {
                    return new src_1.Responses().json(common_1.HttpStatus.BAD_REQUEST, true);
                }
            }
            return new src_1.Responses().json(common_1.HttpStatus.OK, false);
        }
        catch (e) {
            throw new Error(e);
        }
    }
};
CheckCompanyOnSiteOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl])
], CheckCompanyOnSiteOrchestrator);
exports.CheckCompanyOnSiteOrchestrator = CheckCompanyOnSiteOrchestrator;
//# sourceMappingURL=check-company-on-site.orchestrator.js.map