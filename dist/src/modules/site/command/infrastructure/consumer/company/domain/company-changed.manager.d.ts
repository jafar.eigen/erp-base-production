import { BaseConsumerManager, IUserPayload } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../producer/site-producer.service';
import { SiteEntity } from 'src/modules/site/command/domain/entities/site.entity';
export declare class CompanyChangedManager extends BaseConsumerManager {
    siteDataService: SiteDataServiceImpl;
    siteProducerService: SiteProducerServiceImpl;
    protected company: any;
    protected sites: SiteEntity[];
    protected isNewAddress: boolean;
    user: IUserPayload;
    kafkaPayload: any;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(dataTransform: Partial<any[]>): Promise<void>;
    protected updateAndSaveSite(): Promise<void>;
    protected generateSite(): Promise<void>;
    protected isNewAddressExistOnSite(): SiteEntity;
    protected removeOldHeadOffice(): Promise<void>;
    protected findOldHeadOffice(): SiteEntity;
    protected produceChangedData(sites: SiteEntity[]): Promise<void>;
}
