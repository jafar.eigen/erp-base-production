"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateSiteAddressManager = void 0;
const equal_address_site_company_consumer_1 = require("src/modules/site/command/domain/usecases/managers/consumers/equal-address-site-company-consumer");
class UpdateSiteAddressManager {
    constructor(sites, companyMessage, isNewAddress) {
        this.sites = sites;
        this.companyMessage = companyMessage;
        this.isNewAddress = isNewAddress;
    }
    async execute() {
        this.companyStatus = this.companyMessage.status;
        this.companyReqInfo = this.companyMessage.request_info;
        this.companyAddress = this.companyMessage.address;
        return await this.applySitesChange(this.sites);
    }
    async applySitesChange(sites) {
        sites = await Promise.all(sites.map(async (site) => {
            if (this.isNewAddress && (0, equal_address_site_company_consumer_1.isEqualAddress)(site, this.companyMessage)) {
                Object.assign(site, this.getValidAddress(this.companyMessage));
            }
            site.companies = await this.applySiteCompaniesChange(site.companies);
            return site;
        }));
        return sites;
    }
    async applySiteCompaniesChange(siteCompanies) {
        siteCompanies = await Promise.all(siteCompanies.map((siteComp) => {
            var _a;
            Object.assign(siteComp, {
                company_name: this.companyMessage.name,
                company_code: (_a = this.companyMessage) === null || _a === void 0 ? void 0 : _a.code,
                company_status: this.companyStatus
            });
            return siteComp;
        }));
        return siteCompanies;
    }
    getValidAddress(site) {
        return {
            postal_code: site.postal_code,
            latitude: site.latitude,
            longitude: site.longitude,
            address: site.address,
            city: site.city,
            province: site.province,
            country: site.country,
            sub_district: site.sub_district,
            village: site.village,
            hamlet: site.hamlet,
            is_head_office: true
        };
    }
}
exports.UpdateSiteAddressManager = UpdateSiteAddressManager;
//# sourceMappingURL=update-site-address.manager.js.map