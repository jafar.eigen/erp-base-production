"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyDeletedOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const site_data_service_1 = require("src/modules/site/command/data/services/site-data.service");
const site_producer_service_1 = require("../../../producer/site-producer.service");
const handle_company_deleted_manager_1 = require("./managers/handle-company-deleted.manager");
let CompanyDeletedOrchestrator = class CompanyDeletedOrchestrator extends src_1.BaseConsumerManager {
    constructor(siteDataService, siteProducerService) {
        super(siteDataService);
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
    }
    async execute(message) {
        const handleCOmpanyDeleted = new handle_company_deleted_manager_1.HandleCompanyDeleted(message, this.siteDataService, this.siteProducerService);
        await handleCOmpanyDeleted.run();
    }
};
CompanyDeletedOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl,
        site_producer_service_1.SiteProducerServiceImpl])
], CompanyDeletedOrchestrator);
exports.CompanyDeletedOrchestrator = CompanyDeletedOrchestrator;
//# sourceMappingURL=company-deleted.orchestrator.js.map