"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateSiteManager = void 0;
const src_1 = require("src");
const generate_unique_code_manager_1 = require("src/modules/site/command/domain/usecases/managers/consumers/generate-unique-code.manager");
const generate_unique_name_manager_1 = require("src/modules/site/command/domain/usecases/managers/consumers/generate-unique-name.manager");
class GenerateSiteManager {
    constructor(data, siteDataService, siteProducerDataService, user) {
        this.data = data;
        this.siteDataService = siteDataService;
        this.siteProducerDataService = siteProducerDataService;
        this.user = user;
    }
    async run() {
        await this.mapData();
        await this.saveData();
        await this.setKafkaPayload();
        await this.produceData();
    }
    async mapData() {
        var _a;
        this.data.companies = [
            {
                company_id: this.data.id,
                company_name: this.data.name,
                company_code: (_a = this.data) === null || _a === void 0 ? void 0 : _a.code,
                company_status: src_1.STATUS.ACTIVE
            }
        ];
        delete this.data.id;
        delete this.data.created_at;
        delete this.data.updated_at;
        delete this.data.group_id;
        this.data.status = src_1.STATUS.ACTIVE;
        if (Array.isArray(this.data.contacts) && this.data.contacts.length > 0) {
            this.data.contacts = this.data.contacts.map((contact) => {
                delete contact.id;
                delete contact.company_id;
                return contact;
            });
        }
        Object.assign(this.data, {
            is_generated: true,
            is_head_office: true,
            name: await new generate_unique_name_manager_1.GenerateUniqueNameManager(this.data.name, this.siteDataService).run(),
            code: await new generate_unique_code_manager_1.GenerateUniqueCodeManager(this.data.code, this.siteDataService).run()
        });
    }
    async saveData() {
        const transactionData = await this.siteDataService.save(this.data);
        Object.assign(transactionData, { levels: [] });
        Object.assign(this, { transactionData });
    }
    async setKafkaPayload() {
        Object.assign(this, {
            kafkaPayload: {
                user: this.user,
                data: this.transactionData,
                old: null
            }
        });
    }
    async produceData() {
        await this.siteProducerDataService.baseActivated(this.kafkaPayload);
    }
}
exports.GenerateSiteManager = GenerateSiteManager;
//# sourceMappingURL=generate-site.manager.js.map