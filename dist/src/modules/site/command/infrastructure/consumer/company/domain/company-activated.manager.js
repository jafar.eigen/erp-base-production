"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyActivatedManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
const site_data_service_1 = require("src/modules/site/command/data/services/site-data.service");
const site_producer_service_1 = require("../../../producer/site-producer.service");
const generate_unique_name_manager_1 = require("src/modules/site/command/domain/usecases/managers/consumers/generate-unique-name.manager");
const generate_unique_code_manager_1 = require("src/modules/site/command/domain/usecases/managers/consumers/generate-unique-code.manager");
let CompanyActivatedManager = class CompanyActivatedManager extends src_1.BaseConsumerManager {
    constructor(siteDataService, siteProducerService) {
        super(siteDataService);
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
    }
    async transformData(value) {
        var _a;
        this.user = value.user;
        value.data.companies = [
            {
                company_id: value.data.id,
                company_name: value.data.name,
                company_code: (_a = value.data) === null || _a === void 0 ? void 0 : _a.code,
                company_status: src_1.STATUS.ACTIVE
            }
        ];
        delete value.data.id;
        delete value.data.created_at;
        delete value.data.updated_at;
        delete value.data.group_id;
        value.data.status = src_1.STATUS.ACTIVE;
        if (Array.isArray(value.data.contacts) && value.data.contacts.length > 0) {
            value.data.contacts = value.data.contacts.map((contact) => {
                delete contact.id;
                delete contact.company_id;
                return contact;
            });
        }
        Object.assign(value.data, {
            is_generated: true,
            is_head_office: true,
            name: await new generate_unique_name_manager_1.GenerateUniqueNameManager(value.data.name, this.siteDataService).run(),
            code: await new generate_unique_code_manager_1.GenerateUniqueCodeManager(value.data.code, this.siteDataService).run()
        });
        return value.data;
    }
    async processData(dataTransform) {
        const transactionData = await this.siteDataService.save(dataTransform);
        Object.assign(transactionData, { levels: [] });
        Object.assign(this, { transactionData });
        this.setKafkaPayload();
        this.produceData();
    }
    async setKafkaPayload() {
        Object.assign(this, {
            kafkaPayload: {
                user: this.user,
                data: this.transactionData,
                old: null
            }
        });
    }
    async produceData() {
        await this.siteProducerService.baseActivated(this.kafkaPayload);
    }
};
CompanyActivatedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl,
        site_producer_service_1.SiteProducerServiceImpl])
], CompanyActivatedManager);
exports.CompanyActivatedManager = CompanyActivatedManager;
//# sourceMappingURL=company-activated.manager.js.map