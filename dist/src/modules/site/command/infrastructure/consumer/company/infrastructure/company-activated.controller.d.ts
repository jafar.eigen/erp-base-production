import { Logger } from 'winston';
import { CompanyActivatedManager } from '../domain/company-activated.manager';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CompanyActivatedController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private companyActivatedManager;
    private siteProducerService;
    constructor(logger: Logger, clientSentry: SentryService, companyActivatedManager: CompanyActivatedManager, siteProducerService: SiteProducerServiceImpl);
}
export {};
