"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteLevelDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const group_sites_model_1 = require("../../data/models/group-sites.model");
class GroupSiteLevelDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_site),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", group_sites_model_1.GroupSite)
], GroupSiteLevelDTO.prototype, "group_site", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_transformer_1.Transform)((body) => {
        return (body.value = undefined);
    }),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], GroupSiteLevelDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], GroupSiteLevelDTO.prototype, "group_site_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer' }),
    (0, class_validator_1.IsInt)(),
    (0, class_transformer_1.Transform)(({ value }) => parseInt(value)),
    __metadata("design:type", Number)
], GroupSiteLevelDTO.prototype, "number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(5),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], GroupSiteLevelDTO.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteLevelDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteLevelDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteLevelDTO.prototype, "deleted_at", void 0);
exports.GroupSiteLevelDTO = GroupSiteLevelDTO;
//# sourceMappingURL=group-site-level.dto.js.map