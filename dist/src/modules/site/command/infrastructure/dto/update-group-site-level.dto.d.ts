import { GroupSiteLevelEntity } from '../../domain/entities/group-site-level.entity';
export declare class UpdateGroupSiteLevelDTO implements GroupSiteLevelEntity {
    id: string;
    group_site_id: string;
    number: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
