import { GroupSiteContact } from '../../data/models/group-site-contact.model';
import { GroupSite } from '../../data/models/group-sites.model';
export declare class GroupSiteContactDTO implements GroupSiteContact {
    group_site: GroupSite;
    id: string;
    group_site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
