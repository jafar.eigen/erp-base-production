import { SiteCompanyEntity } from '../../domain/entities/site-company.entity';
export declare class UpdateSiteCompanyDTO implements SiteCompanyEntity {
    id: string;
    site_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
