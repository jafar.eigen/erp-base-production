"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteContactDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class SiteContactDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "site_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.ValidateIf)((body) => body.phone),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(30),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.ValidateIf)((body) => body.ext),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "ext", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.ValidateIf)((body) => body.email),
    (0, class_validator_1.IsEmail)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false, nullable: true }),
    (0, class_validator_1.ValidateIf)((body) => body.notes),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], SiteContactDTO.prototype, "notes", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], SiteContactDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], SiteContactDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], SiteContactDTO.prototype, "deleted_at", void 0);
exports.SiteContactDTO = SiteContactDTO;
//# sourceMappingURL=site-contact.dto.js.map