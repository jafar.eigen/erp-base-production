"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateSiteDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const src_1 = require("src");
const src_2 = require("src");
const update_site_company_dto_1 = require("./update-site-company.dto");
const update_site_contact_dto_1 = require("./update-site-contact.dto");
const update_site_level_dto_1 = require("./update-site-level.dto");
class UpdateSiteDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "group_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.postal_code),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value !== '' ? parseInt(value) : null;
    }),
    (0, class_validator_1.IsInt)(),
    __metadata("design:type", Number)
], UpdateSiteDTO.prototype, "postal_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.website_url),
    (0, class_validator_1.IsUrl)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "website_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.longitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "longitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.latitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "latitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.sub_district),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "sub_district", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.helmet),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "hamlet", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.village),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "village", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(5),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Kota Bandung","value":{"long_name":"Kota Bandung","short_name":"Kota Bandung","types":["administrative_area_level_2","political"]}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Jawa Barat","value":{"long_name":"Jawa Barat","short_name":"Jawa Barat","types":["administrative_area_level_1","political"],"state_code":"Jawa Barat","country_code":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "province", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Indonesia","value":{"long_name":"Indonesia","short_name":"ID","types":["country","political"],"iso2":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "country", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateSiteDTO.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateSiteDTO.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], UpdateSiteDTO.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "request_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "branch_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_name),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "branch_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_code),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateSiteDTO.prototype, "branch_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_site_level_dto_1.UpdateSiteLevelDTO],
        default: update_site_level_dto_1.UpdateSiteLevelDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_site_level_dto_1.UpdateSiteLevelDTO),
    __metadata("design:type", Array)
], UpdateSiteDTO.prototype, "levels", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_site_contact_dto_1.UpdateSiteContactDTO],
        default: update_site_contact_dto_1.UpdateSiteContactDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_site_contact_dto_1.UpdateSiteContactDTO),
    __metadata("design:type", Array)
], UpdateSiteDTO.prototype, "contacts", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_site_company_dto_1.UpdateSiteCompanyDTO],
        default: update_site_company_dto_1.UpdateSiteCompanyDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_site_company_dto_1.UpdateSiteCompanyDTO),
    __metadata("design:type", Array)
], UpdateSiteDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateSiteDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateSiteDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateSiteDTO.prototype, "deleted_at", void 0);
exports.UpdateSiteDTO = UpdateSiteDTO;
//# sourceMappingURL=update-site.dto.js.map