"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGroupSiteDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const src_1 = require("src");
const src_2 = require("src");
const update_group_site_company_dto_1 = require("./update-group-site-company.dto");
const update_group_site_contact_dto_1 = require("./update-group-site-contact.dto");
const update_group_site_level_dto_1 = require("./update-group-site-level.dto");
class UpdateGroupSiteDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "group_company_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "gid", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Array)
], UpdateGroupSiteDTO.prototype, "sites", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "group_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer' }),
    (0, class_validator_1.IsInt)(),
    (0, class_transformer_1.Transform)(({ value }) => parseInt(value)),
    __metadata("design:type", Number)
], UpdateGroupSiteDTO.prototype, "postal_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.longitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "longitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.latitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "latitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(5),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Kota Bandung","value":{"long_name":"Kota Bandung","short_name":"Kota Bandung","types":["administrative_area_level_2","political"]}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Jawa Barat","value":{"long_name":"Jawa Barat","short_name":"Jawa Barat","types":["administrative_area_level_1","political"],"state_code":"Jawa Barat","country_code":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "province", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Indonesia","value":{"long_name":"Indonesia","short_name":"ID","types":["country","political"],"iso2":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "country", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.website_url),
    (0, class_validator_1.IsUrl)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "website_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.sub_district),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "sub_district", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.helmet),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "hamlet", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.village),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "village", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateGroupSiteDTO.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateGroupSiteDTO.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], UpdateGroupSiteDTO.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "request_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "branch_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_name),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "branch_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_code),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateGroupSiteDTO.prototype, "branch_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_group_site_level_dto_1.UpdateGroupSiteLevelDTO],
        default: update_group_site_level_dto_1.UpdateGroupSiteLevelDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_group_site_level_dto_1.UpdateGroupSiteLevelDTO),
    __metadata("design:type", Array)
], UpdateGroupSiteDTO.prototype, "levels", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_group_site_contact_dto_1.UpdateGroupSiteContactDTO],
        default: update_group_site_contact_dto_1.UpdateGroupSiteContactDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_group_site_contact_dto_1.UpdateGroupSiteContactDTO),
    __metadata("design:type", Array)
], UpdateGroupSiteDTO.prototype, "contacts", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_group_site_company_dto_1.UpdateGroupSiteCompanyDTO],
        default: update_group_site_company_dto_1.UpdateGroupSiteCompanyDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_group_site_company_dto_1.UpdateGroupSiteCompanyDTO),
    __metadata("design:type", Array)
], UpdateGroupSiteDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupSiteDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupSiteDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupSiteDTO.prototype, "deleted_at", void 0);
exports.UpdateGroupSiteDTO = UpdateGroupSiteDTO;
//# sourceMappingURL=update-group-site.dto.js.map