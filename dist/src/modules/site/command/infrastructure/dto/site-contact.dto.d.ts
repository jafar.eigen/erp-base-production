import { SiteContactEntity } from '../../domain/entities/site-contact.entity';
export declare class SiteContactDTO implements SiteContactEntity {
    id: string;
    site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
