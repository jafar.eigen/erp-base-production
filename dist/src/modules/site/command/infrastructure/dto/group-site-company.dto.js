"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteCompanyDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const site_company_entity_1 = require("../../domain/entities/site-company.entity");
class GroupSiteCompanyDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "group_site_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "site_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.company_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "company_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "company_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "company_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enumName: 'Site Company Status',
        enum: Object.values(site_company_entity_1.SiteCompanyStatuses)
    }),
    (0, class_validator_1.IsIn)(Object.values(site_company_entity_1.SiteCompanyStatuses)),
    __metadata("design:type", String)
], GroupSiteCompanyDTO.prototype, "company_status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteCompanyDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteCompanyDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], GroupSiteCompanyDTO.prototype, "deleted_at", void 0);
exports.GroupSiteCompanyDTO = GroupSiteCompanyDTO;
//# sourceMappingURL=group-site-company.dto.js.map