import { RequestInfo } from 'src';
import { STATUS } from 'src';
import { ApprovalPerCompany } from 'src';
import { SiteEntity } from '../../domain/entities/site.entity';
import { UpdateSiteCompanyDTO } from './update-site-company.dto';
import { UpdateSiteContactDTO } from './update-site-contact.dto';
import { UpdateSiteLevelDTO } from './update-site-level.dto';
export declare class UpdateSiteDTO implements SiteEntity {
    id: string;
    group_id: string;
    code: string;
    name: string;
    status: STATUS;
    postal_code: number;
    website_url: string;
    longitude: string;
    latitude: string;
    sub_district: string;
    hamlet: string;
    village: string;
    address: string;
    city: string;
    province: string;
    country: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    request_info: RequestInfo;
    branch_id: string;
    branch_name: string;
    branch_code: string;
    levels: UpdateSiteLevelDTO[];
    contacts: UpdateSiteContactDTO[];
    companies: UpdateSiteCompanyDTO[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
