import { GroupSiteCompanyEntity } from '../../domain/entities/group-site-company.entity';
export declare class GroupSiteCompanyDTO implements GroupSiteCompanyEntity {
    group_site_id: string;
    id: string;
    site_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
