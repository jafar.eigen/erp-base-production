import { SiteLevelEntity } from '../../domain/entities/site-level.entity';
export declare class SiteLevelDTO implements SiteLevelEntity {
    id: string;
    site_id: string;
    number: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
