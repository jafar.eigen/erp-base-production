"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGroupSiteDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const group_site_level_dto_1 = require("./group-site-level.dto");
const group_site_company_dto_1 = require("./group-site-company.dto");
const group_site_contact_dto_1 = require("./group-site-contact.dto");
const src_1 = require("src");
const src_2 = require("src");
class CreateGroupSiteDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_company_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "group_company_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.gid),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "gid", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.sites),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Array)
], CreateGroupSiteDTO.prototype, "sites", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "group_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(10),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'integer', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.postal_code),
    (0, class_transformer_1.Transform)(({ value }) => {
        return value !== '' ? parseInt(value) : null;
    }),
    (0, class_validator_1.IsInt)(),
    __metadata("design:type", Number)
], CreateGroupSiteDTO.prototype, "postal_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.website_url),
    (0, class_validator_1.IsUrl)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "website_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.longitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "longitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.latitude),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "latitude", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.sub_district),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "sub_district", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.helmet),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(10),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "hamlet", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.village),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MaxLength)(50),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "village", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(5),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Kota Bandung","value":{"long_name":"Kota Bandung","short_name":"Kota Bandung","types":["administrative_area_level_2","political"]}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Jawa Barat","value":{"long_name":"Jawa Barat","short_name":"Jawa Barat","types":["administrative_area_level_1","political"],"state_code":"Jawa Barat","country_code":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "province", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        default: '{"label":"Indonesia","value":{"long_name":"Indonesia","short_name":"ID","types":["country","political"],"iso2":"ID"}}'
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "country", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateGroupSiteDTO.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateGroupSiteDTO.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateGroupSiteDTO.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "request_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "branch_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_name),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "branch_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', nullable: true, required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.branch_code),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateGroupSiteDTO.prototype, "branch_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [group_site_level_dto_1.GroupSiteLevelDTO],
        default: group_site_level_dto_1.GroupSiteLevelDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => group_site_level_dto_1.GroupSiteLevelDTO),
    __metadata("design:type", Array)
], CreateGroupSiteDTO.prototype, "levels", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [group_site_contact_dto_1.GroupSiteContactDTO],
        default: group_site_contact_dto_1.GroupSiteContactDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => group_site_contact_dto_1.GroupSiteContactDTO),
    __metadata("design:type", Array)
], CreateGroupSiteDTO.prototype, "contacts", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [group_site_company_dto_1.GroupSiteCompanyDTO],
        default: group_site_company_dto_1.GroupSiteCompanyDTO
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => group_site_company_dto_1.GroupSiteCompanyDTO),
    __metadata("design:type", Array)
], CreateGroupSiteDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateGroupSiteDTO.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateGroupSiteDTO.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateGroupSiteDTO.prototype, "deleted_at", void 0);
exports.CreateGroupSiteDTO = CreateGroupSiteDTO;
//# sourceMappingURL=create-group-site.dto.js.map