import { ApprovalPerCompany } from 'src';
import { GroupSiteEntity } from '../../domain/entities/group-site.entity';
import { GroupSiteLevelDTO } from './group-site-level.dto';
import { GroupSiteCompanyDTO } from './group-site-company.dto';
import { GroupSiteContactDTO } from './group-site-contact.dto';
import { SiteEntity } from '../../domain/entities/site.entity';
import { STATUS } from 'src';
import { RequestInfo } from 'src';
export declare class CreateGroupSiteDTO implements GroupSiteEntity {
    group_company_id: string;
    gid: string;
    sites?: SiteEntity[];
    id: string;
    group_id: string;
    code: string;
    name: string;
    status: STATUS;
    postal_code: number;
    website_url: string;
    longitude: string;
    latitude: string;
    sub_district: string;
    hamlet: string;
    village: string;
    address: string;
    city: string;
    province: string;
    country: string;
    creator_id: string;
    creator_name: string;
    editor_id?: string;
    editor_name?: string;
    deleted_by_id?: string;
    deleted_by_name?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    request_info: RequestInfo;
    branch_id: string;
    branch_name: string;
    branch_code: string;
    levels: GroupSiteLevelDTO[];
    contacts: GroupSiteContactDTO[];
    companies: GroupSiteCompanyDTO[];
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
