import { GroupSiteLevel } from '../../data/models/group-site-level.model';
import { GroupSite } from '../../data/models/group-sites.model';
export declare class GroupSiteLevelDTO implements GroupSiteLevel {
    group_site: GroupSite;
    id: string;
    group_site_id: string;
    number: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
