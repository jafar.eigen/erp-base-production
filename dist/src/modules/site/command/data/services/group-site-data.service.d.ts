import { RequestType } from 'src';
import { Repository } from 'typeorm';
import { GroupSiteDataService } from '../../domain/contracts/group-site-data-service.contract';
import { GroupSiteEntity } from '../../domain/entities/group-site.entity';
import { SiteEntity } from '../../domain/entities/site.entity';
import { GroupSite } from '../models/group-sites.model';
import { Site } from '../models/site.model';
export declare class GroupSiteDataServiceImpl implements GroupSiteDataService {
    private groupRepo;
    private siteRepo;
    constructor(groupRepo: Repository<GroupSite>, siteRepo: Repository<Site>);
    save(groupSite: GroupSiteEntity): Promise<GroupSiteEntity>;
    saveMany(groupSites: GroupSiteEntity[]): Promise<GroupSiteEntity[]>;
    count(): Promise<number>;
    findOneByName(name: string): Promise<GroupSiteEntity>;
    findOneByCode(code: string): Promise<GroupSiteEntity>;
    findOneById(groupId: string): Promise<GroupSiteEntity>;
    update(groupId: string, updatedData: GroupSiteEntity): Promise<GroupSiteEntity>;
    updateAndSetStatusesToChild(groupId: string, updatedData: GroupSiteEntity, siteRequestType: RequestType): Promise<GroupSiteEntity>;
    findOneAndDelete(groupId: string): Promise<GroupSiteEntity>;
    findByIds(groupIds: string[]): Promise<GroupSiteEntity[]>;
    findSitesByGroupIds(groupIds: string[]): Promise<SiteEntity[]>;
    findSitesByGroupId(groupId: string): Promise<SiteEntity[]>;
    findOne(groupId: string): Promise<GroupSiteEntity>;
    findOneByCodeSiteUserCompany(code: string): Promise<GroupSiteEntity>;
}
