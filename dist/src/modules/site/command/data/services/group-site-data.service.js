"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const database_config_1 = require("src/utils/database.config");
const typeorm_2 = require("typeorm");
const group_sites_model_1 = require("../models/group-sites.model");
const site_model_1 = require("../models/site.model");
let GroupSiteDataServiceImpl = class GroupSiteDataServiceImpl {
    constructor(groupRepo, siteRepo) {
        this.groupRepo = groupRepo;
        this.siteRepo = siteRepo;
    }
    async save(groupSite) {
        return await this.groupRepo.save(groupSite);
    }
    async saveMany(groupSites) {
        return await this.groupRepo.save(groupSites);
    }
    async count() {
        return await this.groupRepo.count({ withDeleted: true });
    }
    async findOneByName(name) {
        return await this.groupRepo.findOne({ where: { name } });
    }
    async findOneByCode(code) {
        return await this.groupRepo.findOne({ where: { code } });
    }
    async findOneById(groupId) {
        return await this.groupRepo.findOne(groupId, {
            relations: ['contacts', 'levels', 'companies']
        });
    }
    async update(groupId, updatedData) {
        const group = await this.groupRepo.findOne(groupId, {
            relations: ['levels', 'contacts', 'companies']
        });
        Object.assign(group, updatedData);
        return await this.groupRepo.save(group);
    }
    async updateAndSetStatusesToChild(groupId, updatedData, siteRequestType) {
        const group = await this.groupRepo.findOne(groupId, {
            relations: ['levels', 'contacts', 'companies']
        });
        Object.assign(group, updatedData);
        const saved = await this.groupRepo.save(group);
        if (!siteRequestType)
            return saved;
        const sites = await this.siteRepo.find({
            where: { group_id: saved.id }
        });
        await Promise.all(sites.map(async (site) => {
            var _a, _b;
            const oldStatus = (_b = (_a = site.requested_data) === null || _a === void 0 ? void 0 : _a.status) !== null && _b !== void 0 ? _b : siteRequestType;
            Object.assign(site.status, {
                oldStatus
            });
            await this.siteRepo.save(site);
        }));
        return saved;
    }
    async findOneAndDelete(groupId) {
        const group = await this.groupRepo.findOne(groupId);
        return await this.groupRepo.remove(group);
    }
    async findByIds(groupIds) {
        return await this.groupRepo.findByIds(groupIds, {
            relations: ['levels', 'contacts', 'companies']
        });
    }
    async findSitesByGroupIds(groupIds) {
        const sites = await this.siteRepo.find({
            where: { group_id: (0, typeorm_2.In)(groupIds) },
            relations: ['contacts', 'levels', 'companies']
        });
        return sites;
    }
    async findSitesByGroupId(groupId) {
        const sites = await this.siteRepo.find({
            where: { group_id: groupId },
            relations: ['contacts', 'levels', 'companies']
        });
        return sites;
    }
    async findOne(groupId) {
        return await this.groupRepo.findOne(groupId, {
            relations: ['levels', 'contacts', 'companies']
        });
    }
    async findOneByCodeSiteUserCompany(code) {
        return await this.groupRepo.findOne({ where: { code } });
    }
};
GroupSiteDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(group_sites_model_1.GroupSite, database_config_1.SITE_CUD_CONNECTION)),
    __param(1, (0, typeorm_1.InjectRepository)(site_model_1.Site, database_config_1.SITE_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], GroupSiteDataServiceImpl);
exports.GroupSiteDataServiceImpl = GroupSiteDataServiceImpl;
//# sourceMappingURL=group-site-data.service.js.map