import { Repository } from 'typeorm';
import { BaseDataService } from 'src';
import { SiteCompanyEntity } from '../../domain/entities/site-company.entity';
import { SiteEntity } from '../../domain/entities/site.entity';
import { SiteCompany } from '../models/site-company.model';
import { SiteContact } from '../models/site-contact.model';
import { SiteLevel } from '../models/site-level.model';
import { Site } from '../models/site.model';
import { Location } from 'src/modules/location/command/data/models/location.model';
import { LocationEntity } from 'src/modules/location/command/domain/entities/location.entity';
export declare class SiteDataServiceImpl extends BaseDataService<SiteEntity> {
    private siteRepo;
    private siteLevelRepo;
    private siteCompanyRepo;
    private siteContactRepo;
    private locationRepo;
    relations: string[];
    constructor(siteRepo: Repository<Site>, siteLevelRepo: Repository<SiteLevel>, siteCompanyRepo: Repository<SiteCompany>, siteContactRepo: Repository<SiteContact>, locationRepo: Repository<Location>);
    findCompanyBySingleSite(siteId: string): Promise<SiteCompanyEntity[]>;
    findByCompanyId(companyId: string): Promise<SiteEntity[]>;
    findAndDeleteNulledRelation(): Promise<void>;
    findLocationBySitesId(siteId: string): Promise<LocationEntity[]>;
    findSiteByBranchId(branch_id: string): Promise<SiteEntity[]>;
}
