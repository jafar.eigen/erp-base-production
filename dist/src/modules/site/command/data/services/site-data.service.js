"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const src_1 = require("src");
const database_config_1 = require("src/utils/database.config");
const site_company_model_1 = require("../models/site-company.model");
const site_contact_model_1 = require("../models/site-contact.model");
const site_level_model_1 = require("../models/site-level.model");
const site_model_1 = require("../models/site.model");
const location_model_1 = require("src/modules/location/command/data/models/location.model");
let SiteDataServiceImpl = class SiteDataServiceImpl extends src_1.BaseDataService {
    constructor(siteRepo, siteLevelRepo, siteCompanyRepo, siteContactRepo, locationRepo) {
        super(siteRepo);
        this.siteRepo = siteRepo;
        this.siteLevelRepo = siteLevelRepo;
        this.siteCompanyRepo = siteCompanyRepo;
        this.siteContactRepo = siteContactRepo;
        this.locationRepo = locationRepo;
        this.relations = ['contacts', 'levels', 'companies'];
    }
    async findCompanyBySingleSite(siteId) {
        var _a;
        return (_a = (await this.siteRepo.findOne(siteId, {
            relations: ['companies']
        }))) === null || _a === void 0 ? void 0 : _a.companies;
    }
    async findByCompanyId(companyId) {
        return await this.siteRepo.find({
            relations: ['contacts', 'levels', 'companies'],
            join: {
                alias: 'site',
                innerJoin: {
                    companies: 'site.companies'
                }
            },
            where: (querySite) => {
                querySite.where('companies.company_id = :companyId', { companyId });
            }
        });
    }
    async findAndDeleteNulledRelation() {
        const levels = await this.siteLevelRepo.find({
            where: {
                site_id: (0, typeorm_2.IsNull)()
            }
        });
        const contacts = await this.siteContactRepo.find({
            where: {
                site_id: (0, typeorm_2.IsNull)()
            }
        });
        const companies = await this.siteCompanyRepo.find({
            site_id: (0, typeorm_2.IsNull)()
        });
        if (levels) {
            await this.siteLevelRepo.remove(levels);
        }
        if (contacts) {
            await this.siteContactRepo.remove(contacts);
        }
        if (companies) {
            await this.siteCompanyRepo.remove(companies);
        }
    }
    async findLocationBySitesId(siteId) {
        return await this.locationRepo.find({
            where: { site_id: siteId, is_generated: 0 }
        });
    }
    async findSiteByBranchId(branch_id) {
        return await this.siteRepo.find({
            relations: this.relations,
            where: { branch_id }
        });
    }
};
SiteDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(site_model_1.Site, database_config_1.SITE_CUD_CONNECTION)),
    __param(1, (0, typeorm_1.InjectRepository)(site_level_model_1.SiteLevel, database_config_1.SITE_CUD_CONNECTION)),
    __param(2, (0, typeorm_1.InjectRepository)(site_company_model_1.SiteCompany, database_config_1.SITE_CUD_CONNECTION)),
    __param(3, (0, typeorm_1.InjectRepository)(site_contact_model_1.SiteContact, database_config_1.SITE_CUD_CONNECTION)),
    __param(4, (0, typeorm_1.InjectRepository)(location_model_1.Location, database_config_1.SITE_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], SiteDataServiceImpl);
exports.SiteDataServiceImpl = SiteDataServiceImpl;
//# sourceMappingURL=site-data.service.js.map