"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Site = void 0;
const src_1 = require("src");
const typeorm_1 = require("typeorm");
const group_sites_model_1 = require("./group-sites.model");
const site_company_model_1 = require("./site-company.model");
const site_contact_model_1 = require("./site-contact.model");
const site_level_model_1 = require("./site-level.model");
let Site = class Site {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Site.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.STATUS }),
    __metadata("design:type", String)
], Site.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], Site.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], Site.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], Site.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], Site.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], Site.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], Site.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], Site.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], Site.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], Site.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'branch_id', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "branch_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_code', length: '30', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "branch_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_id', nullable: true }),
    __metadata("design:type", String)
], Site.prototype, "group_id", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_generated', nullable: true, default: false }),
    __metadata("design:type", Boolean)
], Site.prototype, "is_generated", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'is_head_office', nullable: true, default: false }),
    __metadata("design:type", Boolean)
], Site.prototype, "is_head_office", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Site.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Site.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Site.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_level_model_1.SiteLevel, (siteLevel) => siteLevel.site, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], Site.prototype, "levels", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_contact_model_1.SiteContact, (siteContact) => siteContact.site, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], Site.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_company_model_1.SiteCompany, (siteCompany) => siteCompany.site, {
        cascade: ['insert', 'soft-remove', 'update']
    }),
    __metadata("design:type", Array)
], Site.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_sites_model_1.GroupSite, (group) => group.sites),
    (0, typeorm_1.JoinColumn)({ name: 'group_id' }),
    __metadata("design:type", group_sites_model_1.GroupSite)
], Site.prototype, "group", void 0);
Site = __decorate([
    (0, typeorm_1.Entity)('sites')
], Site);
exports.Site = Site;
//# sourceMappingURL=site.model.js.map