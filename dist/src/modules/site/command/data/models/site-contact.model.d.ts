import { SiteContactEntity } from '../../domain/entities/site-contact.entity';
import { Site } from './site.model';
export declare class SiteContact implements SiteContactEntity {
    id: string;
    site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    site: Site;
}
