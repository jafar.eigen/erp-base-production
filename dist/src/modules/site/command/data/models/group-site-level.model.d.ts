import { SiteLevelEntity } from '../../domain/entities/site-level.entity';
import { GroupSite } from './group-sites.model';
export declare class GroupSiteLevel implements Partial<SiteLevelEntity> {
    id: string;
    number: number;
    name: string;
    group_site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSite;
}
