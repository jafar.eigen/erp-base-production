import { SiteLevelEntity } from '../../domain/entities/site-level.entity';
import { Site } from './site.model';
export declare class SiteLevel implements SiteLevelEntity {
    id: string;
    number: number;
    name: string;
    site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    site: Site;
}
