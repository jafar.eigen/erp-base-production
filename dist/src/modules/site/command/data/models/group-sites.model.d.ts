import { RequestInfo, STATUS, ApprovalPerCompany } from 'src';
import { GroupCompanies } from 'src/modules/company/command/data/models/group-companies.model';
import { GroupSiteEntity } from '../../domain/entities/group-site.entity';
import { SiteEntity } from '../../domain/entities/site.entity';
import { GroupSiteCompany } from './group-site-company.model';
import { GroupSiteContact } from './group-site-contact.model';
import { GroupSiteLevel } from './group-site-level.model';
import { Site } from './site.model';
export declare class GroupSite implements GroupSiteEntity {
    id: string;
    name: string;
    code: string;
    status: STATUS;
    request_info: RequestInfo;
    address: string;
    city: string;
    province: string;
    country: string;
    sub_district: string;
    hamlet: string;
    village: string;
    postal_code: number;
    website_url: string;
    longitude: string;
    latitude: string;
    creator_id: string;
    creator_name: string;
    editor_id: string;
    editor_name: string;
    deleted_by_id: string;
    deleted_by_name: string;
    has_requested_process: boolean;
    approval: ApprovalPerCompany;
    requested_data: Partial<SiteEntity>;
    branch_id: string;
    branch_name: string;
    branch_code: string;
    gid: string;
    group_company_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    sites: Site[];
    levels: GroupSiteLevel[];
    contacts: GroupSiteContact[];
    companies: GroupSiteCompany[];
    group_company: GroupCompanies;
}
