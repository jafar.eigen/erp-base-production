import { GroupSiteCompanyEntity } from '../../domain/entities/group-site-company.entity';
import { GroupSite } from './group-sites.model';
export declare class GroupSiteCompany implements GroupSiteCompanyEntity {
    id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    company_status: string;
    group_site_id: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSite;
}
