"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSite = void 0;
const src_1 = require("src");
const group_companies_model_1 = require("src/modules/company/command/data/models/group-companies.model");
const typeorm_1 = require("typeorm");
const group_site_company_model_1 = require("./group-site-company.model");
const group_site_contact_model_1 = require("./group-site-contact.model");
const group_site_level_model_1 = require("./group-site-level.model");
const site_model_1 = require("./site.model");
let GroupSite = class GroupSite {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], GroupSite.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], GroupSite.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.STATUS }),
    __metadata("design:type", String)
], GroupSite.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], GroupSite.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'address' }),
    __metadata("design:type", String)
], GroupSite.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'city' }),
    __metadata("design:type", String)
], GroupSite.prototype, "city", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'province' }),
    __metadata("design:type", String)
], GroupSite.prototype, "province", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { name: 'country' }),
    __metadata("design:type", String)
], GroupSite.prototype, "country", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'sub_district', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "sub_district", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'hamlet', length: '10', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "hamlet", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'village', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "village", void 0);
__decorate([
    (0, typeorm_1.Column)('int', { name: 'postal_code', nullable: true }),
    __metadata("design:type", Number)
], GroupSite.prototype, "postal_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'website_url', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "website_url", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'longitude', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'latitude', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], GroupSite.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], GroupSite.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], GroupSite.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'branch_id', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "branch_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_name', length: '50', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "branch_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'branch_code', length: '20', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "branch_code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'gid' }),
    __metadata("design:type", String)
], GroupSite.prototype, "gid", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'group_company_id', nullable: true }),
    __metadata("design:type", String)
], GroupSite.prototype, "group_company_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSite.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSite.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], GroupSite.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => site_model_1.Site, (site) => site.group),
    __metadata("design:type", Array)
], GroupSite.prototype, "sites", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_level_model_1.GroupSiteLevel, (level) => level.group_site, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupSite.prototype, "levels", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_contact_model_1.GroupSiteContact, (contact) => contact.group_site, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupSite.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_site_company_model_1.GroupSiteCompany, (company) => company.group_site, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], GroupSite.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => group_companies_model_1.GroupCompanies, (groupCompany) => groupCompany.group_sites),
    (0, typeorm_1.JoinColumn)({ name: 'group_company_id' }),
    __metadata("design:type", group_companies_model_1.GroupCompanies)
], GroupSite.prototype, "group_company", void 0);
GroupSite = __decorate([
    (0, typeorm_1.Entity)('group_sites')
], GroupSite);
exports.GroupSite = GroupSite;
//# sourceMappingURL=group-sites.model.js.map