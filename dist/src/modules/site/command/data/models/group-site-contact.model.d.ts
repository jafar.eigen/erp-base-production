import { SiteContactEntity } from '../../domain/entities/site-contact.entity';
import { GroupSite } from './group-sites.model';
export declare class GroupSiteContact implements Partial<SiteContactEntity> {
    id: string;
    group_site_id: string;
    phone: string;
    ext: string;
    email: string;
    notes: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    group_site: GroupSite;
}
