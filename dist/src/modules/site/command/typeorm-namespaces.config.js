"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const company_contact_model_1 = require("src/modules/company/command/data/models/company-contact.model");
const company_model_1 = require("src/modules/company/command/data/models/company.model");
const group_companies_model_1 = require("src/modules/company/command/data/models/group-companies.model");
const group_location_model_1 = require("src/modules/location/command/data/models/group-location.model");
const location_map_raw_model_1 = require("src/modules/location/command/data/models/location-map-raw.model");
const location_map_model_1 = require("src/modules/location/command/data/models/location-map.model");
const location_model_1 = require("src/modules/location/command/data/models/location.model");
const config_2 = require("src/utils/config");
const group_site_company_model_1 = require("./data/models/group-site-company.model");
const group_site_contact_model_1 = require("./data/models/group-site-contact.model");
const group_site_level_model_1 = require("./data/models/group-site-level.model");
const group_sites_model_1 = require("./data/models/group-sites.model");
const site_company_model_1 = require("./data/models/site-company.model");
const site_contact_model_1 = require("./data/models/site-contact.model");
const site_level_model_1 = require("./data/models/site-level.model");
const site_model_1 = require("./data/models/site.model");
exports.default = (0, config_1.registerAs)('typeormSiteCommand', () => ({
    type: 'mysql',
    host: config_2.variableConfig.SITE_MYSQL_COMMAND_HOST,
    port: config_2.variableConfig.SITE_MYSQL_COMMAND_PORT,
    username: config_2.variableConfig.SITE_MYSQL_COMMAND_USERNAME,
    password: config_2.variableConfig.SITE_MYSQL_COMMAND_PASSWORD,
    database: config_2.variableConfig.SITE_MYSQL_COMMAND_DATABASE,
    entities: [
        site_model_1.Site,
        site_company_model_1.SiteCompany,
        site_contact_model_1.SiteContact,
        site_level_model_1.SiteLevel,
        group_sites_model_1.GroupSite,
        group_site_contact_model_1.GroupSiteContact,
        group_site_level_model_1.GroupSiteLevel,
        group_site_company_model_1.GroupSiteCompany,
        group_companies_model_1.GroupCompanies,
        company_model_1.Company,
        company_contact_model_1.Contact,
        location_model_1.Location,
        location_map_model_1.LocationMap,
        group_location_model_1.GroupLocation,
        location_map_raw_model_1.LocationMapRow
    ],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map