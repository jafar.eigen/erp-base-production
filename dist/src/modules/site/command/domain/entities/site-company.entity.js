"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteCompanyStatuses = void 0;
var SiteCompanyStatuses;
(function (SiteCompanyStatuses) {
    SiteCompanyStatuses["DRAFT"] = "draft";
    SiteCompanyStatuses["ACTIVE"] = "active";
    SiteCompanyStatuses["INACTIVE"] = "inactive";
    SiteCompanyStatuses["REQUESTED"] = "requested";
    SiteCompanyStatuses["OPEN"] = "open";
    SiteCompanyStatuses["DECLINE"] = "declined";
})(SiteCompanyStatuses = exports.SiteCompanyStatuses || (exports.SiteCompanyStatuses = {}));
//# sourceMappingURL=site-company.entity.js.map