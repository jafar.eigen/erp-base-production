export interface SiteResultBatch {
    success: string[];
    failed: string[];
}
