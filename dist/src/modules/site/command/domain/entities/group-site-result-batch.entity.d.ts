export interface GroupSiteResultBatch {
    success: string[];
    failed: string[];
}
