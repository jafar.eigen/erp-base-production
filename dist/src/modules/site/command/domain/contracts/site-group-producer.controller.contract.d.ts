import { IResponses } from 'src';
import { CreateGroupSiteDTO } from '../../infrastructure/dto/create-group-site.dto';
import { UpdateGroupSiteDTO } from '../../infrastructure/dto/update-group-site.dto';
export interface GroupSiteProducerController {
    activate(groupIds: string[], headers: string): Promise<IResponses>;
    deactivate(groupIds: string[], headers: string): Promise<IResponses>;
    create(body: CreateGroupSiteDTO, headers: string): Promise<IResponses>;
    update(groupId: string, updatedData: UpdateGroupSiteDTO, headers: string): Promise<IResponses>;
    delete(groupIds: string[], headers: string): Promise<IResponses>;
    request(groupId: string, headers: string): Promise<IResponses>;
    approve(groupId: string, headers: string, step: number): Promise<IResponses>;
    decline(groupId: string, headers: string, step: number): Promise<IResponses>;
}
