import { GroupSiteEntity } from '../entities/group-site.entity';
import { SiteEntity } from '../entities/site.entity';
export interface GroupSiteDataService {
    save(groupSite: GroupSiteEntity): Promise<GroupSiteEntity>;
    saveMany(groupSite: GroupSiteEntity[]): Promise<GroupSiteEntity[]>;
    count(): Promise<number>;
    findOneById(groupId: string): Promise<GroupSiteEntity>;
    findOneByName(name: string): Promise<GroupSiteEntity>;
    findOneByCode(code: string): Promise<GroupSiteEntity>;
    update(groupId: string, updatedData: GroupSiteEntity): Promise<GroupSiteEntity>;
    findOneAndDelete(groupId: string): Promise<GroupSiteEntity>;
    findByIds(groupIds: string[]): Promise<GroupSiteEntity[]>;
    findSitesByGroupIds(groupIds: string[]): Promise<SiteEntity[]>;
    findOne(groupId: string): Promise<GroupSiteEntity>;
}
