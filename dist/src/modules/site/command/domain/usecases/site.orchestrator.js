"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const site_data_service_1 = require("../../data/services/site-data.service");
const site_producer_service_1 = require("../../infrastructure/producer/site-producer.service");
const activate_site_manager_1 = require("./managers/company/activate-site.manager");
const group_site_data_service_1 = require("../../data/services/group-site-data.service");
const switch_group_on_site_manager_1 = require("./managers/group/switch-group-on-site.manager");
const update_site_manager_1 = require("./managers/company/update-site.manager");
const delete_site_manager_1 = require("./managers/company/delete-site.manager");
const deactivate_site_manager_1 = require("./managers/company/deactivate-site-manager");
const submit_site_request_manager_1 = require("./managers/approvals/company/submit-site-request.manager");
const approve_site_request_manager_1 = require("./managers/approvals/company/approve-site-request.manager");
const declined_site_request_manager_1 = require("./managers/approvals/company/declined-site-request.manager");
const create_site_manager_1 = require("./managers/company/create-site-manager");
const cancel_site_request_manager_1 = require("./managers/company/cancel-site-request.manager");
const src_1 = require("src");
let SiteOrchestrator = class SiteOrchestrator extends src_1.BaseOrchestrator {
    constructor(siteDataService, siteProducerService, groupDataService) {
        super();
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.groupDataService = groupDataService;
    }
    async create(site) {
        return await new create_site_manager_1.CreateSiteManager(this.siteDataService, this.siteProducerService, this.groupDataService, site).execute();
    }
    async update(siteId, updatedData) {
        return await new update_site_manager_1.UpdateSiteManager(this.siteDataService, this.siteProducerService, siteId, updatedData).execute();
    }
    async delete(siteIds) {
        const deleteSite = new delete_site_manager_1.DeleteSiteManager(siteIds, this.siteDataService, this.siteProducerService);
        await deleteSite.execute();
        return deleteSite.getResult();
    }
    async deactivate(siteIds) {
        const deactivateSite = new deactivate_site_manager_1.DeactivateSiteManager(siteIds, this.siteDataService, this.siteProducerService);
        await deactivateSite.execute();
        return deactivateSite.getResult();
    }
    async activate(siteIds) {
        const activateSite = new activate_site_manager_1.ActivateSiteManager(siteIds, this.siteDataService, this.siteProducerService);
        await activateSite.execute();
        return activateSite.getResult();
    }
    async switchGroup(siteId, groupId) {
        return await new switch_group_on_site_manager_1.SwitchGroupOnSite(this.siteDataService, this.groupDataService, this.siteProducerService, siteId, groupId).execute();
    }
    async request(siteId) {
        const submitSite = new submit_site_request_manager_1.SubmitSiteRequestManager(siteId, this.siteDataService, this.siteProducerService, this.groupDataService);
        await submitSite.execute();
        return submitSite.getResult();
    }
    async approve(siteId, step) {
        return await new approve_site_request_manager_1.ApproveSiteRequestManager(this.siteDataService, this.siteProducerService, siteId, step).execute();
    }
    async decline(siteId, step) {
        return await new declined_site_request_manager_1.DeclinedSiteRequestManager(this.siteDataService, this.siteProducerService, siteId, step).execute();
    }
    async cancel(siteId) {
        return await new cancel_site_request_manager_1.CancelSiteRequestManager(siteId, this.siteDataService, this.siteProducerService).execute();
    }
    async rollbackStatusTrx(entityId) {
        throw new Error('Method not implemented.');
    }
};
SiteOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [site_data_service_1.SiteDataServiceImpl,
        site_producer_service_1.SiteProducerServiceImpl,
        group_site_data_service_1.GroupSiteDataServiceImpl])
], SiteOrchestrator);
exports.SiteOrchestrator = SiteOrchestrator;
//# sourceMappingURL=site.orchestrator.js.map