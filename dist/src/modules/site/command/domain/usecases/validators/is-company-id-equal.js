"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsCompanyEqual = void 0;
const IsCompanyEqual = (siteToCheck, site) => {
    const companyIdToCheck = siteToCheck === null || siteToCheck === void 0 ? void 0 : siteToCheck.companies[0].company_id;
    const companyId = site === null || site === void 0 ? void 0 : site.companies[0].company_id;
    return companyIdToCheck === companyId;
};
exports.IsCompanyEqual = IsCompanyEqual;
//# sourceMappingURL=is-company-id-equal.js.map