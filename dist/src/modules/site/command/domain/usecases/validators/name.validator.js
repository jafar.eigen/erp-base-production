"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
const common_1 = require("@nestjs/common");
const is_company_id_equal_1 = require("./is-company-id-equal");
class Name {
    constructor(siteDataService, site, siteId = null) {
        this.siteDataService = siteDataService;
        this.site = site;
        this.siteId = siteId;
    }
    async execute() {
        var _a;
        const site = await this.siteDataService.findByName(this.site.name);
        if (this.siteId !== ((_a = site[0]) === null || _a === void 0 ? void 0 : _a.id) && (0, is_company_id_equal_1.IsCompanyEqual)(this.site, site[0])) {
            throw new common_1.BadRequestException(`Name already exists.`);
        }
        if (site[0] && !this.siteId && (0, is_company_id_equal_1.IsCompanyEqual)(this.site, site[0])) {
            throw new common_1.BadRequestException(`Name already exists.`);
        }
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map