"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeSiteGroup = void 0;
class CodeSiteGroup {
    constructor(groupSiteDataService, group, groupSiteId = null) {
        this.groupSiteDataService = groupSiteDataService;
        this.group = group;
        this.groupSiteId = groupSiteId;
    }
    async execute() {
        const group = await this.groupSiteDataService.findOneByCode(this.group.code);
        if (group && this.groupSiteId !== group.id) {
            throw new Error('Code already exists.');
        }
        if (!this.groupSiteId && group) {
            throw new Error('Code already exists.');
        }
    }
}
exports.CodeSiteGroup = CodeSiteGroup;
//# sourceMappingURL=code-site-group.validator.js.map