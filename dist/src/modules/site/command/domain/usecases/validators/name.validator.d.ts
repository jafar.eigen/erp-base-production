import { SiteDataServiceImpl } from '../../../data/services/site-data.service';
import { SiteEntity } from '../../entities/site.entity';
export declare class Name {
    private siteDataService;
    private site;
    private siteId;
    constructor(siteDataService: SiteDataServiceImpl, site: SiteEntity, siteId?: string);
    execute(): Promise<void | Error>;
}
