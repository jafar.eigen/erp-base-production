"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NameSiteGroup = void 0;
class NameSiteGroup {
    constructor(groupSiteDataService, group, groupSiteId = null) {
        this.groupSiteDataService = groupSiteDataService;
        this.group = group;
        this.groupSiteId = groupSiteId;
    }
    async execute() {
        const group = await this.groupSiteDataService.findOneByName(this.group.name);
        if (group && this.groupSiteId !== group.id) {
            throw new Error('Name already exists.');
        }
        if (!this.groupSiteId && group) {
            throw new Error('Name already exists.');
        }
    }
}
exports.NameSiteGroup = NameSiteGroup;
//# sourceMappingURL=name-site-group.validator.js.map