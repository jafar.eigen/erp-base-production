"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
const common_1 = require("@nestjs/common");
const is_company_id_equal_1 = require("./is-company-id-equal");
class Code {
    constructor(siteDataService, site, siteId) {
        this.siteDataService = siteDataService;
        this.site = site;
        this.siteId = siteId;
    }
    async execute() {
        const site = await this.siteDataService.findByCode(this.site.code);
        if (this.siteId !== (site === null || site === void 0 ? void 0 : site.id) && (0, is_company_id_equal_1.IsCompanyEqual)(site, this.site)) {
            throw new common_1.BadRequestException(`Code already exists.`);
        }
        if (site && !this.siteId && (0, is_company_id_equal_1.IsCompanyEqual)(site, this.site)) {
            throw new common_1.BadRequestException(`Code already exists.`);
        }
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map