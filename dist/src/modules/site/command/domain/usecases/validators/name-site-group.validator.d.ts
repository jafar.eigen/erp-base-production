import { GroupSiteDataServiceImpl } from '../../../data/services/group-site-data.service';
import { GroupSiteEntity } from '../../entities/group-site.entity';
export declare class NameSiteGroup {
    private groupSiteDataService;
    private group;
    private groupSiteId;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, group: GroupSiteEntity, groupSiteId?: string);
    execute(): Promise<void>;
}
