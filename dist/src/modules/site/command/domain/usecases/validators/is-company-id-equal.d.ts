import { SiteEntity } from '../../entities/site.entity';
export declare const IsCompanyEqual: (siteToCheck: SiteEntity, site: SiteEntity) => boolean;
