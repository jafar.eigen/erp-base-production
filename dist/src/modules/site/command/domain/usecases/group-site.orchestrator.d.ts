import { ApprovalPerCompany } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from '../../data/services/group-site-data.service';
import { SiteDataServiceImpl } from '../../data/services/site-data.service';
import { CreateGroupSiteDTO } from '../../infrastructure/dto/create-group-site.dto';
import { SiteProducerServiceImpl } from '../../infrastructure/producer/site-producer.service';
import { GroupSiteResultBatch } from '../entities/group-site-result-batch.entity';
import { GroupSiteEntity } from '../entities/group-site.entity';
export declare class GroupSiteOrchestrator {
    private groupSiteDataService;
    private siteDataService;
    private siteProducerService;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    create(body: CreateGroupSiteDTO, approvals: ApprovalPerCompany[], user: IUserPayload): Promise<GroupSiteEntity>;
    update(groupId: string, body: GroupSiteEntity, approvals: ApprovalPerCompany[], user: IUserPayload): Promise<GroupSiteEntity>;
    delete(groupIds: string[], approvals: ApprovalPerCompany[], user: IUserPayload): Promise<GroupSiteResultBatch>;
    activate(groupIds: string[], user: IUserPayload, approvals: ApprovalPerCompany[]): Promise<GroupSiteResultBatch>;
    deactivate(groupIds: string[], user: IUserPayload, approvals: ApprovalPerCompany[]): Promise<GroupSiteResultBatch>;
    request(groupId: string, user: IUserPayload): Promise<GroupSiteEntity>;
    approve(groupId: string, user: IUserPayload, step: number): Promise<GroupSiteEntity>;
    decline(groupId: string, step: number, user: IUserPayload): Promise<GroupSiteEntity>;
}
