"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteGroupSiteManager = void 0;
const delete_group_site_request_manager_1 = require("../approvals/group/delete-group-site-request.manager");
class DeleteGroupSiteManager {
    constructor(groupSiteDataService, siteProducerService, groupIds, user, approvals) {
        this.groupSiteDataService = groupSiteDataService;
        this.siteProducerService = siteProducerService;
        this.groupIds = groupIds;
        this.user = user;
        this.approvals = approvals;
        this.failedMessages = [];
        this.successMessages = [];
    }
    async execute() {
        const sites = await this.groupSiteDataService.findSitesByGroupIds(this.groupIds);
        if (sites.length) {
            for (const site of sites) {
                this.failedMessages.push(`Group site with id ${site.group_id} still had member (site company)`);
            }
            return this;
        }
        const groups = await this.groupSiteDataService.findByIds(this.groupIds);
        await Promise.all(groups.map(async (group) => {
            const approvalDelete = await new delete_group_site_request_manager_1.DeleteGroupSiteRequestManager(group.id, this.user, this.approvals, this.groupSiteDataService, this.siteProducerService).run();
            if (approvalDelete) {
                this.successMessages.push(`Group site with id ${group.id} successfully deleted.`);
            }
        }));
        return this;
    }
    getMessages() {
        return {
            success: this.successMessages,
            failed: this.failedMessages
        };
    }
}
exports.DeleteGroupSiteManager = DeleteGroupSiteManager;
//# sourceMappingURL=delete-group-site.manager.js.map