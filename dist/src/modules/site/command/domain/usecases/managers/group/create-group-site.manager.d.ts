import { ApprovalPerCompany, IUserPayload } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { GroupSiteDataServiceImpl } from '../../../../data/services/group-site-data.service';
import { SiteProducerServiceImpl } from '../../../../infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../entities/group-site.entity';
export declare class CreateGroupSiteManager {
    private groupSiteDataService;
    private siteDataService;
    private siteProducerService;
    private group;
    private user;
    private approvals;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, group: GroupSiteEntity, user: IUserPayload, approvals: ApprovalPerCompany[]);
    execute(): Promise<GroupSiteEntity>;
    private generateGroupId;
    private checkSiteCompanyNameBySiteGroupName;
}
