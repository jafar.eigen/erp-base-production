"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateSiteManager = void 0;
const src_1 = require("src");
class GenerateSiteManager {
    constructor(siteDataService, kafkaService, data, user) {
        this.siteDataService = siteDataService;
        this.kafkaService = kafkaService;
        this.data = data;
        this.user = user;
    }
    async run() {
        await this.generate();
    }
    async generate() {
        await Promise.all(this.data.companies.map(async (company) => {
            const site = await this.siteDataService.save(this.getValidFormSite(this.data, company));
            await this.kafkaService.baseCreated({
                data: site,
                old: null,
                user: this.user
            });
        }));
    }
    getValidFormSite(group, company) {
        var _a, _b;
        const groupId = group.id;
        delete group.id;
        delete group.sites;
        (_a = group.levels) === null || _a === void 0 ? void 0 : _a.map((level) => {
            delete level.id;
            delete level.group_site_id;
            delete level.created_at;
            delete level.updated_at;
            return level;
        });
        (_b = group.contacts) === null || _b === void 0 ? void 0 : _b.map((contact) => {
            delete contact.id;
            delete contact.group_site_id;
            delete contact.created_at;
            delete contact.updated_at;
            return contact;
        });
        delete company.id;
        delete company.group_site_id;
        delete company.created_at;
        delete company.updated_at;
        const site = Object.assign({}, group, {
            companies: [company],
            request_info: src_1.RequestInfo.EDIT_ACTIVE,
            group_id: groupId,
            status: src_1.STATUS.DRAFT
        });
        delete group.id;
        delete site.id;
        group.id = groupId;
        return site;
    }
}
exports.GenerateSiteManager = GenerateSiteManager;
//# sourceMappingURL=generate-site.manager.js.map