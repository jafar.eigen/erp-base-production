import { KafkaPayload } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../../entities/group-site.entity';
export declare class DeclinedGroupSiteRequestManager {
    private readonly id;
    private readonly user;
    private readonly step;
    private readonly dataService;
    private readonly kafkaService;
    data: GroupSiteEntity;
    constructor(id: string, user: IUserPayload, step: number, dataService: GroupSiteDataServiceImpl, kafkaService: SiteProducerServiceImpl);
    getRecentApproval(): Promise<void>;
    responseApproval(): Promise<void>;
    run(): Promise<GroupSiteEntity>;
    protected submitRequest(data: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected produceTopic(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
}
