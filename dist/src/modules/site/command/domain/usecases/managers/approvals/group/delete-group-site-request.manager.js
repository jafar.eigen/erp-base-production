"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteGroupSiteRequestManager = void 0;
const src_1 = require("src");
const src_2 = require("src");
class DeleteGroupSiteRequestManager {
    constructor(id, user, approvals, dataService, kafkaService) {
        this.id = id;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
    async run() {
        await this.getRecentApproval();
        await this.processRequest();
        return this.SiteGroup;
    }
    async getRecentApproval() {
        const SiteGroup = await this.dataService.findOne(this.id);
        Object.assign(this, { SiteGroup });
        this.assignApproval();
    }
    async processRequest() {
        const builder = new src_2.SubmitRequest();
        const SiteGroup = await builder
            .setRequestType(src_2.RequestType.DELETE)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setData(this.SiteGroup)
            .setSubmitRequestFunction(this.submitRequest)
            .setPassApprovalFunction(this.skiApproval)
            .setProduceSubmitRequestTopic(this.produceTopic)
            .setProducePassApprovalTopic(this.produceSubmissionTopicSuccess)
            .run();
        Object.assign(this, { SiteGroup });
    }
    async submitRequest(group) {
        return await this.dataService.update(this.id, group);
    }
    async skiApproval(group) {
        return await this.dataService.findOneAndDelete(group.id);
    }
    async produceTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    async produceSubmissionTopicSuccess(payload) {
        return this.kafkaService.groupSiteDeleted(payload);
    }
    assignApproval() {
        const approval = (0, src_1.findApproval)(this.approvals, this.SiteGroup, this.user);
        Object.assign(this.SiteGroup, { approval: approval });
    }
}
exports.DeleteGroupSiteRequestManager = DeleteGroupSiteRequestManager;
//# sourceMappingURL=delete-group-site-request.manager.js.map