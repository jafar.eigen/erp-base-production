import { ApprovalPerCompany } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from '../../../../data/services/group-site-data.service';
import { SiteProducerServiceImpl } from '../../../../infrastructure/producer/site-producer.service';
import { GroupSiteResultBatch } from '../../../entities/group-site-result-batch.entity';
export declare class DeleteGroupSiteManager {
    private groupSiteDataService;
    private siteProducerService;
    private groupIds;
    private user;
    private approvals;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, groupIds: string[], user: IUserPayload, approvals: ApprovalPerCompany[]);
    private failedMessages;
    private successMessages;
    execute(): Promise<this>;
    getMessages(): GroupSiteResultBatch;
}
