"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGroupSiteApprovalManager = exports.UpdateManager = void 0;
const common_1 = require("@nestjs/common");
const src_1 = require("src");
class UpdateManager {
    constructor(id, updatedData, user, approvals, dataService) {
        this.id = id;
        this.updatedData = updatedData;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
    }
    async run() {
        await this.getRecentData();
        await this.generateApproval();
        return await this.save();
    }
    async getRecentData() {
        const transactionData = await this.dataService.findOneById(this.id);
        if (!transactionData)
            throw new common_1.HttpException(`Approval with ${this.id} is not found`, common_1.HttpStatus.BAD_GATEWAY);
        Object.assign(this, { transactionData });
    }
    async generateApproval() {
        const builder = new src_1.GenerateApproval();
        const transactionData = await builder
            .setData(this.transactionData)
            .setNewData(this.updatedData)
            .setUser(this.user)
            .setApproval(this.approvals)
            .run();
        Object.assign(this, { transactionData });
    }
}
exports.UpdateManager = UpdateManager;
class UpdateGroupSiteApprovalManager extends UpdateManager {
    async save() {
        return await this.dataService.update(this.id, this.transactionData);
    }
    postApproval() {
        return;
    }
}
exports.UpdateGroupSiteApprovalManager = UpdateGroupSiteApprovalManager;
//# sourceMappingURL=update-group-site-approval.manager.js.map