"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteSiteManager = void 0;
const src_1 = require("src");
class DeleteSiteManager extends src_1.DeleteManager {
    constructor(siteIds, siteDataService, siteProducerService) {
        super(siteIds, siteDataService, siteProducerService);
        this.siteIds = siteIds;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(site) {
        switch (site.is_generated) {
            case true:
                if (site.is_generated) {
                    return false;
                }
                break;
            default:
                if (await this.hasRelation(site)) {
                    return false;
                }
        }
        return true;
    }
    async onSuccess(sites) {
        sites.forEach((site) => {
            this.result.success.push(`site with id ${site.code} successfully deleted`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
    async hasRelation(site) {
        const locations = await this.siteDataService.findLocationBySitesId(site.id);
        if (locations.length === 0)
            return false;
        return true;
    }
}
exports.DeleteSiteManager = DeleteSiteManager;
//# sourceMappingURL=delete-site.manager.js.map