import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
export declare class GenerateUniqueNameManager {
    protected name: string;
    protected readonly dataService: SiteDataServiceImpl;
    constructor(name: string, dataService: SiteDataServiceImpl);
    run(): Promise<string>;
    protected getName(name: string): Promise<string>;
}
