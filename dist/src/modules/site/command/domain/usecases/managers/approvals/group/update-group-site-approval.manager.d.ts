import { ApprovalPerCompany } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataService } from '../../../../contracts/group-site-data-service.contract';
import { GroupSiteEntity } from '../../../../entities/group-site.entity';
export declare abstract class UpdateManager<Entity, Service extends GroupSiteDataService> {
    readonly id: string;
    private readonly updatedData;
    private readonly user;
    private readonly approvals;
    readonly dataService: Service;
    transactionData: Entity;
    constructor(id: string, updatedData: Partial<Entity>, user: IUserPayload, approvals: ApprovalPerCompany[], dataService: Service);
    run(): Promise<Entity>;
    abstract save(): Promise<Entity>;
    abstract postApproval(): Promise<void>;
    protected getRecentData(): Promise<void>;
    protected generateApproval(): Promise<void>;
}
export declare class UpdateGroupSiteApprovalManager extends UpdateManager<GroupSiteEntity, GroupSiteDataService> {
    save(): Promise<GroupSiteEntity>;
    postApproval(): Promise<void>;
}
