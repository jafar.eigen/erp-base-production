import { ApprovalPerCompany, Approver } from 'src';
import { KafkaPayload } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../../entities/group-site.entity';
export declare class DeleteGroupSiteRequestManager {
    private readonly id;
    private readonly user;
    private readonly approvals;
    private readonly dataService;
    private readonly kafkaService;
    SiteGroup: GroupSiteEntity;
    approver: Approver;
    constructor(id: string, user: IUserPayload, approvals: ApprovalPerCompany[], dataService: GroupSiteDataServiceImpl, kafkaService: SiteProducerServiceImpl);
    run(): Promise<GroupSiteEntity>;
    protected getRecentApproval(): Promise<void>;
    protected processRequest(): Promise<void>;
    protected submitRequest(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected skiApproval(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected produceTopic(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected produceSubmissionTopicSuccess(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected assignApproval(): void;
}
