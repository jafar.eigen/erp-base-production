import { IUserPayload } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../entities/group-site.entity';
export declare class GenerateSiteManager {
    protected readonly siteDataService: SiteDataServiceImpl;
    protected readonly kafkaService: SiteProducerServiceImpl;
    protected data: GroupSiteEntity;
    protected readonly user: IUserPayload;
    constructor(siteDataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, data: GroupSiteEntity, user: IUserPayload);
    run(): Promise<void>;
    protected generate(): Promise<void>;
    private getValidFormSite;
}
