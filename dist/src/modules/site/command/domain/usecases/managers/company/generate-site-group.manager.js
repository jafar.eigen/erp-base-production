"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateSiteGroupManager = void 0;
class GenerateSiteGroupManager {
    constructor(dataService, kafkaService, data) {
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.data = data;
    }
    async run() {
        await this.dataService.save(await this.getValidFormGroup(this.data));
    }
    async getValidFormGroup(site) {
        if (Array.isArray(site.levels) && site.levels.length > 0) {
            site.levels = site.levels.map((level) => {
                delete level.id;
                delete level.site_id;
                delete level.created_at;
                delete level.updated_at;
                return level;
            });
        }
        if (Array.isArray(site.contacts) && site.contacts.length > 0) {
            site.contacts = site.contacts.map((contact) => {
                delete contact.id;
                delete contact.site_id;
                delete contact.created_at;
                delete contact.updated_at;
                return contact;
            });
        }
        if (Array.isArray(site.companies) && site.companies.length > 0) {
            site.companies = site.companies.map((company) => {
                delete company.id;
                delete company.site_id;
                delete company.created_at;
                delete company.updated_at;
                return company;
            });
        }
        const group = Object.assign({}, site, {
            gid: await this.generateGroupId()
        });
        return group;
    }
    async generateGroupId() {
        let counter = await this.dataService.count();
        return String(++counter).padStart(3, '0');
    }
}
exports.GenerateSiteGroupManager = GenerateSiteGroupManager;
//# sourceMappingURL=generate-site-group.manager.js.map