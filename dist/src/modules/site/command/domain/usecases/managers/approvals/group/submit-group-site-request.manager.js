"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitGroupSiteRequestManager = void 0;
const src_1 = require("src");
const generate_site_manager_1 = require("../../group/generate-site.manager");
class SubmitGroupSiteRequestManager {
    constructor(id, user, dataService, kafkaService, siteDataService) {
        this.id = id;
        this.user = user;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.siteDataService = siteDataService;
    }
    async run() {
        await this.getRecentApproval();
        await this.processRequest();
        return this.data;
    }
    async getRecentApproval() {
        const data = await this.dataService.findOne(this.id);
        Object.assign(this, { data });
    }
    async processRequest() {
        const builder = new src_1.SubmitRequest();
        const requestType = this.data.request_info === src_1.RequestInfo.EDIT_DATA
            ? src_1.RequestType.UPDATE
            : src_1.RequestType.CREATE;
        const data = await builder
            .setRequestType(requestType)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setData(this.data)
            .setSubmitRequestFunction(this.submitRequest)
            .setPassApprovalFunction(this.skiApproval)
            .setProduceSubmitRequestTopic(this.produceApprovalSubmittedTopic)
            .setProducePassApprovalTopic(this.produceApprovalPassedTopic)
            .run();
        Object.assign(this, { data });
        if (this.isAbleToGenerateSites()) {
            this.generateSites();
        }
    }
    async submitRequest(group) {
        return await this.dataService.update(this.id, group);
    }
    async skiApproval(group) {
        return await this.dataService.update(this.id, group);
    }
    async produceApprovalSubmittedTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    async produceApprovalPassedTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    isAbleToGenerateSites() {
        const approvalNone = this.data.approval === null;
        return (approvalNone ||
            (this.data.request_info === src_1.RequestInfo.CREATE_DATA &&
                this.data.status === src_1.STATUS.ACTIVE &&
                !this.data.has_requested_process));
    }
    async generateSites() {
        new generate_site_manager_1.GenerateSiteManager(this.siteDataService, this.kafkaService, this.data, this.user).run();
    }
}
exports.SubmitGroupSiteRequestManager = SubmitGroupSiteRequestManager;
//# sourceMappingURL=submit-group-site-request.manager.js.map