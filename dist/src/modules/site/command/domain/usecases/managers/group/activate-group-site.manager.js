"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateGroupSiteManager = void 0;
const src_1 = require("src");
const activate_group_site_request_manager_1 = require("../approvals/group/activate-group-site-request.manager");
class ActivateGroupSiteManager {
    constructor(groupDataService, siteDataService, siteProducerService, groupIds, user, approvals) {
        this.groupDataService = groupDataService;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.groupIds = groupIds;
        this.user = user;
        this.approvals = approvals;
        this.successMessages = [];
        this.failedMessages = [];
    }
    async execute() {
        const groups = await this.groupDataService.findByIds(this.groupIds);
        await Promise.all(groups.map(async (group) => {
            if (group.status !== src_1.STATUS.INACTIVE) {
                this.failedMessages.push(`Group site with id ${group.id} is not inactive.`);
            }
            else {
                await new activate_group_site_request_manager_1.ActivateGroupSiteRequestManager(group.id, this.user, this.approvals, this.groupDataService, this.siteDataService, this.siteProducerService).run();
                this.successMessages.push(`Group site with id ${group.id} successfully activated.`);
            }
        }));
        return this;
    }
    getMessages() {
        return {
            success: this.successMessages,
            failed: this.failedMessages
        };
    }
}
exports.ActivateGroupSiteManager = ActivateGroupSiteManager;
//# sourceMappingURL=activate-group-site.manager.js.map