import { SubmitManager } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../../entities/site.entity';
export declare class SubmitSiteRequestManager extends SubmitManager<SiteEntity> {
    readonly id: string;
    readonly dataService: SiteDataServiceImpl;
    readonly kafkaService: SiteProducerServiceImpl;
    readonly groupService: GroupSiteDataServiceImpl;
    result: SiteEntity;
    constructor(id: string, dataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, groupService: GroupSiteDataServiceImpl);
    onSuccess(sites: SiteEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): SiteEntity;
    protected isApprovalRequestDone(site: SiteEntity): boolean;
    protected isEditData(site: SiteEntity): boolean;
    protected isDraftToActive(site: SiteEntity): boolean;
    protected generateGroupSite(site: SiteEntity): Promise<void>;
}
