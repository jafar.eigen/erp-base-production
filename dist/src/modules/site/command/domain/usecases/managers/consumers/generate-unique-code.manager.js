"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateUniqueCodeManager = void 0;
class GenerateUniqueCodeManager {
    constructor(code, dataService) {
        this.code = code;
        this.dataService = dataService;
    }
    async run() {
        const code = await this.getCode(this.code);
        return code;
    }
    async getCode(code) {
        const site = await this.dataService.findByCode(code);
        if (site) {
            const formattedCode = `${site.code} (New)`;
            return await this.getCode(formattedCode);
        }
        else {
            return code;
        }
    }
}
exports.GenerateUniqueCodeManager = GenerateUniqueCodeManager;
//# sourceMappingURL=generate-unique-code.manager.js.map