import { BaseResultBatch, DeleteManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class DeleteSiteManager extends DeleteManager<SiteEntity> {
    siteIds: string[];
    siteDataService: SiteDataServiceImpl;
    siteProducerService: SiteProducerServiceImpl;
    result: BaseResultBatch;
    constructor(siteIds: string[], siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    validateStatus(site: SiteEntity): Promise<boolean>;
    onSuccess(sites: SiteEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
    protected hasRelation(site: SiteEntity): Promise<boolean>;
}
