"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateGroupSiteRequestManager = void 0;
const src_1 = require("src");
const src_2 = require("src");
const inherit_status_to_site_company_manager_1 = require("../../group/inherit-status-to-site-company.manager");
class ActivateGroupSiteRequestManager {
    constructor(id, user, approvals, dataService, siteService, kafkaService) {
        this.id = id;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
        this.siteService = siteService;
        this.kafkaService = kafkaService;
    }
    async run() {
        await this.getRecentApproval();
        await this.processRequest();
        return this.SiteGroup;
    }
    async getRecentApproval() {
        const SiteGroup = await this.dataService.findOne(this.id);
        Object.assign(this, { SiteGroup });
        this.assignApproval();
    }
    async processRequest() {
        const builder = new src_2.SubmitRequest();
        const SiteGroup = await builder
            .setRequestType(src_2.RequestType.ACTIVE)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setData(this.SiteGroup)
            .setSubmitRequestFunction(this.submitRequest)
            .setPassApprovalFunction(this.skiApproval)
            .setProduceSubmitRequestTopic(this.produceTopic)
            .setProducePassApprovalTopic(this.produceSubmissionTopicSuccess)
            .run();
        Object.assign(this, { SiteGroup });
        if (this.isApprovalDone()) {
            new inherit_status_to_site_company_manager_1.InheritStatusToSiteCompany(this.dataService, this.siteService, this.kafkaService, this.SiteGroup, this.user).run();
        }
    }
    assignApproval() {
        const approval = (0, src_1.findApproval)(this.approvals, this.SiteGroup, this.user);
        Object.assign(this.SiteGroup, { approval });
    }
    async submitRequest(group) {
        return await this.dataService.update(group.id, group);
    }
    async skiApproval(group) {
        return await this.dataService.update(group.id, group);
    }
    async produceTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    async produceSubmissionTopicSuccess(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    isApprovalDone() {
        const isApprovalNone = this.approvals === null;
        return isApprovalNone && !this.SiteGroup.has_requested_process;
    }
}
exports.ActivateGroupSiteRequestManager = ActivateGroupSiteRequestManager;
//# sourceMappingURL=activate-group-site-request.manager.js.map