"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGroupSiteManager = void 0;
const code_site_group_validator_1 = require("../../validators/code-site-group.validator");
const name_site_group_validator_1 = require("../../validators/name-site-group.validator");
const update_group_site_approval_manager_1 = require("../approvals/group/update-group-site-approval.manager");
class UpdateGroupSiteManager {
    constructor(groupSiteDataService, groupId, updatedData, user, approvals) {
        this.groupSiteDataService = groupSiteDataService;
        this.groupId = groupId;
        this.updatedData = updatedData;
        this.user = user;
        this.approvals = approvals;
    }
    async execute() {
        await this.validateUniqueAttributes();
        return await new update_group_site_approval_manager_1.UpdateGroupSiteApprovalManager(this.groupId, this.updatedData, this.user, this.approvals, this.groupSiteDataService).run();
    }
    async validateUniqueAttributes() {
        await new code_site_group_validator_1.CodeSiteGroup(this.groupSiteDataService, this.updatedData, this.groupId).execute();
        await new name_site_group_validator_1.NameSiteGroup(this.groupSiteDataService, this.updatedData, this.groupId).execute();
    }
}
exports.UpdateGroupSiteManager = UpdateGroupSiteManager;
//# sourceMappingURL=update-group-site.manager.js.map