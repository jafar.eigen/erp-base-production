"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateSiteManager = void 0;
const src_1 = require("src");
const code_validator_1 = require("../../validators/code.validator");
const name_validator_1 = require("../../validators/name.validator");
class UpdateSiteManager extends src_1.UpdateManager {
    constructor(siteDataService, siteProducerService, siteId, updatedData) {
        super(siteDataService, siteProducerService);
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.siteId = siteId;
        this.updatedData = updatedData;
    }
    async beforeProcess() {
        await this.validateDuplicateAttributes();
    }
    async prepareData() {
        this.entityId = this.siteId;
        return this.updatedData;
    }
    async afterProcess(entity) {
        return;
    }
    async validateDuplicateAttributes() {
        await new name_validator_1.Name(this.siteDataService, this.updatedData, this.siteId).execute();
        await new code_validator_1.Code(this.siteDataService, this.updatedData, this.siteId).execute();
    }
}
exports.UpdateSiteManager = UpdateSiteManager;
//# sourceMappingURL=update-site.manager.js.map