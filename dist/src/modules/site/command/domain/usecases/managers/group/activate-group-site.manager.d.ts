import { IUserPayload, ApprovalPerCompany } from 'src';
import { GroupSiteDataServiceImpl } from '../../../../data/services/group-site-data.service';
import { SiteDataServiceImpl } from '../../../../data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../../infrastructure/producer/site-producer.service';
import { GroupSiteResultBatch } from '../../../entities/group-site-result-batch.entity';
export declare class ActivateGroupSiteManager {
    private groupDataService;
    private siteDataService;
    private siteProducerService;
    private groupIds;
    private user;
    private approvals;
    constructor(groupDataService: GroupSiteDataServiceImpl, siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, groupIds: string[], user: IUserPayload, approvals: ApprovalPerCompany[]);
    private successMessages;
    private failedMessages;
    execute(): Promise<this>;
    getMessages(): GroupSiteResultBatch;
}
