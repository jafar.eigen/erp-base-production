"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveSiteRequestManager = void 0;
const src_1 = require("src");
class ApproveSiteRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.APPROVED;
    }
    async afterProcess() {
        if (this.isApprovalDone() && this.isEditData()) {
            await this.dataService.findAndDeleteNulledRelation();
        }
        return this.entity;
    }
    isApprovalDone() {
        return !this.entity.has_requested_process || !this.entity.approval;
    }
    isEditData() {
        return this.entity.request_info === src_1.RequestInfo.EDIT_DATA;
    }
}
exports.ApproveSiteRequestManager = ApproveSiteRequestManager;
//# sourceMappingURL=approve-site-request.manager.js.map