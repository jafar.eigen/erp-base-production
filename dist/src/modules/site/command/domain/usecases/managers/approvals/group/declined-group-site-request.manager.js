"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclinedGroupSiteRequestManager = void 0;
const src_1 = require("src");
class DeclinedGroupSiteRequestManager {
    constructor(id, user, step, dataService, kafkaService) {
        this.id = id;
        this.user = user;
        this.step = step;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
    async getRecentApproval() {
        const recentApproval = await this.dataService.findOne(this.id);
        Object.assign(this, { data: recentApproval });
    }
    async responseApproval() {
        const builder = new src_1.ResponseApproval();
        const data = await builder
            .setResponseType(src_1.ResponseType.DECLINED)
            .setData(this.data)
            .setStepNumber(this.step)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setUnfinishedDataApprovalFunction(this.submitRequest)
            .setUpdateDataFunction(this.submitRequest)
            .setDataUpdatedProducer(this.produceTopic)
            .run();
        Object.assign(this, { data });
    }
    async run() {
        await this.getRecentApproval();
        await this.responseApproval();
        return this.data;
    }
    async submitRequest(data) {
        return await this.dataService.update(this.id, data);
    }
    async produceTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
}
exports.DeclinedGroupSiteRequestManager = DeclinedGroupSiteRequestManager;
//# sourceMappingURL=declined-group-site-request.manager.js.map