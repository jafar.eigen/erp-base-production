import { CreateManager } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class CreateSiteManager extends CreateManager<SiteEntity> {
    protected siteDataService: SiteDataServiceImpl;
    protected siteProducerService: SiteProducerServiceImpl;
    protected groupSiteDataService: GroupSiteDataServiceImpl;
    protected site: SiteEntity;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, groupSiteDataService: GroupSiteDataServiceImpl, site: SiteEntity);
    prepareData(): Promise<SiteEntity>;
    beforeProcess(): Promise<void>;
    protected findGroupSiteByCodeAndUpdateToNull(): Promise<void>;
}
