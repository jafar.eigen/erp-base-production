"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InactiveGroupSiteManager = void 0;
const src_1 = require("src");
const inactive_group_site_request_manager_1 = require("../approvals/group/inactive-group-site-request.manager");
class InactiveGroupSiteManager {
    constructor(groupDataService, siteDataService, siteProducerService, groupIds, user, approvals) {
        this.groupDataService = groupDataService;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.groupIds = groupIds;
        this.user = user;
        this.approvals = approvals;
        this.successMessages = [];
        this.failedMessages = [];
    }
    async execute() {
        const groups = await this.groupDataService.findByIds(this.groupIds);
        await Promise.all(groups.map(async (group) => {
            if (group.status !== src_1.STATUS.ACTIVE) {
                this.failedMessages.push(`Group site with id ${group.id} is not active.`);
                return;
            }
            if (await this.isGroupMemberHasRelation(group.id)) {
                this.failedMessages.push(`Group site with id ${group.id} has member with currently used on commpany.`);
                return;
            }
            await new inactive_group_site_request_manager_1.InactiveGroupSiteRequestManager(group.id, this.user, this.approvals, this.groupDataService, this.siteDataService, this.siteProducerService).run();
            this.successMessages.push(`Group site with id ${group.id} successfully deactivated.`);
        }));
        return this;
    }
    async isGroupMemberHasRelation(groupId) {
        const sites = await this.getMembers(groupId);
        let isProcessDone = false;
        if (!sites)
            return isProcessDone;
        for (const site of sites) {
            for (const company of site === null || site === void 0 ? void 0 : site.companies) {
                if (await this.siteCompanyHasRelationToCompany(company.company_id)) {
                    isProcessDone = true;
                    break;
                }
            }
            if (!isProcessDone)
                break;
        }
        return isProcessDone;
    }
    async siteCompanyHasRelationToCompany(siteId) {
        const response = await this.siteProducerService.isSiteExistOnCompany(siteId);
        return response && response.message;
    }
    async getMembers(groupId) {
        return await this.groupDataService.findSitesByGroupId(groupId);
    }
    getMessages() {
        return {
            success: this.successMessages,
            failed: this.failedMessages
        };
    }
}
exports.InactiveGroupSiteManager = InactiveGroupSiteManager;
//# sourceMappingURL=inactive-group-site.manager.js.map