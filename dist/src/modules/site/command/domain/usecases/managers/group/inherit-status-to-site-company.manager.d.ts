import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../entities/group-site.entity';
import { SiteEntity } from '../../../entities/site.entity';
export declare class InheritStatusToSiteCompany {
    protected groupSiteDataService: GroupSiteDataServiceImpl;
    protected siteDataService: SiteDataServiceImpl;
    protected kafkaService: SiteProducerServiceImpl;
    protected readonly group: GroupSiteEntity;
    protected readonly user: IUserPayload;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, siteDataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, group: GroupSiteEntity, user: IUserPayload);
    run(): Promise<void>;
    protected getSitesCompany(): Promise<SiteEntity[]>;
}
