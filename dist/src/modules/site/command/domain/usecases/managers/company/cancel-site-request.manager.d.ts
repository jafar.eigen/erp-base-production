import { CancelManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class CancelSiteRequestManager extends CancelManager<SiteEntity> {
    readonly siteId: string;
    readonly dataService: SiteDataServiceImpl;
    readonly kafkaService: SiteProducerServiceImpl;
    constructor(siteId: string, dataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl);
}
