"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwitchGroupOnSite = void 0;
const src_1 = require("src");
class SwitchGroupOnSite {
    constructor(siteDataService, groupDataService, siteProducerService, siteId, destinationGroupId) {
        this.siteDataService = siteDataService;
        this.groupDataService = groupDataService;
        this.siteProducerService = siteProducerService;
        this.siteId = siteId;
        this.destinationGroupId = destinationGroupId;
        this.getParameters();
    }
    async getParameters() {
        const session = src_1.UserSession.getInstance();
        this.user = session.getUser();
        this.approvals = session.getApprovals();
    }
    async execute() {
        var _a;
        const site = await this.siteDataService.findOneOrFail(this.siteId);
        const group = await this.groupDataService.findOneById(this.destinationGroupId);
        const oldData = Object.assign({}, site);
        const payload = Object.assign({}, {
            user: this.user,
            data: {},
            old: oldData
        });
        Object.assign(site, {
            group_id: group.id,
            editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
            editor_name: this.user.username
        });
        const saved = await this.siteDataService.save(site);
        Object.assign(payload, {
            data: saved
        });
        await this.siteProducerService.baseChanged(payload);
        return saved;
    }
}
exports.SwitchGroupOnSite = SwitchGroupOnSite;
//# sourceMappingURL=switch-group-on-site.manager.js.map