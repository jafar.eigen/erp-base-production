"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateValidatorManager = void 0;
const code_validator_1 = require("../../validators/code.validator");
const name_validator_1 = require("../../validators/name.validator");
class CreateValidatorManager {
    constructor(siteDataService, site, siteId = null) {
        this.siteDataService = siteDataService;
        this.site = site;
        this.siteId = siteId;
    }
    async execute() {
        await new name_validator_1.Name(this.siteDataService, this.site, this.siteId).execute();
        await new code_validator_1.Code(this.siteDataService, this.site, this.siteId).execute();
    }
}
exports.CreateValidatorManager = CreateValidatorManager;
//# sourceMappingURL=create-validator.manager.js.map