import { ApprovalPerCompany } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from '../../../../data/services/group-site-data.service';
import { GroupSiteEntity } from '../../../entities/group-site.entity';
export declare class UpdateGroupSiteManager {
    private groupSiteDataService;
    private groupId;
    private updatedData;
    private user;
    private approvals;
    constructor(groupSiteDataService: GroupSiteDataServiceImpl, groupId: string, updatedData: GroupSiteEntity, user: IUserPayload, approvals: ApprovalPerCompany[]);
    execute(): Promise<GroupSiteEntity>;
    validateUniqueAttributes(): Promise<void>;
}
