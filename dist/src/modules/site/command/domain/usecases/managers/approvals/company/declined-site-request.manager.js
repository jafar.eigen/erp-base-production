"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclinedSiteRequestManager = void 0;
const src_1 = require("src");
class DeclinedSiteRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.DECLINED;
    }
}
exports.DeclinedSiteRequestManager = DeclinedSiteRequestManager;
//# sourceMappingURL=declined-site-request.manager.js.map