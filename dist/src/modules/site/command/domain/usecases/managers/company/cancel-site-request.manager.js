"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelSiteRequestManager = void 0;
const src_1 = require("src");
class CancelSiteRequestManager extends src_1.CancelManager {
    constructor(siteId, dataService, kafkaService) {
        super(siteId, dataService, kafkaService);
        this.siteId = siteId;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
}
exports.CancelSiteRequestManager = CancelSiteRequestManager;
//# sourceMappingURL=cancel-site-request.manager.js.map