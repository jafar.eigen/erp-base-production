"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateSiteManager = void 0;
const src_1 = require("src");
class ActivateSiteManager extends src_1.ActiveManager {
    constructor(siteIds, siteDataService, siteProducerService) {
        super(siteIds, siteDataService, siteProducerService);
        this.siteIds = siteIds;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async validateStatus(site) {
        if (site.status !== src_1.STATUS.INACTIVE)
            return false;
        if (site.is_generated)
            return false;
        return true;
    }
    async onSuccess(sites) {
        sites.forEach((site) => {
            this.result.success.push(`Site with id ${site.code} successfully activated`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.ActivateSiteManager = ActivateSiteManager;
//# sourceMappingURL=activate-site.manager.js.map