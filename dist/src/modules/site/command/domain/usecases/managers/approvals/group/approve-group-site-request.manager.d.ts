import { KafkaPayload } from 'src';
import { IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../../entities/group-site.entity';
export declare class ApproveGroupSiteRequestManager {
    private readonly id;
    private readonly user;
    private readonly step;
    private readonly dataService;
    private readonly siteService;
    private readonly kafkaService;
    protected data: GroupSiteEntity;
    constructor(id: string, user: IUserPayload, step: number, dataService: GroupSiteDataServiceImpl, siteService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl);
    getRecentApproval(): Promise<void>;
    responseApproval(): Promise<void>;
    run(): Promise<GroupSiteEntity>;
    protected submitRequest(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected delete(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected updateUnfinishedData(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected produceTopic(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected produceTopicDeleted(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected isApprovalDone(): boolean;
}
