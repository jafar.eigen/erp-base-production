import { BaseResultBatch, ActiveManager } from 'src';
import { SiteDataServiceImpl } from '../../../../data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../../infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class ActivateSiteManager extends ActiveManager<SiteEntity> {
    siteIds: string[];
    siteDataService: SiteDataServiceImpl;
    siteProducerService: SiteProducerServiceImpl;
    result: BaseResultBatch;
    constructor(siteIds: string[], siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl);
    validateStatus(site: SiteEntity): Promise<boolean>;
    onSuccess(sites: SiteEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
}
