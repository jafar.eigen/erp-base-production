"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGroupSiteManager = void 0;
const src_1 = require("src");
class CreateGroupSiteManager {
    constructor(groupSiteDataService, siteDataService, siteProducerService, group, user, approvals) {
        this.groupSiteDataService = groupSiteDataService;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.group = group;
        this.user = user;
        this.approvals = approvals;
    }
    async execute() {
        var _a, _b;
        const approval = (0, src_1.findApproval)(this.approvals, null, this.user);
        Object.assign(this.group, {
            status: src_1.STATUS.DRAFT,
            gid: await this.generateGroupId(),
            creator_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
            creator_name: this.user.username,
            editor_id: (_b = this.user.uuid) !== null && _b !== void 0 ? _b : this.user.id,
            editor_name: this.user.username,
            approval: approval,
            request_info: src_1.RequestInfo.CREATE_DATA,
            has_requested_process: false
        });
        await this.checkSiteCompanyNameBySiteGroupName();
        const group = await this.groupSiteDataService.save(this.group);
        await this.siteProducerService.groupSiteCreated({
            data: group,
            user: this.user,
            old: null
        });
        return group;
    }
    async generateGroupId() {
        let counter = await this.groupSiteDataService.count();
        return String(++counter).padStart(3, '0');
    }
    async checkSiteCompanyNameBySiteGroupName() {
        const site = await this.siteDataService.findByName(this.group.name);
        if (site[0]) {
            site[0].name = null;
            const saved = await this.siteDataService.save(site[0]);
            await this.siteProducerService.baseChanged({
                user: this.user,
                data: saved,
                old: site[0]
            });
        }
    }
}
exports.CreateGroupSiteManager = CreateGroupSiteManager;
//# sourceMappingURL=create-group-site.manager.js.map