"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitSiteRequestManager = void 0;
const src_1 = require("src");
const generate_site_group_manager_1 = require("../../company/generate-site-group.manager");
class SubmitSiteRequestManager extends src_1.SubmitManager {
    constructor(id, dataService, kafkaService, groupService) {
        super([id], dataService, kafkaService);
        this.id = id;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.groupService = groupService;
    }
    async onSuccess(sites) {
        if (sites.length !== 0) {
            if (this.isApprovalRequestDone(sites[0]) &&
                this.isDraftToActive(sites[0])) {
                this.generateGroupSite(sites[0]);
            }
            if (this.isApprovalRequestDone(sites[0]) && this.isEditData(sites[0])) {
                ;
                async () => await this.dataService.findAndDeleteNulledRelation();
            }
        }
        this.result = sites[0];
    }
    async onFailed(messages) {
        if (messages.length > 0) {
            throw new Error(messages[0]);
        }
    }
    getResult() {
        return this.result;
    }
    isApprovalRequestDone(site) {
        return site.approval === null || !site.has_requested_process;
    }
    isEditData(site) {
        return site.request_info === src_1.RequestInfo.EDIT_DATA;
    }
    isDraftToActive(site) {
        return (site.request_info === src_1.RequestInfo.CREATE_DATA &&
            site.status === src_1.STATUS.DRAFT);
    }
    async generateGroupSite(site) {
        new generate_site_group_manager_1.GenerateSiteGroupManager(this.groupService, this.kafkaService, site).run();
    }
}
exports.SubmitSiteRequestManager = SubmitSiteRequestManager;
//# sourceMappingURL=submit-site-request.manager.js.map