"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InheritStatusToSiteCompany = void 0;
class InheritStatusToSiteCompany {
    constructor(groupSiteDataService, siteDataService, kafkaService, group, user) {
        this.groupSiteDataService = groupSiteDataService;
        this.siteDataService = siteDataService;
        this.kafkaService = kafkaService;
        this.group = group;
        this.user = user;
    }
    async run() {
        const sites = await this.getSitesCompany();
        await Promise.all(sites === null || sites === void 0 ? void 0 : sites.map(async (site) => {
            var _a;
            const siteToUpdate = Object.assign({}, site);
            const payload = {
                user: this.user,
                data: null,
                old: siteToUpdate
            };
            Object.assign(site, {
                status: this.group.status,
                request_info: this.group.request_info,
                editor_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
                editor_name: this.user.username
            });
            const saved = await this.siteDataService.save(site);
            Object.assign(payload, {
                data: saved
            });
            await this.kafkaService.baseChanged(payload);
        }));
    }
    async getSitesCompany() {
        return await this.groupSiteDataService.findSitesByGroupId(this.group.id);
    }
}
exports.InheritStatusToSiteCompany = InheritStatusToSiteCompany;
//# sourceMappingURL=inherit-status-to-site-company.manager.js.map