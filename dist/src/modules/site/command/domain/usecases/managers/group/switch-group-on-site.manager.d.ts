import { IUserPayload, ApprovalPerCompany } from 'src';
import { GroupSiteDataServiceImpl } from '../../../../data/services/group-site-data.service';
import { SiteDataServiceImpl } from '../../../../data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../../../infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class SwitchGroupOnSite {
    private siteDataService;
    private groupDataService;
    private siteProducerService;
    private siteId;
    private destinationGroupId;
    user: IUserPayload;
    approvals: ApprovalPerCompany[];
    constructor(siteDataService: SiteDataServiceImpl, groupDataService: GroupSiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, siteId: string, destinationGroupId: string);
    getParameters(): Promise<void>;
    execute(): Promise<SiteEntity>;
}
