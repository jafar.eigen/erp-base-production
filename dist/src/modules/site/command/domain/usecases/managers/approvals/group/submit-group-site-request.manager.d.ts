import { Approver, KafkaPayload, IUserPayload } from 'src';
import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../../entities/group-site.entity';
export declare class SubmitGroupSiteRequestManager {
    private readonly id;
    private readonly user;
    private readonly dataService;
    private readonly kafkaService;
    private readonly siteDataService;
    data: GroupSiteEntity;
    approver: Approver;
    constructor(id: string, user: IUserPayload, dataService: GroupSiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, siteDataService: SiteDataServiceImpl);
    run(): Promise<GroupSiteEntity>;
    protected getRecentApproval(): Promise<void>;
    protected processRequest(): Promise<void>;
    protected submitRequest(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected skiApproval(group: GroupSiteEntity): Promise<GroupSiteEntity>;
    protected produceApprovalSubmittedTopic(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected produceApprovalPassedTopic(payload: KafkaPayload<IUserPayload, GroupSiteEntity, GroupSiteEntity>): Promise<void>;
    protected isAbleToGenerateSites(): boolean;
    protected generateSites(): Promise<void>;
}
