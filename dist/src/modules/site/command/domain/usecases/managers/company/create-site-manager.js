"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSiteManager = void 0;
const src_1 = require("src");
const create_validator_manager_1 = require("./create-validator.manager");
class CreateSiteManager extends src_1.CreateManager {
    constructor(siteDataService, siteProducerService, groupSiteDataService, site) {
        super(siteDataService, siteProducerService);
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
        this.groupSiteDataService = groupSiteDataService;
        this.site = site;
    }
    async prepareData() {
        await this.findGroupSiteByCodeAndUpdateToNull();
        return this.site;
    }
    async beforeProcess() {
        await new create_validator_manager_1.CreateValidatorManager(this.siteDataService, this.site).execute();
    }
    async findGroupSiteByCodeAndUpdateToNull() {
        const group = await this.groupSiteDataService.findOneByCodeSiteUserCompany(this.site.code);
        if (group) {
            group.code = null;
            await this.groupSiteDataService.save(group);
        }
    }
}
exports.CreateSiteManager = CreateSiteManager;
//# sourceMappingURL=create-site-manager.js.map