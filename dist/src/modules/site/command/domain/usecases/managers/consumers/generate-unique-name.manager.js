"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateUniqueNameManager = void 0;
class GenerateUniqueNameManager {
    constructor(name, dataService) {
        this.name = name;
        this.dataService = dataService;
    }
    async run() {
        const name = await this.getName(this.name);
        return name;
    }
    async getName(name) {
        const site = await this.dataService.findByName(name);
        if (site[0]) {
            const formattedName = `${site[0].name} (New)`;
            return await this.getName(formattedName);
        }
        else {
            return name;
        }
    }
}
exports.GenerateUniqueNameManager = GenerateUniqueNameManager;
//# sourceMappingURL=generate-unique-name.manager.js.map