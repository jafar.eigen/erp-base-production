import { BaseActionManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../../entities/site.entity';
export declare class DeclinedSiteRequestManager extends BaseActionManager<SiteEntity> {
    readonly dataService: SiteDataServiceImpl;
    readonly kafkaService: SiteProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, id: string, step: number);
}
