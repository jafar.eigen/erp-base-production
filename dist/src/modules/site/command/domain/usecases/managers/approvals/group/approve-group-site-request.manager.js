"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveGroupSiteRequestManager = void 0;
const src_1 = require("src");
const src_2 = require("src");
const inherit_status_to_site_company_manager_1 = require("../../group/inherit-status-to-site-company.manager");
class ApproveGroupSiteRequestManager {
    constructor(id, user, step, dataService, siteService, kafkaService) {
        this.id = id;
        this.user = user;
        this.step = step;
        this.dataService = dataService;
        this.siteService = siteService;
        this.kafkaService = kafkaService;
    }
    async getRecentApproval() {
        const recentApproval = await this.dataService.findOne(this.id);
        Object.assign(this, { data: recentApproval });
    }
    async responseApproval() {
        const builder = new src_2.ResponseApproval();
        const data = await builder
            .setResponseType(src_2.ResponseType.APPROVED)
            .setData(this.data)
            .setUser(this.user)
            .setStepNumber(this.step)
            .setDataService(this.dataService)
            .setKafkaService(this.kafkaService)
            .setUnfinishedDataApprovalFunction(this.updateUnfinishedData)
            .setUpdateDataFunction(this.submitRequest)
            .setDeleteDataFunction(this.delete)
            .setDataUpdatedProducer(this.produceTopic)
            .setDataDeletedProducer(this.produceTopicDeleted)
            .run();
        Object.assign(this, { data });
        if (this.isApprovalDone()) {
            new inherit_status_to_site_company_manager_1.InheritStatusToSiteCompany(this.dataService, this.siteService, this.kafkaService, this.data, this.user).run();
        }
    }
    async run() {
        await this.getRecentApproval();
        await this.responseApproval();
        return this.data;
    }
    async submitRequest(group) {
        return await this.dataService.update(group.id, group);
    }
    async delete(group) {
        return await this.dataService.findOneAndDelete(group.id);
    }
    async updateUnfinishedData(group) {
        return await this.dataService.update(group.id, group);
    }
    async produceTopic(payload) {
        return this.kafkaService.groupSiteChanged(payload);
    }
    async produceTopicDeleted(payload) {
        return this.kafkaService.groupSiteDeleted(payload);
    }
    isApprovalDone() {
        const isToActiveOrInactive = this.data.request_info === src_1.RequestInfo.EDIT_ACTIVE ||
            this.data.request_info === src_1.RequestInfo.EDIT_INACTIVE;
        return !this.data.has_requested_process && isToActiveOrInactive;
    }
}
exports.ApproveGroupSiteRequestManager = ApproveGroupSiteRequestManager;
//# sourceMappingURL=approve-group-site-request.manager.js.map