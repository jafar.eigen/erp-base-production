import { UpdateManager } from 'src';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class UpdateSiteManager extends UpdateManager<SiteEntity> {
    private siteDataService;
    private siteProducerService;
    private siteId;
    private updatedData;
    entityId: string;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, siteId: string, updatedData: SiteEntity);
    beforeProcess(): Promise<void>;
    prepareData(): Promise<SiteEntity>;
    afterProcess(entity: SiteEntity): Promise<void>;
    validateDuplicateAttributes(): Promise<void>;
}
