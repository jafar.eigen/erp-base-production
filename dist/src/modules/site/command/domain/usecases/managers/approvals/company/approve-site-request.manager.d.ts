import { BaseActionManager } from 'src';
import { SiteEntity } from '../../../../entities/site.entity';
import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
export declare class ApproveSiteRequestManager extends BaseActionManager<SiteEntity> {
    readonly dataService: SiteDataServiceImpl;
    readonly kafkaService: SiteProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: SiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, id: string, step: number);
    afterProcess(): Promise<SiteEntity>;
    protected isApprovalDone(): boolean;
    protected isEditData(): boolean;
}
