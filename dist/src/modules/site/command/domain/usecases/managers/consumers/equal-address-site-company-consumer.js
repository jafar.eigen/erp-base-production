"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEqualAddress = void 0;
const isEqualAddress = (site, company) => {
    const { address, province, city, country } = company;
    return (site.address === address &&
        site.province === province &&
        site.city === city &&
        site.country === country);
};
exports.isEqualAddress = isEqualAddress;
//# sourceMappingURL=equal-address-site-company-consumer.js.map