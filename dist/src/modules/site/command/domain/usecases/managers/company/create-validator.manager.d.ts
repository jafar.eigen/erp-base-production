import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
import { SiteEntity } from '../../../entities/site.entity';
export declare class CreateValidatorManager {
    private siteDataService;
    private site;
    private siteId;
    constructor(siteDataService: SiteDataServiceImpl, site: SiteEntity, siteId?: string);
    execute(): Promise<void | Error>;
}
