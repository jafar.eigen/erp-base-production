import { GroupSiteDataServiceImpl } from 'src/modules/site/command/data/services/group-site-data.service';
import { SiteProducerServiceImpl } from 'src/modules/site/command/infrastructure/producer/site-producer.service';
import { GroupSiteEntity } from '../../../entities/group-site.entity';
import { SiteEntity } from '../../../entities/site.entity';
export declare class GenerateSiteGroupManager {
    protected dataService: GroupSiteDataServiceImpl;
    protected kafkaService: SiteProducerServiceImpl;
    protected data: SiteEntity;
    constructor(dataService: GroupSiteDataServiceImpl, kafkaService: SiteProducerServiceImpl, data: SiteEntity);
    run(): Promise<void>;
    protected getValidFormGroup(site: any): Promise<GroupSiteEntity>;
    protected generateGroupId(): Promise<string>;
}
