import { SiteDataServiceImpl } from 'src/modules/site/command/data/services/site-data.service';
export declare class GenerateUniqueCodeManager {
    protected code: string;
    protected readonly dataService: SiteDataServiceImpl;
    constructor(code: string, dataService: SiteDataServiceImpl);
    run(): Promise<string>;
    protected getCode(code: string): Promise<string>;
}
