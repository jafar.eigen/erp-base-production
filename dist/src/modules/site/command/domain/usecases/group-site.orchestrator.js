"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSiteOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const group_site_data_service_1 = require("../../data/services/group-site-data.service");
const site_data_service_1 = require("../../data/services/site-data.service");
const site_producer_service_1 = require("../../infrastructure/producer/site-producer.service");
const activate_group_site_manager_1 = require("./managers/group/activate-group-site.manager");
const create_group_site_manager_1 = require("./managers/group/create-group-site.manager");
const update_group_site_manager_1 = require("./managers/group/update-group-site.manager");
const delete_group_site_manager_1 = require("./managers/group/delete-group-site.manager");
const approve_group_site_request_manager_1 = require("./managers/approvals/group/approve-group-site-request.manager");
const submit_group_site_request_manager_1 = require("./managers/approvals/group/submit-group-site-request.manager");
const declined_group_site_request_manager_1 = require("./managers/approvals/group/declined-group-site-request.manager");
const inactive_group_site_manager_1 = require("./managers/group/inactive-group-site.manager");
let GroupSiteOrchestrator = class GroupSiteOrchestrator {
    constructor(groupSiteDataService, siteDataService, siteProducerService) {
        this.groupSiteDataService = groupSiteDataService;
        this.siteDataService = siteDataService;
        this.siteProducerService = siteProducerService;
    }
    async create(body, approvals, user) {
        return await new create_group_site_manager_1.CreateGroupSiteManager(this.groupSiteDataService, this.siteDataService, this.siteProducerService, body, user, approvals).execute();
    }
    async update(groupId, body, approvals, user) {
        return await new update_group_site_manager_1.UpdateGroupSiteManager(this.groupSiteDataService, groupId, body, user, approvals).execute();
    }
    async delete(groupIds, approvals, user) {
        const manager = await new delete_group_site_manager_1.DeleteGroupSiteManager(this.groupSiteDataService, this.siteProducerService, groupIds, user, approvals).execute();
        return manager.getMessages();
    }
    async activate(groupIds, user, approvals) {
        const group = await new activate_group_site_manager_1.ActivateGroupSiteManager(this.groupSiteDataService, this.siteDataService, this.siteProducerService, groupIds, user, approvals).execute();
        return group.getMessages();
    }
    async deactivate(groupIds, user, approvals) {
        const group = await new inactive_group_site_manager_1.InactiveGroupSiteManager(this.groupSiteDataService, this.siteDataService, this.siteProducerService, groupIds, user, approvals).execute();
        return group.getMessages();
    }
    async request(groupId, user) {
        return await new submit_group_site_request_manager_1.SubmitGroupSiteRequestManager(groupId, user, this.groupSiteDataService, this.siteProducerService, this.siteDataService).run();
    }
    async approve(groupId, user, step) {
        return await new approve_group_site_request_manager_1.ApproveGroupSiteRequestManager(groupId, user, step, this.groupSiteDataService, this.siteDataService, this.siteProducerService).run();
    }
    async decline(groupId, step, user) {
        return await new declined_group_site_request_manager_1.DeclinedGroupSiteRequestManager(groupId, user, step, this.groupSiteDataService, this.siteProducerService).run();
    }
};
GroupSiteOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [group_site_data_service_1.GroupSiteDataServiceImpl,
        site_data_service_1.SiteDataServiceImpl,
        site_producer_service_1.SiteProducerServiceImpl])
], GroupSiteOrchestrator);
exports.GroupSiteOrchestrator = GroupSiteOrchestrator;
//# sourceMappingURL=group-site.orchestrator.js.map