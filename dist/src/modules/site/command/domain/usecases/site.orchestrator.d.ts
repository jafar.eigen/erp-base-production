import { SiteDataServiceImpl } from '../../data/services/site-data.service';
import { SiteProducerServiceImpl } from '../../infrastructure/producer/site-producer.service';
import { SiteResultBatch } from '../entities/site-result-batch.entity';
import { SiteEntity } from '../entities/site.entity';
import { GroupSiteDataServiceImpl } from '../../data/services/group-site-data.service';
import { BaseOrchestrator } from 'src';
export declare class SiteOrchestrator extends BaseOrchestrator<SiteEntity> {
    private siteDataService;
    private siteProducerService;
    private groupDataService;
    constructor(siteDataService: SiteDataServiceImpl, siteProducerService: SiteProducerServiceImpl, groupDataService: GroupSiteDataServiceImpl);
    create(site: SiteEntity): Promise<SiteEntity>;
    update(siteId: string, updatedData: SiteEntity): Promise<SiteEntity>;
    delete(siteIds: string[]): Promise<SiteResultBatch>;
    deactivate(siteIds: string[]): Promise<SiteResultBatch>;
    activate(siteIds: string[]): Promise<SiteResultBatch>;
    switchGroup(siteId: string, groupId: string): Promise<SiteEntity>;
    request(siteId: string): Promise<SiteEntity>;
    approve(siteId: string, step: number): Promise<SiteEntity>;
    decline(siteId: string, step: number): Promise<SiteEntity>;
    cancel(siteId: string): Promise<SiteEntity>;
    rollbackStatusTrx(entityId: string[]): Promise<SiteEntity>;
}
