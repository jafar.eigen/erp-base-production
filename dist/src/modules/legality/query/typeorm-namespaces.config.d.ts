import { ConnectionOptions } from 'typeorm';
declare const _default: (() => ConnectionOptions) & import("@nestjs/config").ConfigFactoryKeyHost;
export default _default;
