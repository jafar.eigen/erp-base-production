"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityReadDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const database_config_1 = require("../../../../../utils/database.config");
const legality_read_model_1 = require("../models/legality-read.model");
let LegalityReadDataServiceImpl = class LegalityReadDataServiceImpl {
    constructor(legalityReadRepo) {
        this.legalityReadRepo = legalityReadRepo;
    }
    async findOneOrFail(legalityId) {
        return await this.legalityReadRepo.findOneOrFail(legalityId, {
            relations: [
                'companies',
                'contacts',
                'groups',
                'issuers',
                'sites',
                'dates'
            ]
        });
    }
    async index(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.legalityReadRepo, options, extraQuery);
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.legalityReadRepo, options, {
            relations: [
                'companies',
                'contacts',
                'groups',
                'issuers',
                'sites',
                'dates'
            ]
        });
    }
};
LegalityReadDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(legality_read_model_1.LegalityRead, database_config_1.LEGALITY_READ_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], LegalityReadDataServiceImpl);
exports.LegalityReadDataServiceImpl = LegalityReadDataServiceImpl;
//# sourceMappingURL=legality-read-data.service.js.map