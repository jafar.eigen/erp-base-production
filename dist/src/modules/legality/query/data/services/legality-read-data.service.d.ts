import { FindManyOptions, Repository } from 'typeorm';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { LegalityReadDataService } from '../../domain/contracts/legality-read-data-service.contract';
import { LegalityRead } from '../models/legality-read.model';
import { LegalityEntityRead } from '../../domain/entities/legality-read.entity';
export declare class LegalityReadDataServiceImpl implements LegalityReadDataService {
    private legalityReadRepo;
    constructor(legalityReadRepo: Repository<LegalityRead>);
    findOneOrFail(legalityId: string): Promise<LegalityEntityRead>;
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<LegalityEntityRead>): Promise<Pagination<LegalityEntityRead>>;
}
