"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityContactsRead = void 0;
const typeorm_1 = require("typeorm");
const legality_read_model_1 = require("./legality-read.model");
let LegalityContactsRead = class LegalityContactsRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], LegalityContactsRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'legality_id', nullable: true }),
    __metadata("design:type", String)
], LegalityContactsRead.prototype, "legality_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'contact_id', nullable: true }),
    __metadata("design:type", String)
], LegalityContactsRead.prototype, "contact_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'contact_name', nullable: true }),
    __metadata("design:type", String)
], LegalityContactsRead.prototype, "contact_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'description', nullable: true }),
    __metadata("design:type", String)
], LegalityContactsRead.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityContactsRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityContactsRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityContactsRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => legality_read_model_1.LegalityRead, (legality) => legality.contacts, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'legality_id' }),
    __metadata("design:type", legality_read_model_1.LegalityRead)
], LegalityContactsRead.prototype, "legality", void 0);
LegalityContactsRead = __decorate([
    (0, typeorm_1.Entity)('legality_contacts')
], LegalityContactsRead);
exports.LegalityContactsRead = LegalityContactsRead;
//# sourceMappingURL=legality-contacts-read.model.js.map