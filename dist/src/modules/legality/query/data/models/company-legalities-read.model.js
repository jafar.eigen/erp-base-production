"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyLegalitiesRead = void 0;
const typeorm_1 = require("typeorm");
const legality_read_model_1 = require("./legality-read.model");
let CompanyLegalitiesRead = class CompanyLegalitiesRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], CompanyLegalitiesRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'legality_id', nullable: true }),
    __metadata("design:type", String)
], CompanyLegalitiesRead.prototype, "legality_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_id', nullable: true }),
    __metadata("design:type", String)
], CompanyLegalitiesRead.prototype, "company_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_name', nullable: true }),
    __metadata("design:type", String)
], CompanyLegalitiesRead.prototype, "company_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'company_code', nullable: true }),
    __metadata("design:type", String)
], CompanyLegalitiesRead.prototype, "company_code", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyLegalitiesRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyLegalitiesRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], CompanyLegalitiesRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => legality_read_model_1.LegalityRead, (legality) => legality.companies, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'legality_id' }),
    __metadata("design:type", legality_read_model_1.LegalityRead)
], CompanyLegalitiesRead.prototype, "legality", void 0);
CompanyLegalitiesRead = __decorate([
    (0, typeorm_1.Entity)('company_legalities')
], CompanyLegalitiesRead);
exports.CompanyLegalitiesRead = CompanyLegalitiesRead;
//# sourceMappingURL=company-legalities-read.model.js.map