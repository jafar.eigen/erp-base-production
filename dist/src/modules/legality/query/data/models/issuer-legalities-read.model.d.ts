import { IssuerLegalitiesReadEntity } from '../../domain/entities/issuer-legalities-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class IssuerLegalityRead implements IssuerLegalitiesReadEntity {
    id: string;
    issuer_name: string;
    issuer_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
