import { CompanyLegalitiesReadEntity } from '../../domain/entities/company-legalities-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class CompanyLegalitiesRead implements CompanyLegalitiesReadEntity {
    id: string;
    legality_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
