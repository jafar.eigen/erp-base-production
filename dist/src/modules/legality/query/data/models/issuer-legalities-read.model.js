"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IssuerLegalityRead = void 0;
const typeorm_1 = require("typeorm");
const legality_read_model_1 = require("./legality-read.model");
let IssuerLegalityRead = class IssuerLegalityRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], IssuerLegalityRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'issuer_name', nullable: true }),
    __metadata("design:type", String)
], IssuerLegalityRead.prototype, "issuer_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'issuer_status', nullable: true }),
    __metadata("design:type", String)
], IssuerLegalityRead.prototype, "issuer_status", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], IssuerLegalityRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], IssuerLegalityRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], IssuerLegalityRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => legality_read_model_1.LegalityRead, (legality) => legality.issuers),
    __metadata("design:type", legality_read_model_1.LegalityRead)
], IssuerLegalityRead.prototype, "legality", void 0);
IssuerLegalityRead = __decorate([
    (0, typeorm_1.Entity)('issuer_legalities')
], IssuerLegalityRead);
exports.IssuerLegalityRead = IssuerLegalityRead;
//# sourceMappingURL=issuer-legalities-read.model.js.map