import { GroupLegalitiesReadEntity } from '../../domain/entities/group-legalities-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class GroupLegalityRead implements GroupLegalitiesReadEntity {
    id: string;
    legality_id: string;
    group_id: string;
    group_name: string;
    group_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
