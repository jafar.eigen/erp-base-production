import { LegalityContactsReadEntity } from '../../domain/entities/legality-contacts-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class LegalityContactsRead implements LegalityContactsReadEntity {
    id: string;
    legality_id: string;
    contact_id: string;
    contact_name: string;
    description: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
