"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityRead = void 0;
const typeorm_1 = require("typeorm");
const legality_read_entity_1 = require("../../domain/entities/legality-read.entity");
const src_1 = require("src");
const legality_contacts_read_model_1 = require("./legality-contacts-read.model");
const legality_dates_read_model_1 = require("./legality-dates-read.model");
const company_legalities_read_model_1 = require("./company-legalities-read.model");
const group_legalities_read_model_1 = require("./group-legalities-read.model");
const issuer_legalities_read_model_1 = require("./issuer-legalities-read.model");
const legality_sites_read_model_1 = require("./legality-sites-read.model");
let LegalityRead = class LegalityRead {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], LegalityRead.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_link', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "account_link", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_name', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "account_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_password', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "account_password", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'issuer_id', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "issuer_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_id', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: legality_read_entity_1.LegalityStatus }),
    __metadata("design:type", String)
], LegalityRead.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], LegalityRead.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], LegalityRead.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], LegalityRead.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], LegalityRead.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], LegalityRead.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityRead.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityRead.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], LegalityRead.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => legality_contacts_read_model_1.LegalityContactsRead, (legalityContact) => legalityContact.legality, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], LegalityRead.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => legality_dates_read_model_1.LegalityDatesRead, (legalityDate) => legalityDate.legality),
    __metadata("design:type", Array)
], LegalityRead.prototype, "dates", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => company_legalities_read_model_1.CompanyLegalitiesRead, (companyLegality) => companyLegality.legality),
    __metadata("design:type", Array)
], LegalityRead.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_legalities_read_model_1.GroupLegalityRead, (groupLegality) => groupLegality.legality),
    __metadata("design:type", Array)
], LegalityRead.prototype, "groups", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => issuer_legalities_read_model_1.IssuerLegalityRead, (issuerLegality) => issuerLegality.legality),
    (0, typeorm_1.JoinColumn)({ name: 'issuer_id' }),
    __metadata("design:type", issuer_legalities_read_model_1.IssuerLegalityRead)
], LegalityRead.prototype, "issuers", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => legality_sites_read_model_1.LegalitySiteRead, (legalitySites) => legalitySites.legality),
    (0, typeorm_1.JoinColumn)({ name: 'site_id' }),
    __metadata("design:type", legality_sites_read_model_1.LegalitySiteRead)
], LegalityRead.prototype, "sites", void 0);
LegalityRead = __decorate([
    (0, typeorm_1.Entity)('legalities')
], LegalityRead);
exports.LegalityRead = LegalityRead;
//# sourceMappingURL=legality-read.model.js.map