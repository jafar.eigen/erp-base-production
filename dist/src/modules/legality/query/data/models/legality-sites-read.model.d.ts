import { LegalitySitesReadEntity } from '../../domain/entities/legality-sites-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class LegalitySiteRead implements LegalitySitesReadEntity {
    id: string;
    site_code: string;
    site_name: string;
    group_id: string;
    gid: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
