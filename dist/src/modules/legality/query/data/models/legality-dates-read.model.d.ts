import { LegalityDatesReadEntity } from '../../domain/entities/legality-dates-read.entity';
import { LegalityRead } from './legality-read.model';
export declare class LegalityDatesRead implements LegalityDatesReadEntity {
    id: string;
    legality_id: string;
    start_date: Date;
    expired_date: Date;
    on_behalf_of: string;
    document_path: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: LegalityRead;
}
