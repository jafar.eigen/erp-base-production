import { LegalityDatesReadEntity } from './legality-dates-read.entity';
import { LegalityContactsReadEntity } from './legality-contacts-read.entity';
import { CompanyLegalitiesReadEntity } from './company-legalities-read.entity';
import { GroupLegalitiesReadEntity } from './group-legalities-read.entity';
import { IssuerLegalitiesReadEntity } from './issuer-legalities-read.entity';
import { LegalitySitesReadEntity } from './legality-sites-read.entity';
import { BaseEntity } from 'src';
export declare enum LegalityStatus {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export declare enum RequestInfo {
    CREATE_DATA = "Create Data",
    EDIT_DATA = "Edit Data",
    EDIT_ACTIVE = "Edit Active",
    EDIT_INACTIVE = "Edit Inactive",
    EDIT_DATA_AND_ACTIVE = "Edit Data & Status Active",
    EDIT_DATA_AND_INACTIVE = "Edit Data & Status Inactive",
    DELETE_DATA = "Delete Data",
    ACTIVATE_DATE = "Activate Data"
}
export declare enum SortBy {
    ASC = "ASC",
    DESC = "DESC"
}
export interface LegalityEntityRead extends BaseEntity {
    name: string;
    account_link?: string;
    account_name?: string;
    account_password?: string;
    dates?: LegalityDatesReadEntity[];
    contacts?: LegalityContactsReadEntity[];
    companies?: CompanyLegalitiesReadEntity[];
    groups?: GroupLegalitiesReadEntity[];
    issuers?: IssuerLegalitiesReadEntity;
    sites?: LegalitySitesReadEntity;
    creator_name: string;
    editor_name?: string;
    deleted_by_name?: string;
}
