export declare enum CompanyLegalitiesStatuses {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export interface CompanyLegalitiesReadEntity {
    id: string;
    legality_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
