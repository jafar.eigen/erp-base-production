export interface LegalityFilterEntity {
    search: string;
    statuses: string[];
    requests_info: RequestInfo[];
    updated_by: string[];
    last_update_from: string;
    last_update_to: string;
    companies: string[];
    sites: string[];
    issuers: string[];
    start_date: string;
    expired_date: string;
}
