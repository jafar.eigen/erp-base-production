export declare enum GroupLegalitiesStatuses {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export interface GroupLegalitiesReadEntity {
    id: string;
    group_id: string;
    group_name: string;
    group_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
