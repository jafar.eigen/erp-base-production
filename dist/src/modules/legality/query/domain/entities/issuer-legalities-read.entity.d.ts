export declare enum IssuerLegalitiesStatuses {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export interface IssuerLegalitiesReadEntity {
    id: string;
    issuer_name: string;
    issuer_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
