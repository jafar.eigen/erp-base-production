import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { LegalityEntity } from '../../../command/domain/entities/legality.entity';
export interface ILegalitiesTransformer {
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: LegalityEntity[];
    apply(): Pagination<Partial<LegalityEntity>>;
}
