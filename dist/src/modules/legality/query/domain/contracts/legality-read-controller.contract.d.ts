import { IResponses } from 'src';
import { FilterLegalityDTO } from '../../infrasctructures/dto/filter-legality.dto';
export interface LegalityReadController {
    index(page: number, limit: number, params: FilterLegalityDTO): Promise<IResponses>;
    show(legalityId: string): Promise<IResponses>;
}
