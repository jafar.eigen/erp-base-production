import { LegalityEntity } from '../../../command/domain/entities/legality.entity';
export interface LegalityReadDataService {
    findOneOrFail(legalityId: string): Promise<LegalityEntity>;
}
