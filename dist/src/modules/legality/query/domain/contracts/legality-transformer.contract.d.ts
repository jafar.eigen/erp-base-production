import { LegalityEntity } from '../../../command/domain/entities/legality.entity';
export interface ILegalityTransformer {
    transform(): Partial<LegalityEntity>;
}
