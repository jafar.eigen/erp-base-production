"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityReadOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const legality_read_data_service_1 = require("../../data/services/legality-read-data.service");
const legality_filter_1 = require("./filters/legality.filter");
const legality_sort_1 = require("./filters/legality.sort");
let LegalityReadOrchestrator = class LegalityReadOrchestrator {
    constructor(legalityReadDataService) {
        this.legalityReadDataService = legalityReadDataService;
    }
    async show(legalityId) {
        return await this.legalityReadDataService.findOneOrFail(legalityId);
    }
    async index(page, limit, params) {
        const filter = new legality_filter_1.LegalityFilter(params);
        return await this.legalityReadDataService.index({
            page: page,
            limit: limit,
            route: 'api/legality'
        }, {
            relations: [
                'companies',
                'contacts',
                'groups',
                'issuers',
                'sites',
                'dates'
            ],
            join: {
                alias: 'legalities',
                leftJoin: {
                    contacts: 'legalities.contacts',
                    dates: 'legalities.dates',
                    companies: 'legalities.companies',
                    groups: 'legalities.groups',
                    issuers: 'legalities.issuers',
                    sites: 'legalities.sites'
                }
            },
            where: (querySite) => {
                filter.apply(querySite);
            },
            order: new legality_sort_1.LegalitySort(params).apply()
        });
    }
};
LegalityReadOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [legality_read_data_service_1.LegalityReadDataServiceImpl])
], LegalityReadOrchestrator);
exports.LegalityReadOrchestrator = LegalityReadOrchestrator;
//# sourceMappingURL=legality-read.orchestrator.js.map