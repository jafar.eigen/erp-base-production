"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityFilter = void 0;
const moment = require("moment");
class LegalityFilter {
    constructor(params) {
        this.search = params.search;
        this.statuses = params.statuses;
        this.requests_info = params.requests_info;
        this.last_update_from = params.last_update_from;
        this.last_update_to = params.last_update_to;
        this.last_updated_by = params.updated_by;
        this.companies = params.companies;
        this.sites = params.sites;
        this.issuers = params.issuers;
        this.start_date = params.start_date;
        this.expired_date = params.expired_date;
    }
    apply(query) {
        if (this.search)
            query = this.bySearch(query);
        if (this.statuses)
            query = this.byStatuses(query);
        if (this.requests_info)
            query = this.byRequestsInfo(query);
        if (this.last_update_from && this.last_update_to)
            query = this.byLastUpdateDate(query);
        if (this.last_updated_by)
            query = this.byLastUpdateUser(query);
        if (this.companies)
            query = this.byCompanies(query);
        if (this.sites)
            query = this.bySites(query);
        if (this.issuers)
            query = this.byIssuers(query);
        if (this.start_date)
            query = this.byStartDate(query);
        if (this.expired_date)
            query = this.byExpiredDate(query);
        return query;
    }
    bySearch(query) {
        return query
            .orWhere('legalities.name LIKE :name', {
            name: `%${this.search}%`
        })
            .orWhere('legalities.code LIKE :code', {
            code: `%${this.search}%`
        })
            .orWhere('sites.site_name LIKE :site_name', {
            site_name: `%${this.search}%`
        })
            .orWhere('dates.start_date LIKE :start_date', {
            start_date: `%${moment(new Date(this.search)).format('YYYY-MM-DD')}%`
        })
            .orWhere('dates.expired_date LIKE :expired_date', {
            expired_date: `%${moment(new Date(this.search)).format('YYYY-MM-DD')}%`
        })
            .orWhere('issuers.issuer_name LIKE :issuer_name', {
            issuer_name: `%${this.search}%`
        })
            .orWhere('contacts.contact_name LIKE :contact_name', {
            contact_name: `%${this.search}%`
        });
    }
    byStatuses(query) {
        return query.andWhere('legalities.status IN (:...statuses)', {
            statuses: this.statuses
        });
    }
    byRequestsInfo(query) {
        return query.andWhere('legalities.request_info IN (:...requestsInfo)', {
            requestsInfo: this.requests_info
        });
    }
    byLastUpdateDate(query) {
        return query.andWhere('legalities.updated_at BETWEEN :from AND :to', {
            from: moment(new Date(this.last_update_from)).format('YYYY-MM-DD HH:mm:ss'),
            to: moment(new Date(this.last_update_to)).format('YYYY-MM-DD HH:mm:ss')
        });
    }
    byLastUpdateUser(query) {
        return query.andWhere('legalities.editor_name IN (:...byName)', {
            byName: this.last_updated_by
        });
    }
    byCompanies(query) {
        return query.andWhere('companies.company_name IN (:...companyName)', {
            companyName: this.companies
        });
    }
    bySites(query) {
        return query.andWhere('sites.site_name IN (:...siteName)', {
            siteName: this.sites
        });
    }
    byIssuers(query) {
        return query.andWhere('issuers.issuer_name IN (:...issuerName)', {
            issuerName: this.issuers
        });
    }
    byStartDate(query) {
        return query.andWhere('dates.start_date IN (:...start_date)', {
            start_date: `${moment(new Date(this.start_date)).format('YYYY-MM-DD')}`
        });
    }
    byExpiredDate(query) {
        return query.andWhere('dates.expired_date IN (:...expired_date)', {
            expired_date: `${moment(new Date(this.expired_date)).format('YYYY-MM-DD')}`
        });
    }
}
exports.LegalityFilter = LegalityFilter;
//# sourceMappingURL=legality.filter.js.map