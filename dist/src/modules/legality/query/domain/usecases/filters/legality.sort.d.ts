import { FilterLegalityDTO } from '../../../infrasctructures/dto/filter-legality.dto';
export declare class LegalitySort {
    protected params: FilterLegalityDTO;
    constructor(params: FilterLegalityDTO);
    apply(): any;
}
