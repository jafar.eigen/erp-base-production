import { SelectQueryBuilder } from 'typeorm';
import { FilterLegalityDTO } from '../../../infrasctructures/dto/filter-legality.dto';
import { LegalityEntityRead } from '../../entities/legality-read.entity';
export declare class LegalityFilter {
    private search;
    private statuses;
    private requests_info;
    private last_update_from;
    private last_update_to;
    private last_updated_by;
    private companies;
    private sites;
    private issuers;
    private start_date;
    private expired_date;
    constructor(params: FilterLegalityDTO);
    apply(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected bySearch(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byStatuses(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byRequestsInfo(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byLastUpdateDate(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byLastUpdateUser(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byCompanies(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected bySites(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byIssuers(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byStartDate(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
    protected byExpiredDate(query: SelectQueryBuilder<LegalityEntityRead>): SelectQueryBuilder<LegalityEntityRead>;
}
