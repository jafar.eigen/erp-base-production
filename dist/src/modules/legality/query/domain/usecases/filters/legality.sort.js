"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalitySort = void 0;
const legality_read_entity_1 = require("../../entities/legality-read.entity");
class LegalitySort {
    constructor(params) {
        this.params = params;
    }
    apply() {
        const params = this.params;
        if (params.name_sorted) {
            return { name: params.name_sorted };
        }
        if (params.code_sorted) {
            return { code: params.code_sorted };
        }
        if (params.last_updated_sorted) {
            return { updated_at: params.last_updated_sorted };
        }
        if (params.status_sorted) {
            return { status: params.status_sorted };
        }
        return { created_at: legality_read_entity_1.SortBy.DESC };
    }
}
exports.LegalitySort = LegalitySort;
//# sourceMappingURL=legality.sort.js.map