import { Pagination } from 'nestjs-typeorm-paginate';
import { LegalityReadDataServiceImpl } from '../../data/services/legality-read-data.service';
import { LegalityEntity } from '../../../command/domain/entities/legality.entity';
import { FilterLegalityDTO } from '../../infrasctructures/dto/filter-legality.dto';
import { LegalityEntityRead } from '../entities/legality-read.entity';
export declare class LegalityReadOrchestrator {
    private legalityReadDataService;
    constructor(legalityReadDataService: LegalityReadDataServiceImpl);
    show(legalityId: string): Promise<LegalityEntity>;
    index(page: number, limit: number, params: FilterLegalityDTO): Promise<Pagination<LegalityEntityRead>>;
}
