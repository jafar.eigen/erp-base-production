"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const src_1 = require("src");
const legality_read_model_1 = require("./data/models/legality-read.model");
const legality_contacts_read_model_1 = require("./data/models/legality-contacts-read.model");
const legality_dates_read_model_1 = require("./data/models/legality-dates-read.model");
const legality_sites_read_model_1 = require("./data/models/legality-sites-read.model");
const company_legalities_read_model_1 = require("./data/models/company-legalities-read.model");
const group_legalities_read_model_1 = require("./data/models/group-legalities-read.model");
const issuer_legalities_read_model_1 = require("./data/models/issuer-legalities-read.model");
const database_config_1 = require("../../../utils/database.config");
const legality_read_orchestrator_1 = require("./domain/usecases/legality-read.orchestrator");
const legality_read_data_service_1 = require("./data/services/legality-read-data.service");
const legality_read_controller_1 = require("./infrasctructures/legality-read.controller");
let QueryModule = class QueryModule {
};
QueryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default],
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.LEGALITY_READ_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormLegalityQuery'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([
                legality_read_model_1.LegalityRead,
                legality_contacts_read_model_1.LegalityContactsRead,
                legality_dates_read_model_1.LegalityDatesRead,
                legality_sites_read_model_1.LegalitySiteRead,
                company_legalities_read_model_1.CompanyLegalitiesRead,
                group_legalities_read_model_1.GroupLegalityRead,
                issuer_legalities_read_model_1.IssuerLegalityRead
            ], database_config_1.LEGALITY_READ_CONNECTION)
        ],
        providers: [legality_read_orchestrator_1.LegalityReadOrchestrator, legality_read_data_service_1.LegalityReadDataServiceImpl, src_1.Responses],
        controllers: [legality_read_controller_1.LegalityReadControllerImpl]
    })
], QueryModule);
exports.QueryModule = QueryModule;
//# sourceMappingURL=query.module.js.map