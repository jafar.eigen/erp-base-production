"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const company_legalities_read_model_1 = require("./data/models/company-legalities-read.model");
const group_legalities_read_model_1 = require("./data/models/group-legalities-read.model");
const issuer_legalities_read_model_1 = require("./data/models/issuer-legalities-read.model");
const legality_contacts_read_model_1 = require("./data/models/legality-contacts-read.model");
const legality_dates_read_model_1 = require("./data/models/legality-dates-read.model");
const legality_read_model_1 = require("./data/models/legality-read.model");
const legality_sites_read_model_1 = require("./data/models/legality-sites-read.model");
exports.default = (0, config_1.registerAs)('typeormLegalityQuery', () => ({
    type: 'mysql',
    host: process.env.LEGALITY_MYSQL_QUERY_HOST,
    port: parseInt(process.env.LEGALITY_MYSQL_QUERY_PORT),
    username: process.env.LEGALITY_MYSQL_QUERY_USERNAME,
    password: process.env.LEGALITY_MYSQL_QUERY_PASSWORD,
    database: process.env.LEGALITY_MYSQL_QUERY_DATABASE,
    entities: [
        legality_read_model_1.LegalityRead,
        legality_contacts_read_model_1.LegalityContactsRead,
        legality_dates_read_model_1.LegalityDatesRead,
        legality_sites_read_model_1.LegalitySiteRead,
        company_legalities_read_model_1.CompanyLegalitiesRead,
        group_legalities_read_model_1.GroupLegalityRead,
        issuer_legalities_read_model_1.IssuerLegalityRead
    ]
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map