"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityReadControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const legality_read_orchestrator_1 = require("../domain/usecases/legality-read.orchestrator");
const legalities_transformer_1 = require("./transformer/legalities.transformer");
const legality_transformer_1 = require("./transformer/legality.transformer");
const filter_legality_dto_1 = require("./dto/filter-legality.dto");
let LegalityReadControllerImpl = class LegalityReadControllerImpl {
    constructor(responses, legalityReadOrchestrator) {
        this.responses = responses;
        this.legalityReadOrchestrator = legalityReadOrchestrator;
    }
    async index(page, limit, params) {
        try {
            const legalities = await this.legalityReadOrchestrator.index(page, limit, params);
            return this.responses.json(common_1.HttpStatus.OK, new legalities_transformer_1.LegalitiesTransformer(legalities, params).apply());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
    async show(legalityId) {
        try {
            const legality = await this.legalityReadOrchestrator.show(legalityId);
            return this.responses.json(common_1.HttpStatus.OK, new legality_transformer_1.LegalityTransformer(legality).transform());
        }
        catch (err) {
            return this.responses.json(common_1.HttpStatus.BAD_REQUEST, [], err.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)('limit', common_1.ParseIntPipe)),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, filter_legality_dto_1.FilterLegalityDTO]),
    __metadata("design:returntype", Promise)
], LegalityReadControllerImpl.prototype, "index", null);
__decorate([
    (0, common_1.Get)('/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], LegalityReadControllerImpl.prototype, "show", null);
LegalityReadControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('legality-indexes'),
    (0, common_1.Controller)('legality'),
    __metadata("design:paramtypes", [src_1.Responses,
        legality_read_orchestrator_1.LegalityReadOrchestrator])
], LegalityReadControllerImpl);
exports.LegalityReadControllerImpl = LegalityReadControllerImpl;
//# sourceMappingURL=legality-read.controller.js.map