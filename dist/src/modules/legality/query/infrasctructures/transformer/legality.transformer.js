"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityTransformer = void 0;
const moment = require("moment");
class LegalityTransformer {
    constructor(legality) {
        this.legality = legality;
    }
    transform() {
        return {
            id: this.legality.id,
            name: this.legality.name,
            code: this.legality.code,
            status: this.legality.status,
            account_link: this.legality.account_link,
            account_name: this.legality.account_name,
            account_password: this.legality.account_password,
            issuers: this.legality.issuers,
            request_info: this.legality.request_info,
            user_date: `${moment(this.legality.updated_at).format('DD/MM/YY,HH:mm')}`,
            approval: this.legality.approval,
            creator_id: this.legality.creator_id,
            creator_name: this.legality.creator_name,
            editor_id: this.legality.editor_id,
            editor_name: this.legality.editor_name,
            deleted_by_id: this.legality.deleted_by_id,
            deleted_by_name: this.legality.deleted_by_name,
            has_requested_process: this.legality.has_requested_process,
            requested_data: this.legality.requested_data,
            dates: this.legality.dates,
            contacts: this.legality.contacts,
            companies: this.legality.companies,
            groups: this.legality.groups,
            sites: this.legality.sites
        };
    }
}
exports.LegalityTransformer = LegalityTransformer;
//# sourceMappingURL=legality.transformer.js.map