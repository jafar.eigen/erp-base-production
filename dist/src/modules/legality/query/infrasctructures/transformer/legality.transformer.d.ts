import { ILegalityTransformer } from '../../domain/contracts/legality-transformer.contract';
import { LegalityEntity } from '../../../command/domain/entities/legality.entity';
export declare class LegalityTransformer implements ILegalityTransformer {
    private readonly legality;
    constructor(legality: LegalityEntity);
    transform(): {
        id: string;
        name: string;
        code: string;
        status: import("../../../../..").STATUS;
        account_link: string;
        account_name: string;
        account_password: string;
        issuers: import("../../../command/domain/entities/issuer-legalities.entity").IssuerLegalitiesEntity;
        request_info: import("../../../../..").RequestInfo;
        user_date: string;
        approval: import("../../../../..").ApprovalPerCompany;
        creator_id: string;
        creator_name: string;
        editor_id: string;
        editor_name: string;
        deleted_by_id: string;
        deleted_by_name: string;
        has_requested_process: boolean;
        requested_data: any;
        dates: import("../../../command/domain/entities/legality-dates.entity").LegalityDatesEntity[];
        contacts: import("../../../command/domain/entities/legality-contacts.entity").LegalityContactsEntity[];
        companies: import("../../../command/domain/entities/company-legalities.entity").CompanyLegalitiesEntity[];
        groups: import("../../../command/domain/entities/group-legalities.entity").GroupLegalitiesEntity[];
        sites: import("../../../command/domain/entities/legality-sites.entity").LegalitySitesEntity;
    };
}
