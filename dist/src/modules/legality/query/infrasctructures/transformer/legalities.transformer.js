"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalitiesTransformer = void 0;
const moment = require("moment");
class LegalitiesTransformer {
    constructor(legalities, params) {
        this.params = params;
        Object.assign(this, legalities);
    }
    apply() {
        const filterData = this.filterData();
        const legalities = filterData.map((legality) => {
            return this.fillData(legality);
        });
        return {
            items: legalities,
            meta: this.meta,
            links: this.links
        };
    }
    filterData() {
        return this.items.filter((legality) => {
            if (this.params.no_site && this.params.no_site === 'true') {
                return legality.sites !== null;
            }
            return !this.params.no_site;
        });
    }
    fillData(legality) {
        return {
            id: legality.id,
            name: legality.name,
            code: legality.code,
            status: legality.status,
            account_link: legality.account_link,
            account_name: legality.account_name,
            account_password: legality.account_password,
            issuers: legality.issuers,
            request_info: legality.request_info,
            user_date: `${moment(legality.updated_at).format('DD/MM/YY,HH:mm')}`,
            approval: legality.approval,
            creator_id: legality.creator_id,
            creator_name: legality.creator_name,
            editor_id: legality.editor_id,
            editor_name: legality.editor_name,
            deleted_by_id: legality.deleted_by_id,
            deleted_by_name: legality.deleted_by_name,
            has_requested_process: legality.has_requested_process,
            requested_data: legality.requested_data,
            dates: legality.dates,
            contacts: legality.contacts,
            companies: legality.companies,
            groups: legality.groups,
            sites: legality.sites
        };
    }
}
exports.LegalitiesTransformer = LegalitiesTransformer;
//# sourceMappingURL=legalities.transformer.js.map