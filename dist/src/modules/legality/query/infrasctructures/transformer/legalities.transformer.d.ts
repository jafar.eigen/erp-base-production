import { IPaginationLinks, IPaginationMeta, Pagination } from 'nestjs-typeorm-paginate';
import { ILegalitiesTransformer } from '../../domain/contracts/legalities-transformer.contract';
import { FilterLegalityDTO } from '../dto/filter-legality.dto';
import { LegalityEntityRead } from '../../domain/entities/legality-read.entity';
export declare class LegalitiesTransformer implements ILegalitiesTransformer {
    private params;
    meta: IPaginationMeta;
    links: IPaginationLinks;
    items: LegalityEntityRead[];
    constructor(legalities: Pagination<LegalityEntityRead>, params: FilterLegalityDTO);
    apply(): Pagination<Partial<LegalityEntityRead>>;
    filterData(): LegalityEntityRead[];
    fillData(legality: LegalityEntityRead): any;
}
