import { Responses } from 'src';
import { IResponses } from 'src';
import { LegalityReadController } from '../domain/contracts/legality-read-controller.contract';
import { LegalityReadOrchestrator } from '../domain/usecases/legality-read.orchestrator';
import { FilterLegalityDTO } from './dto/filter-legality.dto';
export declare class LegalityReadControllerImpl implements LegalityReadController {
    private responses;
    private legalityReadOrchestrator;
    constructor(responses: Responses, legalityReadOrchestrator: LegalityReadOrchestrator);
    index(page: number, limit: number, params: FilterLegalityDTO): Promise<IResponses>;
    show(legalityId: string): Promise<IResponses>;
}
