"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterLegalityDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const legality_read_entity_1 = require("../../domain/entities/legality-read.entity");
class FilterLegalityDTO {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.search),
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Search by: Legality Name, Site Name, Start/End Date, Issuer, Contact'
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.no_site),
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'No Site is True/False'
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "no_site", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.statuses),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Draft, Inactive, Active, Requested, Declined'
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "statuses", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requests_info),
    (0, class_transformer_1.Transform)(({ value }) => {
        if (Array.isArray(value))
            return value;
        return [value];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        required: false,
        description: 'Available values : Create Data, Edit Data, Edit Active, Edit Inactive, Edit Data & Status Active, Edit Data & Status Inactive, Delete Data'
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "requests_info", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.updated_by) ? body.updated_by : [body.updated_by];
    }),
    (0, swagger_1.ApiProperty)({
        type: [String],
        isArray: true,
        description: 'Find based on updated by',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "updated_by", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.last_update_from),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update from',
        required: false
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "last_update_from", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.last_update_to),
    (0, swagger_1.ApiProperty)({
        type: 'string',
        description: 'Filter by last update to',
        required: false
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "last_update_to", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.companies) ? body.companies : [body.companies];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by company names',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "companies", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.sites) ? body.sites : [body.sites];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by site names, if option no site just use param no_site',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "sites", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return Array.isArray(body.issuers) ? body.issuers : [body.issuers];
    }),
    (0, swagger_1.ApiProperty)({
        isArray: true,
        type: [String],
        description: 'Filter by Issuers',
        required: false
    }),
    __metadata("design:type", Array)
], FilterLegalityDTO.prototype, "issuers", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.start_date;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Filter by Start Date Document',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "start_date", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.expired_date;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Filter by Expired Date Document',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "expired_date", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.name_sorted;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Sorted By Name, ASC/DESC',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "name_sorted", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.code_sorted;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Sorted By Code, ASC/DESC',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "code_sorted", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.last_updated_sorted;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Sorted By Last Updated, ASC/DESC',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "last_updated_sorted", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => {
        return body.status_sorted;
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: 'Sorted By Status, ASC/DESC',
        required: false
    }),
    __metadata("design:type", String)
], FilterLegalityDTO.prototype, "status_sorted", void 0);
exports.FilterLegalityDTO = FilterLegalityDTO;
//# sourceMappingURL=filter-legality.dto.js.map