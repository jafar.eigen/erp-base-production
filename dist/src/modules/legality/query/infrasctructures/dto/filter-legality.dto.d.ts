import { LegalityFilterEntity } from '../../domain/entities/legality-filter.entity';
import { RequestInfo } from '../../../../site/query/domain/entities/site-read.entity';
import { SortBy } from '../../domain/entities/legality-read.entity';
export declare class FilterLegalityDTO implements LegalityFilterEntity {
    search: string;
    no_site: string;
    statuses: string[];
    requests_info: RequestInfo[];
    updated_by: string[];
    last_update_from: string;
    last_update_to: string;
    companies: string[];
    sites: string[];
    issuers: string[];
    start_date: string;
    expired_date: string;
    name_sorted: SortBy;
    code_sorted: SortBy;
    last_updated_sorted: SortBy;
    status_sorted: SortBy;
}
