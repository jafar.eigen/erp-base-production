"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityDataServiceImpl = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const src_1 = require("src");
const legality_model_1 = require("../models/legality.model");
const database_config_1 = require("../../../../../utils/database.config");
const legality_contacts_model_1 = require("../models/legality-contacts.model");
const legality_dates_model_1 = require("../models/legality-dates.model");
const company_legalities_model_1 = require("../models/company-legalities.model");
const group_legalities_model_1 = require("../models/group-legalities.model");
const issuer_legalities_model_1 = require("../models/issuer-legalities.model");
const legality_sites_model_1 = require("../models/legality-sites.model");
let LegalityDataServiceImpl = class LegalityDataServiceImpl extends src_1.BaseDataService {
    constructor(legalityRepo, legalityContactRepo, legalityDateRepo, legalityCompanyRepo, legalityGroupRepo, legalityIssuerRepo, legalitySiteRepo) {
        super(legalityRepo);
        this.legalityRepo = legalityRepo;
        this.legalityContactRepo = legalityContactRepo;
        this.legalityDateRepo = legalityDateRepo;
        this.legalityCompanyRepo = legalityCompanyRepo;
        this.legalityGroupRepo = legalityGroupRepo;
        this.legalityIssuerRepo = legalityIssuerRepo;
        this.legalitySiteRepo = legalitySiteRepo;
        this.relations = ['contacts', 'dates', 'companies', 'groups', 'issuers'];
    }
    async findByCode(code) {
        return await this.legalityRepo.findOne({
            where: { code: code },
            relations: ['companies']
        });
    }
    async findByName(name) {
        return await this.legalityRepo.find({
            relations: ['companies'],
            where: { name: name }
        });
    }
    async updateDate(updateLegalityDate) {
        return await this.legalityDateRepo.save(updateLegalityDate);
    }
    async findNulledRelationOnLegalityDate() {
        return await this.legalityDateRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
    }
    async findSiteLegalities(siteCode) {
        return await this.legalitySiteRepo.find({
            where: {
                site_code: siteCode
            }
        });
    }
    async saveManySites(sites) {
        return await this.legalityCompanyRepo.save(sites);
    }
    async findCompanyLegalities(companyId) {
        return await this.legalityCompanyRepo.find({
            where: {
                company_id: companyId
            }
        });
    }
    async saveManyCompanies(companies) {
        return await this.legalityCompanyRepo.save(companies);
    }
    async findGroupLegalities(groupId) {
        return await this.legalityGroupRepo.find({
            where: {
                group_id: groupId
            }
        });
    }
    async saveManyGroups(groups) {
        return await this.legalityCompanyRepo.save(groups);
    }
    async findAndDeleteNulledRelation() {
        const contacts = await this.legalityContactRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        const dates = await this.legalityDateRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        const companies = await this.legalityCompanyRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        const groups = await this.legalityGroupRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        const issuers = await this.legalityIssuerRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        const sites = await this.legalitySiteRepo.find({
            where: {
                legality_id: (0, typeorm_2.IsNull)()
            }
        });
        if (contacts) {
            await this.legalityContactRepo.remove(contacts);
        }
        if (dates) {
            await this.legalityDateRepo.remove(dates);
        }
        if (companies) {
            await this.legalityCompanyRepo.remove(companies);
        }
        if (groups) {
            await this.legalityGroupRepo.remove(groups);
        }
        if (issuers) {
            await this.legalityIssuerRepo.remove(issuers);
        }
        if (sites) {
            await this.legalitySiteRepo.remove(sites);
        }
    }
};
LegalityDataServiceImpl = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(legality_model_1.Legality, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(1, (0, typeorm_1.InjectRepository)(legality_contacts_model_1.LegalityContact, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(2, (0, typeorm_1.InjectRepository)(legality_dates_model_1.LegalityDate, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(3, (0, typeorm_1.InjectRepository)(company_legalities_model_1.CompanyLegality, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(4, (0, typeorm_1.InjectRepository)(group_legalities_model_1.GroupLegality, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(5, (0, typeorm_1.InjectRepository)(issuer_legalities_model_1.IssuerLegality, database_config_1.LEGALITY_CUD_CONNECTION)),
    __param(6, (0, typeorm_1.InjectRepository)(legality_sites_model_1.LegalitySite, database_config_1.LEGALITY_CUD_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], LegalityDataServiceImpl);
exports.LegalityDataServiceImpl = LegalityDataServiceImpl;
//# sourceMappingURL=legality-data.service.js.map