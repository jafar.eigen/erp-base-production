import { CompanyLegalitiesEntity } from '../../domain/entities/company-legalities.entity';
import { Legality } from './legality.model';
export declare class CompanyLegality implements CompanyLegalitiesEntity {
    id: string;
    legality_id: string;
    company_id: string;
    company_code: string;
    company_name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: Legality;
}
