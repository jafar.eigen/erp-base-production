"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Legality = void 0;
const typeorm_1 = require("typeorm");
const legality_entity_1 = require("../../domain/entities/legality.entity");
const src_1 = require("src");
const legality_contacts_model_1 = require("./legality-contacts.model");
const legality_dates_model_1 = require("./legality-dates.model");
const company_legalities_model_1 = require("./company-legalities.model");
const group_legalities_model_1 = require("./group-legalities.model");
const issuer_legalities_model_1 = require("./issuer-legalities.model");
const legality_sites_model_1 = require("./legality-sites.model");
let Legality = class Legality {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Legality.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_link', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "account_link", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_name', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "account_name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'account_password', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "account_password", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'issuer_id', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "issuer_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'site_id', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "site_id", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: legality_entity_1.LegalityStatus }),
    __metadata("design:type", String)
], Legality.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('enum', { enum: src_1.RequestInfo }),
    __metadata("design:type", String)
], Legality.prototype, "request_info", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'approval', nullable: true }),
    __metadata("design:type", Object)
], Legality.prototype, "approval", void 0);
__decorate([
    (0, typeorm_1.Column)('json', { name: 'requested_data', nullable: true }),
    __metadata("design:type", Object)
], Legality.prototype, "requested_data", void 0);
__decorate([
    (0, typeorm_1.Column)('boolean', { name: 'has_requested_process', nullable: true }),
    __metadata("design:type", Boolean)
], Legality.prototype, "has_requested_process", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'creator_id', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "creator_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'creator_name', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "creator_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'editor_id', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "editor_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'editor_name', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "editor_name", void 0);
__decorate([
    (0, typeorm_1.Column)('uuid', { name: 'deleted_by_id', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "deleted_by_id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'deleted_by_name', nullable: true }),
    __metadata("design:type", String)
], Legality.prototype, "deleted_by_name", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Legality.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Legality.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Legality.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => legality_contacts_model_1.LegalityContact, (legalityContact) => legalityContact.legality, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], Legality.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => legality_dates_model_1.LegalityDate, (legalityDate) => legalityDate.legality, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], Legality.prototype, "dates", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => company_legalities_model_1.CompanyLegality, (companyLegality) => companyLegality.legality, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], Legality.prototype, "companies", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => group_legalities_model_1.GroupLegality, (groupLegality) => groupLegality.legality, {
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], Legality.prototype, "groups", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => issuer_legalities_model_1.IssuerLegality, (issuerLegality) => issuerLegality.legality, {
        cascade: ['insert', 'update', 'remove'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'issuer_id' }),
    __metadata("design:type", issuer_legalities_model_1.IssuerLegality)
], Legality.prototype, "issuers", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => legality_sites_model_1.LegalitySite, (legalitySite) => legalitySite.legality, {
        cascade: ['insert', 'update', 'remove'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    (0, typeorm_1.JoinColumn)({ name: 'site_id' }),
    __metadata("design:type", legality_sites_model_1.LegalitySite)
], Legality.prototype, "sites", void 0);
Legality = __decorate([
    (0, typeorm_1.Entity)('legalities')
], Legality);
exports.Legality = Legality;
//# sourceMappingURL=legality.model.js.map