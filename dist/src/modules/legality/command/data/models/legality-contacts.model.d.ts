import { LegalityContactsEntity } from '../../domain/entities/legality-contacts.entity';
import { Legality } from './legality.model';
export declare class LegalityContact implements LegalityContactsEntity {
    id: string;
    legality_id: string;
    contact_id: string;
    contact_name: string;
    description: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: Legality;
}
