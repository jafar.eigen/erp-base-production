import { Legality } from './legality.model';
import { IssuerLegalitiesEntity } from '../../domain/entities/issuer-legalities.entity';
export declare class IssuerLegality implements IssuerLegalitiesEntity {
    id: string;
    issuer_name: string;
    issuer_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: Legality;
}
