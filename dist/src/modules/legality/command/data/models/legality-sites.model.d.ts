import { LegalitySitesEntity } from '../../domain/entities/legality-sites.entity';
import { Legality } from './legality.model';
export declare class LegalitySite implements LegalitySitesEntity {
    id: string;
    site_code: string;
    site_name: string;
    group_id: string;
    gid: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: Legality;
}
