import { Legality } from './legality.model';
import { GroupLegalitiesEntity } from '../../domain/entities/group-legalities.entity';
export declare class GroupLegality implements GroupLegalitiesEntity {
    id: string;
    legality_id: string;
    group_id: string;
    group_name: string;
    group_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    legality: Legality;
}
