"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyGroupChangedManager = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const legality_data_service_1 = require("src/modules/legality/command/data/services/legality-data.service");
let CompanyGroupChangedManager = class CompanyGroupChangedManager extends src_1.BaseConsumerManager {
    constructor(legalityDataService) {
        super(legalityDataService);
        this.legalityDataService = legalityDataService;
    }
    async transformData(value) {
        var _a;
        this.group = (_a = value.data) !== null && _a !== void 0 ? _a : value.old;
        const groupLegalities = await this.legalityDataService.findGroupLegalities(this.group.id);
        return groupLegalities;
    }
    async processData(dataTransform) {
        const groups = await Promise.all(dataTransform.map(async (group) => {
            const consume = this.getValidFrom(this.group);
            return Object.assign(group, { consume });
        }));
        await this.legalityDataService.saveManyGroups(groups);
    }
    async getValidFrom(group) {
        return {
            group_name: group.name,
            group_status: group.status
        };
    }
};
CompanyGroupChangedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [legality_data_service_1.LegalityDataServiceImpl])
], CompanyGroupChangedManager);
exports.CompanyGroupChangedManager = CompanyGroupChangedManager;
//# sourceMappingURL=company-group-changed.manager.ts.js.map