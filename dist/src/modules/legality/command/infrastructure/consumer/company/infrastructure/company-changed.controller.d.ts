import { Logger } from 'winston';
import { CompanyChangedManager } from '../domain/company-changed.manager';
import { LegalityProducerServiceImpl } from '../../../producer/legality-producer.service';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CompanyChangedConsumerController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private companyChangedManager;
    private legalityProducerService;
    constructor(logger: Logger, clientSentry: SentryService, companyChangedManager: CompanyChangedManager, legalityProducerService: LegalityProducerServiceImpl);
}
export {};
