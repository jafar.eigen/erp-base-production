import { Logger } from 'winston';
import { LegalityProducerServiceImpl } from '../../../producer/legality-producer.service';
import { CompanyGroupChangedManager } from '../domain/company-group-changed.manager.ts';
import { SentryService } from '@ntegral/nestjs-sentry';
declare const BaseConsumerController: any;
export declare class CompanyGroupChangedConsumerController extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private companyGroupChangedManager;
    private legalityProducerService;
    constructor(logger: Logger, clientSentry: SentryService, companyGroupChangedManager: CompanyGroupChangedManager, legalityProducerService: LegalityProducerServiceImpl);
}
export {};
