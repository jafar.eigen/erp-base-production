import { BaseConsumerManager, IUserPayload, KafkaPayload } from 'src';
import { CompanyLegalitiesEntity } from 'src/modules/legality/command/domain/entities/company-legalities.entity';
import { LegalityDataServiceImpl } from '../../../../data/services/legality-data.service';
export declare class CompanyChangedManager extends BaseConsumerManager {
    private legalityDataService;
    company: any;
    constructor(legalityDataService: LegalityDataServiceImpl);
    transformData(value: Partial<KafkaPayload<IUserPayload, any, any>>): Promise<Partial<CompanyLegalitiesEntity[]>>;
    processData(dataTransform: Partial<CompanyLegalitiesEntity[]>): Promise<void>;
    protected getValidFrom(company: any): Promise<Partial<CompanyLegalitiesEntity>>;
}
