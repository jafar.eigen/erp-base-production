import { BaseConsumerManager, IUserPayload, KafkaPayload } from 'src';
import { LegalityDataServiceImpl } from 'src/modules/legality/command/data/services/legality-data.service';
import { GroupLegalitiesEntity } from 'src/modules/legality/command/domain/entities/group-legalities.entity';
export declare class CompanyGroupChangedManager extends BaseConsumerManager {
    private legalityDataService;
    group: any;
    constructor(legalityDataService: LegalityDataServiceImpl);
    transformData(value: Partial<KafkaPayload<IUserPayload, any, any>>): Promise<Partial<GroupLegalitiesEntity[]>>;
    processData(dataTransform: Partial<GroupLegalitiesEntity[]>): Promise<void>;
    protected getValidFrom(group: any): Promise<Partial<GroupLegalitiesEntity>>;
}
