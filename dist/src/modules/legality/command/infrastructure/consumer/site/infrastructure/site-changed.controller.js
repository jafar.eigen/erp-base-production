"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteChangedConsumerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const src_1 = require("src");
const site_changed_manager_1 = require("../domain/site-changed.manager");
const legality_consumer_topic_1 = require("../../legality-consumer.topic");
const legality_topics_1 = require("../../../../legality.topics");
const legality_producer_service_1 = require("../../../producer/legality-producer.service");
const BaseConsumerController = (0, src_1.getBaseConsumerController)(legality_topics_1.MODULE_TOPIC_LEGALITY, legality_consumer_topic_1.HANDLE_SITE_CHANGED);
let SiteChangedConsumerControllerImpl = class SiteChangedConsumerControllerImpl extends BaseConsumerController {
    constructor(logger, clientSentry, siteChangedManager, legalityProducerService) {
        super(logger, clientSentry, siteChangedManager, legalityProducerService);
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.siteChangedManager = siteChangedManager;
        this.legalityProducerService = legalityProducerService;
    }
};
SiteChangedConsumerControllerImpl = __decorate([
    (0, common_1.Controller)('site'),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        site_changed_manager_1.SiteChangedManager,
        legality_producer_service_1.LegalityProducerServiceImpl])
], SiteChangedConsumerControllerImpl);
exports.SiteChangedConsumerControllerImpl = SiteChangedConsumerControllerImpl;
//# sourceMappingURL=site-changed.controller.js.map