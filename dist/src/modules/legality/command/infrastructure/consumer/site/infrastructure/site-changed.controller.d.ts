import { Logger } from 'winston';
import { SentryService } from '@ntegral/nestjs-sentry';
import { SiteChangedManager } from '../domain/site-changed.manager';
import { LegalityProducerServiceImpl } from '../../../producer/legality-producer.service';
declare const BaseConsumerController: any;
export declare class SiteChangedConsumerControllerImpl extends BaseConsumerController {
    private readonly logger;
    private readonly clientSentry;
    private siteChangedManager;
    private legalityProducerService;
    constructor(logger: Logger, clientSentry: SentryService, siteChangedManager: SiteChangedManager, legalityProducerService: LegalityProducerServiceImpl);
}
export {};
