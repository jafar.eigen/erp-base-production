import { BaseConsumerManager, BaseEntity, IUserPayload, KafkaPayload } from 'src';
import { LegalitySitesEntity } from '../../../../domain/entities/legality-sites.entity';
import { LegalityDataServiceImpl } from '../../../../data/services/legality-data.service';
export declare class SiteChangedManager extends BaseConsumerManager {
    private legalityDataService;
    site: {
        code: string;
    } & Partial<LegalitySitesEntity>;
    constructor(legalityDataService: LegalityDataServiceImpl);
    transformData(value: Partial<KafkaPayload<IUserPayload, any, any>>): Promise<Partial<BaseEntity>>;
    processData(dataTransform: Partial<LegalitySitesEntity[]>): Promise<void>;
    protected getValidFrom(site: any): Promise<Partial<LegalitySitesEntity>>;
}
