"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteChangedManager = void 0;
const src_1 = require("src");
const common_1 = require("@nestjs/common");
const legality_data_service_1 = require("../../../../data/services/legality-data.service");
let SiteChangedManager = class SiteChangedManager extends src_1.BaseConsumerManager {
    constructor(legalityDataService) {
        super(legalityDataService);
        this.legalityDataService = legalityDataService;
    }
    async transformData(value) {
        var _a;
        this.site = (_a = value.data) !== null && _a !== void 0 ? _a : value.old;
        const siteLegalities = await this.legalityDataService.findSiteLegalities(this.site.code);
        const sites = await Promise.all(siteLegalities.map(async (site) => {
            const consume = this.getValidFrom(this.site);
            return Object.assign(site, { consume });
        }));
        return sites;
    }
    async processData(dataTransform) {
        this.legalityDataService.saveManySites(dataTransform);
    }
    async getValidFrom(site) {
        return {
            site_name: site.name,
            site_code: site.code,
            group_id: site.group_id,
            gid: site.groups.gid
        };
    }
};
SiteChangedManager = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [legality_data_service_1.LegalityDataServiceImpl])
], SiteChangedManager);
exports.SiteChangedManager = SiteChangedManager;
//# sourceMappingURL=site-changed.manager.js.map