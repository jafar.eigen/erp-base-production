/// <reference types="multer" />
import { Responses, IResponses } from 'src';
import { LegalityOrchestrator } from '../../domain/usecases/legality.orchestrator';
import { CreateLegalityDatesFileDto } from '../dto/create-legality-dates.file.dto';
declare const BaseController: any;
export declare class LegalityProducerControllerImpl extends BaseController {
    private responses;
    private legalityOrchestrator;
    constructor(responses: Responses, legalityOrchestrator: LegalityOrchestrator);
    storeFile(file: Express.Multer.File, body: CreateLegalityDatesFileDto): Promise<IResponses>;
}
export {};
