"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityProducerControllerImpl = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const platform_express_1 = require("@nestjs/platform-express");
const src_1 = require("src");
const legality_orchestrator_1 = require("../../domain/usecases/legality.orchestrator");
const create_legality_dto_1 = require("../dto/create-legality.dto");
const update_legality_dto_1 = require("../dto/update-legality.dto");
const create_legality_dates_file_dto_1 = require("../dto/create-legality-dates.file.dto");
const store_date_file_config_1 = require("../../../../../utils/store-date-file.config");
const BaseController = (0, src_1.getBaseController)({
    createModelVm: create_legality_dto_1.CreateLegalityDto,
    updateModelVm: update_legality_dto_1.UpdateLegalityDto
});
let LegalityProducerControllerImpl = class LegalityProducerControllerImpl extends BaseController {
    constructor(responses, legalityOrchestrator) {
        super(new src_1.Responses(), legalityOrchestrator, new src_1.JSONParse());
        this.responses = responses;
        this.legalityOrchestrator = legalityOrchestrator;
    }
    async storeFile(file, body) {
        try {
            const datesFileDto = await this.legalityOrchestrator.storeFile(body, file);
            return this.responses.json(common_1.HttpStatus.OK, datesFileDto);
        }
        catch (error) {
            throw new common_1.BadRequestException(error.message);
        }
    }
};
__decorate([
    (0, common_1.Post)('date/document'),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({ type: create_legality_dates_file_dto_1.CreateLegalityDatesFileDto }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('document_path', store_date_file_config_1.storeFileDateConfig)),
    __param(0, (0, common_1.UploadedFile)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_legality_dates_file_dto_1.CreateLegalityDatesFileDto]),
    __metadata("design:returntype", Promise)
], LegalityProducerControllerImpl.prototype, "storeFile", null);
LegalityProducerControllerImpl = __decorate([
    (0, swagger_1.ApiTags)('legality'),
    (0, common_1.Controller)('legality'),
    __metadata("design:paramtypes", [src_1.Responses,
        legality_orchestrator_1.LegalityOrchestrator])
], LegalityProducerControllerImpl);
exports.LegalityProducerControllerImpl = LegalityProducerControllerImpl;
//# sourceMappingURL=legality-producer.controller.js.map