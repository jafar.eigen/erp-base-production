import { ClientKafka } from '@nestjs/microservices';
import { BaseProducerServiceImpl } from 'src';
import { LegalityEntity } from '../../domain/entities/legality.entity';
export declare class LegalityProducerServiceImpl extends BaseProducerServiceImpl<LegalityEntity> {
    client: ClientKafka;
    moduleName: string;
    TOPIC_CREATED: string;
    TOPIC_CHANGED: string;
    TOPIC_ACTIVATED: string;
    TOPIC_DELETED: string;
    constructor(client: ClientKafka);
    addNewTopics(): string[];
}
