import { LegalityContactsEntity } from '../../domain/entities/legality-contacts.entity';
export declare class UpdateLegalityContactsDto implements LegalityContactsEntity {
    id: string;
    legality_id: string;
    contact_id: string;
    contact_name: string;
    description: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
