"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGroupLegalitiesDto = void 0;
const group_legalities_entity_1 = require("../../domain/entities/group-legalities.entity");
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class UpdateGroupLegalitiesDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.id),
    __metadata("design:type", String)
], UpdateGroupLegalitiesDto.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.legality_id),
    __metadata("design:type", String)
], UpdateGroupLegalitiesDto.prototype, "legality_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.group_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateGroupLegalitiesDto.prototype, "group_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], UpdateGroupLegalitiesDto.prototype, "group_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enumName: 'Group Legalities Status',
        enum: Object.values(group_legalities_entity_1.GroupLegalitiesStatuses)
    }),
    (0, class_validator_1.IsIn)(Object.values(group_legalities_entity_1.GroupLegalitiesStatuses)),
    __metadata("design:type", String)
], UpdateGroupLegalitiesDto.prototype, "group_status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupLegalitiesDto.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupLegalitiesDto.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateGroupLegalitiesDto.prototype, "deleted_at", void 0);
exports.UpdateGroupLegalitiesDto = UpdateGroupLegalitiesDto;
//# sourceMappingURL=update-group-legalities.dto.js.map