"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLegalityDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const src_1 = require("src");
const update_legality_sites_dto_1 = require("./update-legality-sites.dto");
const update_company_legalities_dto_1 = require("./update-company-legalities.dto");
const update_group_legalities_dto_1 = require("./update-group-legalities.dto");
const update_issuer_legalities_dto_1 = require("./update-issuer-legalities.dto");
const update_legality_contacts_dto_1 = require("./update-legality-contacts.dto");
const update_legality_dates_dto_1 = require("./update-legality-dates.dto");
class UpdateLegalityDto {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.name),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "code", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_link),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "account_link", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_name),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "account_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_password),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "account_password", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.issuer_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "issuer_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "site_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateLegalityDto.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], UpdateLegalityDto.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], UpdateLegalityDto.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "request_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_legality_sites_dto_1.UpdateLegalitySitesDto],
        default: update_legality_sites_dto_1.UpdateLegalitySitesDto,
        required: false
    }),
    (0, class_transformer_1.Type)(() => update_legality_sites_dto_1.UpdateLegalitySitesDto),
    __metadata("design:type", update_legality_sites_dto_1.UpdateLegalitySitesDto)
], UpdateLegalityDto.prototype, "sites", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_company_legalities_dto_1.UpdateCompanyLegalitiesDto],
        default: update_company_legalities_dto_1.UpdateCompanyLegalitiesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_company_legalities_dto_1.UpdateCompanyLegalitiesDto),
    __metadata("design:type", Array)
], UpdateLegalityDto.prototype, "companies", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_group_legalities_dto_1.UpdateGroupLegalitiesDto],
        default: update_group_legalities_dto_1.UpdateGroupLegalitiesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_group_legalities_dto_1.UpdateGroupLegalitiesDto),
    __metadata("design:type", Array)
], UpdateLegalityDto.prototype, "groups", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: update_issuer_legalities_dto_1.UpdateIssuerLegalitiesDto,
        default: update_issuer_legalities_dto_1.UpdateIssuerLegalitiesDto,
        required: false
    }),
    (0, class_transformer_1.Type)(() => update_issuer_legalities_dto_1.UpdateIssuerLegalitiesDto),
    __metadata("design:type", update_issuer_legalities_dto_1.UpdateIssuerLegalitiesDto)
], UpdateLegalityDto.prototype, "issuers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_legality_contacts_dto_1.UpdateLegalityContactsDto],
        default: update_legality_contacts_dto_1.UpdateLegalityContactsDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_legality_contacts_dto_1.UpdateLegalityContactsDto),
    __metadata("design:type", Array)
], UpdateLegalityDto.prototype, "contacts", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [update_legality_dates_dto_1.UpdateLegalityDatesDto],
        default: update_legality_dates_dto_1.UpdateLegalityDatesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => update_legality_dates_dto_1.UpdateLegalityDatesDto),
    __metadata("design:type", Array)
], UpdateLegalityDto.prototype, "dates", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], UpdateLegalityDto.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLegalityDto.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLegalityDto.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateLegalityDto.prototype, "deleted_at", void 0);
exports.UpdateLegalityDto = UpdateLegalityDto;
//# sourceMappingURL=update-legality.dto.js.map