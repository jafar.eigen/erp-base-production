"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLegalitySitesDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class CreateLegalitySitesDto {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.legality_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "legality_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.IsUUID)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "site_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "site_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "site_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "group_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalitySitesDto.prototype, "gid", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalitySitesDto.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalitySitesDto.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalitySitesDto.prototype, "deleted_at", void 0);
exports.CreateLegalitySitesDto = CreateLegalitySitesDto;
//# sourceMappingURL=create-legality-sites.dto.js.map