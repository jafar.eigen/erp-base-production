import { IssuerLegalitiesEntity } from '../../domain/entities/issuer-legalities.entity';
export declare class UpdateIssuerLegalitiesDto implements IssuerLegalitiesEntity {
    id: string;
    legality_id: string;
    issuer_id: string;
    issuer_name: string;
    issuer_status: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
