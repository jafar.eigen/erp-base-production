"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLegalityDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const src_1 = require("src");
const class_transformer_1 = require("class-transformer");
const create_legality_contacts_dto_1 = require("./create-legality-contacts.dto");
const create_legality_dates_dto_1 = require("./create-legality-dates.dto");
const create_company_legalities_dto_1 = require("./create-company-legalities.dto");
const create_group_legalities_dto_1 = require("./create-group-legalities.dto");
const create_issuer_legalities_dto_1 = require("./create-issuer-legalities.dto");
const create_legality_sites_dto_1 = require("./create-legality-sites.dto");
class CreateLegalityDto {
}
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.name),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.code),
    (0, class_transformer_1.Transform)(({ value }) => value.toUpperCase()),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "code", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.status),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_link),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "account_link", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_name),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "account_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string', required: false }),
    (0, class_validator_1.ValidateIf)((body) => body.account_password),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "account_password", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.issuer_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "issuer_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.site_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "site_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.approval),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateLegalityDto.prototype, "approval", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.requested_data),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Object)
], CreateLegalityDto.prototype, "requested_data", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.has_requested_process),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Boolean)
], CreateLegalityDto.prototype, "has_requested_process", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.request_info),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "request_info", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: create_legality_sites_dto_1.CreateLegalitySitesDto,
        default: create_legality_sites_dto_1.CreateLegalitySitesDto,
        required: false
    }),
    (0, class_transformer_1.Type)(() => create_legality_sites_dto_1.CreateLegalitySitesDto),
    __metadata("design:type", create_legality_sites_dto_1.CreateLegalitySitesDto)
], CreateLegalityDto.prototype, "sites", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: create_issuer_legalities_dto_1.CreateIssuerLegalitiesDto,
        default: create_issuer_legalities_dto_1.CreateIssuerLegalitiesDto,
        required: false
    }),
    (0, class_transformer_1.Type)(() => create_issuer_legalities_dto_1.CreateIssuerLegalitiesDto),
    __metadata("design:type", create_issuer_legalities_dto_1.CreateIssuerLegalitiesDto)
], CreateLegalityDto.prototype, "issuers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [create_company_legalities_dto_1.CreateCompanyLegalitiesDto],
        default: create_company_legalities_dto_1.CreateCompanyLegalitiesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => create_company_legalities_dto_1.CreateCompanyLegalitiesDto),
    __metadata("design:type", Array)
], CreateLegalityDto.prototype, "companies", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [create_group_legalities_dto_1.CreateGroupLegalitiesDto],
        default: create_group_legalities_dto_1.CreateGroupLegalitiesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => create_group_legalities_dto_1.CreateGroupLegalitiesDto),
    __metadata("design:type", Array)
], CreateLegalityDto.prototype, "groups", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [create_legality_contacts_dto_1.CreateLegalityContactsDto],
        default: create_legality_contacts_dto_1.CreateLegalityContactsDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => create_legality_contacts_dto_1.CreateLegalityContactsDto),
    __metadata("design:type", Array)
], CreateLegalityDto.prototype, "contacts", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [create_legality_dates_dto_1.CreateLegalityDatesDto],
        default: create_legality_dates_dto_1.CreateLegalityDatesDto,
        required: false
    }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => create_legality_dates_dto_1.CreateLegalityDatesDto),
    __metadata("design:type", Array)
], CreateLegalityDto.prototype, "dates", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "creator_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.creator_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "creator_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "editor_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.editor_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "editor_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_id),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "deleted_by_id", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_by_name),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", String)
], CreateLegalityDto.prototype, "deleted_by_name", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalityDto.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalityDto.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], CreateLegalityDto.prototype, "deleted_at", void 0);
exports.CreateLegalityDto = CreateLegalityDto;
//# sourceMappingURL=create-legality.dto.js.map