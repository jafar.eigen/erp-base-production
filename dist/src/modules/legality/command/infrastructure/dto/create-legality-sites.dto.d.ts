import { LegalitySitesEntity } from '../../domain/entities/legality-sites.entity';
export declare class CreateLegalitySitesDto implements LegalitySitesEntity {
    id: string;
    legality_id: string;
    site_id: string;
    site_code: string;
    site_name: string;
    group_id: string;
    gid: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
