"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateIssuerLegalitiesDto = void 0;
const issuer_legalities_entity_1 = require("../../domain/entities/issuer-legalities.entity");
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class UpdateIssuerLegalitiesDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.id),
    __metadata("design:type", String)
], UpdateIssuerLegalitiesDto.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        required: false,
        nullable: true,
        default: 'replace with uuid'
    }),
    (0, class_validator_1.ValidateIf)((body) => body.legality_id),
    __metadata("design:type", String)
], UpdateIssuerLegalitiesDto.prototype, "legality_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.ValidateIf)((body) => body.issuer_id),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], UpdateIssuerLegalitiesDto.prototype, "issuer_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(1),
    (0, class_validator_1.MaxLength)(300),
    __metadata("design:type", String)
], UpdateIssuerLegalitiesDto.prototype, "issuer_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'string',
        enumName: 'Group Legalities Status',
        enum: Object.values(issuer_legalities_entity_1.IssuerLegalitiesStatuses)
    }),
    (0, class_validator_1.IsIn)(Object.values(issuer_legalities_entity_1.IssuerLegalitiesStatuses)),
    __metadata("design:type", String)
], UpdateIssuerLegalitiesDto.prototype, "issuer_status", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.created_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateIssuerLegalitiesDto.prototype, "created_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.updated_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateIssuerLegalitiesDto.prototype, "updated_at", void 0);
__decorate([
    (0, class_validator_1.ValidateIf)((body) => body.deleted_at),
    (0, class_validator_1.IsEmpty)(),
    __metadata("design:type", Date)
], UpdateIssuerLegalitiesDto.prototype, "deleted_at", void 0);
exports.UpdateIssuerLegalitiesDto = UpdateIssuerLegalitiesDto;
//# sourceMappingURL=update-issuer-legalities.dto.js.map