import { CompanyLegalitiesEntity } from '../../domain/entities/company-legalities.entity';
export declare class UpdateCompanyLegalitiesDto implements CompanyLegalitiesEntity {
    id: string;
    legality_id: string;
    company_id: string;
    company_name: string;
    company_code: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
