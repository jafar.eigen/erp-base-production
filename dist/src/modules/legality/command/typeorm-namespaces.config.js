"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const config_2 = require("src/utils/config");
const company_contact_model_1 = require("src/modules/company/command/data/models/company-contact.model");
const company_model_1 = require("src/modules/company/command/data/models/company.model");
const group_companies_model_1 = require("src/modules/company/command/data/models/group-companies.model");
const group_site_company_model_1 = require("src/modules/site/command/data/models/group-site-company.model");
const group_site_contact_model_1 = require("src/modules/site/command/data/models/group-site-contact.model");
const group_site_level_model_1 = require("src/modules/site/command/data/models/group-site-level.model");
const group_sites_model_1 = require("src/modules/site/command/data/models/group-sites.model");
const site_company_model_1 = require("src/modules/site/command/data/models/site-company.model");
const site_contact_model_1 = require("src/modules/site/command/data/models/site-contact.model");
const site_level_model_1 = require("src/modules/site/command/data/models/site-level.model");
const site_model_1 = require("src/modules/site/command/data/models/site.model");
const company_legalities_model_1 = require("./data/models/company-legalities.model");
const group_legalities_model_1 = require("./data/models/group-legalities.model");
const issuer_legalities_model_1 = require("./data/models/issuer-legalities.model");
const legality_contacts_model_1 = require("./data/models/legality-contacts.model");
const legality_dates_model_1 = require("./data/models/legality-dates.model");
const legality_sites_model_1 = require("./data/models/legality-sites.model");
const legality_model_1 = require("./data/models/legality.model");
exports.default = (0, config_1.registerAs)('typeormLegalityCommand', () => ({
    type: 'mysql',
    host: config_2.variableConfig.LEGALITY_MYSQL_COMMAND_HOST,
    port: parseInt(process.env.COMPANY_MYSQL_COMMAND_PORT),
    username: process.env.LEGALITY_MYSQL_COMMAND_USERNAME,
    password: process.env.LEGALITY_MYSQL_COMMAND_PASSWORD,
    database: process.env.LEGALITY_MYSQL_COMMAND_DATABASE,
    entities: [
        company_model_1.Company,
        company_contact_model_1.Contact,
        group_companies_model_1.GroupCompanies,
        group_sites_model_1.GroupSite,
        group_site_level_model_1.GroupSiteLevel,
        group_site_contact_model_1.GroupSiteContact,
        group_site_company_model_1.GroupSiteCompany,
        site_model_1.Site,
        site_level_model_1.SiteLevel,
        site_contact_model_1.SiteContact,
        site_company_model_1.SiteCompany,
        legality_model_1.Legality,
        legality_contacts_model_1.LegalityContact,
        legality_dates_model_1.LegalityDate,
        legality_sites_model_1.LegalitySite,
        company_legalities_model_1.CompanyLegality,
        group_legalities_model_1.GroupLegality,
        issuer_legalities_model_1.IssuerLegality
    ],
    synchronize: true
}));
//# sourceMappingURL=typeorm-namespaces.config.js.map