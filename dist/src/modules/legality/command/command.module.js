"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const microservice_config_1 = require("../../../utils/microservice.config");
const typeorm_namespaces_config_1 = require("./typeorm-namespaces.config");
const database_config_1 = require("../../../utils/database.config");
const company_model_1 = require("../../company/command/data/models/company.model");
const company_contact_model_1 = require("../../company/command/data/models/company-contact.model");
const group_companies_model_1 = require("../../company/command/data/models/group-companies.model");
const group_sites_model_1 = require("../../site/command/data/models/group-sites.model");
const group_site_level_model_1 = require("../../site/command/data/models/group-site-level.model");
const group_site_contact_model_1 = require("../../site/command/data/models/group-site-contact.model");
const group_site_company_model_1 = require("../../site/command/data/models/group-site-company.model");
const site_level_model_1 = require("../../site/command/data/models/site-level.model");
const site_contact_model_1 = require("../../site/command/data/models/site-contact.model");
const site_company_model_1 = require("../../site/command/data/models/site-company.model");
const legality_model_1 = require("./data/models/legality.model");
const legality_contacts_model_1 = require("./data/models/legality-contacts.model");
const legality_dates_model_1 = require("./data/models/legality-dates.model");
const src_1 = require("src");
const src_2 = require("src");
const legality_producer_controller_1 = require("./infrastructure/producer/legality-producer.controller");
const company_legalities_model_1 = require("./data/models/company-legalities.model");
const group_legalities_model_1 = require("./data/models/group-legalities.model");
const issuer_legalities_model_1 = require("./data/models/issuer-legalities.model");
const legality_sites_model_1 = require("./data/models/legality-sites.model");
const legality_orchestrator_1 = require("./domain/usecases/legality.orchestrator");
const legality_data_service_1 = require("./data/services/legality-data.service");
const legality_producer_service_1 = require("./infrastructure/producer/legality-producer.service");
const legality_consumer_orchestrator_1 = require("./domain/usecases/legality-consumer.orchestrator");
const company_changed_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-changed.controller");
const company_group_changed_controller_1 = require("./infrastructure/consumer/company/infrastructure/company-group-changed.controller");
const company_changed_manager_1 = require("./infrastructure/consumer/company/domain/company-changed.manager");
const company_group_changed_manager_ts_1 = require("./infrastructure/consumer/company/domain/company-group-changed.manager.ts");
const site_changed_manager_1 = require("./infrastructure/consumer/site/domain/site-changed.manager");
const site_changed_controller_1 = require("./infrastructure/consumer/site/infrastructure/site-changed.controller");
const microservices_1 = require("@nestjs/microservices");
let CommandModule = class CommandModule {
};
CommandModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                load: [typeorm_namespaces_config_1.default, microservice_config_1.default],
                isGlobal: true
            }),
            microservices_1.ClientsModule.registerAsync([
                {
                    name: microservice_config_1.KAFKA_CLIENT_NAME,
                    imports: [config_1.ConfigModule],
                    useFactory: (configService) => configService.get('kafkaClientConfig'),
                    inject: [config_1.ConfigService]
                }
            ]),
            typeorm_1.TypeOrmModule.forRootAsync({
                name: database_config_1.LEGALITY_CUD_CONNECTION,
                imports: [config_1.ConfigModule],
                useFactory: (configService) => configService.get('typeormLegalityCommand'),
                inject: [config_1.ConfigService]
            }),
            typeorm_1.TypeOrmModule.forFeature([
                company_model_1.Company,
                company_contact_model_1.Contact,
                group_companies_model_1.GroupCompanies,
                group_sites_model_1.GroupSite,
                group_site_level_model_1.GroupSiteLevel,
                group_site_contact_model_1.GroupSiteContact,
                group_site_company_model_1.GroupSiteCompany,
                site_level_model_1.SiteLevel,
                site_contact_model_1.SiteContact,
                site_company_model_1.SiteCompany,
                legality_model_1.Legality,
                legality_contacts_model_1.LegalityContact,
                legality_dates_model_1.LegalityDate,
                legality_sites_model_1.LegalitySite,
                company_legalities_model_1.CompanyLegality,
                group_legalities_model_1.GroupLegality,
                issuer_legalities_model_1.IssuerLegality
            ], database_config_1.LEGALITY_CUD_CONNECTION)
        ],
        providers: [
            src_1.Responses,
            src_2.JSONParse,
            legality_orchestrator_1.LegalityOrchestrator,
            legality_consumer_orchestrator_1.LegalityConsumerOrchestrator,
            legality_data_service_1.LegalityDataServiceImpl,
            legality_producer_controller_1.LegalityProducerControllerImpl,
            legality_producer_service_1.LegalityProducerServiceImpl,
            company_changed_manager_1.CompanyChangedManager,
            company_group_changed_manager_ts_1.CompanyGroupChangedManager,
            site_changed_manager_1.SiteChangedManager
        ],
        controllers: [
            legality_producer_controller_1.LegalityProducerControllerImpl,
            company_changed_controller_1.CompanyChangedConsumerController,
            company_group_changed_controller_1.CompanyGroupChangedConsumerController,
            site_changed_controller_1.SiteChangedConsumerControllerImpl
        ]
    })
], CommandModule);
exports.CommandModule = CommandModule;
//# sourceMappingURL=command.module.js.map