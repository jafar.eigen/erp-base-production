import { DeactiveManager, BaseResultBatch } from 'src';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
import { LegalityEntity } from '../../entities/legality.entity';
export declare class DeactivateLegalityManager extends DeactiveManager<LegalityEntity> {
    private legalityIds;
    private legalityDataService;
    private legalityProducerService;
    result: BaseResultBatch;
    constructor(legalityIds: string[], legalityDataService: LegalityDataServiceImpl, legalityProducerService: LegalityProducerServiceImpl);
    onSuccess(legalities: LegalityEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
}
