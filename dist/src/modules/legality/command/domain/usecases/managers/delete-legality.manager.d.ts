import { DeleteManager, BaseResultBatch } from 'src';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
import { LegalityEntity } from '../../entities/legality.entity';
export declare class DeleteLegalityManager extends DeleteManager<LegalityEntity> {
    private legalityIds;
    protected legalityDataService: LegalityDataServiceImpl;
    protected legalityProducerService: LegalityProducerServiceImpl;
    result: BaseResultBatch;
    constructor(legalityIds: string[], legalityDataService: LegalityDataServiceImpl, legalityProducerService: LegalityProducerServiceImpl);
    onSuccess(legalities: LegalityEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): BaseResultBatch;
    protected isApprovalRequestDone(legality: LegalityEntity): boolean;
    protected isDeletedData(legality: LegalityEntity): boolean;
    protected removeDocument(legality: LegalityEntity): Promise<void>;
}
