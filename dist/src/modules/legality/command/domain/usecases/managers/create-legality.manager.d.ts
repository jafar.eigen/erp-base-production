import { CreateManager } from 'src';
import { LegalityEntity, LegalityStatus } from '../../entities/legality.entity';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
export declare class CreateLegalityManager extends CreateManager<LegalityEntity> {
    protected legalityDataService: LegalityDataServiceImpl;
    protected legalityProducerService: LegalityProducerServiceImpl;
    protected legality: LegalityEntity;
    protected legalityStatus: typeof LegalityStatus;
    constructor(legalityDataService: LegalityDataServiceImpl, legalityProducerService: LegalityProducerServiceImpl, legality: LegalityEntity);
    prepareData(): Promise<LegalityEntity>;
    beforeProcess(): Promise<void>;
    processData(): Promise<LegalityEntity>;
    afterProcess(entity: LegalityEntity): Promise<void>;
    protected validation(): Promise<void | Error>;
    params: any;
    protected generateData(legality: LegalityEntity): Promise<LegalityEntity>;
    protected produceTopic(legality: LegalityEntity): Promise<void>;
}
