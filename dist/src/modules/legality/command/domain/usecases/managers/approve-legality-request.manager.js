"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApproveLegalityRequestManager = void 0;
const fs = require("fs");
const src_1 = require("src");
const CopyDocumentFile_1 = require("src/helpers/CopyDocumentFile");
class ApproveLegalityRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.APPROVED;
    }
    async afterProcess() {
        if (this.isApprovalDone() && this.isDraftToActive()) {
            await this.moveDocument();
        }
        if (this.isApprovalDone() && this.isEditData()) {
            await this.moveDocument();
            await this.removeDocument();
            await this.dataService.findAndDeleteNulledRelation();
        }
        return this.entity;
    }
    isDraftToActive() {
        return (this.entity.request_info === src_1.RequestInfo.CREATE_DATA &&
            this.entity.status === src_1.STATUS.ACTIVE);
    }
    isApprovalDone() {
        return !this.entity.has_requested_process || !this.entity.approval;
    }
    isEditData() {
        return this.entity.request_info === src_1.RequestInfo.EDIT_DATA;
    }
    async moveDocument() {
        const dates = await new CopyDocumentFile_1.CopyDocumentFile(this.entity.id, this.entity.dates).run();
        await this.dataService.updateDate(dates);
    }
    async removeDocument() {
        const dates = await this.dataService.findNulledRelationOnLegalityDate();
        await Promise.all(dates.map(async (date) => {
            fs.unlinkSync(`.uploads/${date.document_path}`);
        }));
    }
}
exports.ApproveLegalityRequestManager = ApproveLegalityRequestManager;
//# sourceMappingURL=approve-legality-request.manager.js.map