"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityCompanyChangedManager = void 0;
class LegalityCompanyChangedManager {
    constructor(message, legalityDataService) {
        this.message = message;
        this.legalityDataService = legalityDataService;
    }
    async run() {
        await this.setData();
        await this.execute();
    }
    async setData() {
        var _a;
        this.company = (_a = this.message.data) !== null && _a !== void 0 ? _a : this.message.old;
        const companyLegalities = await this.legalityDataService.findCompanyLegalities(this.company.id);
        Object.assign(this, { companyLegalities });
    }
    async execute() {
        const companies = await Promise.all(this.companyLegalities.map(async (company) => {
            const consume = this.getValidFrom(this.company);
            return Object.assign(company, { consume });
        }));
        await this.legalityDataService.saveManyCompanies(companies);
    }
    async getValidFrom(company) {
        return {
            company_name: company.name,
            company_code: company.code
        };
    }
}
exports.LegalityCompanyChangedManager = LegalityCompanyChangedManager;
//# sourceMappingURL=legality-company-changed.manager.js.map