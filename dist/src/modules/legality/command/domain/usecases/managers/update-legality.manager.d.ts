import { UpdateManager } from 'src';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityEntity } from '../../entities/legality.entity';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
export declare class UpdateLegalityManager extends UpdateManager<LegalityEntity> {
    protected legalityDataService: LegalityDataServiceImpl;
    protected legalityProducerService: LegalityProducerServiceImpl;
    protected legalityId: string;
    protected legality: LegalityEntity;
    entityId: string;
    constructor(legalityDataService: LegalityDataServiceImpl, legalityProducerService: LegalityProducerServiceImpl, legalityId: string, legality: LegalityEntity);
    beforeProcess(): Promise<void>;
    prepareData(): Promise<LegalityEntity>;
    afterProcess(entity: LegalityEntity): Promise<void>;
    protected validation(): Promise<void | Error>;
}
