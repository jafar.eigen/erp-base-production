import { LegalityDataServiceImpl } from '../../../../data/services/legality-data.service';
import { CompanyLegalitiesEntity } from '../../../entities/company-legalities.entity';
export declare class LegalityCompanyChangedManager {
    protected readonly message: any;
    protected legalityDataService: LegalityDataServiceImpl;
    protected company: any;
    protected companyLegalities: CompanyLegalitiesEntity[];
    constructor(message: any, legalityDataService: LegalityDataServiceImpl);
    run(): Promise<void>;
    setData(): Promise<void>;
    protected execute(): Promise<void>;
    protected getValidFrom(company: any): Promise<Partial<CompanyLegalitiesEntity>>;
}
