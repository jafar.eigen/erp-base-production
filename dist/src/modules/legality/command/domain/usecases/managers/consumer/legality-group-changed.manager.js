"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityGroupChangedManager = void 0;
class LegalityGroupChangedManager {
    constructor(message, legalityDataService) {
        this.message = message;
        this.legalityDataService = legalityDataService;
    }
    async run() {
        await this.setData();
        await this.execute();
    }
    async setData() {
        var _a;
        this.group = (_a = this.message.data) !== null && _a !== void 0 ? _a : this.message.old;
        const groupLegalities = await this.legalityDataService.findGroupLegalities(this.group.id);
        Object.assign(this, { groupLegalities });
    }
    async execute() {
        const groups = await Promise.all(this.groupLegalities.map(async (group) => {
            const consume = this.getValidFrom(this.group);
            return Object.assign(group, { consume });
        }));
        await this.legalityDataService.saveManyGroups(groups);
    }
    async getValidFrom(group) {
        return {
            group_name: group.name,
            group_status: group.status
        };
    }
}
exports.LegalityGroupChangedManager = LegalityGroupChangedManager;
//# sourceMappingURL=legality-group-changed.manager.js.map