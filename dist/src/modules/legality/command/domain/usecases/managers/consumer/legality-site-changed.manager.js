"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalitySiteChangedManager = void 0;
class LegalitySiteChangedManager {
    constructor(message, legalityDataService) {
        this.message = message;
        this.legalityDataService = legalityDataService;
    }
    async run() {
        await this.setData();
        await this.execute();
    }
    async setData() {
        var _a;
        this.site = (_a = this.message.data) !== null && _a !== void 0 ? _a : this.message.old;
        const siteLegalities = await this.legalityDataService.findSiteLegalities(this.site.id);
        Object.assign(this, { siteLegalities });
    }
    async execute() {
        const sites = await Promise.all(this.siteLegalities.map(async (site) => {
            const consume = this.getValidFrom(this.site);
            return Object.assign(site, { consume });
        }));
        await this.legalityDataService.saveManySites(sites);
    }
    async getValidFrom(site) {
        return {
            site_name: site.name,
            site_code: site.code,
            group_id: site.group_id,
            gid: site.groups.gid
        };
    }
}
exports.LegalitySiteChangedManager = LegalitySiteChangedManager;
//# sourceMappingURL=legality-site-changed.manager.js.map