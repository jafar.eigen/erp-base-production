import { SubmitManager } from 'src';
import { LegalityEntity } from '../../entities/legality.entity';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
export declare class SubmitLegalityRequestManager extends SubmitManager<LegalityEntity> {
    readonly id: string;
    readonly dataService: LegalityDataServiceImpl;
    readonly kafkaService: LegalityProducerServiceImpl;
    result: LegalityEntity;
    constructor(id: string, dataService: LegalityDataServiceImpl, kafkaService: LegalityProducerServiceImpl);
    onSuccess(legality: LegalityEntity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
    getResult(): LegalityEntity;
    protected isApprovalRequestDone(legality: LegalityEntity): boolean;
    protected isEditData(legality: LegalityEntity): boolean;
    protected isDraftToActive(legality: LegalityEntity): boolean;
    protected moveDocument(legality: LegalityEntity): Promise<void>;
    protected removeDocument(): Promise<void>;
}
