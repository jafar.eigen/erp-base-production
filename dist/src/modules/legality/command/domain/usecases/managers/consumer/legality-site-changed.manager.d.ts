import { KafkaPayload, IUserPayload } from 'src';
import { LegalityDataServiceImpl } from '../../../../data/services/legality-data.service';
import { LegalitySitesEntity } from '../../../entities/legality-sites.entity';
export declare class LegalitySiteChangedManager {
    protected readonly message: KafkaPayload<IUserPayload, any, any>;
    protected legalityDataService: LegalityDataServiceImpl;
    protected site: any;
    protected siteLegalities: LegalitySitesEntity[];
    constructor(message: KafkaPayload<IUserPayload, any, any>, legalityDataService: LegalityDataServiceImpl);
    run(): Promise<void>;
    setData(): Promise<void>;
    protected execute(): Promise<void>;
    protected getValidFrom(site: any): Promise<Partial<LegalitySitesEntity>>;
}
