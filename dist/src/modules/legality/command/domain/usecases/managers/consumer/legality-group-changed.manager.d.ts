import { KafkaPayload, IUserPayload } from 'src';
import { LegalityDataServiceImpl } from '../../../../data/services/legality-data.service';
import { GroupLegalitiesEntity } from '../../../entities/group-legalities.entity';
export declare class LegalityGroupChangedManager {
    protected readonly message: KafkaPayload<IUserPayload, any, any>;
    protected legalityDataService: LegalityDataServiceImpl;
    protected group: any;
    protected groupLegalities: GroupLegalitiesEntity[];
    constructor(message: KafkaPayload<IUserPayload, any, any>, legalityDataService: LegalityDataServiceImpl);
    run(): Promise<void>;
    setData(): Promise<void>;
    protected execute(): Promise<void>;
    protected getValidFrom(group: any): Promise<Partial<GroupLegalitiesEntity>>;
}
