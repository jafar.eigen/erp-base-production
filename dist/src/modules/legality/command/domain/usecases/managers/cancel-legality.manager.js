"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelLegalityManager = void 0;
const src_1 = require("src");
class CancelLegalityManager extends src_1.CancelManager {
    constructor(legalityId, dataService, kafkaService) {
        super(legalityId, dataService, kafkaService);
        this.legalityId = legalityId;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
}
exports.CancelLegalityManager = CancelLegalityManager;
//# sourceMappingURL=cancel-legality.manager.js.map