import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityEntity } from '../../entities/legality.entity';
export declare class CreateValidatorManager {
    private legalityDataService;
    private legality;
    private companyId;
    private legalityId;
    constructor(legalityDataService: LegalityDataServiceImpl, legality: LegalityEntity, companyId: string, legalityId?: string);
    execute(): Promise<void | Error>;
}
