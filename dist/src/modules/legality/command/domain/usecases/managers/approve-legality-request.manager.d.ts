import { BaseActionManager } from 'src';
import { LegalityEntity } from '../../entities/legality.entity';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
export declare class ApproveLegalityRequestManager extends BaseActionManager<LegalityEntity> {
    readonly dataService: LegalityDataServiceImpl;
    readonly kafkaService: LegalityProducerServiceImpl;
    readonly id: string;
    readonly step: number;
    responseType: string;
    constructor(dataService: LegalityDataServiceImpl, kafkaService: LegalityProducerServiceImpl, id: string, step: number);
    afterProcess(): Promise<LegalityEntity>;
    protected isDraftToActive(): boolean;
    protected isApprovalDone(): boolean;
    protected isEditData(): boolean;
    protected moveDocument(): Promise<void>;
    protected removeDocument(): Promise<void>;
}
