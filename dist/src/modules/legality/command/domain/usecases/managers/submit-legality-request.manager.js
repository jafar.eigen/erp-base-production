"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitLegalityRequestManager = void 0;
const fs = require("fs");
const src_1 = require("src");
const CopyDocumentFile_1 = require("src/helpers/CopyDocumentFile");
class SubmitLegalityRequestManager extends src_1.SubmitManager {
    constructor(id, dataService, kafkaService) {
        super([id], dataService, kafkaService);
        this.id = id;
        this.dataService = dataService;
        this.kafkaService = kafkaService;
    }
    async onSuccess(legality) {
        if (this.isApprovalRequestDone(legality[0]) &&
            this.isDraftToActive(legality[0])) {
            await this.moveDocument(legality[0]);
        }
        this.result = legality[0];
    }
    async onFailed(messages) {
        if (messages.length > 0)
            throw new Error(messages[0]);
    }
    getResult() {
        return this.result;
    }
    isApprovalRequestDone(legality) {
        return legality.approval === null || !legality.has_requested_process;
    }
    isEditData(legality) {
        return legality.request_info === src_1.RequestInfo.EDIT_DATA;
    }
    isDraftToActive(legality) {
        return (legality.request_info === src_1.RequestInfo.CREATE_DATA &&
            legality.status === src_1.STATUS.ACTIVE);
    }
    async moveDocument(legality) {
        const dates = await new CopyDocumentFile_1.CopyDocumentFile(legality.id, legality.dates).run();
        await this.dataService.updateDate(dates);
    }
    async removeDocument() {
        const dates = await this.dataService.findNulledRelationOnLegalityDate();
        await Promise.all(dates.map(async (date) => {
            fs.unlinkSync(`.uploads/${date.document_path}`);
        }));
    }
}
exports.SubmitLegalityRequestManager = SubmitLegalityRequestManager;
//# sourceMappingURL=submit-legality-request.manager.js.map