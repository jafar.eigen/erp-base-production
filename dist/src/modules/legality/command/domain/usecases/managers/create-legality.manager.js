"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLegalityManager = void 0;
const _ = require("lodash");
const src_1 = require("src");
const legality_entity_1 = require("../../entities/legality.entity");
const create_validator_manager_1 = require("./create-validator.manager");
class CreateLegalityManager extends src_1.CreateManager {
    constructor(legalityDataService, legalityProducerService, legality) {
        super(legalityDataService, legalityProducerService);
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
        this.legality = legality;
        this.legalityStatus = legality_entity_1.LegalityStatus;
    }
    async prepareData() {
        return this.legality;
    }
    async beforeProcess() {
        await this.validation();
    }
    async processData() {
        var _a, _b;
        const approval = this._findApprovals();
        Object.assign(this.entity, {
            issuer_id: this.entity.issuers.id,
            site_id: this.entity.sites.id,
            amount_sites: 0,
            group_name: this.user.company.name,
            creator_id: (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id,
            creator_name: this.user.username,
            editor_id: (_b = this.user.uuid) !== null && _b !== void 0 ? _b : this.user.id,
            editor_name: this.user.username,
            has_requested_process: false,
            status: src_1.STATUS.DRAFT,
            request_info: legality_entity_1.RequestInfo.CREATE_DATA,
            approval: approval
        });
        const legality = _.cloneDeep(this.entity);
        return await this.generateData(legality);
    }
    async afterProcess(entity) {
        return;
    }
    async validation() {
        await Promise.all(this.legality.companies.map(async (company) => {
            await new create_validator_manager_1.CreateValidatorManager(this.legalityDataService, this.legality, company.company_id).execute();
        }));
    }
    async generateData(legality) {
        const data = await Promise.all(this.legality.companies.map(async (company) => {
            const form = _.cloneDeep(legality);
            Object.assign(form, {
                companies: [company],
                groups: []
            });
            const saved = await this.legalityDataService.save(form);
            await this.produceTopic(saved);
            return saved;
        }));
        await Promise.all(this.legality.groups.map(async (group) => {
            const form = _.cloneDeep(legality);
            Object.assign(form, {
                companies: [],
                groups: [group]
            });
            const saved = await this.legalityDataService.save(form);
            await this.produceTopic(saved);
        }));
        return data[0];
    }
    async produceTopic(legality) {
        await this.legalityProducerService.baseCreated({
            user: this.user,
            data: legality,
            old: null
        });
    }
}
exports.CreateLegalityManager = CreateLegalityManager;
//# sourceMappingURL=create-legality.manager.js.map