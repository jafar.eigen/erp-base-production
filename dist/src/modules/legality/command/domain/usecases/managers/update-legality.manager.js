"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLegalityManager = void 0;
const src_1 = require("src");
const create_validator_manager_1 = require("./create-validator.manager");
class UpdateLegalityManager extends src_1.UpdateManager {
    constructor(legalityDataService, legalityProducerService, legalityId, legality) {
        super(legalityDataService, legalityProducerService);
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
        this.legalityId = legalityId;
        this.legality = legality;
        this.entityId = legalityId;
    }
    async beforeProcess() {
        await this.validation();
    }
    async prepareData() {
        Object.assign(this.legality, {
            issuer_id: this.legality.issuers.id,
            site_id: this.legality.sites.id
        });
        return this.legality;
    }
    async afterProcess(entity) {
        return;
    }
    async validation() {
        await Promise.all(this.legality.companies.map(async (company) => {
            await new create_validator_manager_1.CreateValidatorManager(this.legalityDataService, this.legality, company.company_id, this.legalityId).execute();
        }));
    }
}
exports.UpdateLegalityManager = UpdateLegalityManager;
//# sourceMappingURL=update-legality.manager.js.map