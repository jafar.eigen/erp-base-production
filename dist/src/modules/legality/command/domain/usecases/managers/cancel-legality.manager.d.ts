import { CancelManager } from 'src';
import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityEntity } from '../../entities/legality.entity';
import { LegalityProducerServiceImpl } from '../../../infrastructure/producer/legality-producer.service';
export declare class CancelLegalityManager extends CancelManager<LegalityEntity> {
    readonly legalityId: string;
    readonly dataService: LegalityDataServiceImpl;
    readonly kafkaService: LegalityProducerServiceImpl;
    constructor(legalityId: string, dataService: LegalityDataServiceImpl, kafkaService: LegalityProducerServiceImpl);
}
