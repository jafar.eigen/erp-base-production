"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeactivateLegalityManager = void 0;
const src_1 = require("src");
class DeactivateLegalityManager extends src_1.DeactiveManager {
    constructor(legalityIds, legalityDataService, legalityProducerService) {
        super(legalityIds, legalityDataService, legalityProducerService);
        this.legalityIds = legalityIds;
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(legalities) {
        legalities.forEach((legality) => {
            this.result.success.push(`legality with id ${legality.code} is not active.`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.DeactivateLegalityManager = DeactivateLegalityManager;
//# sourceMappingURL=deactivate-legality.manager.js.map