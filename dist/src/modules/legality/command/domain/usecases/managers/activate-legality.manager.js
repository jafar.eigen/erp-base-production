"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivateLegalityManager = void 0;
const src_1 = require("src");
class ActivateLegalityManager extends src_1.ActiveManager {
    constructor(legalityIds, legalityDataService, legalityProducerService) {
        super(legalityIds, legalityDataService, legalityProducerService);
        this.legalityIds = legalityIds;
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(legalities) {
        legalities.forEach((legality) => {
            this.result.success.push(`legality with id ${legality.code} successfully activated`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
}
exports.ActivateLegalityManager = ActivateLegalityManager;
//# sourceMappingURL=activate-legality.manager.js.map