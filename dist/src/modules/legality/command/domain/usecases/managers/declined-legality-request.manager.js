"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclinedLegalityRequestManager = void 0;
const src_1 = require("src");
class DeclinedLegalityRequestManager extends src_1.BaseActionManager {
    constructor(dataService, kafkaService, id, step) {
        super(dataService, kafkaService, id, step);
        this.dataService = dataService;
        this.kafkaService = kafkaService;
        this.id = id;
        this.step = step;
        this.responseType = src_1.ResponseType.DECLINED;
    }
}
exports.DeclinedLegalityRequestManager = DeclinedLegalityRequestManager;
//# sourceMappingURL=declined-legality-request.manager.js.map