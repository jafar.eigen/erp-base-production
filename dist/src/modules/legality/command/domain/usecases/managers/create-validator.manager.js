"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateValidatorManager = void 0;
const name_validator_1 = require("../validators/name.validator");
const code_validator_1 = require("../validators/code.validator");
class CreateValidatorManager {
    constructor(legalityDataService, legality, companyId, legalityId = null) {
        this.legalityDataService = legalityDataService;
        this.legality = legality;
        this.companyId = companyId;
        this.legalityId = legalityId;
    }
    async execute() {
        await new name_validator_1.Name(this.legalityDataService, this.legality, this.companyId, this.legalityId).execute();
        await new code_validator_1.Code(this.legalityDataService, this.legality, this.companyId, this.legalityId).execute();
    }
}
exports.CreateValidatorManager = CreateValidatorManager;
//# sourceMappingURL=create-validator.manager.js.map