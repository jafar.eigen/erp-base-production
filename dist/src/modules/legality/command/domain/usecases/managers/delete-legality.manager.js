"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteLegalityManager = void 0;
const fs = require("fs");
const src_1 = require("src");
class DeleteLegalityManager extends src_1.DeleteManager {
    constructor(legalityIds, legalityDataService, legalityProducerService) {
        super(legalityIds, legalityDataService, legalityProducerService);
        this.legalityIds = legalityIds;
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
        this.result = {
            success: [],
            failed: []
        };
    }
    async onSuccess(legalities) {
        legalities.forEach(async (legality) => {
            if (this.isApprovalRequestDone(legality) &&
                this.isDeletedData(legality)) {
                await this.removeDocument(legality);
            }
            this.result.success.push(`legality with id ${legality.code} successfully deleted`);
        });
    }
    async onFailed(messages) {
        this.result.failed = messages;
    }
    getResult() {
        return this.result;
    }
    isApprovalRequestDone(legality) {
        return legality.approval === null || !legality.has_requested_process;
    }
    isDeletedData(legality) {
        return legality.request_info === src_1.RequestInfo.DELETE_DATA;
    }
    async removeDocument(legality) {
        await Promise.all(legality.dates.map(async (date) => {
            fs.unlinkSync(`./uploads/${date.document_path}`);
        }));
    }
}
exports.DeleteLegalityManager = DeleteLegalityManager;
//# sourceMappingURL=delete-legality.manager.js.map