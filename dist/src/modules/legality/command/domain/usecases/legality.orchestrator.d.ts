/// <reference types="multer" />
import { LegalityEntity } from '../entities/legality.entity';
import { LegalityDataServiceImpl } from '../../data/services/legality-data.service';
import { LegalityDatesFileEntity } from '../entities/legality-dates-file.entity';
import { LegalityProducerServiceImpl } from '../../infrastructure/producer/legality-producer.service';
import { BaseOrchestrator } from 'src';
import { LegalityResultBatchEntity } from '../entities/legality-result-batch.entity';
export declare class LegalityOrchestrator extends BaseOrchestrator<LegalityEntity> {
    private legalityDataService;
    private legalityProducerService;
    constructor(legalityDataService: LegalityDataServiceImpl, legalityProducerService: LegalityProducerServiceImpl);
    create(legality: LegalityEntity): Promise<LegalityEntity>;
    update(legalityId: string, legality: LegalityEntity): Promise<LegalityEntity>;
    request(legalityId: string): Promise<LegalityEntity>;
    delete(legalityId: string[]): Promise<LegalityResultBatchEntity>;
    activate(legalityIds: string[]): Promise<LegalityResultBatchEntity>;
    deactivate(legalityIds: string[]): Promise<LegalityResultBatchEntity>;
    approve(legalityId: string, step: number): Promise<LegalityEntity>;
    decline(legalityId: string, step: number): Promise<LegalityEntity>;
    cancel(legalityId: string): Promise<LegalityEntity>;
    storeFile(body: LegalityDatesFileEntity, file: Express.Multer.File): Promise<LegalityDatesFileEntity>;
    rollbackStatusTrx(entityId: string[]): Promise<LegalityEntity>;
}
