"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const create_legality_manager_1 = require("./managers/create-legality.manager");
const legality_data_service_1 = require("../../data/services/legality-data.service");
const legality_producer_service_1 = require("../../infrastructure/producer/legality-producer.service");
const update_legality_manager_1 = require("./managers/update-legality.manager");
const submit_legality_request_manager_1 = require("./managers/submit-legality-request.manager");
const src_1 = require("src");
const delete_legality_manager_1 = require("./managers/delete-legality.manager");
const approve_legality_request_manager_1 = require("./managers/approve-legality-request.manager");
const declined_legality_request_manager_1 = require("./managers/declined-legality-request.manager");
const activate_legality_manager_1 = require("./managers/activate-legality.manager");
const deactivate_legality_manager_1 = require("./managers/deactivate-legality.manager");
const cancel_legality_manager_1 = require("./managers/cancel-legality.manager");
let LegalityOrchestrator = class LegalityOrchestrator extends src_1.BaseOrchestrator {
    constructor(legalityDataService, legalityProducerService) {
        super();
        this.legalityDataService = legalityDataService;
        this.legalityProducerService = legalityProducerService;
    }
    async create(legality) {
        return await new create_legality_manager_1.CreateLegalityManager(this.legalityDataService, this.legalityProducerService, legality).execute();
    }
    async update(legalityId, legality) {
        return await new update_legality_manager_1.UpdateLegalityManager(this.legalityDataService, this.legalityProducerService, legalityId, legality).execute();
    }
    async request(legalityId) {
        const submitLegality = new submit_legality_request_manager_1.SubmitLegalityRequestManager(legalityId, this.legalityDataService, this.legalityProducerService);
        await submitLegality.execute();
        return submitLegality.getResult();
    }
    async delete(legalityId) {
        const deleteLegality = new delete_legality_manager_1.DeleteLegalityManager(legalityId, this.legalityDataService, this.legalityProducerService);
        await deleteLegality.execute();
        return deleteLegality.getResult();
    }
    async activate(legalityIds) {
        const activateLegality = new activate_legality_manager_1.ActivateLegalityManager(legalityIds, this.legalityDataService, this.legalityProducerService);
        await activateLegality.execute();
        return activateLegality.getResult();
    }
    async deactivate(legalityIds) {
        const deactivateLegality = new deactivate_legality_manager_1.DeactivateLegalityManager(legalityIds, this.legalityDataService, this.legalityProducerService);
        await deactivateLegality.execute();
        return deactivateLegality.getResult();
    }
    async approve(legalityId, step) {
        return await new approve_legality_request_manager_1.ApproveLegalityRequestManager(this.legalityDataService, this.legalityProducerService, legalityId, step).execute();
    }
    async decline(legalityId, step) {
        return await new declined_legality_request_manager_1.DeclinedLegalityRequestManager(this.legalityDataService, this.legalityProducerService, legalityId, step).execute();
    }
    async cancel(legalityId) {
        return await new cancel_legality_manager_1.CancelLegalityManager(legalityId, this.legalityDataService, this.legalityProducerService).execute();
    }
    async storeFile(body, file) {
        body.document_path = (0, src_1.getFilePath)(file);
        return body;
    }
    async rollbackStatusTrx(entityId) {
        throw new Error('Method not implemented.');
    }
};
LegalityOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [legality_data_service_1.LegalityDataServiceImpl,
        legality_producer_service_1.LegalityProducerServiceImpl])
], LegalityOrchestrator);
exports.LegalityOrchestrator = LegalityOrchestrator;
//# sourceMappingURL=legality.orchestrator.js.map