import { KafkaPayload, IUserPayload } from 'src';
import { LegalityDataServiceImpl } from '../../data/services/legality-data.service';
export declare class LegalityConsumerOrchestrator {
    private legalityDataService;
    constructor(legalityDataService: LegalityDataServiceImpl);
    handleCompanyChange(message: KafkaPayload<IUserPayload, any, any>): Promise<void>;
    handleSiteChange(message: KafkaPayload<IUserPayload, any, any>): Promise<void>;
    handleGroupChange(message: KafkaPayload<IUserPayload, any, any>): Promise<void>;
}
