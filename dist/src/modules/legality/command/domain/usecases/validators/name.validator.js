"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
class Name {
    constructor(legalityDataService, updatedData, companyId, legalityId = null) {
        this.legalityDataService = legalityDataService;
        this.updatedData = updatedData;
        this.companyId = companyId;
        this.legalityId = legalityId;
    }
    async execute() {
        this.legalities = await this.legalityDataService.findByName(this.updatedData.name);
        await Promise.all(this.legalities.map(async (legality) => {
            if (legality &&
                !this.legalityId &&
                legality.companies[0].company_id === this.companyId) {
                throw new Error(`Name already exists. For company ${legality.companies[0].company_name}`);
            }
            if (legality &&
                this.legalityId !== legality.id &&
                legality.companies[0].company_id === this.companyId) {
                throw new Error(`Name already exists. For company ${legality.companies[0].company_name}`);
            }
        }));
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map