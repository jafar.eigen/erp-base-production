import { LegalityDataServiceImpl } from '../../../data/services/legality-data.service';
import { LegalityEntity } from '../../entities/legality.entity';
export declare class Code {
    private legalityDataService;
    private updatedData;
    private companyId;
    private legalityId;
    constructor(legalityDataService: LegalityDataServiceImpl, updatedData: LegalityEntity, companyId: string, legalityId?: string);
    private legalities;
    execute(): Promise<void | Error>;
}
