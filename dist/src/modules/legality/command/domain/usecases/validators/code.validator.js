"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
class Code {
    constructor(legalityDataService, updatedData, companyId, legalityId = null) {
        this.legalityDataService = legalityDataService;
        this.updatedData = updatedData;
        this.companyId = companyId;
        this.legalityId = legalityId;
    }
    async execute() {
        this.legalities = [
            await this.legalityDataService.findByCode(this.updatedData.code)
        ];
        await Promise.all(this.legalities.map(async (legality) => {
            if (legality &&
                !this.legalityId &&
                legality.companies[0].company_id === this.companyId) {
                throw new Error(`Code already exists. For company ${legality.companies[0].company_name}`);
            }
            if (legality &&
                this.legalityId !== legality.id &&
                legality.companies[0].company_id === this.companyId) {
                throw new Error(`Code already exists. For company ${legality.companies[0].company_name}`);
            }
        }));
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map