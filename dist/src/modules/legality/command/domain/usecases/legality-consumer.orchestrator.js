"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LegalityConsumerOrchestrator = void 0;
const common_1 = require("@nestjs/common");
const legality_data_service_1 = require("../../data/services/legality-data.service");
const legality_company_changed_manager_1 = require("./managers/consumer/legality-company-changed.manager");
const legality_site_changed_manager_1 = require("./managers/consumer/legality-site-changed.manager");
const legality_group_changed_manager_1 = require("./managers/consumer/legality-group-changed.manager");
let LegalityConsumerOrchestrator = class LegalityConsumerOrchestrator {
    constructor(legalityDataService) {
        this.legalityDataService = legalityDataService;
    }
    async handleCompanyChange(message) {
        return await new legality_company_changed_manager_1.LegalityCompanyChangedManager(message, this.legalityDataService).run();
    }
    async handleSiteChange(message) {
        return await new legality_site_changed_manager_1.LegalitySiteChangedManager(message, this.legalityDataService).run();
    }
    async handleGroupChange(message) {
        return await new legality_group_changed_manager_1.LegalityGroupChangedManager(message, this.legalityDataService).run();
    }
};
LegalityConsumerOrchestrator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [legality_data_service_1.LegalityDataServiceImpl])
], LegalityConsumerOrchestrator);
exports.LegalityConsumerOrchestrator = LegalityConsumerOrchestrator;
//# sourceMappingURL=legality-consumer.orchestrator.js.map