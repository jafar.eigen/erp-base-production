import { LegalityDatesEntity } from './legality-dates.entity';
import { LegalityContactsEntity } from './legality-contacts.entity';
import { ApprovalPerCompany, BaseEntity } from 'src';
import { CompanyLegalitiesEntity } from './company-legalities.entity';
import { GroupLegalitiesEntity } from './group-legalities.entity';
import { IssuerLegalitiesEntity } from './issuer-legalities.entity';
import { LegalitySitesEntity } from './legality-sites.entity';
export declare enum LegalityStatus {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined"
}
export declare enum RequestInfo {
    CREATE_DATA = "Create Data",
    EDIT_DATA = "Edit Data",
    EDIT_ACTIVE = "Edit Active",
    EDIT_INACTIVE = "Edit Inactive",
    EDIT_DATA_AND_ACTIVE = "Edit Data & Status Active",
    EDIT_DATA_AND_INACTIVE = "Edit Data & Status Inactive",
    DELETE_DATA = "Delete Data",
    ACTIVATE_DATE = "Activate Data"
}
export interface LegalityEntity extends BaseEntity {
    name: string;
    account_link?: string;
    account_name?: string;
    account_password?: string;
    issuer_id?: string;
    site_id?: string;
    approval?: ApprovalPerCompany;
    requested_data?: any;
    has_requested_process?: boolean;
    dates?: LegalityDatesEntity[];
    contacts?: LegalityContactsEntity[];
    companies?: CompanyLegalitiesEntity[];
    groups?: GroupLegalitiesEntity[];
    issuers?: IssuerLegalitiesEntity;
    sites?: LegalitySitesEntity;
    creator_name: string;
    editor_name?: string;
    deleted_by_name?: string;
}
