"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IssuerLegalitiesStatuses = void 0;
var IssuerLegalitiesStatuses;
(function (IssuerLegalitiesStatuses) {
    IssuerLegalitiesStatuses["DRAFT"] = "draft";
    IssuerLegalitiesStatuses["ACTIVE"] = "active";
    IssuerLegalitiesStatuses["INACTIVE"] = "inactive";
    IssuerLegalitiesStatuses["REQUESTED"] = "requested";
    IssuerLegalitiesStatuses["OPEN"] = "open";
    IssuerLegalitiesStatuses["DECLINE"] = "declined";
})(IssuerLegalitiesStatuses = exports.IssuerLegalitiesStatuses || (exports.IssuerLegalitiesStatuses = {}));
//# sourceMappingURL=issuer-legalities.entity.js.map