"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestInfo = exports.LegalityStatus = void 0;
var LegalityStatus;
(function (LegalityStatus) {
    LegalityStatus["DRAFT"] = "draft";
    LegalityStatus["ACTIVE"] = "active";
    LegalityStatus["INACTIVE"] = "inactive";
    LegalityStatus["REQUESTED"] = "requested";
    LegalityStatus["OPEN"] = "open";
    LegalityStatus["DECLINE"] = "declined";
})(LegalityStatus = exports.LegalityStatus || (exports.LegalityStatus = {}));
var RequestInfo;
(function (RequestInfo) {
    RequestInfo["CREATE_DATA"] = "Create Data";
    RequestInfo["EDIT_DATA"] = "Edit Data";
    RequestInfo["EDIT_ACTIVE"] = "Edit Active";
    RequestInfo["EDIT_INACTIVE"] = "Edit Inactive";
    RequestInfo["EDIT_DATA_AND_ACTIVE"] = "Edit Data & Status Active";
    RequestInfo["EDIT_DATA_AND_INACTIVE"] = "Edit Data & Status Inactive";
    RequestInfo["DELETE_DATA"] = "Delete Data";
    RequestInfo["ACTIVATE_DATE"] = "Activate Data";
})(RequestInfo = exports.RequestInfo || (exports.RequestInfo = {}));
//# sourceMappingURL=legality.entity.js.map