export interface LegalityResultBatchEntity {
    success: string[];
    failed: string[];
}
