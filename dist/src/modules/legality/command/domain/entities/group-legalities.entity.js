"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupLegalitiesStatuses = void 0;
var GroupLegalitiesStatuses;
(function (GroupLegalitiesStatuses) {
    GroupLegalitiesStatuses["DRAFT"] = "draft";
    GroupLegalitiesStatuses["ACTIVE"] = "active";
    GroupLegalitiesStatuses["INACTIVE"] = "inactive";
    GroupLegalitiesStatuses["REQUESTED"] = "requested";
    GroupLegalitiesStatuses["OPEN"] = "open";
    GroupLegalitiesStatuses["DECLINE"] = "declined";
})(GroupLegalitiesStatuses = exports.GroupLegalitiesStatuses || (exports.GroupLegalitiesStatuses = {}));
//# sourceMappingURL=group-legalities.entity.js.map