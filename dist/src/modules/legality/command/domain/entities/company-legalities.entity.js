"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyLegalitiesStatuses = void 0;
var CompanyLegalitiesStatuses;
(function (CompanyLegalitiesStatuses) {
    CompanyLegalitiesStatuses["DRAFT"] = "draft";
    CompanyLegalitiesStatuses["ACTIVE"] = "active";
    CompanyLegalitiesStatuses["INACTIVE"] = "inactive";
    CompanyLegalitiesStatuses["REQUESTED"] = "requested";
    CompanyLegalitiesStatuses["OPEN"] = "open";
    CompanyLegalitiesStatuses["DECLINE"] = "declined";
})(CompanyLegalitiesStatuses = exports.CompanyLegalitiesStatuses || (exports.CompanyLegalitiesStatuses = {}));
//# sourceMappingURL=company-legalities.entity.js.map