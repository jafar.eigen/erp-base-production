export interface LegalitySitesEntity {
    id: string;
    site_code: string;
    site_name: string;
    group_id: string;
    gid: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
