import { KafkaOptions } from '@nestjs/microservices';
export declare const KAFKA_CLIENT_NAME = "kafka_erp_company";
declare const _default: (() => KafkaOptions) & import("@nestjs/config").ConfigFactoryKeyHost;
export default _default;
