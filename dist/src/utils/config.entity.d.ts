export interface ConfigEntity {
    BRANCH_MYSQL_COMMAND_HOST: string;
    BRANCH_MYSQL_COMMAND_PORT: number;
    BRANCH_MYSQL_COMMAND_PASSWORD: string;
    BRANCH_MYSQL_COMMAND_DATABASE: string;
    BRANCH_MYSQL_COMMAND_USERNAME: string;
    BRANCH_MYSQL_QUERY_HOST: string;
    BRANCH_MYSQL_QUERY_PORT: number;
    BRANCH_MYSQL_QUERY_PASSWORD: string;
    BRANCH_MYSQL_QUERY_DATABASE: string;
    BRANCH_MYSQL_QUERY_USERNAME: string;
    COMPANY_MYSQL_COMMAND_HOST: string;
    COMPANY_MYSQL_COMMAND_PORT: number;
    COMPANY_MYSQL_COMMAND_PASSWORD: string;
    COMPANY_MYSQL_COMMAND_DATABASE: string;
    COMPANY_MYSQL_COMMAND_USERNAME: string;
    COMPANY_MYSQL_QUERY_HOST: string;
    COMPANY_MYSQL_QUERY_PORT: number;
    COMPANY_MYSQL_QUERY_PASSWORD: string;
    COMPANY_MYSQL_QUERY_DATABASE: string;
    COMPANY_MYSQL_QUERY_USERNAME: string;
    LEGALITY_MYSQL_COMMAND_HOST: string;
    LEGALITY_MYSQL_COMMAND_PORT: number;
    LEGALITY_MYSQL_COMMAND_PASSWORD: string;
    LEGALITY_MYSQL_COMMAND_DATABASE: string;
    LEGALITY_MYSQL_COMMAND_USERNAME: string;
    LEGALITY_MYSQL_QUERY_HOST: string;
    LEGALITY_MYSQL_QUERY_PORT: number;
    LEGALITY_MYSQL_QUERY_PASSWORD: string;
    LEGALITY_MYSQL_QUERY_DATABASE: string;
    LEGALITY_MYSQL_QUERY_USERNAME: string;
    SITE_MYSQL_COMMAND_HOST: string;
    SITE_MYSQL_COMMAND_PORT: number;
    SITE_MYSQL_COMMAND_PASSWORD: string;
    SITE_MYSQL_COMMAND_DATABASE: string;
    SITE_MYSQL_COMMAND_USERNAME: string;
    SITE_MYSQL_QUERY_HOST: string;
    SITE_MYSQL_QUERY_PORT: number;
    SITE_MYSQL_QUERY_PASSWORD: string;
    SITE_MYSQL_QUERY_DATABASE: string;
    SITE_MYSQL_QUERY_USERNAME: string;
    LOCATION_MYSQL_COMMAND_HOST: string;
    LOCATION_MYSQL_COMMAND_PORT: number;
    LOCATION_MYSQL_COMMAND_PASSWORD: string;
    LOCATION_MYSQL_COMMAND_DATABASE: string;
    LOCATION_MYSQL_COMMAND_USERNAME: string;
    LOCATION_MYSQL_QUERY_HOST: string;
    LOCATION_MYSQL_QUERY_PORT: number;
    LOCATION_MYSQL_QUERY_PASSWORD: string;
    LOCATION_MYSQL_QUERY_DATABASE: string;
    LOCATION_MYSQL_QUERY_USERNAME: string;
    KAFKA_HOST: [string];
    KAFKA_TOPIC_PARTITION: number;
    KAFKA_ALLOW_TOPIC_DELETION: true;
    KAFKA_TOPICS_CONSUME: [
        {
            module_name: string;
            topic_consume: [string];
        }
    ];
    APP_PORT: number;
    SENTRY_DSN: string;
    SENTRY_ENV: string;
}
