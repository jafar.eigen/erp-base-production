"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initConfigModule = exports.initConfigApp = exports.variableConfig = void 0;
const NodeVault = require("node-vault");
exports.variableConfig = {};
let rootKey = '';
let options = {};
let vault;
function cekConfiguration() {
    if (!process.env.VAULT_TOKEN)
        throw new Error('Vault Token is not defined.');
    if (!process.env.VAULT_ADDR)
        throw new Error('Vault Address is not defined.');
}
async function initConfig() {
    rootKey = process.env.VAULT_TOKEN;
    options = {
        apiVersion: 'v1',
        endpoint: process.env.VAULT_ADDR,
        token: rootKey
    };
    vault = NodeVault(options);
}
async function initConfigApp() {
    initConfig();
    if (parseInt(process.env.SRC_VAULT) === 0) {
        Object.assign(exports.variableConfig, {
            APP_PORT: process.env.APP_PORT,
            KAFKA_HOST: process.env.KAFKA_HOST.split(','),
            KAFKA_TOPIC_PARTITION: process.env.KAFKA_TOPIC_PARTITION,
            KAFKA_ALLOW_TOPIC_DELETION: process.env.KAFKA_ALLOW_TOPIC_DELETION,
            SENTRY_DSN: process.env.SENTRY_DSN,
            SENTRY_ENV: process.env.SENTRY_ENV
        });
    }
    else {
        cekConfiguration();
        const result = await vault.read('secret/data/app-config');
        Object.assign(exports.variableConfig, result.data.data);
    }
}
exports.initConfigApp = initConfigApp;
async function initConfigModule(moduleName) {
    if (parseInt(process.env.SRC_VAULT) === 0) {
        if (isNaN(parseInt(process.env[`${moduleName}_MYSQL_COMMAND_PORT`])))
            throw new Error(`${moduleName} Command Port is not a number!`);
        if (isNaN(parseInt(process.env[`${moduleName}_MYSQL_QUERY_PORT`])))
            throw new Error(`${moduleName} Query Port is not a number!`);
        const mysql = {
            commandHost: process.env[`${moduleName}_MYSQL_COMMAND_HOST`],
            commandPort: parseInt(process.env[`${moduleName}_MYSQL_COMMAND_PORT`]),
            commandDataBase: process.env[`${moduleName}_MYSQL_COMMAND_DATABASE`],
            commandUserName: process.env[`${moduleName}_MYSQL_COMMAND_USERNAME`],
            commandPassword: process.env[`${moduleName}_MYSQL_COMMAND_PASSWORD`],
            queryHost: process.env[`${moduleName}_MYSQL_QUERY_HOST`],
            queryPort: parseInt(process.env[`${moduleName}_MYSQL_QUERY_PORT`]),
            queryDataBase: process.env[`${moduleName}_MYSQL_QUERY_DATABASE`],
            queryUserName: process.env[`${moduleName}_MYSQL_QUERY_USERNAME`],
            queryPassword: process.env[`${moduleName}_MYSQL_QUERY_PASSWORD`]
        };
        Object.assign(exports.variableConfig, await JSON.parse(`{
        "${moduleName}_MYSQL_COMMAND_HOST": "${mysql.commandHost}",
        "${moduleName}_MYSQL_COMMAND_PORT": ${mysql.commandPort},
        "${moduleName}_MYSQL_COMMAND_PASSWORD": "${mysql.commandPassword}",
        "${moduleName}_MYSQL_COMMAND_DATABASE": "${mysql.commandDataBase}",
        "${moduleName}_MYSQL_COMMAND_USERNAME": "${mysql.commandUserName}",
        "${moduleName}_MYSQL_QUERY_HOST": "${mysql.queryHost}",
        "${moduleName}_MYSQL_QUERY_PORT": ${mysql.queryPort},
        "${moduleName}_MYSQL_QUERY_PASSWORD": "${mysql.queryPassword}",
        "${moduleName}_MYSQL_QUERY_DATABASE": "${mysql.queryDataBase}",
        "${moduleName}_MYSQL_QUERY_USERNAME": "${mysql.queryUserName}"
      }`));
    }
    else {
        initConfig();
        const result = await vault.read(`secret/data/${moduleName.toLowerCase()}`);
        Object.assign(exports.variableConfig, result.data.data);
    }
}
exports.initConfigModule = initConfigModule;
//# sourceMappingURL=config.js.map