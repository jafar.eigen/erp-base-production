"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LEGALITY_READ_CONNECTION = exports.LEGALITY_CUD_CONNECTION = exports.BRANCH_READ_CONNECTION = exports.BRANCH_CUD_CONNECTION = exports.LOCATION_READ_CONNECTION = exports.LOCATION_CUD_CONNECTION = exports.SITE_READ_CONNECTION = exports.SITE_CUD_CONNECTION = exports.COMPANY_READ_CONNECTION = exports.COMPANY_CUD_CONNECTION = void 0;
exports.COMPANY_CUD_CONNECTION = 'company_cud_connection';
exports.COMPANY_READ_CONNECTION = 'company_read_connection';
exports.SITE_CUD_CONNECTION = 'site_cud_connection';
exports.SITE_READ_CONNECTION = 'site_read_connection';
exports.LOCATION_CUD_CONNECTION = 'location_cud_connection';
exports.LOCATION_READ_CONNECTION = 'location_read_connection';
exports.BRANCH_CUD_CONNECTION = 'branch_cud_connection';
exports.BRANCH_READ_CONNECTION = 'branch_read_connection';
exports.LEGALITY_CUD_CONNECTION = 'legality_cud_connection';
exports.LEGALITY_READ_CONNECTION = 'legality_read_connection';
//# sourceMappingURL=database.config.js.map