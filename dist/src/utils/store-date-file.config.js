"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.storeFileDateConfig = exports.editDateFileName = void 0;
const common_1 = require("@nestjs/common");
const path_1 = require("path");
const uuid_1 = require("uuid");
const multer_1 = require("multer");
const moment = require("moment");
const year = moment().format('YYYY');
const month = moment().format('M');
const dateFileFilter = (req, file, callback) => {
    if (file.mimetype.match(/\/(jpg|jpeg|png|pdf)$/)) {
        callback(null, true);
    }
    else {
        callback(new common_1.HttpException(`Unsupported file type ${(0, path_1.extname)(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
    }
};
const editDateFileName = (req, file, callback) => {
    const fileExtName = (0, path_1.extname)(file.originalname);
    const randomName = (0, uuid_1.v4)();
    callback(null, `${randomName}${fileExtName}`);
};
exports.editDateFileName = editDateFileName;
exports.storeFileDateConfig = {
    storage: (0, multer_1.diskStorage)({
        destination: `./uploads/tmp/legality/${year}/${month}`,
        filename: exports.editDateFileName
    }),
    fileFilter: dateFileFilter
};
//# sourceMappingURL=store-date-file.config.js.map