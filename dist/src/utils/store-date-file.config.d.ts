import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
export declare const editDateFileName: (req: any, file: any, callback: any) => void;
export declare const storeFileDateConfig: MulterOptions;
