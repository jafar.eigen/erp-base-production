"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KAFKA_CLIENT_NAME = void 0;
const config_1 = require("@nestjs/config");
const microservices_1 = require("@nestjs/microservices");
const config_2 = require("./config");
exports.KAFKA_CLIENT_NAME = 'kafka_erp_company';
exports.default = (0, config_1.registerAs)('kafkaClientConfig', () => ({
    transport: microservices_1.Transport.KAFKA,
    options: {
        client: {
            clientId: 'erp_company',
            brokers: config_2.variableConfig.KAFKA_HOST,
            retry: {
                retries: 1000
            }
        },
        consumer: {
            groupId: 'erp_company_consumer'
        }
    }
}));
//# sourceMappingURL=microservice.config.js.map