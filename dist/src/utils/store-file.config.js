"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoreFileConfig = void 0;
const path_1 = require("path");
const uuid_1 = require("uuid");
const multer_1 = require("multer");
const common_1 = require("@nestjs/common");
const year = new Date().getFullYear();
const month = new Date().getMonth();
const fileFilter = (req, file, callback) => {
    if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
        callback(null, true);
    }
    else {
        callback(new common_1.HttpException(`Unsupported file type ${(0, path_1.extname)(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
    }
};
const editFileName = (req, file, callback) => {
    const fileExtName = (0, path_1.extname)(file.originalname);
    const randomName = (0, uuid_1.v4)();
    callback(null, `${randomName}${fileExtName}`);
};
exports.StoreFileConfig = {
    storage: (0, multer_1.diskStorage)({
        destination: `./uploads/images/company/${year}/${month}`,
        filename: editFileName
    }),
    fileFilter: fileFilter
};
//# sourceMappingURL=store-file.config.js.map