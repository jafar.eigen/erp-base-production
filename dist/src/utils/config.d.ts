import { ConfigEntity } from './config.entity';
export declare const variableConfig: Partial<ConfigEntity>;
export declare function initConfigApp(): Promise<void>;
export declare function initConfigModule(moduleName: string): Promise<void>;
