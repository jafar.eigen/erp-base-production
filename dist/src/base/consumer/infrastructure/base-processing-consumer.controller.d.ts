import { Logger } from 'winston';
import { SentryService } from '@ntegral/nestjs-sentry';
import { ConsumerOption, RetryTopicEntity } from '../../domain/entities/kafka/kafka-topic-consumer.interface';
import { BaseProducerServiceImpl } from '../../infrastructure/base-producer.service';
export declare abstract class BaseProcessingConsumerController {
    readonly logger: Logger;
    readonly clientSentry: SentryService;
    private baseProducerService;
    multipleTopic: RetryTopicEntity[];
    protected consumer: any;
    protected data: any;
    protected consumerOptions: ConsumerOption[];
    abstract moduleName: string;
    constructor(logger: Logger, clientSentry: SentryService, baseProducerService: BaseProducerServiceImpl<any>);
    onModuleInit(): Promise<void>;
    consumerSubscribeTopic(): Promise<void>;
    subscibeTopic(topic: string): Promise<void>;
    registerMultipleTopics(moduleName: string, kafkaTopic: string): Promise<void>;
    getConsumerOptions(): Promise<ConsumerOption[]>;
    getConsumerOptionsDefault(): ConsumerOption[];
    consumerListenTopic(): Promise<void>;
}
