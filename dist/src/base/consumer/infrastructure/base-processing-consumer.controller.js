"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseProcessingConsumerController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const kafkajs_1 = require("kafkajs");
const generate_topic_retry_1 = require("../../utils/generate-topic-retry");
const config_1 = require("../../../utils/config");
const consumer_helper_1 = require("../utils/consumer.helper");
const base_producer_service_1 = require("../../infrastructure/base-producer.service");
const base_processing_retry_1 = require("../domain/usecase/base-processing.retry");
let BaseProcessingConsumerController = class BaseProcessingConsumerController {
    constructor(logger, clientSentry, baseProducerService) {
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.baseProducerService = baseProducerService;
        this.multipleTopic = [];
        this.consumerOptions = [];
    }
    async onModuleInit() {
        var _a, _b;
        this.consumer = new kafkajs_1.Kafka({
            clientId: `${this.moduleName.toLowerCase()}`,
            brokers: config_1.variableConfig.KAFKA_HOST,
            connectionTimeout: parseInt((_a = process.env.KAFKA_TIMEOUT) !== null && _a !== void 0 ? _a : '10000'),
            authenticationTimeout: parseInt((_b = process.env.KAFKA_TIMEOUT) !== null && _b !== void 0 ? _b : '10000')
        }).consumer({
            groupId: `${this.moduleName.toLowerCase()}_consumer`
        });
        this.consumerOptions.push(...(await this.getConsumerOptions()));
        this.consumerOptions.push(...this.getConsumerOptionsDefault());
        if (this.consumerOptions.length > 0) {
            this.consumerOptions.forEach(async (consumerOption) => {
                const checkConsumerOption = this.consumerOptions.filter((consumerOption2) => consumerOption2.topicName === consumerOption.topicName);
                if (checkConsumerOption.length > 1)
                    throw new Error(`Topic ${consumerOption.topicName} is double register at module ${this.moduleName} consumer!`);
                await this.registerMultipleTopics(this.moduleName, consumerOption.topicName);
            });
        }
        else {
            throw new Error(`Registered topic is not available at module ${this.moduleName}`);
        }
        (0, consumer_helper_1.validateTopicConsume)(this.moduleName, this.multipleTopic);
        await this.consumer.connect();
        await this.consumerSubscribeTopic();
        await this.consumerListenTopic();
    }
    async consumerSubscribeTopic() {
        await Promise.all(this.multipleTopic.map(async ({ topic, retry0, retry1, retry2 }) => {
            await this.subscibeTopic(topic);
            await this.subscibeTopic(retry0);
            await this.subscibeTopic(retry1);
            await this.subscibeTopic(retry2);
        }));
    }
    async subscibeTopic(topic) {
        await this.consumer.subscribe({
            topic,
            fromBeginning: false
        });
    }
    async registerMultipleTopics(moduleName, kafkaTopic) {
        const { retry0, retry1, retry2, dlq } = (0, generate_topic_retry_1.generateRetryTopic)(moduleName, kafkaTopic);
        this.multipleTopic.push({
            topic: kafkaTopic,
            retry0: retry0,
            retry1: retry1,
            retry2: retry2,
            dlq: dlq
        });
        (0, consumer_helper_1.registerToVariableConfig)(moduleName, kafkaTopic);
        this.baseProducerService.subscribeNewTopics([retry0, retry1, retry2, dlq]);
    }
    async getConsumerOptions() {
        return [];
    }
    getConsumerOptionsDefault() {
        return [];
    }
    async consumerListenTopic() {
        this.consumer.run({
            eachMessage: async ({ topic, partition, message }) => {
                const messageValue = await JSON.parse(message.value.toString());
                let nextTopicRetry = '-';
                const kafkaTopic = this.multipleTopic.filter((objectTopic) => objectTopic.topic === topic ||
                    objectTopic.retry0 === topic ||
                    objectTopic.retry1 === topic ||
                    objectTopic.retry2 === topic);
                const indexConsumerOption = this.consumerOptions.findIndex((consumerOption) => consumerOption.topicName === kafkaTopic[0].topic);
                switch (topic) {
                    case kafkaTopic[0].topic:
                        nextTopicRetry = kafkaTopic[0].retry0;
                        break;
                    case kafkaTopic[0].retry0:
                        nextTopicRetry = kafkaTopic[0].retry1;
                        break;
                    case kafkaTopic[0].retry1:
                        nextTopicRetry = kafkaTopic[0].retry2;
                        break;
                    case kafkaTopic[0].retry2:
                        nextTopicRetry = kafkaTopic[0].dlq;
                        break;
                }
                if (nextTopicRetry !== '-') {
                    console.log(`${this.moduleName} start consume topic: ${topic}.`);
                    await new base_processing_retry_1.BaseProcessingRetryPattern(this.logger, this.clientSentry, this.baseProducerService, this.consumerOptions[indexConsumerOption]).execute(this.moduleName, topic, nextTopicRetry, messageValue);
                }
            }
        });
    }
};
BaseProcessingConsumerController = __decorate([
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __param(1, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService,
        base_producer_service_1.BaseProducerServiceImpl])
], BaseProcessingConsumerController);
exports.BaseProcessingConsumerController = BaseProcessingConsumerController;
//# sourceMappingURL=base-processing-consumer.controller.js.map