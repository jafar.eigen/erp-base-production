"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBaseConsumerController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const kafkajs_1 = require("kafkajs");
const generate_topic_retry_1 = require("../../utils/generate-topic-retry");
const config_1 = require("../../../utils/config");
const base_producer_service_1 = require("../../infrastructure/base-producer.service");
const consumer_retry_pattern_1 = require("../domain/usecase/consumer-retry-pattern");
function getBaseConsumerController(moduleName, topicKafka) {
    const topicRetry = (0, generate_topic_retry_1.generateRetryTopic)(moduleName, topicKafka);
    const topicRetry0 = topicRetry.retry0.trim();
    const topicRetry1 = topicRetry.retry1.trim();
    const topicRetry2 = topicRetry.retry2.trim();
    const topicDLQ = topicRetry.dlq.trim();
    if (!config_1.variableConfig.KAFKA_TOPICS_CONSUME) {
        Object.assign(config_1.variableConfig, {
            KAFKA_TOPICS_CONSUME: [
                {
                    module_name: moduleName,
                    topic_consume: [topicKafka]
                }
            ]
        });
    }
    else {
        const objIndex = config_1.variableConfig.KAFKA_TOPICS_CONSUME.findIndex((obj) => obj.module_name === moduleName);
        if (objIndex === -1) {
            config_1.variableConfig.KAFKA_TOPICS_CONSUME.push({
                module_name: moduleName,
                topic_consume: [topicKafka]
            });
        }
        else {
            config_1.variableConfig.KAFKA_TOPICS_CONSUME[objIndex].topic_consume.push(topicKafka);
        }
    }
    let BaseConsumerController = class BaseConsumerController {
        constructor(logger, clientSentry, baseConsumerManager, baseProducerService) {
            this.logger = logger;
            this.clientSentry = clientSentry;
            this.baseConsumerManager = baseConsumerManager;
            this.baseProducerService = baseProducerService;
            this.consumer = new kafkajs_1.Kafka({
                clientId: `${moduleName.toLowerCase()}_${topicKafka.toLowerCase()}`,
                brokers: config_1.variableConfig.KAFKA_HOST
            }).consumer({
                groupId: `${moduleName.toLowerCase()}_${topicKafka.toLowerCase()}_consumer`
            });
            if (!config_1.variableConfig.KAFKA_TOPICS_CONSUME)
                throw new Error('KAFKA TOPICS CONSUME not set!');
            const objIndex = config_1.variableConfig.KAFKA_TOPICS_CONSUME.findIndex((obj) => obj.module_name === moduleName);
            if (objIndex === -1)
                throw new Error(`Kafka topic consume module ${moduleName} bermasalah.`);
            const statusTopicConsume = config_1.variableConfig.KAFKA_TOPICS_CONSUME[objIndex].topic_consume.filter((consumeTopic) => consumeTopic === topicKafka);
            if (statusTopicConsume.length === 0)
                throw new Error(`Kafka topic consume ${topicKafka} di module ${moduleName} belum terdaftar.`);
        }
        async onModuleInit() {
            await this.consumer.connect();
            await this.consumerSubscribeTopic();
            await this.consumerListenTopic();
        }
        async consumerSubscribeTopic() {
            await this.consumer.subscribe({
                topic: topicKafka,
                fromBeginning: false
            });
            await this.consumer.subscribe({
                topic: topicRetry0,
                fromBeginning: false
            });
            await this.consumer.subscribe({
                topic: topicRetry1,
                fromBeginning: false
            });
            await this.consumer.subscribe({
                topic: topicRetry2,
                fromBeginning: false
            });
        }
        async consumerListenTopic() {
            this.consumer.run({
                eachMessage: async ({ topic, partition, message }) => {
                    const messageValue = await JSON.parse(message.value.toString());
                    let nextTopicRetry = '-';
                    switch (topic) {
                        case topicKafka:
                            nextTopicRetry = topicRetry0;
                            break;
                        case topicRetry0:
                            nextTopicRetry = topicRetry1;
                            break;
                        case topicRetry1:
                            nextTopicRetry = topicRetry2;
                            break;
                        case topicRetry2:
                            nextTopicRetry = topicDLQ;
                            break;
                    }
                    if (nextTopicRetry !== '-') {
                        this.retryPattern = await new consumer_retry_pattern_1.ConsumerRetryPattern(this.logger, this.clientSentry, this.baseConsumerManager, this.baseProducerService);
                        await this.processMessage({
                            topic,
                            nextTopicRetry,
                            messageValue
                        });
                    }
                    return;
                }
            });
        }
        async processMessage({ topic, nextTopicRetry, messageValue }) {
            return await this.retryPattern.execute(moduleName, topic, nextTopicRetry, messageValue);
        }
    };
    BaseConsumerController = __decorate([
        __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
        __param(1, (0, nestjs_sentry_1.InjectSentry)()),
        __metadata("design:paramtypes", [Object, nestjs_sentry_1.SentryService, Object, base_producer_service_1.BaseProducerServiceImpl])
    ], BaseConsumerController);
    return BaseConsumerController;
}
exports.getBaseConsumerController = getBaseConsumerController;
//# sourceMappingURL=base-consumer.controller.js.map