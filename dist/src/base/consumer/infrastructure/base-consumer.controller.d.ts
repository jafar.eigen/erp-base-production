import { BaseConsumerManager } from '../domain/usecase/managers/base-consumer.manager';
export declare function getBaseConsumerController<Entity, BaseConsumerManagerImpl extends BaseConsumerManager>(moduleName: string, topicKafka: string): any;
