import { Logger } from 'winston';
import { SentryService } from '@ntegral/nestjs-sentry';
import { BaseEntity } from '../../../domain/entities/base.entity';
import { BaseProducerServiceImpl } from '../../../infrastructure/base-producer.service';
import { ConsumerOption } from '../../../domain/entities/kafka/kafka-topic-consumer.interface';
export declare class BaseProcessingRetryPattern {
    private readonly logger;
    private readonly clientSentry;
    private producerService;
    private consumerOption;
    constructor(logger: Logger, clientSentry: SentryService, producerService: BaseProducerServiceImpl<Partial<BaseEntity>>, consumerOption: ConsumerOption);
    execute(moduleName: string, topic: string, topicRetry: string, message: any): Promise<any>;
}
