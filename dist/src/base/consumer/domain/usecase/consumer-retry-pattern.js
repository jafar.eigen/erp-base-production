"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsumerRetryPattern = void 0;
const moment = require("moment");
class ConsumerRetryPattern {
    constructor(logger, clientSentry, baseConsumerManager, baseProducerService) {
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.baseConsumerManager = baseConsumerManager;
        this.baseProducerService = baseProducerService;
    }
    async execute(moduleName, topic, topicRetry, message) {
        try {
            console.log(`${moduleName} Consume ${topic} start.`);
            return await this.baseConsumerManager.execute(message);
        }
        catch (error) {
            this.logger.error({
                timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
                module_name: moduleName,
                from_topic: topic,
                to_topic: topicRetry,
                error_message: error.message,
                value: Object.assign({}, message)
            });
            this.clientSentry.error(JSON.stringify({
                timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
                module_name: moduleName,
                from_topic: topic,
                to_topic: topicRetry,
                error_message: error.message,
                value: Object.assign({}, message)
            }));
            await this.baseProducerService.baseRetry(topicRetry, message);
        }
    }
}
exports.ConsumerRetryPattern = ConsumerRetryPattern;
//# sourceMappingURL=consumer-retry-pattern.js.map