"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseProcessingRetryPattern = void 0;
const moment = require("moment");
const consumer_topics_base_process_manager_1 = require("./managers/consumer-topics-base-process.manager");
class BaseProcessingRetryPattern {
    constructor(logger, clientSentry, producerService, consumerOption) {
        this.logger = logger;
        this.clientSentry = clientSentry;
        this.producerService = producerService;
        this.consumerOption = consumerOption;
    }
    async execute(moduleName, topic, topicRetry, message) {
        try {
            return await new consumer_topics_base_process_manager_1.ConsumerTopicsBaseProcessManager(this.consumerOption, this.producerService).execute(this.consumerOption, message);
        }
        catch (error) {
            this.logger.error({
                timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
                module_name: moduleName,
                from_topic: topic,
                to_topic: topicRetry,
                error_message: error.message,
                value: Object.assign({}, message)
            });
            this.clientSentry.error(JSON.stringify({
                timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
                module_name: moduleName,
                from_topic: topic,
                to_topic: topicRetry,
                error_message: error.message,
                value: Object.assign({}, message)
            }));
            await this.producerService.baseRetry(topicRetry, message);
        }
    }
}
exports.BaseProcessingRetryPattern = BaseProcessingRetryPattern;
//# sourceMappingURL=base-processing.retry.js.map