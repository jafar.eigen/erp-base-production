import { Logger } from 'winston';
import { SentryService } from '@ntegral/nestjs-sentry';
import { BaseEntity } from '../../../domain/entities/base.entity';
import { BaseConsumerManager } from './managers/base-consumer.manager';
import { BaseProducerServiceImpl } from '../../../infrastructure/base-producer.service';
export declare class ConsumerRetryPattern {
    private readonly logger;
    private readonly clientSentry;
    baseConsumerManager: BaseConsumerManager;
    private baseProducerService;
    constructor(logger: Logger, clientSentry: SentryService, baseConsumerManager: BaseConsumerManager, baseProducerService: BaseProducerServiceImpl<Partial<BaseEntity>>);
    execute(moduleName: string, topic: string, topicRetry: string, message: any): Promise<any>;
}
