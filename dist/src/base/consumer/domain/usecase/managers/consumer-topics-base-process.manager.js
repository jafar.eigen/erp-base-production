"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsumerTopicsBaseProcessManager = void 0;
class ConsumerTopicsBaseProcessManager {
    constructor(consumerOption, producerService) {
        this.consumerOption = consumerOption;
        this.producerService = producerService;
        this.transformData = consumerOption.transformData;
        this.beforeProcess = consumerOption.beforeProcess;
        this.processData = consumerOption.processData;
        this.afterProcess = consumerOption.afterProcess;
    }
    async execute(consumerOption, message) {
        let transformData;
        if (this.transformData) {
            transformData = this.transformData;
        }
        else {
            transformData = this.transformDataDefault;
        }
        let processData;
        if (this.processData) {
            processData = this.processData;
        }
        else {
            processData = this.processDataDefault;
        }
        this.entityData = await transformData(consumerOption, message);
        if (this.beforeProcess) {
            await this.beforeProcess();
        }
        const resultProcess = await processData(consumerOption, this.entityData);
        if (this.afterProcess) {
            await this.afterProcess(consumerOption, resultProcess);
        }
    }
    setUserConsumerOption(message) {
        this.consumerOption.user = message.user;
    }
    async transformDataDefault(consumerOption, value) {
        var _a;
        return {
            id: (_a = value.data.uuid) !== null && _a !== void 0 ? _a : value.data.id,
            code: value.data.code,
            name: value.data.name,
            status: value.data.status
        };
    }
    async processDataDefault(consumerOption, dataTransform) {
        return await consumerOption.repositories[0].save(dataTransform);
    }
}
exports.ConsumerTopicsBaseProcessManager = ConsumerTopicsBaseProcessManager;
//# sourceMappingURL=consumer-topics-base-process.manager.js.map