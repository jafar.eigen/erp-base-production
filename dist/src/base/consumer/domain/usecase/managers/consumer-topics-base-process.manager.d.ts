import { BaseProducerServiceImpl } from 'src';
import { Repository } from 'typeorm';
import { ConsumerOption } from '../../../../domain/entities/kafka/kafka-topic-consumer.interface';
export declare class ConsumerTopicsBaseProcessManager {
    consumerOption: ConsumerOption;
    producerService: BaseProducerServiceImpl<any>;
    entityData: any;
    transformData: (consumerOption: ConsumerOption, message: Partial<any>) => Promise<any>;
    beforeProcess: () => Promise<void>;
    processData: (consumerOption: ConsumerOption, message: Partial<any>) => Promise<any>;
    afterProcess: (consumerOption: ConsumerOption, resultProcess: any) => Promise<void>;
    repositories: Repository<any>[];
    constructor(consumerOption: ConsumerOption, producerService: BaseProducerServiceImpl<any>);
    execute(consumerOption: ConsumerOption, message: any): Promise<void>;
    setUserConsumerOption(message: any): void;
    transformDataDefault(consumerOption: ConsumerOption, value: Partial<any>): Promise<any>;
    processDataDefault(consumerOption: ConsumerOption, dataTransform: any): Promise<any>;
}
