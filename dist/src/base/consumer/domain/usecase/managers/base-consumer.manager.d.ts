import { BaseDataService } from '../../../../data/services/base-data.service';
export declare abstract class BaseConsumerManager {
    dataService: BaseDataService<Partial<any>>;
    constructor(dataService: BaseDataService<Partial<any>>);
    execute(message: any): Promise<any>;
    transformData(value: Partial<any>): Promise<Partial<any>>;
    processData(dataTransform: Partial<any>): Promise<any>;
    protected onSave(message: Partial<any>): Promise<void>;
    protected onDelete(id: string[]): Promise<void>;
}
