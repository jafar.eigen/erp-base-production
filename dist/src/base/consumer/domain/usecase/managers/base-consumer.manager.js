"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseConsumerManager = void 0;
class BaseConsumerManager {
    constructor(dataService) {
        this.dataService = dataService;
    }
    async execute(message) {
        const dataTransform = await this.transformData(message);
        return await this.processData(dataTransform);
    }
    async transformData(value) {
        var _a;
        return {
            id: (_a = value.data.uuid) !== null && _a !== void 0 ? _a : value.data.id,
            code: value.data.code,
            name: value.data.name,
            status: value.data.status
        };
    }
    async processData(dataTransform) {
        await this.onSave(dataTransform);
    }
    async onSave(message) {
        await this.dataService.save(message);
    }
    async onDelete(id) {
        await this.dataService.delete(id);
    }
}
exports.BaseConsumerManager = BaseConsumerManager;
//# sourceMappingURL=base-consumer.manager.js.map