"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseConsumerOption = void 0;
class BaseConsumerOption {
    constructor(topicName, repositories) {
        this.topicName = topicName;
        this.repositories = repositories;
    }
    async getOption() {
        this.consumerOption = await this.setConsumerOption();
        return {
            topicName: this.consumerOption.topicName,
            repositories: this.consumerOption.repositories,
            transformData: this.consumerOption.transformData,
            beforeProcess: this.consumerOption.beforeProcess,
            processData: this.consumerOption.processData,
            afterProcess: this.consumerOption.afterProcess,
            additionalModule: this.consumerOption.additionalModule
        };
    }
}
exports.BaseConsumerOption = BaseConsumerOption;
//# sourceMappingURL=base-consumer.option.js.map