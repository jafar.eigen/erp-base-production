import { Repository } from 'typeorm';
import { ConsumerOption } from '../../domain/entities/kafka/kafka-topic-consumer.interface';
export declare abstract class BaseConsumerOption {
    topicName: string;
    repositories: Repository<any>[];
    consumerOption: ConsumerOption;
    constructor(topicName: string, repositories: Repository<any>[]);
    abstract setConsumerOption(): Promise<ConsumerOption>;
    getOption(): Promise<ConsumerOption>;
}
