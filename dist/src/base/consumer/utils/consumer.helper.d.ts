import { RetryTopicEntity } from '../../domain/entities/kafka/kafka-topic-consumer.interface';
export declare function registerToVariableConfig(moduleName: string, kafkaTopic: string): void;
export declare function validateTopicConsume(moduleName: string, topics: RetryTopicEntity[]): void;
