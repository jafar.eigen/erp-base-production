"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateTopicConsume = exports.registerToVariableConfig = void 0;
const config_1 = require("../../../utils/config");
function registerToVariableConfig(moduleName, kafkaTopic) {
    if (!config_1.variableConfig.KAFKA_TOPICS_CONSUME) {
        Object.assign(config_1.variableConfig, {
            KAFKA_TOPICS_CONSUME: [
                {
                    module_name: moduleName,
                    topic_consume: [kafkaTopic]
                }
            ]
        });
    }
    else {
        const objIndex = config_1.variableConfig.KAFKA_TOPICS_CONSUME.findIndex((obj) => obj.module_name === moduleName);
        if (objIndex === -1) {
            config_1.variableConfig.KAFKA_TOPICS_CONSUME.push({
                module_name: moduleName,
                topic_consume: [kafkaTopic]
            });
        }
        else {
            config_1.variableConfig.KAFKA_TOPICS_CONSUME[objIndex].topic_consume.push(kafkaTopic);
        }
    }
}
exports.registerToVariableConfig = registerToVariableConfig;
function validateTopicConsume(moduleName, topics) {
    if (!config_1.variableConfig.KAFKA_TOPICS_CONSUME)
        throw new Error('KAFKA TOPICS CONSUME not set!');
    const objIndex = config_1.variableConfig.KAFKA_TOPICS_CONSUME.findIndex((obj) => obj.module_name === moduleName);
    if (objIndex === -1)
        throw new Error(`Kafka topic consume module ${moduleName} bermasalah.`);
    topics.forEach((topic) => {
        const statusTopicConsume = config_1.variableConfig.KAFKA_TOPICS_CONSUME[objIndex].topic_consume.filter((consumeTopic) => consumeTopic === topic.topic);
        if (statusTopicConsume.length === 0)
            throw new Error(`Kafka topic consume ${topic.topic} di module ${moduleName} belum terdaftar.`);
    });
}
exports.validateTopicConsume = validateTopicConsume;
//# sourceMappingURL=consumer.helper.js.map