export interface TopicRetry {
    retry0: string;
    retry1: string;
    retry2: string;
    dlq: string;
}
export declare function generateRetryTopic(moduleName: string, topic: string): TopicRetry;
