"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSession = void 0;
const json_parse_helper_1 = require("../../helpers/json-parse.helper");
const approval_entity_1 = require("../domain/entities/approval.entity");
const user_payload_interface_ts_1 = require("../domain/entities/user/user-payload-interface.ts");
class UserSession {
    constructor() {
        this.jsonParser = new json_parse_helper_1.JSONParse();
    }
    static getInstance() {
        if (!UserSession.instance) {
            UserSession.instance = new UserSession();
        }
        return UserSession.instance;
    }
    set(headers) {
        var _a, _b;
        const user = (_a = this.jsonParser.decrypt(headers['e-user'])) !== null && _a !== void 0 ? _a : user_payload_interface_ts_1.MockUser;
        const approvals = (_b = this.jsonParser.decrypt(headers['e-approval'])) !== null && _b !== void 0 ? _b : approval_entity_1.MockApprovalNone;
        this.user = user;
        this.approvals = approvals;
    }
    getUser() {
        return this.user;
    }
    getApprovals() {
        return this.approvals;
    }
}
exports.UserSession = UserSession;
//# sourceMappingURL=UserSession.js.map