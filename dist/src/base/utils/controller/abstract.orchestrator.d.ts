/// <reference types="multer" />
import { BaseResultBatch } from '../../domain/entities/base-result-batch.entity';
export declare abstract class AbstractOrchestrator {
    abstract create(entity: any): Promise<any>;
    abstract update(entityId: string, updatedData: any, file?: Express.Multer.File, newAddress?: string): Promise<any>;
    abstract approve(entityId: string, step: number): Promise<any>;
    abstract decline(entityId: string, step: number): Promise<any>;
    abstract request(entityId: string): Promise<any>;
    abstract activate(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
    abstract deactivate(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
    abstract cancel(entityId: string): Promise<any>;
    abstract rollbackStatusTrx(entityId: string[]): Promise<any>;
    abstract delete(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
}
