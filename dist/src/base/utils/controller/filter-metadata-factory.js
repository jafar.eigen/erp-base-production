"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatEntityName = exports.filterMetadata = void 0;
require("reflect-metadata");
const filterMetadata = (TypeClass, metadataKey, excludedValues, name) => {
    class CloneTypeClass extends TypeClass {
    }
    const metadata = Reflect.getMetadata(metadataKey, CloneTypeClass.prototype) || [];
    const metadataFiltered = metadata.filter((item) => excludedValues.indexOf(item) === -1);
    const className = name ? name : TypeClass.name;
    Reflect.defineMetadata(metadataKey, metadataFiltered, CloneTypeClass.prototype);
    Object.defineProperty(CloneTypeClass, 'name', {
        value: className
    });
    return CloneTypeClass;
};
exports.filterMetadata = filterMetadata;
const formatEntityName = (entity, create = true) => {
    if (create) {
        return entity.name.replace('VM', 'CreateVM');
    }
    return entity.name.replace('VM', 'UpdateVM');
};
exports.formatEntityName = formatEntityName;
//# sourceMappingURL=filter-metadata-factory.js.map