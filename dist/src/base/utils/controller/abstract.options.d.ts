export interface AbstractOption {
    createModelVm: {
        new (): any;
    };
    updateModelVm: {
        new (): any;
    };
}
