import 'reflect-metadata';
export declare const filterMetadata: (TypeClass: {
    new (): any;
}, metadataKey: string, excludedValues: string[], name?: string) => any;
export declare const formatEntityName: (entity: {
    new (): any;
}, create?: boolean) => any;
