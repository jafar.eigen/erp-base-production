"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateRetryTopic = void 0;
function generateRetryTopic(moduleName, topic) {
    const topicRetry0 = `${moduleName.toUpperCase()}_${topic.toUpperCase()}_RETRY0`;
    const topicRetry1 = `${moduleName.toUpperCase()}_${topic.toUpperCase()}_RETRY1`;
    const topicRetry2 = `${moduleName.toUpperCase()}_${topic.toUpperCase()}_RETRY2`;
    const topicDLQ = `${moduleName.toUpperCase()}_DLQ`;
    return {
        retry0: topicRetry0.trim(),
        retry1: topicRetry1.trim(),
        retry2: topicRetry2.trim(),
        dlq: topicDLQ.trim()
    };
}
exports.generateRetryTopic = generateRetryTopic;
//# sourceMappingURL=generate-topic-retry.js.map