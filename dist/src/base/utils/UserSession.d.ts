import { ApprovalPerCompany } from '../domain/entities/approval.entity';
import { IUserPayload } from '../domain/entities/user/user-payload-interface.ts';
export declare class UserSession {
    private static instance;
    private jsonParser;
    private user;
    private approvals;
    static getInstance(): UserSession;
    set(headers: string): void;
    getUser(): IUserPayload;
    getApprovals(): ApprovalPerCompany[];
}
