import { Pagination } from 'nestjs-typeorm-paginate';
import { BaseReadDataService } from '../../data/services/base-read-data.service';
export declare abstract class BaseReadOrchestrator<Entity, FilterEntityDTO> {
    baseReadDataService: BaseReadDataService<Entity>;
    constructor(baseReadDataService: BaseReadDataService<Entity>);
    show(entityId: string): Promise<Entity>;
    abstract index(page: number, limit: number, params: FilterEntityDTO): Promise<Pagination<Entity>>;
}
