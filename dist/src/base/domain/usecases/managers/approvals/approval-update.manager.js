"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApprovalUpdateManager = void 0;
const common_1 = require("@nestjs/common");
const generate_approval_helper_1 = require("../../../../../helpers/approval/generate-approval.helper");
class ApprovalUpdateManager {
    constructor(entityId, updatedData, user, approvals, dataService, file, newAddress) {
        this.entityId = entityId;
        this.updatedData = updatedData;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
        this.file = file;
        this.newAddress = newAddress;
    }
    async execute() {
        await this.getRecentData();
        await this.generateApproval();
        return await this.update();
    }
    async getRecentData() {
        const transactionData = await this.dataService.findOne(this.entityId);
        if (!transactionData)
            throw new common_1.HttpException(`Approval with ${this.entityId} is not found`, common_1.HttpStatus.BAD_GATEWAY);
        Object.assign(this, { transactionData });
    }
    async generateApproval() {
        const builder = new generate_approval_helper_1.GenerateApproval();
        const transactionData = await builder
            .setNewAddress(this.newAddress)
            .setData(this.transactionData)
            .setNewData(this.updatedData)
            .setUser(this.user)
            .setApproval(this.approvals)
            .setFile(this.file)
            .run();
        Object.assign(this, { transactionData });
    }
    async update() {
        return await this.dataService.update(this.transactionData);
    }
    async postApproval() {
        return;
    }
}
exports.ApprovalUpdateManager = ApprovalUpdateManager;
//# sourceMappingURL=approval-update.manager.js.map