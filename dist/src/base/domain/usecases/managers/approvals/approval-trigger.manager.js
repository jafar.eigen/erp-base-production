"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApprovalTriggerManager = void 0;
const submit_request_helper_1 = require("../../../../../helpers/approval/submit-request.helper");
const find_company_approval_helper_1 = require("../../../../../helpers/approval/find-company-approval.helper");
const approval_entity_1 = require("../../../entities/approval.entity");
const base_entity_1 = require("../../../entities/base.entity");
class ApprovalTriggerManager {
    constructor(entityId, user, approvals, dataService, producerService, requestType, approvallStatus, statusGroup, withChild) {
        this.entityId = entityId;
        this.user = user;
        this.approvals = approvals;
        this.dataService = dataService;
        this.producerService = producerService;
        this.requestType = requestType;
        this.approvallStatus = approvallStatus;
        this.statusGroup = statusGroup;
        this.withChild = withChild;
    }
    async execute(options) {
        await this.getRecentApproval();
        await this.processRequest(options);
        return this.data;
    }
    async getRecentApproval() {
        const data = await this.dataService.findOne(this.entityId);
        Object.assign(this, { data });
        if (this.requestType !== submit_request_helper_1.RequestType.REQUEST)
            this.assignApproval();
    }
    async processRequest(options) {
        const { entityData, customProduceTopic } = options;
        const builder = new submit_request_helper_1.SubmitRequest();
        const skipApprovalFunction = this.setSkipApprovalFunction();
        const requestType = this.setRequestType();
        if (entityData)
            Object.assign(this.data, entityData);
        let produceApprovalSubmitted;
        let produceApprovalSkipped;
        if (customProduceTopic) {
            produceApprovalSubmitted = customProduceTopic;
            produceApprovalSkipped = customProduceTopic;
        }
        else {
            produceApprovalSubmitted = this.produceApprovalSubmitted;
            produceApprovalSkipped = this.produceApprovalSkipped;
        }
        const data = await builder
            .setRequestType(requestType)
            .setApprovallStatus(this.approvallStatus)
            .setUser(this.user)
            .setDataService(this.dataService)
            .setKafkaService(this.producerService)
            .setData(this.data)
            .setPassApprovalFunction(skipApprovalFunction)
            .setSubmitRequestFunction(this.submitRequest)
            .setProduceSubmitRequestTopic(produceApprovalSubmitted)
            .setProducePassApprovalTopic(produceApprovalSkipped)
            .run();
        Object.assign(this, { data });
    }
    setSkipApprovalFunction() {
        if (this.requestType === submit_request_helper_1.RequestType.DELETE && !this.withChild) {
            return this.skipApprovalDelete;
        }
        else if (this.requestType === submit_request_helper_1.RequestType.REQUEST) {
            return this.skipApprovalRequest;
        }
        else if (this.requestType !== submit_request_helper_1.RequestType.REQUEST && this.withChild) {
            if (this.requestType === submit_request_helper_1.RequestType.ACTIVE) {
                return this.skipApprovalWithChild;
            }
            else if (this.requestType === submit_request_helper_1.RequestType.INACTIVE) {
                return this.skipApprovalWithChildDeactivate;
            }
        }
        else {
            return this.skipApproval;
        }
    }
    setRequestType() {
        if (this.requestType === submit_request_helper_1.RequestType.REQUEST) {
            return this.data.request_info === approval_entity_1.RequestInfo.EDIT_DATA
                ? submit_request_helper_1.RequestType.UPDATE
                : submit_request_helper_1.RequestType.CREATE;
        }
        else {
            if (this.withChild) {
                if (this.requestType === submit_request_helper_1.RequestType.ACTIVE) {
                    return submit_request_helper_1.RequestType.ACTIVE_WITH_CHILD;
                }
                else if (this.requestType === submit_request_helper_1.RequestType.INACTIVE) {
                    return submit_request_helper_1.RequestType.INACTIVE_WITH_CHILD;
                }
            }
            else {
                return this.requestType;
            }
        }
    }
    assignApproval() {
        const approval = (0, find_company_approval_helper_1.findApproval)(this.approvals, this.data, this.user);
        Object.assign(this.data, { approval });
    }
    async submitRequest(data) {
        return await this.dataService.update(data);
    }
    async skipApproval(data) {
        Object.assign(data, {
            status: this.approvallStatus
        });
        return await this.dataService.update(data);
    }
    async skipApprovalWithChild(data) {
        return await this.dataService.activateWithChild(data, this.withChild);
    }
    async skipApprovalWithChildDeactivate(data) {
        return await this.dataService.activateWithChild(data, this.withChild);
    }
    async skipApprovalDelete(data) {
        await this.dataService.delete([data.id]);
        return data;
    }
    async skipApprovalRequest(data) {
        if (this.statusGroup && this.requestType === submit_request_helper_1.RequestType.CREATE) {
            await this.dataService.generateTableGroup();
            await this.dataService.saveGroup(data);
        }
        return await this.dataService.update(data);
    }
    async produceApprovalSubmitted(payload) {
        if (payload.data.status === base_entity_1.STATUS.DRAFT)
            return;
        return this.producerService.baseChanged(payload);
    }
    async produceApprovalSkipped(payload) {
        if (this.requestType === submit_request_helper_1.RequestType.DELETE) {
            return this.producerService.baseDeleted(payload);
        }
        else {
            if (this.data.status === base_entity_1.STATUS.DRAFT)
                return;
            console.info('generate without site');
            return this.producerService.baseActivated(payload);
        }
    }
    async customeProduceTopic(payload) {
        return this.producerService.baseChanged(payload);
    }
}
exports.ApprovalTriggerManager = ApprovalTriggerManager;
//# sourceMappingURL=approval-trigger.manager.js.map