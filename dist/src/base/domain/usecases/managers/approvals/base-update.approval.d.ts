import { BaseDataService } from '../../../../data/services/base-data.service';
import { BaseProducerServiceImpl } from '../../../../infrastructure/base-producer.service';
import { BaseApplyManager } from '../base-apply.manager';
export declare abstract class BaseUpdateApproval<Entity> extends BaseApplyManager<Entity> {
    entityId: string;
    updatedData: Partial<Entity>;
    constructor(dataService: BaseDataService<Entity>, producerService: BaseProducerServiceImpl<Entity>, entityId: string, updatedData: Partial<Entity>);
    execute(): Promise<Entity>;
    beforeProcess(): Promise<void>;
    prepareData(): Promise<Entity>;
    afterProcess(entity: Entity): Promise<void>;
}
