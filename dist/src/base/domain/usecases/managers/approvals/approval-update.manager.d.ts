/// <reference types="multer" />
import { BaseDataService } from '../../../../data/services/base-data.service';
import { ApprovalPerCompany } from '../../../entities/approval.entity';
import { IUserPayload } from '../../../entities/user/user-payload-interface.ts';
export declare class ApprovalUpdateManager<Entity> {
    readonly entityId: string;
    private readonly updatedData;
    private readonly user;
    private readonly approvals;
    readonly dataService: BaseDataService<Entity>;
    private file?;
    private readonly newAddress?;
    transactionData: Entity;
    constructor(entityId: string, updatedData: Partial<Entity>, user: IUserPayload, approvals: ApprovalPerCompany[], dataService: BaseDataService<Entity>, file?: Express.Multer.File, newAddress?: string);
    execute(): Promise<Entity>;
    protected getRecentData(): Promise<void>;
    protected generateApproval(): Promise<void>;
    update(): Promise<Entity>;
    postApproval(): Promise<void>;
}
