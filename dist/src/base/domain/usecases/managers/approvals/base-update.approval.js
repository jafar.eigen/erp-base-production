"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseUpdateApproval = void 0;
const common_1 = require("@nestjs/common");
const generate_approval_helper_1 = require("../../../../../helpers/approval/generate-approval.helper");
const base_apply_manager_1 = require("../base-apply.manager");
class BaseUpdateApproval extends base_apply_manager_1.BaseApplyManager {
    constructor(dataService, producerService, entityId, updatedData) {
        super(dataService, producerService);
        this.entityId = entityId;
        this.updatedData = updatedData;
    }
    async execute() {
        this.entity = await this.prepareData();
        await this.beforeProcess();
        const result = await this.dataService.update(this.entity);
        await this.afterProcess(result);
        return result;
    }
    async beforeProcess() {
        const builder = new generate_approval_helper_1.GenerateApproval();
        const entity = await builder
            .setData(this.entity)
            .setNewData(this.updatedData)
            .setUser(this.user)
            .setApproval(this.approvals)
            .run();
        Object.assign(this, { entity });
    }
    async prepareData() {
        const entity = await this.dataService.findOne(this.entityId);
        if (!entity)
            throw new common_1.HttpException(`Approval with ${this.entityId} is not found`, common_1.HttpStatus.BAD_GATEWAY);
        return entity;
    }
    async afterProcess(entity) {
        return;
    }
}
exports.BaseUpdateApproval = BaseUpdateApproval;
//# sourceMappingURL=base-update.approval.js.map