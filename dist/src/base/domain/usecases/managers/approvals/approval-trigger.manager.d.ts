import { BaseDataService } from '../../../../data/services/base-data.service';
import { ApprovalPerCompany, Approver } from '../../../entities/approval.entity';
import { IUserPayload } from '../../../entities/user/user-payload-interface.ts';
import { BaseProducerServiceImpl } from '../../../../infrastructure/base-producer.service';
import { KafkaPayload } from '../../../entities/kafka/payload.interface';
import { BaseEntity } from '../../../entities/base.entity';
interface executeOptions<Entity> {
    entityData?: Partial<Entity>;
    customProduceTopic?: (payload: KafkaPayload<IUserPayload, Entity, Entity>) => Promise<void>;
}
export declare class ApprovalTriggerManager<Entity extends BaseEntity> {
    private readonly entityId;
    private readonly user;
    private readonly approvals;
    private readonly dataService;
    private readonly producerService;
    requestType: string;
    approvallStatus: string;
    statusGroup: boolean;
    withChild?: string;
    data: Entity;
    approver: Approver;
    constructor(entityId: string, user: IUserPayload, approvals: ApprovalPerCompany[], dataService: BaseDataService<Entity>, producerService: BaseProducerServiceImpl<Entity>, requestType: string, approvallStatus: string, statusGroup: boolean, withChild?: string);
    execute(options: executeOptions<Entity>): Promise<Entity>;
    protected getRecentApproval(): Promise<void>;
    protected processRequest(options: executeOptions<Entity>): Promise<void>;
    setSkipApprovalFunction(): any;
    setRequestType(): string;
    protected assignApproval(): void;
    protected submitRequest(data: Entity): Promise<Entity>;
    protected skipApproval(data: {
        id: string;
    } & Entity): Promise<Entity>;
    protected skipApprovalWithChild(data: {
        id: string;
    } & Entity): Promise<Entity>;
    protected skipApprovalWithChildDeactivate(data: {
        id: string;
    } & Entity): Promise<Entity>;
    protected skipApprovalDelete(data: {
        id: string;
    } & Entity): Promise<Entity>;
    protected skipApprovalRequest(data: {
        id: string;
    } & Entity): Promise<Entity>;
    produceApprovalSubmitted(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    protected produceApprovalSkipped(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    customeProduceTopic(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
export {};
