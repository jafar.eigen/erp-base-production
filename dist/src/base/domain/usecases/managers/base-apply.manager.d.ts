/// <reference types="multer" />
import { BaseDataService } from '../../../data/services/base-data.service';
import { BaseProducerServiceImpl } from '../../../infrastructure/base-producer.service';
import { ApplyManagerContract } from '../../contracts/apply-manager.contract';
import { ApprovalPerCompany } from '../../entities/approval.entity';
import { IUserPayload } from '../../entities/user/user-payload-interface.ts';
export declare abstract class BaseApplyManager<Entity> implements ApplyManagerContract<Entity> {
    dataService: BaseDataService<Entity>;
    producerService: BaseProducerServiceImpl<Entity>;
    statusGroup?: boolean;
    file?: Express.Multer.File;
    newAddress?: string;
    user: IUserPayload;
    approvals: ApprovalPerCompany[];
    entity: Entity;
    statusGroupApply: boolean;
    result: Entity;
    constructor(dataService: BaseDataService<Entity>, producerService: BaseProducerServiceImpl<Entity>, statusGroup?: boolean, file?: Express.Multer.File, newAddress?: string);
    getParameters(): Promise<void>;
    execute(): Promise<Entity>;
    _findApprovals(): ApprovalPerCompany;
    abstract beforeProcess(): Promise<void>;
    abstract processData(): Promise<Entity>;
    abstract prepareData(): Promise<Entity>;
    abstract afterProcess(entity: Entity): Promise<void>;
}
