"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseActionManager = void 0;
const response_approval_helper_1 = require("../../../../helpers/response-approval.helper");
const UserSession_1 = require("../../../utils/UserSession");
const base_entity_1 = require("../../entities/base.entity");
class BaseActionManager {
    constructor(dataService, producerService, idEntity, step) {
        this.dataService = dataService;
        this.producerService = producerService;
        this.idEntity = idEntity;
        this.step = step;
        this.getParameters();
    }
    async getParameters() {
        const session = UserSession_1.UserSession.getInstance();
        this.user = session.getUser();
    }
    async execute() {
        await this.getRecentApproval();
        await this.responseApproval();
        return await this.afterProcess();
    }
    async afterProcess() {
        return this.entity;
    }
    async getRecentApproval() {
        var _a;
        const recentApproval = await this.dataService.findOne(this.idEntity);
        const companyId = this.findCompanyId(recentApproval);
        const user_category = (_a = this.user.companies.find((comp) => comp.company.id === companyId)) === null || _a === void 0 ? void 0 : _a.user_category;
        this.user.user_category = user_category;
        Object.assign(this, { entity: recentApproval });
    }
    async responseApproval() {
        const builder = new response_approval_helper_1.ResponseApproval();
        this.data = this.entity;
        const entity = await builder
            .setResponseType(this.responseType)
            .setData(this.entity)
            .setUser(this.user)
            .setStepNumber(this.step)
            .setDataService(this.dataService)
            .setKafkaService(this.producerService)
            .setUpdateDataFunction(this.submitRequest)
            .setDeleteDataFunction(this.deleteData)
            .setUnfinishedDataApprovalFunction(this.submitRequest)
            .setDataUpdatedProducer(this.produceEntityUpdatedTopic)
            .setDataActivatedProducer(this.produceEntityActivatedTopic)
            .setCustomActivatedDataProducer(this.produceEntityActivatedWithSiteTopic)
            .setDataDeletedProducer(this.produceEntityDeletedTopic)
            .run();
        Object.assign(this, { entity });
    }
    async submitRequest(entity) {
        return await this.dataService.update(entity);
    }
    async deleteData(entity) {
        await this.dataService.delete([entity.id]);
        return entity;
    }
    async produceEntityUpdatedTopic(payload) {
        if (this.data.status === base_entity_1.STATUS.DRAFT)
            return;
        return this.producerService.baseChanged(payload);
    }
    async produceEntityActivatedTopic(payload) {
        return this.producerService.baseActivated(payload);
    }
    async produceEntityDeletedTopic(payload) {
        return this.producerService.baseDeleted(payload);
    }
    findCompanyId(companyQuery) {
        var _a, _b, _c, _d, _e;
        const { company_id, company, companies, id } = companyQuery;
        const companies1 = companies ? companies[0] : null;
        return ((_e = (_d = (_c = (_b = (_a = company_id !== null && company_id !== void 0 ? company_id : company === null || company === void 0 ? void 0 : company.id) !== null && _a !== void 0 ? _a : company === null || company === void 0 ? void 0 : company.uuid) !== null && _b !== void 0 ? _b : companies1 === null || companies1 === void 0 ? void 0 : companies1.company_id) !== null && _c !== void 0 ? _c : companies1 === null || companies1 === void 0 ? void 0 : companies1.id) !== null && _d !== void 0 ? _d : companies1 === null || companies1 === void 0 ? void 0 : companies1.uuid) !== null && _e !== void 0 ? _e : id);
    }
    async produceEntityActivatedWithSiteTopic(payload) {
        return;
    }
}
exports.BaseActionManager = BaseActionManager;
//# sourceMappingURL=base-action.manager.js.map