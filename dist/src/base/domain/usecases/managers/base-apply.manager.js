"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseApplyManager = void 0;
const UserSession_1 = require("../../../utils/UserSession");
const find_company_approval_helper_1 = require("../../../../helpers/approval/find-company-approval.helper");
class BaseApplyManager {
    constructor(dataService, producerService, statusGroup, file, newAddress) {
        this.dataService = dataService;
        this.producerService = producerService;
        this.statusGroup = statusGroup;
        this.file = file;
        this.newAddress = newAddress;
        this.statusGroupApply = statusGroup === undefined ? false : true;
        this.getParameters();
    }
    async getParameters() {
        const session = UserSession_1.UserSession.getInstance();
        this.user = session.getUser();
        this.approvals = session.getApprovals();
    }
    async execute() {
        this.entity = await this.prepareData();
        await this.beforeProcess();
        this.result = await this.processData();
        await this.afterProcess(this.result);
        return this.result;
    }
    _findApprovals() {
        return (0, find_company_approval_helper_1.findApproval)(this.approvals, null, this.user);
    }
}
exports.BaseApplyManager = BaseApplyManager;
//# sourceMappingURL=base-apply.manager.js.map