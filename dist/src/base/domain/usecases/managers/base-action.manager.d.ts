import { ActionManagerContract } from '../../contracts/action-manager.contract';
import { IUserPayload } from '../../entities/user/user-payload-interface.ts';
import { BaseDataService } from '../../../data/services/base-data.service';
import { KafkaPayload } from '../../entities/kafka/payload.interface';
import { BaseProducerServiceImpl } from '../../../infrastructure/base-producer.service';
import { BaseEntity } from '../../entities/base.entity';
export declare abstract class BaseActionManager<Entity extends BaseEntity> implements ActionManagerContract<Entity> {
    dataService: BaseDataService<Entity>;
    producerService: BaseProducerServiceImpl<Entity>;
    idEntity: string;
    step: number;
    entity: Entity;
    data: Entity;
    user: IUserPayload;
    abstract responseType: string;
    constructor(dataService: BaseDataService<Entity>, producerService: BaseProducerServiceImpl<Entity>, idEntity: string, step: number);
    getParameters(): Promise<void>;
    execute(): Promise<Entity>;
    afterProcess(): Promise<Entity>;
    getRecentApproval(): Promise<void>;
    responseApproval(): Promise<void>;
    protected submitRequest(entity: Entity): Promise<Entity>;
    protected deleteData(entity: {
        id: string;
    } & Entity): Promise<Entity>;
    protected produceEntityUpdatedTopic(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    protected produceEntityActivatedTopic(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    protected produceEntityDeletedTopic(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    private findCompanyId;
    produceEntityActivatedWithSiteTopic(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
