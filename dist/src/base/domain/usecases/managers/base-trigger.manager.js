"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseTriggerManager = void 0;
const approval_trigger_manager_1 = require("./approvals/approval-trigger.manager");
const UserSession_1 = require("../../../utils/UserSession");
class BaseTriggerManager {
    constructor(entityIds, dataService, producerService, statusGroup) {
        this.entityIds = entityIds;
        this.dataService = dataService;
        this.producerService = producerService;
        this.statusGroup = statusGroup;
        this.succeedProcess = [];
        this.failedProcess = [];
        this.statusGroupApply = statusGroup === undefined ? false : true;
        this.getParameters();
    }
    async getParameters() {
        const session = UserSession_1.UserSession.getInstance();
        this.user = session.getUser();
        this.approvals = session.getApprovals();
    }
    async execute() {
        await this.getEntityByIds();
        await this.beforeProcess();
        await this.processEachEntity();
        await this.onSuccess(this.succeedProcess);
        await this.onFailed(this.failedProcess);
    }
    async getEntityByIds() {
        const entities = await this.dataService.findByIds(this.entityIds);
        Object.assign(this, { entities });
    }
    async processEachEntity() {
        await Promise.all(await this.entities.map(async (entity) => {
            try {
                this.entityAssign = undefined;
                this.customMessageFailed = undefined;
                const validateResult = await this.validateStatus(entity);
                if (validateResult) {
                    const entityResult = await new approval_trigger_manager_1.ApprovalTriggerManager(entity.id, this.user, this.approvals, this.dataService, this.producerService, this.requestType, this.approvallStatus, this.statusGroupApply).execute({
                        entityData: this.entityAssign,
                        customProduceTopic: this.customProduceTopic
                    });
                    this.succeedProcess.push(entityResult);
                }
                else {
                    this.customMessageFailed
                        ? this.failedProcess.push(this.customMessageFailed)
                        : this.failedProcess.push(`Can't process entity with code: ${entity.code}.`);
                }
            }
            catch (err) {
                console.log('Error Process: ', err.message);
                this.failedProcess.push(err.message);
            }
        }));
    }
    async beforeProcess() {
        return;
    }
}
exports.BaseTriggerManager = BaseTriggerManager;
//# sourceMappingURL=base-trigger.manager.js.map