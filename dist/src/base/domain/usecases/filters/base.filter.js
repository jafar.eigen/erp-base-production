"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseFilter = void 0;
const typeorm_1 = require("typeorm");
class BaseFilter {
    constructor(option) {
        this.query = option.query;
        this.filters = option.filters;
    }
    generate(query) {
        if (this.query)
            this.byQuery(query);
        if (this.filters)
            this.byFilter(query);
        return query;
    }
    byFilter(query) {
        for (const filters of this.filters) {
            query.andWhere(new typeorm_1.Brackets((subQuery) => {
                for (const filter of filters) {
                    const operation = filter.operation
                        ? filter.operation.toUpperCase()
                        : 'IN';
                    switch (operation) {
                        case 'BETWEEN':
                            this.checkFormTo(filter);
                            subQuery.orWhere(`${filter.field} ${operation} :from AND :to`, {
                                from: filter.from,
                                to: filter.to
                            });
                            break;
                        case 'IN':
                        case 'NOT IN':
                            if (!Array.isArray(filter.value))
                                filter.value = filter.value.toString().split(',');
                            this.checkValue(filter);
                            subQuery.orWhere(`${filter.field} ${operation} (:...values)`, {
                                values: filter.value
                            });
                            break;
                        default:
                            this.checkValue(filter);
                            subQuery.orWhere(`${filter.field} ${operation} :value`, {
                                value: filter.value
                            });
                    }
                }
            }));
            return query;
        }
    }
    checkFormTo(filterOption) {
        const operation = filterOption.operation
            ? filterOption.operation.toUpperCase()
            : 'IN';
        if (!filterOption.from || !filterOption.to)
            throw new Error(`Form or to params not be null for this operation ${operation} at filter ${filterOption.field}!`);
    }
    checkValue(filterOption) {
        const operation = filterOption.operation
            ? filterOption.operation.toUpperCase()
            : 'IN';
        if (!filterOption.value)
            throw new Error(`Value not be null for this operation ${operation} at filter ${filterOption.field}!`);
        switch (operation) {
            case 'IN':
            case 'NOT IN':
                if (!Array.isArray(filterOption.value))
                    throw new Error(`Value must be an array for this operation ${operation} at filter ${filterOption.field}!`);
                break;
            default:
                if (Array.isArray(filterOption.value))
                    throw new Error(`Value must be not an array for this operation ${operation} at filter ${filterOption.field}!`);
        }
    }
    byQuery(query) {
        return query.andWhere(new typeorm_1.Brackets((subQuery) => {
            for (const field of this.query.fields) {
                subQuery.orWhere(`${field} LIKE :value`, {
                    value: `%${this.query.value}%`
                });
            }
        }));
    }
}
exports.BaseFilter = BaseFilter;
//# sourceMappingURL=base.filter.js.map