import { SelectQueryBuilder } from 'typeorm';
export interface QueryOption {
    fields: string[];
    value: string;
}
export interface FilterOption {
    field: string;
    operation?: string;
    value?: string[] | string | number[] | number;
    from?: string | number;
    to?: string | number;
}
interface Option {
    query?: QueryOption;
    filters?: [FilterOption[]];
}
export declare class BaseFilter<Entity> {
    query: QueryOption;
    filters: [FilterOption[]];
    constructor(option: Option);
    generate(query: SelectQueryBuilder<Entity>): SelectQueryBuilder<Entity>;
    protected byFilter(query: SelectQueryBuilder<Entity>): SelectQueryBuilder<Entity>;
    checkFormTo(filterOption: FilterOption): void;
    checkValue(filterOption: FilterOption): void;
    protected byQuery(query: SelectQueryBuilder<Entity>): SelectQueryBuilder<Entity>;
}
export {};
