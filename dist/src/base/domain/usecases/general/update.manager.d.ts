import { BaseApplyManager } from '../managers/base-apply.manager';
export declare abstract class UpdateManager<Entity> extends BaseApplyManager<Entity> {
    abstract entityId: string;
    processData(): Promise<Entity>;
    afterProcess(entity: Entity): Promise<void>;
}
