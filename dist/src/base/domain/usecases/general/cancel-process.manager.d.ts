import { BaseTriggerManager } from '../managers/base-trigger.manager';
import { BaseEntity } from '../../entities/base.entity';
import { RequestType } from '../../../../helpers/approval/submit-request.helper';
import { STATUS } from '../../entities/base.entity';
import { KafkaPayload } from '../../entities/kafka/payload.interface';
import { IUserPayload } from '../../entities/user/user-payload-interface.ts';
export declare abstract class CancelProcessManager<Entity extends BaseEntity> extends BaseTriggerManager<Entity> {
    requestType: RequestType;
    approvallStatus: STATUS;
    beforeProcess(): Promise<void>;
    validateStatus(entity: Entity): Promise<boolean>;
    produceTopicCancelProcess(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
