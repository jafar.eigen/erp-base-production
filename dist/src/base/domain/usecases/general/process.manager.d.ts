import { RequestType } from '../../../../helpers/approval/submit-request.helper';
import { BaseEntity } from '../../entities/base.entity';
import { BaseTriggerManager } from '../managers/base-trigger.manager';
import { KafkaPayload } from '../../entities/kafka/payload.interface';
import { IUserPayload } from '../../entities/user/user-payload-interface.ts';
export declare abstract class ProcessManager<Entity extends BaseEntity> extends BaseTriggerManager<Entity> {
    requestType: RequestType;
    approvallStatus: string;
    changeToStatus: string;
    getEntityByIds(): Promise<void>;
    beforeProcess(): Promise<void>;
    validateStatus(entity: Entity): Promise<boolean>;
    processEachEntity(): Promise<void>;
    produceTopicProcess(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
