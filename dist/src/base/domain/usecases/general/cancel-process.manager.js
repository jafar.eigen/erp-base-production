"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelProcessManager = void 0;
const base_trigger_manager_1 = require("../managers/base-trigger.manager");
const submit_request_helper_1 = require("../../../../helpers/approval/submit-request.helper");
const base_entity_1 = require("../../entities/base.entity");
class CancelProcessManager extends base_trigger_manager_1.BaseTriggerManager {
    constructor() {
        super(...arguments);
        this.requestType = submit_request_helper_1.RequestType.CANCEL;
        this.approvallStatus = base_entity_1.STATUS.CANCEL;
    }
    async beforeProcess() {
        this.customProduceTopic = this.produceTopicCancelProcess;
        return;
    }
    async validateStatus(entity) {
        return true;
    }
    async produceTopicCancelProcess(payload) {
        return this.producerService.baseCancelProcess(payload);
    }
}
exports.CancelProcessManager = CancelProcessManager;
//# sourceMappingURL=cancel-process.manager.js.map