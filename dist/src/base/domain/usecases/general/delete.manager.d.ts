import { RequestType } from '../../../../helpers/approval/submit-request.helper';
import { BaseEntity, STATUS } from '../../entities/base.entity';
import { BaseTriggerManager } from '../managers/base-trigger.manager';
export declare abstract class DeleteManager<Entity extends BaseEntity> extends BaseTriggerManager<Entity> {
    requestType: RequestType;
    approvallStatus: STATUS;
    validateStatus(entity: Entity): Promise<boolean>;
}
