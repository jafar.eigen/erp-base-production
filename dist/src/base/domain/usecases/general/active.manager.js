"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActiveManager = void 0;
const submit_request_helper_1 = require("../../../../helpers/approval/submit-request.helper");
const base_entity_1 = require("../../entities/base.entity");
const base_trigger_manager_1 = require("../managers/base-trigger.manager");
class ActiveManager extends base_trigger_manager_1.BaseTriggerManager {
    constructor() {
        super(...arguments);
        this.requestType = submit_request_helper_1.RequestType.ACTIVE;
        this.approvallStatus = base_entity_1.STATUS.ACTIVE;
    }
    async validateStatus(entity) {
        return entity.status === base_entity_1.STATUS.INACTIVE;
    }
}
exports.ActiveManager = ActiveManager;
//# sourceMappingURL=active.manager.js.map