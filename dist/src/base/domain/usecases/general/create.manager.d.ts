import { BaseApplyManager } from '../managers/base-apply.manager';
export declare abstract class CreateManager<Entity> extends BaseApplyManager<Entity> {
    processData(): Promise<Entity>;
    afterProcess(entity: Entity): Promise<void>;
}
