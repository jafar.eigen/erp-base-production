"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelManager = void 0;
const _ = require("lodash");
const cancel_validator_1 = require("../../../validators/cancel.validator");
const UserSession_1 = require("../../../utils/UserSession");
class CancelManager {
    constructor(entityId, dataService, producerService) {
        this.entityId = entityId;
        this.dataService = dataService;
        this.producerService = producerService;
        this.getParameters();
    }
    async getParameters() {
        const session = UserSession_1.UserSession.getInstance();
        this.user = session.getUser();
        this.approvals = session.getApprovals();
    }
    async execute() {
        this.entity = await this.dataService.findOne(this.entityId);
        await new cancel_validator_1.CancelValidatorManager(this.entity).execute();
        const oldEntity = _.cloneDeep(this.entity);
        Object.assign(this.entity, {
            has_requested_process: false
        });
        const updated = await this.dataService.update(this.entity);
        await this.produceTopic(updated, oldEntity);
        return updated;
    }
    async produceTopic(entity, oldEntity) {
        await this.producerService.baseChanged({
            user: this.user,
            data: entity,
            old: oldEntity
        });
    }
}
exports.CancelManager = CancelManager;
//# sourceMappingURL=cancel.manager.js.map