"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateManager = void 0;
const approval_entity_1 = require("../../entities/approval.entity");
const base_entity_1 = require("../../entities/base.entity");
const base_apply_manager_1 = require("../managers/base-apply.manager");
class CreateManager extends base_apply_manager_1.BaseApplyManager {
    async processData() {
        var _a, _b, _c, _d;
        const approval = this._findApprovals();
        Object.assign(this.entity, {
            amount_sites: 0,
            group_name: this.user.company.name,
            creator_id: (_b = (_a = this.user.uuid) !== null && _a !== void 0 ? _a : this.user.id) !== null && _b !== void 0 ? _b : this.user.id,
            creator_name: this.user.username,
            editor_id: (_d = (_c = this.user.uuid) !== null && _c !== void 0 ? _c : this.user.id) !== null && _d !== void 0 ? _d : this.user.id,
            editor_name: this.user.username,
            has_requested_process: false,
            status: base_entity_1.STATUS.DRAFT,
            request_info: approval_entity_1.RequestInfo.CREATE_DATA,
            approval: approval
        });
        const result = await this.dataService.save(this.entity);
        if (this.statusGroupApply === true) {
            await this.dataService.generateTableGroup();
            await this.dataService.saveGroup(this.entity);
        }
        return result;
    }
    async afterProcess(entity) {
        await this.producerService.baseCreated({
            user: this.user,
            activity: approval_entity_1.RequestInfo.CREATE_DATA,
            mac_address: 'XX-XX-XX-XX-XX-XX',
            data: entity,
            old: null
        });
    }
}
exports.CreateManager = CreateManager;
//# sourceMappingURL=create.manager.js.map