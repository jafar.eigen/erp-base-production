"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteManager = void 0;
const submit_request_helper_1 = require("../../../../helpers/approval/submit-request.helper");
const base_trigger_manager_1 = require("../managers/base-trigger.manager");
class DeleteManager extends base_trigger_manager_1.BaseTriggerManager {
    constructor() {
        super(...arguments);
        this.requestType = submit_request_helper_1.RequestType.DELETE;
    }
    async validateStatus(entity) {
        return true;
    }
}
exports.DeleteManager = DeleteManager;
//# sourceMappingURL=delete.manager.js.map