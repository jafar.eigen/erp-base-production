import { BaseDataService } from '../../../data/services/base-data.service';
import { BaseProducerServiceImpl } from '../../../infrastructure/base-producer.service';
import { ApprovalPerCompany } from '../../entities/approval.entity';
import { IUserPayload } from '../../entities/user/user-payload-interface.ts';
export declare abstract class CancelManager<Entity> {
    entityId: string;
    dataService: BaseDataService<Entity>;
    producerService: BaseProducerServiceImpl<Entity>;
    user: IUserPayload;
    approvals: ApprovalPerCompany[];
    entity: Entity;
    constructor(entityId: string, dataService: BaseDataService<Entity>, producerService: BaseProducerServiceImpl<Entity>);
    getParameters(): Promise<void>;
    execute(): Promise<Entity>;
    protected produceTopic(entity: Entity, oldEntity: Entity): Promise<void>;
}
