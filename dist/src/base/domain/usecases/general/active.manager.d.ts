import { RequestType } from '../../../../helpers/approval/submit-request.helper';
import { BaseEntity } from '../../entities/base.entity';
import { STATUS } from '../../entities/base.entity';
import { BaseTriggerManager } from '../managers/base-trigger.manager';
export declare abstract class ActiveManager<Entity extends BaseEntity> extends BaseTriggerManager<Entity> {
    requestType: RequestType;
    approvallStatus: STATUS;
    validateStatus(entity: Entity): Promise<boolean>;
}
