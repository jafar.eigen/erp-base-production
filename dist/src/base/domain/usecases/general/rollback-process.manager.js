"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RollbackProcessManager = void 0;
const submit_request_helper_1 = require("../../../../helpers/approval/submit-request.helper");
const get_rollback_status_helper_1 = require("../../../../helpers/get-rollback-status.helper");
const approval_trigger_manager_1 = require("../managers/approvals/approval-trigger.manager");
const base_trigger_manager_1 = require("../managers/base-trigger.manager");
class RollbackProcessManager extends base_trigger_manager_1.BaseTriggerManager {
    constructor() {
        super(...arguments);
        this.requestType = submit_request_helper_1.RequestType.ROLLBACK;
        this.approvallStatus = '-';
        this.changeToStatus = '-';
    }
    async getEntityByIds() {
        const entities = await this.dataService.findByIds(this.entityIds);
        Object.assign(this, { entities });
    }
    async beforeProcess() {
        this.customProduceTopic = this.produceTopicRollbackProcess;
        return;
    }
    async validateStatus(entity) {
        return true;
    }
    async processEachEntity() {
        await Promise.all(await this.entities.map(async (entity) => {
            try {
                this.entityAssign = undefined;
                this.customMessageFailed = undefined;
                const validateResult = await this.validateStatus(entity);
                if (validateResult) {
                    const entityRollback = await new get_rollback_status_helper_1.GetRollbackStatusHelper(entity).get();
                    const statusRollback = this.changeToStatus === '-'
                        ? entityRollback.status
                        : this.changeToStatus;
                    const entityResult = await new approval_trigger_manager_1.ApprovalTriggerManager(entity.id, this.user, this.approvals, this.dataService, this.producerService, this.requestType, statusRollback, this.statusGroupApply).execute({
                        entityData: this.entityAssign,
                        customProduceTopic: this.customProduceTopic
                    });
                    this.succeedProcess.push(entityResult);
                }
                else {
                    this.customMessageFailed
                        ? this.failedProcess.push(this.customMessageFailed)
                        : this.failedProcess.push(`Can't process entity with code: ${entity.code}.`);
                }
            }
            catch (err) {
                this.failedProcess.push(err.message);
            }
        }));
    }
    async produceTopicRollbackProcess(payload) {
        return this.producerService.baseRollbackProcess(payload);
    }
}
exports.RollbackProcessManager = RollbackProcessManager;
//# sourceMappingURL=rollback-process.manager.js.map