"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubmitManager = void 0;
const submit_request_helper_1 = require("../../../../helpers/approval/submit-request.helper");
const base_entity_1 = require("../../entities/base.entity");
const base_trigger_manager_1 = require("../managers/base-trigger.manager");
class SubmitManager extends base_trigger_manager_1.BaseTriggerManager {
    constructor() {
        super(...arguments);
        this.requestType = submit_request_helper_1.RequestType.REQUEST;
        this.approvallStatus = base_entity_1.STATUS.ACTIVE;
    }
    async validateStatus(entity) {
        return true;
    }
}
exports.SubmitManager = SubmitManager;
//# sourceMappingURL=submit.manager.js.map