"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateManager = void 0;
const approval_entity_1 = require("../../entities/approval.entity");
const base_apply_manager_1 = require("../managers/base-apply.manager");
const approval_update_manager_1 = require("../managers/approvals/approval-update.manager");
class UpdateManager extends base_apply_manager_1.BaseApplyManager {
    async processData() {
        return await new approval_update_manager_1.ApprovalUpdateManager(this.entityId, this.entity, this.user, this.approvals, this.dataService, this.file, this.newAddress).execute();
    }
    async afterProcess(entity) {
        await this.producerService.baseChanged({
            user: this.user,
            activity: approval_entity_1.RequestInfo.EDIT_DATA,
            mac_address: 'XX-XX-XX-XX-XX-XX',
            data: entity,
            old: null
        });
    }
}
exports.UpdateManager = UpdateManager;
//# sourceMappingURL=update.manager.js.map