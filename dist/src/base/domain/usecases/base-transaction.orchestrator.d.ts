/// <reference types="multer" />
import { BaseResultBatch } from '../entities/base-result-batch.entity';
export declare abstract class BaseTransactionOrchestrator<Entity> {
    abstract create(entity: Entity): Promise<Entity | Entity[]>;
    abstract update(entityId: string, updatedData: Entity, file?: Express.Multer.File, newAddress?: string): Promise<Entity>;
    abstract approve(entityId: string, step: number): Promise<Entity>;
    abstract decline(entityId: string, step: number): Promise<Entity>;
    abstract request(entityId: string): Promise<Entity>;
    abstract activate(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
    abstract deactivate(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
    abstract cancel(entityId: string): Promise<Entity>;
    abstract delete(entityIds: string[], withChild?: string): Promise<BaseResultBatch>;
    abstract confirmProcess(ids: string[]): Promise<any>;
    abstract confirmRollback(ids: string[]): Promise<any>;
    abstract confirmCancel(ids: string[]): Promise<any>;
}
