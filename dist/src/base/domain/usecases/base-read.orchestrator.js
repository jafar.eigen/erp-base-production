"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseReadOrchestrator = void 0;
class BaseReadOrchestrator {
    constructor(baseReadDataService) {
        this.baseReadDataService = baseReadDataService;
    }
    async show(entityId) {
        return await this.baseReadDataService.findOneOrFail(entityId);
    }
}
exports.BaseReadOrchestrator = BaseReadOrchestrator;
//# sourceMappingURL=base-read.orchestrator.js.map