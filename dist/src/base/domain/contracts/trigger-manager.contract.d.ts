export interface TriggerManagerContract<Entity> {
    entities: Entity[];
    succeedProcess: Entity[];
    failedProcess: string[];
    execute(): Promise<void>;
    getEntityByIds(): Promise<void>;
    processEachEntity(): Promise<void>;
    validateStatus(entity: Entity): Promise<boolean>;
    onSuccess(entity: Entity[]): Promise<void>;
    onFailed(messages: string[]): Promise<void>;
}
