export interface ApplyManagerContract<Entity> {
    beforeProcess(): Promise<void>;
    prepareData(): Promise<Entity>;
    afterProcess(entity: Entity): Promise<void>;
}
