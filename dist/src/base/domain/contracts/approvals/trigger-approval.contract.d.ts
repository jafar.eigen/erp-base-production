import { Approver } from '../../entities/approval.entity';
export interface TriggerApprovalContract<Entity> {
    entity: Entity;
    approver: Approver;
    execute(): Promise<Entity>;
    getRecentApproval(): Promise<void>;
    processRequest(): Promise<void>;
}
