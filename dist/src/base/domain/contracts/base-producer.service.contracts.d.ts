import { OnModuleInit } from '@nestjs/common';
import { KafkaPayload } from '../entities/kafka/payload.interface';
import { IUserPayload } from '../entities/user/user-payload-interface.ts';
export interface BaseProducerService<Entity> extends OnModuleInit {
    baseCreated(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseChanged(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseActivated(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseDeleted(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
