export interface ActionManagerContract<Entity> {
    entity: Entity;
    execute(): Promise<Entity>;
    getRecentApproval(): Promise<void>;
    responseApproval(): Promise<void>;
}
