export interface BaseCommand<Entity> {
    save(entity: Entity): Promise<Entity>;
    saveMany(entities: Entity[]): Promise<Entity[]>;
    update(criteria: string | number, entity: Entity): Promise<Entity>;
    delete(criteria: string | number): Promise<Entity>;
}
