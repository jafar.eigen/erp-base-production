import { BaseResultBatch } from '../entities/base-result-batch.entity';
export interface BaseOrchestrator<Entity> {
    create(entity: Entity): Promise<Entity>;
    request(entityId: string): Promise<Entity>;
    update(entityId: string, updatedData: Entity): Promise<Entity>;
    approve(step: number): Promise<Entity>;
    decline(entityId: string, step: number): Promise<Entity>;
    activate(entityIds: string[]): Promise<BaseResultBatch>;
    deactivate(entityIds: string[]): Promise<BaseResultBatch>;
    cancel(entityId: string): Promise<Entity>;
    delete(entityIds: string[]): Promise<BaseResultBatch>;
}
