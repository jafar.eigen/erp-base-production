"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModuleTransaction = exports.STATUS = void 0;
var STATUS;
(function (STATUS) {
    STATUS["DRAFT"] = "draft";
    STATUS["ACTIVE"] = "active";
    STATUS["INACTIVE"] = "inactive";
    STATUS["REQUESTED"] = "requested";
    STATUS["OPEN"] = "open";
    STATUS["DECLINE"] = "declined";
    STATUS["DELETED"] = "deleted";
    STATUS["IN_PROCESS"] = "in_process";
    STATUS["WAITING"] = "waiting";
    STATUS["CANCEL"] = "cancel";
    STATUS["DONE"] = "done";
})(STATUS = exports.STATUS || (exports.STATUS = {}));
var ModuleTransaction;
(function (ModuleTransaction) {
    ModuleTransaction["sales_order"] = "sales_order";
    ModuleTransaction["exchange_product"] = "exchange_product";
    ModuleTransaction["purchase_request"] = "purchase_request";
    ModuleTransaction["purchase_order"] = "purchase_order";
    ModuleTransaction["picking_list"] = "picking_list";
    ModuleTransaction["packing_slip"] = "packing_slip";
    ModuleTransaction["shipping_orders"] = "shipping_orders";
    ModuleTransaction["receipts"] = "receipts";
    ModuleTransaction["outgoing_transfer"] = "outgoing_transfer";
    ModuleTransaction["incoming_transfer"] = "incoming_transfer";
    ModuleTransaction["delivery_order"] = "delivery_order";
    ModuleTransaction["delivery_cost"] = "delivery_cost";
    ModuleTransaction["document_exchange"] = "document_exchange";
    ModuleTransaction["manufacturing_orders"] = "manufacturing_orders";
    ModuleTransaction["production_orders"] = "production_orders";
    ModuleTransaction["unbuild_orders"] = "unbuild_orders";
    ModuleTransaction["work_orders"] = "work_orders";
    ModuleTransaction["scraps"] = "scraps";
})(ModuleTransaction = exports.ModuleTransaction || (exports.ModuleTransaction = {}));
//# sourceMappingURL=base.entity.js.map