export interface BaseResultBatch {
    success: string[];
    failed: string[];
}
