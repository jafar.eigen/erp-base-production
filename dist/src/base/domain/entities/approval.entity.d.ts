export interface ApprovalPerCompany {
    company_id: string;
    approval: Approval[];
}
export interface Approval {
    process: ApprovalProcess;
    step: number;
    type: ApprovalType;
    approver: Approver[];
}
export interface ApprovalProcess {
    is_done: boolean;
    has_decline: boolean;
}
export interface Approver {
    id: string;
    name: string;
    code: string;
    status: ApproverStatus;
    level?: string;
    reason?: string;
}
export declare enum ApprovalType {
    NONE = "none",
    FLAT = "flat",
    STEP = "step",
    HIGHEST = "highest"
}
export declare enum ApproverStatus {
    DECLINED = "declined",
    APPROVED = "approved"
}
export declare enum RequestInfo {
    CREATE_DATA = "Create Data",
    EDIT_DATA = "Edit Data",
    EDIT_ACTIVE = "Edit Active",
    EDIT_ACTIVE_WITH_CHILD = "Edit Active with Child",
    EDIT_INACTIVE = "Edit Inactive",
    EDIT_INACTIVE_WITH_CHILD = "Edit Inactive with Child",
    EDIT_DATA_AND_ACTIVE = "Edit Data & Status Active",
    EDIT_DATA_AND_INACTIVE = "Edit Data & Status Inactive",
    DELETE_DATA = "Delete Data",
    ACTIVATE_DATA = "Activate Data",
    PROCESS = "Process Transaction",
    ROLLBACK = "Rollback Transaction",
    CANCEL = "Cancel Transaction"
}
export declare const ApproverHighestLevel = "LV-1";
export declare const HighestStage = 0;
export declare const MockApprovalNone: ApprovalPerCompany[];
