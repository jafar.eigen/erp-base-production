export interface KafkaPayload<User, Data, OldData> {
    user: User;
    activity?: string;
    mac_address?: string;
    data: Data;
    old?: OldData;
}
