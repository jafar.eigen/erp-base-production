import { IUserPayload } from '../user/user-payload-interface.ts';
import { Repository } from 'typeorm';
export interface BaseConsumerEntity {
    id: string;
    code: string;
    name: string;
    status: string;
}
export interface RetryTopicEntity {
    topic: string;
    retry0: string;
    retry1: string;
    retry2: string;
    dlq: string;
}
export interface ConsumerOption {
    topicName: string;
    repositories: Repository<any>[];
    transformData?: (consumerOption: ConsumerOption, message: Partial<any>) => Promise<any>;
    beforeProcess?: () => Promise<void>;
    processData?: (consumerOption: ConsumerOption, value: Partial<any>) => Promise<any>;
    afterProcess?: (consumerOption: ConsumerOption, resultProcess: any) => Promise<void>;
    additionalModule?: any[];
    user?: IUserPayload;
}
export interface optionsInt {
    moduleName: string;
    kafkaTopics: string[];
    kafkaTopicsBaseProcess?: ConsumerOption[];
}
