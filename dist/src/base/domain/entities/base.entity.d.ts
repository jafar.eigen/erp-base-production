import { ApprovalPerCompany, RequestInfo } from './approval.entity';
export declare enum STATUS {
    DRAFT = "draft",
    ACTIVE = "active",
    INACTIVE = "inactive",
    REQUESTED = "requested",
    OPEN = "open",
    DECLINE = "declined",
    DELETED = "deleted",
    IN_PROCESS = "in_process",
    WAITING = "waiting",
    CANCEL = "cancel",
    DONE = "done"
}
export declare enum ModuleTransaction {
    sales_order = "sales_order",
    exchange_product = "exchange_product",
    purchase_request = "purchase_request",
    purchase_order = "purchase_order",
    picking_list = "picking_list",
    packing_slip = "packing_slip",
    shipping_orders = "shipping_orders",
    receipts = "receipts",
    outgoing_transfer = "outgoing_transfer",
    incoming_transfer = "incoming_transfer",
    delivery_order = "delivery_order",
    delivery_cost = "delivery_cost",
    document_exchange = "document_exchange",
    manufacturing_orders = "manufacturing_orders",
    production_orders = "production_orders",
    unbuild_orders = "unbuild_orders",
    work_orders = "work_orders",
    scraps = "scraps"
}
export interface BaseEntity<RequestEntity = any, CompanyEntity = string> {
    id: string;
    status: STATUS;
    code?: string;
    request_info?: RequestInfo;
    creator_id?: string;
    editor_id?: string;
    deleted_by_id?: string;
    approval?: ApprovalPerCompany;
    requested_data?: RequestEntity;
    has_requested_process?: boolean;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
    [key: string]: any;
}
