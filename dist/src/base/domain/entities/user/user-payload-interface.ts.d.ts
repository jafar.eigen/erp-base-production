export interface IUserPayload {
    uuid: string;
    id: string;
    code: string;
    username: string;
    company: IUserCompany;
    companies: IUserCompanies[];
    user_category: IUserCategory;
    contact: null;
    all_site: boolean;
    sites: [];
    iat: number;
    exp: number;
}
export interface IUserCategory {
    code: string;
    name: string;
    uuid: string;
    id: string;
}
export interface IUserCompany {
    name: string;
    code: string;
    uuid: string;
    id: string;
}
export interface IUserCompanies {
    company: IUserCompany;
    user_category: IUserCategory;
}
export declare const MockUser: IUserPayload;
