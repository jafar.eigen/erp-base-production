"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MockUser = void 0;
exports.MockUser = {
    uuid: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e',
    id: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e',
    code: 'ADM',
    username: 'admin',
    companies: [
        {
            company: {
                name: 'Eigen',
                code: 'E',
                uuid: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e',
                id: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e'
            },
            user_category: {
                code: 'ADM',
                name: 'superadmin',
                uuid: '54cd61f1-7a75-40e4-b7ff-126d1b66b651',
                id: '54cd61f1-7a75-40e4-b7ff-126d1b66b651'
            }
        }
    ],
    company: {
        name: 'Eigen',
        code: 'E',
        uuid: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e',
        id: '2b92b641-c4f0-45da-b5a0-1dfad9f1283e'
    },
    user_category: {
        code: 'ADM',
        name: 'superadmin',
        uuid: '54cd61f1-7a75-40e4-b7ff-126d1b66b651',
        id: '54cd61f1-7a75-40e4-b7ff-126d1b66b651'
    },
    contact: null,
    all_site: true,
    sites: [],
    iat: 1619518281,
    exp: 1620727881
};
//# sourceMappingURL=user-payload-interface.ts.js.map