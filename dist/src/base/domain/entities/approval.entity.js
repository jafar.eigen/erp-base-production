"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MockApprovalNone = exports.HighestStage = exports.ApproverHighestLevel = exports.RequestInfo = exports.ApproverStatus = exports.ApprovalType = void 0;
var ApprovalType;
(function (ApprovalType) {
    ApprovalType["NONE"] = "none";
    ApprovalType["FLAT"] = "flat";
    ApprovalType["STEP"] = "step";
    ApprovalType["HIGHEST"] = "highest";
})(ApprovalType = exports.ApprovalType || (exports.ApprovalType = {}));
var ApproverStatus;
(function (ApproverStatus) {
    ApproverStatus["DECLINED"] = "declined";
    ApproverStatus["APPROVED"] = "approved";
})(ApproverStatus = exports.ApproverStatus || (exports.ApproverStatus = {}));
var RequestInfo;
(function (RequestInfo) {
    RequestInfo["CREATE_DATA"] = "Create Data";
    RequestInfo["EDIT_DATA"] = "Edit Data";
    RequestInfo["EDIT_ACTIVE"] = "Edit Active";
    RequestInfo["EDIT_ACTIVE_WITH_CHILD"] = "Edit Active with Child";
    RequestInfo["EDIT_INACTIVE"] = "Edit Inactive";
    RequestInfo["EDIT_INACTIVE_WITH_CHILD"] = "Edit Inactive with Child";
    RequestInfo["EDIT_DATA_AND_ACTIVE"] = "Edit Data & Status Active";
    RequestInfo["EDIT_DATA_AND_INACTIVE"] = "Edit Data & Status Inactive";
    RequestInfo["DELETE_DATA"] = "Delete Data";
    RequestInfo["ACTIVATE_DATA"] = "Activate Data";
    RequestInfo["PROCESS"] = "Process Transaction";
    RequestInfo["ROLLBACK"] = "Rollback Transaction";
    RequestInfo["CANCEL"] = "Cancel Transaction";
})(RequestInfo = exports.RequestInfo || (exports.RequestInfo = {}));
exports.ApproverHighestLevel = 'LV-1';
exports.HighestStage = 0;
exports.MockApprovalNone = null;
//# sourceMappingURL=approval.entity.js.map