import { IUserPayload } from './user/user-payload-interface.ts';
import { ApprovalPerCompany } from './approval.entity';
export interface ParamsEntity {
    user: IUserPayload;
    approvals?: ApprovalPerCompany[];
}
