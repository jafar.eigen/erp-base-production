import { STATUS } from './base.entity';
export interface BaseConsumerEntity {
    id: string;
    code: string;
    name: string;
    status: STATUS;
}
