import { AbstractOrchestrator } from '../utils/controller/abstract.orchestrator';
import { AbstractOption } from '../utils/controller/abstract.options';
export declare function getBaseController<BaseOrchestrator extends AbstractOrchestrator>(options: AbstractOption): any;
