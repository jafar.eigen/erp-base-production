"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBaseTransactionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const filter_metadata_factory_1 = require("../utils/controller/filter-metadata-factory");
const UserSession_1 = require("../utils/UserSession");
const metadataKey = 'swagger/apiModelPropertiesArray';
const excludedCreateMetadata = [];
const excludedUpdateMetadata = [];
function getBaseTransactionController(options) {
    const createModelVm = options.createModelVm;
    const updateModelVm = options.updateModelVm;
    const EntityCreateVM = (0, filter_metadata_factory_1.filterMetadata)(createModelVm, metadataKey, excludedCreateMetadata, (0, filter_metadata_factory_1.formatEntityName)(createModelVm));
    const EntityUpdateVM = (0, filter_metadata_factory_1.filterMetadata)(updateModelVm, metadataKey, excludedUpdateMetadata, (0, filter_metadata_factory_1.formatEntityName)(updateModelVm));
    class EntityCreateVMT extends EntityCreateVM {
    }
    class EntityUpdateVMT extends EntityUpdateVM {
    }
    class BaseTransactionController {
        constructor(responses, baseOrchestrator, jsonParser) {
            this.responses = responses;
            this.baseOrchestrator = baseOrchestrator;
            this.jsonParser = jsonParser;
        }
        async create(body, headers) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const base = await this.baseOrchestrator.create(body);
                return this.responses.json(common_1.HttpStatus.CREATED, base);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async activate(headers, baseIds) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.activate(baseIds);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async deactivate(baseIds, headers) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.deactivate(baseIds);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async update(baseId, body, headers) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.update(baseId, body);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async approve(headers, baseId, step) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.approve(baseId, step);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async decline(headers, baseId, step) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.decline(baseId, step);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async request(headers, baseId) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.request(baseId);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async cancel(headers, entityId) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.cancel(entityId);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async delete(headers, entityIds) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.delete(entityIds);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async confirmProcess(headers, ids) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.confirmProcess(ids);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async confirmCancel(headers, ids) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.confirmCancel(ids);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
        async confirmRollback(headers, ids) {
            try {
                const session = UserSession_1.UserSession.getInstance();
                session.set(headers);
                const result = await this.baseOrchestrator.confirmRollback(ids);
                return this.responses.json(common_1.HttpStatus.OK, result);
            }
            catch (err) {
                throw new common_1.BadRequestException(err.message);
            }
        }
    }
    __decorate([
        (0, common_1.Post)(),
        (0, swagger_1.ApiBody)({ type: createModelVm }),
        __param(0, (0, common_1.Body)()),
        __param(1, (0, common_1.Headers)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [EntityCreateVMT, String]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "create", null);
    __decorate([
        (0, common_1.Put)('activate'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Array]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "activate", null);
    __decorate([
        (0, common_1.Put)('deactivate'),
        __param(0, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
        __param(1, (0, common_1.Headers)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Array, String]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "deactivate", null);
    __decorate([
        (0, common_1.Put)(':id'),
        (0, swagger_1.ApiBody)({ type: updateModelVm }),
        __param(0, (0, common_1.Param)('id')),
        __param(1, (0, common_1.Body)()),
        __param(2, (0, common_1.Headers)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, EntityUpdateVMT, String]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "update", null);
    __decorate([
        (0, common_1.Put)(':id/approve'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Param)('id')),
        __param(2, (0, common_1.Query)('step', common_1.ParseIntPipe)),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, String, Number]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "approve", null);
    __decorate([
        (0, common_1.Put)(':id/decline'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Param)('id')),
        __param(2, (0, common_1.Query)('step', common_1.ParseIntPipe)),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, String, Number]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "decline", null);
    __decorate([
        (0, common_1.Put)(':id/request'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Param)('id')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, String]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "request", null);
    __decorate([
        (0, common_1.Put)(':id/cancel'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Param)('id')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, String]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "cancel", null);
    __decorate([
        (0, common_1.Delete)(),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Query)('ids', common_1.ParseArrayPipe)),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Array]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "delete", null);
    __decorate([
        (0, common_1.Put)('confirm/process'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Query)('ids')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Array]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "confirmProcess", null);
    __decorate([
        (0, common_1.Put)('confirm/cancel'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Query)('ids')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Array]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "confirmCancel", null);
    __decorate([
        (0, common_1.Put)('confirm/rollback'),
        __param(0, (0, common_1.Headers)()),
        __param(1, (0, common_1.Query)('ids')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Array]),
        __metadata("design:returntype", Promise)
    ], BaseTransactionController.prototype, "confirmRollback", null);
    return BaseTransactionController;
}
exports.getBaseTransactionController = getBaseTransactionController;
//# sourceMappingURL=base-transaction.controller.js.map