"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseProducerServiceImpl = void 0;
const generate_topic_retry_1 = require("../utils/generate-topic-retry");
const config_1 = require("../../utils/config");
class BaseProducerServiceImpl {
    constructor(client) {
        this.client = client;
        this.TOPIC_PROCESS = '-';
        this.TOPIC_ROLLBACK_PROCESS = '-';
        this.TOPIC_CANCEL_PROCESS = '-';
    }
    async onModuleInit() {
        const topics = [];
        this.TOPIC_CREATED !== '-' && topics.push(this.TOPIC_CREATED);
        this.TOPIC_CHANGED !== '-' && topics.push(this.TOPIC_CHANGED);
        this.TOPIC_ACTIVATED !== '-' && topics.push(this.TOPIC_ACTIVATED);
        this.TOPIC_DELETED !== '-' && topics.push(this.TOPIC_DELETED);
        if (this.TOPIC_PROCESS !== '-')
            topics.push(this.TOPIC_PROCESS);
        if (this.TOPIC_ROLLBACK_PROCESS !== '-')
            topics.push(this.TOPIC_ROLLBACK_PROCESS);
        if (this.TOPIC_CANCEL_PROCESS !== '-')
            topics.push(this.TOPIC_CANCEL_PROCESS);
        if (config_1.variableConfig.KAFKA_TOPICS_CONSUME) {
            const objIndex = config_1.variableConfig.KAFKA_TOPICS_CONSUME.findIndex((obj) => obj.module_name === this.moduleName);
            if (objIndex !== -1) {
                config_1.variableConfig.KAFKA_TOPICS_CONSUME[objIndex].topic_consume.map((topicConsume) => {
                    const topicRetry = (0, generate_topic_retry_1.generateRetryTopic)(this.moduleName, topicConsume);
                    topics.push(topicConsume.trim() + '.reply', topicRetry.retry0, topicRetry.retry1, topicRetry.retry2);
                });
            }
        }
        topics.push(...this.addNewTopics(), (0, generate_topic_retry_1.generateRetryTopic)(this.moduleName, '-').dlq);
        topics.forEach((topic) => this.client.subscribeToResponseOf(topic));
    }
    subscribeNewTopics(topics) {
        topics.forEach((topic) => this.client.subscribeToResponseOf(topic));
    }
    async baseCreated(payload) {
        if (this.TOPIC_CREATED !== '-') {
            console.log(this.TOPIC_CREATED, ' triggered');
            this.client.send(this.TOPIC_CREATED, payload).toPromise();
        }
    }
    async baseChanged(payload) {
        if (this.TOPIC_CHANGED !== '-') {
            console.log(this.TOPIC_CHANGED, ' triggered');
            this.client.send(this.TOPIC_CHANGED, payload).toPromise();
        }
    }
    async baseActivated(payload) {
        if (this.TOPIC_ACTIVATED !== '-') {
            console.log(this.TOPIC_ACTIVATED, ' triggered');
            this.client.send(this.TOPIC_ACTIVATED, payload).toPromise();
        }
    }
    async baseDeleted(payload) {
        if (this.TOPIC_DELETED !== '-') {
            console.log(this.TOPIC_DELETED, ' triggered');
            this.client.send(this.TOPIC_DELETED, payload).toPromise();
        }
    }
    async baseRetry(topicRetry, payload) {
        console.log(topicRetry, ' triggered');
        this.client.send(topicRetry, payload).toPromise();
    }
    async baseProcess(payload) {
        if (this.TOPIC_PROCESS === '-') {
            throw new Error(`TOPIC PROCESS Not set for this producer module ${this.moduleName}.`);
        }
        console.log(this.TOPIC_PROCESS, ' triggered');
        this.client.send(this.TOPIC_PROCESS, payload).toPromise();
    }
    async baseRollbackProcess(payload) {
        if (this.TOPIC_ROLLBACK_PROCESS === '-') {
            throw new Error(`TOPIC ROLLBACK PROCESS Not set for this producer module ${this.moduleName}.`);
        }
        console.log(this.TOPIC_ROLLBACK_PROCESS, ' triggered');
        this.client.send(this.TOPIC_ROLLBACK_PROCESS, payload).toPromise();
    }
    async baseCancelProcess(payload) {
        if (this.TOPIC_CANCEL_PROCESS === '-') {
            throw new Error(`TOPIC CANCEL PROCESS Not set for this producer module ${this.moduleName}.`);
        }
        console.log(this.TOPIC_CANCEL_PROCESS, ' triggered');
        this.client.send(this.TOPIC_CANCEL_PROCESS, payload).toPromise();
    }
}
exports.BaseProducerServiceImpl = BaseProducerServiceImpl;
//# sourceMappingURL=base-producer.service.js.map