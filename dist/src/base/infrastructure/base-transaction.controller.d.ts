import { AbstractTransactionOrchestrator } from '../utils/controller/abstract-transaction.orchestrator';
import { AbstractOption } from '../utils/controller/abstract.options';
export declare function getBaseTransactionController<BaseOrchestrator extends AbstractTransactionOrchestrator>(options: AbstractOption): any;
