import { IResponses } from '../../helpers/responses-interface';
export declare abstract class BaseReadController<FilterQueryDTO> {
    abstract index(page: number, limit: number, params: FilterQueryDTO): Promise<IResponses>;
    abstract show(entityId: string): Promise<IResponses>;
}
