import { ClientKafka } from '@nestjs/microservices';
import { KafkaPayload } from '../domain/entities/kafka/payload.interface';
import { IUserPayload } from '../domain/entities/user/user-payload-interface.ts';
import { BaseProducerService } from '../domain/contracts/base-producer.service.contracts';
export declare abstract class BaseProducerServiceImpl<Entity> implements BaseProducerService<Entity> {
    client: ClientKafka;
    abstract moduleName: string;
    abstract TOPIC_CREATED: string;
    abstract TOPIC_CHANGED: string;
    abstract TOPIC_ACTIVATED: string;
    abstract TOPIC_DELETED: string;
    TOPIC_PROCESS?: string;
    TOPIC_ROLLBACK_PROCESS?: string;
    TOPIC_CANCEL_PROCESS?: string;
    constructor(client: ClientKafka);
    onModuleInit(): Promise<void>;
    abstract addNewTopics(): string[];
    subscribeNewTopics(topics: string[]): void;
    baseCreated(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseChanged(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseActivated(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseDeleted(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseRetry(topicRetry: string, payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseProcess(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseRollbackProcess(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
    baseCancelProcess(payload: KafkaPayload<IUserPayload, Entity, Entity>): Promise<void>;
}
