"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDataService = void 0;
const base_entity_1 = require("../../domain/entities/base.entity");
const typeorm_1 = require("typeorm");
class BaseDataService {
    constructor(repository) {
        this.repository = repository;
        this.relations = [];
    }
    async save(entityBase) {
        return await this.repository.save(entityBase);
    }
    async saveMany(entityBase) {
        return await this.repository.save(entityBase);
    }
    async update(updateData, id) {
        return await this.repository.save(updateData);
    }
    async delete(entityBaseIds) {
        return await this.repository.delete(entityBaseIds);
    }
    async find(params) {
        return await this.repository.find({
            where: params,
            relations: this.relations
        });
    }
    async findOne(entityBaseId) {
        return await this.repository.findOne(entityBaseId, {
            relations: this.relations
        });
    }
    async findByCode(code) {
        return await this.repository.findOne({
            where: { code },
            relations: this.relations
        });
    }
    async findByCompanyCode(company, code) {
        return await this.repository.findOne({
            where: { code },
            relations: this.relations
        });
    }
    async findByName(name) {
        return await this.repository.find({
            where: { name },
            relations: this.relations
        });
    }
    async findById(id) {
        return await this.repository.findOne(id, {
            relations: this.relations
        });
    }
    async findByIds(entityIds) {
        return await this.repository.findByIds(entityIds, {
            relations: this.relations
        });
    }
    async findOneOrFail(entityId, extraQuery) {
        return await this.repository.findOneOrFail(entityId, {
            relations: this.relations
        });
    }
    async activateWithChild(entity, withChild) {
        if (withChild) {
            let childEntities = await this.findChilds(entity.id);
            childEntities = childEntities.map((childEntity) => {
                return Object.assign(Object.assign({}, childEntity), { status: base_entity_1.STATUS.ACTIVE });
            });
            await this.repository.save(childEntities);
        }
        return await this.repository.save(entity);
    }
    async findChilds(parent_id) {
        return await this.repository.find({ where: { parent_id } });
    }
    async deactivateWithChild(entity, withChild) {
        if (withChild) {
            let childEntities = await this.findChilds(entity.id);
            childEntities = childEntities.map((childEntity) => {
                return Object.assign(Object.assign({}, childEntity), { status: base_entity_1.STATUS.INACTIVE });
            });
            await this.repository.save(childEntities);
        }
        return await this.repository.save(entity);
    }
    async generateTableGroup() {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        await manager.query(`CREATE TABLE IF NOT EXISTS ${tableName}_group LIKE ${tableName}`);
        const dataColumn = await manager.query(`SHOW FIELDS FROM ${tableName}_group WHERE Field = 'gid';`);
        if (dataColumn.length === 0) {
            console.log('Generate table group success.');
            await manager.query(`ALTER TABLE ${tableName}_group ADD gid int NOT NULL AUTO_INCREMENT UNIQUE FIRST;`);
        }
    }
    async saveGroup(entityBase) {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        const tableFields = await manager.query(`SHOW FIELDS FROM ${tableName}_group;`);
        let fields = '';
        tableFields.forEach((tableField) => {
            if (tableField.Field !== 'gid')
                fields = fields + tableField.Field + ', ';
        });
        if (fields !== '') {
            fields = fields.trim().slice(0, -1);
            const SQLSyntax = `INSERT INTO ${tableName}_group (${fields}) SELECT ${fields} FROM ${tableName} WHERE id = '${entityBase.id}' LIMIT 1;`;
            const result = await manager.query(SQLSyntax);
            console.log('Insert to table group success.');
            return result;
        }
        else {
            return entityBase;
        }
    }
    async findGroupById(id) {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        const result = await manager.query(`SELECT * FROM ${tableName}_group WHERE id = '${id}';`);
        return result[0];
    }
    async findGroupByIds(ids) {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        const result = await manager.query(`SELECT * FROM ${tableName}_group WHERE id IN ("${ids
            .toString()
            .replace(/\,/gi, `","`)}");`);
        return result;
    }
    async findGroupByGID(gid) {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        const result = await manager.query(`SELECT * FROM ${tableName}_group WHERE gid = '${gid}';`);
        return result[0];
    }
    async findGroupByGIDs(gids) {
        const manager = (0, typeorm_1.getConnectionManager)().get(this.repository.metadata.connection.name);
        const tableName = this.repository.metadata.tableName;
        const result = await manager.query(`SELECT * FROM ${tableName}_group WHERE gid IN ("${gids
            .toString()
            .replace(/\,/gi, `","`)}");`);
        return result;
    }
}
exports.BaseDataService = BaseDataService;
//# sourceMappingURL=base-data.service.js.map