import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { FindManyOptions, Repository } from 'typeorm';
export declare abstract class BaseReadDataService<Entity> {
    readRepository: Repository<Entity>;
    relations: string[];
    constructor(readRepository: Repository<Entity>);
    findOne(entityId: string): Promise<Entity>;
    findOneOrFail(entityId: string): Promise<Entity>;
    index(options: IPaginationOptions, extraQuery?: FindManyOptions<Entity>): Promise<Pagination<Entity>>;
}
