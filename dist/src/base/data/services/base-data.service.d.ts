import { SelectQueryBuilder, DeleteResult, Repository } from 'typeorm';
export declare abstract class BaseDataService<Entity> {
    repository: Repository<Entity>;
    protected relations: string[];
    constructor(repository: Repository<Entity>);
    save(entityBase: Entity): Promise<Entity>;
    saveMany(entityBase: Entity[]): Promise<Entity[]>;
    update(updateData: Entity, id?: string): Promise<Entity>;
    delete(entityBaseIds: string[]): Promise<DeleteResult>;
    find(params: Object): Promise<Entity[]>;
    findOne(entityBaseId: string): Promise<Entity>;
    findByCode(code: string): Promise<Entity>;
    findByCompanyCode(company: string, code: string): Promise<Entity>;
    findByName(name: string): Promise<Entity[]>;
    findById(id: string): Promise<Entity>;
    findByIds(entityIds: string[]): Promise<Entity[]>;
    findOneOrFail(entityId: string, extraQuery?: SelectQueryBuilder<Entity>): Promise<Entity>;
    activateWithChild(entity: {
        id: string;
    } & Entity, withChild: string): Promise<Entity>;
    findChilds(parent_id: string): Promise<Entity[]>;
    deactivateWithChild(entity: {
        id: string;
    } & Entity, withChild: string): Promise<Entity>;
    generateTableGroup(): Promise<void>;
    saveGroup(entityBase: any): Promise<Entity>;
    findGroupById(id: string): Promise<Entity>;
    findGroupByIds(ids: string[]): Promise<Entity[]>;
    findGroupByGID(gid: string): Promise<Entity>;
    findGroupByGIDs(gids: string[]): Promise<Entity[]>;
}
