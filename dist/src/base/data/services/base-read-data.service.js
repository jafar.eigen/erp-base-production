"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseReadDataService = void 0;
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
class BaseReadDataService {
    constructor(readRepository) {
        this.readRepository = readRepository;
        this.relations = [];
    }
    async findOne(entityId) {
        return await this.readRepository.findOne(entityId);
    }
    async findOneOrFail(entityId) {
        return await this.readRepository.findOneOrFail(entityId, {
            relations: this.relations
        });
    }
    async index(options, extraQuery = null) {
        if (extraQuery) {
            return (0, nestjs_typeorm_paginate_1.paginate)(this.readRepository, options, Object.assign(extraQuery));
        }
        return (0, nestjs_typeorm_paginate_1.paginate)(this.readRepository, options, {
            relations: this.relations
        });
    }
}
exports.BaseReadDataService = BaseReadDataService;
//# sourceMappingURL=base-read-data.service.js.map