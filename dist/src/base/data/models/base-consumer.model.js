"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseConsumerModel = void 0;
const base_entity_1 = require("src/base/domain/entities/base.entity");
const typeorm_1 = require("typeorm");
class BaseConsumerModel {
}
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], BaseConsumerModel.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'code', length: '36', nullable: true }),
    __metadata("design:type", String)
], BaseConsumerModel.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'name', length: '50' }),
    __metadata("design:type", String)
], BaseConsumerModel.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar', { name: 'status', length: '36', default: base_entity_1.STATUS.DRAFT }),
    __metadata("design:type", String)
], BaseConsumerModel.prototype, "status", void 0);
exports.BaseConsumerModel = BaseConsumerModel;
//# sourceMappingURL=base-consumer.model.js.map