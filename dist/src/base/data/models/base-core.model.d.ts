import { BaseEntity, STATUS } from '../../domain/entities/base.entity';
export declare abstract class BaseCoreModel implements BaseEntity {
    id: string;
    status: STATUS;
}
