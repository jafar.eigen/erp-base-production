import { STATUS } from 'src/base/domain/entities/base.entity';
import { BaseConsumerEntity } from '../../domain/entities/base-consumer.entity';
export declare class BaseConsumerModel implements BaseConsumerEntity {
    id: string;
    code: string;
    name: string;
    status: STATUS;
}
