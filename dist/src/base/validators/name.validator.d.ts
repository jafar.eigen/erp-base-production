import { BaseDataService } from '../data/services/base-data.service';
export declare class Name<Entity extends {
    id: string;
}, DataService extends BaseDataService<Entity>> {
    private dataService;
    private entity;
    private entityId;
    constructor(dataService: DataService, entity: {
        name: string;
    } & Entity, entityId?: string);
    execute(): Promise<void | Error>;
}
