import { BaseDataService } from '../data/services/base-data.service';
import { BaseEntity } from '../domain/entities/base.entity';
export declare class CodeUnique<Entity extends BaseEntity, DataService extends BaseDataService<Entity>> {
    private dataService;
    private entity;
    constructor(dataService: DataService, entity: Entity);
    execute(): Promise<void | Error>;
}
