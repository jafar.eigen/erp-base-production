"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeUnique = void 0;
class CodeUnique {
    constructor(dataService, entity) {
        this.dataService = dataService;
        this.entity = entity;
    }
    async execute() {
        const entities = await this.dataService.find({
            company_id: this.entity.company_id,
            code: this.entity.code
        });
        if (entities.length > 0) {
            const entity = entities[0];
            if (entity.code == this.entity.code)
                throw new Error(`Code ${this.entity.code} already exists.`);
        }
    }
}
exports.CodeUnique = CodeUnique;
//# sourceMappingURL=unique-code.validation.js.map