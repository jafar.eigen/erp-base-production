import { BaseDataService } from '../data/services/base-data.service';
export declare class Code<Entity extends {
    id: string;
}, DataService extends BaseDataService<Entity>> {
    private dataService;
    private entity;
    private entityId;
    constructor(dataService: DataService, entity: {
        code: string;
    } & Entity, entityId?: string);
    execute(): Promise<void | Error>;
}
