"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelValidatorManager = void 0;
const common_1 = require("@nestjs/common");
const has_approver_helper_1 = require("../../helpers/approval/has-approver.helper");
class CancelValidatorManager {
    constructor(entity) {
        this.entity = entity;
    }
    async execute() {
        if (this.entity.has_requested_process === false) {
            throw new common_1.BadRequestException(`Unable to cancel this entity without request process.`);
        }
        if ((0, has_approver_helper_1.hasApprover)(this.entity.approval)) {
            throw new common_1.BadRequestException('Unable to cancel this entity because data has approver.');
        }
    }
}
exports.CancelValidatorManager = CancelValidatorManager;
//# sourceMappingURL=cancel.validator.js.map