"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Name = void 0;
class Name {
    constructor(dataService, entity, entityId = null) {
        this.dataService = dataService;
        this.entity = entity;
        this.entityId = entityId;
    }
    async execute() {
        const entities = await this.dataService.findByName(this.entity.name);
        const entity = entities[0];
        if (entity && this.entityId !== entity.id) {
            throw new Error('Name already exists.');
        }
        if (!this.entityId && entity) {
            throw new Error('Name already exists.');
        }
    }
}
exports.Name = Name;
//# sourceMappingURL=name.validator.js.map