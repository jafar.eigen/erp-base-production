import { BaseDataService } from '../data/services/base-data.service';
import { BaseEntity } from '../domain/entities/base.entity';
export declare class CodeAndName<Entity extends BaseEntity, DataService extends BaseDataService<Entity>> {
    private dataService;
    private entity;
    private readonly name;
    constructor(dataService: DataService, entity: Entity, name: string);
    execute(): Promise<void | Error>;
}
