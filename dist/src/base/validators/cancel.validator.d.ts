import { ApprovalPerCompany } from '../domain/entities/approval.entity';
export declare class CancelValidatorManager<Entity> {
    private entity;
    constructor(entity: Partial<{
        has_requested_process: boolean;
        approval: ApprovalPerCompany;
    } & Entity>);
    execute(): Promise<void | Error>;
}
