"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeAndName = void 0;
class CodeAndName {
    constructor(dataService, entity, name) {
        this.dataService = dataService;
        this.entity = entity;
        this.name = name;
    }
    async execute() {
        const entities = await this.dataService.find({
            company_id: this.entity.company_id,
            code: this.entity.code,
            name: this.name
        });
        if (entities.length > 0) {
            const entity = entities[0];
            if (entity.name == this.name)
                throw new Error(`Code ${this.entity.code} with name ${this.name} already exists.`);
        }
    }
}
exports.CodeAndName = CodeAndName;
//# sourceMappingURL=unique-code-name.validation.js.map