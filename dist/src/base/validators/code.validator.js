"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Code = void 0;
class Code {
    constructor(dataService, entity, entityId = null) {
        this.dataService = dataService;
        this.entity = entity;
        this.entityId = entityId;
    }
    async execute() {
        const entity = await this.dataService.findByCode(this.entity.code);
        if (entity && this.entityId !== entity.id) {
            throw new Error('Code already exists.');
        }
        if (!this.entityId && entity) {
            throw new Error('Code already exists.');
        }
    }
}
exports.Code = Code;
//# sourceMappingURL=code.validator.js.map