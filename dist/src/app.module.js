"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const platform_express_1 = require("@nestjs/platform-express");
const types_1 = require("@sentry/types");
const nestjs_sentry_1 = require("@ntegral/nestjs-sentry");
const nest_winston_1 = require("nest-winston");
const winston = require("winston");
const branch_module_1 = require("./modules/branch/branch.module");
const company_module_1 = require("./modules/company/company.module");
const location_module_1 = require("./modules/location/location.module");
const legality_module_1 = require("./modules/legality/legality.module");
const site_module_1 = require("./modules/site/site.module");
let AppModule = class AppModule {
    constructor(client) {
        this.client = client;
        client.log('Base Template App loaded!');
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true
            }),
            nest_winston_1.WinstonModule.forRoot({
                level: 'info',
                format: winston.format.json(),
                defaultMeta: { service: 'user-service' },
                transports: [
                    new winston.transports.File({
                        filename: 'log/error.log',
                        level: 'error'
                    })
                ]
            }),
            nestjs_sentry_1.SentryModule.forRoot({
                debug: true,
                dsn: process.env.SENTRY_DSN,
                logLevel: types_1.LogLevel.Debug,
                environment: process.env.SENTRY_ENV,
                tracesSampleRate: 1.0
            }),
            platform_express_1.MulterModule.register({
                dest: './uploads'
            }),
            branch_module_1.BranchModule,
            company_module_1.CompanyModule,
            legality_module_1.LegalityModule,
            location_module_1.LocationModule,
            site_module_1.SiteModule
        ]
    }),
    __param(0, (0, nestjs_sentry_1.InjectSentry)()),
    __metadata("design:paramtypes", [nestjs_sentry_1.SentryService])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map